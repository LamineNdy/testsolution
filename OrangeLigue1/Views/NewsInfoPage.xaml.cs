// -------------------------------  Copyright ---------------------------------
//	This software has been developped by France Telecom, FT/RD/RD/MAPS/DVC/MSF
//
//	Copyright (c) France Telecom 2009-2010
//
// COPYRIGHT    : This file is the property of FRANCE TELECOM.
//                It cannot be copied, used, or modified without obtaining
//                an authorization from the authors or a mandated
//                member of FRANCE TELECOM.
//                If such an authorization is provided, any modified version or
//                copy of the software has to contain this header.
//
// WARRANTIES   : This software is made available by the authors in the  hope
//                that it will be useful, but without any warranty.
//                France Telecom is not liable for any consequence related to the
//                use of the provided software.
//
// AUTHORS      : France Telecom  / RD / MAPS / DVC / MSF
// ----------------------------------------------------------------------------
using System;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media.Imaging;
using OrangeLigue1.ViewModels;
using OrangeLigue1.Model;
using System.Collections.Generic;
using System.Text;
using System.Windows.Resources;
using System.IO;
using OrangeLigue1.Tools;
using Orange.SDK.Enablers.Ligue1;
using System.Globalization;

namespace OrangeLigue1.Views
{
    public partial class NewsInfoPage : Microsoft.Phone.Controls.PhoneApplicationPage
    {
        private Dictionary<string, object> _transientState = null;
        private NewsInfoPageViewModel _viewModel = null;
        private bool _codeInitiatedNavigation = false;
        private bool _isLoading = false;
        private bool _waitingForWebBrowserLayout = false;
		private NewsInfo _articleToLoad = null;

        public NewsInfoPage()
        {
            InitializeComponent();
            LayoutUpdated += OnLayoutUpdated;
            WebBrowserControl.Navigating += OnWebBrowserControlNavigating;
            WebBrowserControl.Navigated += OnWebBrowserControlNavigated;
            WebBrowserControl.LoadCompleted += OnWebBrowserControlLoadCompleted;
            VisualStateManager.GoToState(this, InitState.Name, false);
        }

        // Executes when the user navigates to this page.
        protected override void OnNavigatedTo(System.Windows.Navigation.NavigationEventArgs e)
        {
 	        base.OnNavigatedTo(e);
            _transientState = new Dictionary<string, object>(State);
		}

        private void OnLayoutUpdated(object sender, EventArgs e)
        {
            if (_transientState == null)
            {
                return;
            }
            LayoutUpdated -= OnLayoutUpdated;
            Dispatcher.BeginInvoke(() =>
            {
                if (_transientState != null)
                {
                    try
                    {
                        _viewModel = new NewsInfoPageViewModel();
                        RestoreTransientState(_transientState);
                        _viewModel.PropertyChanged += OnViewModelPropertyChanged;
                        DataContext = _viewModel;
                        Reload();
                    }
                    catch (Exception) { }
                }
                _transientState = null;
            });
        }

        protected override void OnNavigatedFrom(System.Windows.Navigation.NavigationEventArgs e)
        {
            base.OnNavigatedFrom(e);
            SaveTransientState();
        }

        #region Transient State

        private const string NewsIdStateKey = "NewsId";

        private void SaveTransientState()
        {
            if (_viewModel != null && _viewModel.CurrentNewsInfo != null)
            {
                State[NewsIdStateKey] = _viewModel.CurrentNewsInfo.Id;
            }
        }

        private void RestoreTransientState(IDictionary<string, object> transientState)
        {
            if (_viewModel != null)
            {
                string newsId = null;
                if (transientState.ContainsKey(NewsIdStateKey))
                {
                    try { newsId = transientState[NewsIdStateKey].ToString(); }
                    catch (Exception) { }
                }
                else if (NavigationContext.QueryString.ContainsKey("article"))
                {
                    newsId = NavigationContext.QueryString["article"];
                }
                _viewModel.CurrentNewsInfo = _viewModel.GetNewsInfoFromNewsId(newsId);
            }
        }

        #endregion

        private void UpdateVisualState(bool useTransitions)
        {
            if (_viewModel == null)
            {
                return;
            }

            if (_isLoading && (_viewModel.CurrentNewsInfo == null))
            {
                VisualStateManager.GoToState(this, LoadingState.Name, useTransitions);
                return;
            }

            if (_viewModel.CurrentNewsInfo == null)
            {
                MessageTextBlock.Text = Localization.GetString("ArticlePage_NoArticleMessage");
                VisualStateManager.GoToState(this, MessageState.Name, useTransitions);
                return;
            }

            VisualStateManager.GoToState(this, LoadedState.Name, useTransitions);
        }

        private void OnViewModelPropertyChanged(object sender, System.ComponentModel.PropertyChangedEventArgs e)
        {
            if (object.ReferenceEquals(sender, _viewModel) == false)
            {
                return;
            }
            if (e.PropertyName == "CurrentNewsInfo")
            {
                Reload();
            }
        }

        private void Reload()
        {
            if (_viewModel == null)
            {
                return;
            }
            NewsInfo article = _viewModel.CurrentNewsInfo;
            if (article != null)
            {
                NavigateToArticleSource(article);
            }
            UpdateVisualState(true);
        }

        private void NavigateToArticleSource(NewsInfo article)
        {
			if (_isLoading)
			{
				//	H4CK: web browser control is crashing if we try to load a new content while the current one is loading
				//	(see bug 0008331)
				//	To avoid this, if we are "loading" the content, we don't try to load the new one, but we wait for the current
				//	one to be totally loaded...)
				//	Note: to avoid display of current article, then display of the next one, we collapse the web browser control
				//	during downloading of content
				_articleToLoad = article;
				return;
			}

			_articleToLoad = null;
            if (article == null || string.IsNullOrEmpty(article.Text))
            {
                return;
            }

            _isLoading = true;
            try
            {
                StringBuilder document;
                StreamResourceInfo templateResource = Application.GetResourceStream(new Uri("Assets/ArticleTemplate.html", UriKind.Relative));
                using (StreamReader templateReader = new StreamReader(templateResource.Stream))
                {
                    document = new StringBuilder(templateReader.ReadToEnd());
                }

                document.Replace("{ArticleTitle}", article.Title);
                document.Replace("{ArticleTimestamp}", NewsRelativeTimestampConverter.GetNormalRepresentation(DateTime.Parse(article.Date + " " + article.Hour, CultureInfo.InvariantCulture).ToUniversalTime()));
                document.Replace("{ArticleImageDisplay}", "none");
                document.Replace("{ArticleContent}", article.Text);
                document.Replace("{MoreInfoDisplay}", "none");

                try
                {
                	WebBrowserControl.NavigateToString(document.ToString());
                }
                catch (InvalidOperationException)
                {
					_isLoading = false;
                    // H4CK for issue 8256.
                    if (!_waitingForWebBrowserLayout)
                    {
                        _waitingForWebBrowserLayout = true;
                        WebBrowserControl.LayoutUpdated += OnWebBrowserControlLayoutUpdated;
                    }
                }
            }
            catch (Exception error)
            {
                _codeInitiatedNavigation = false;
                _isLoading = false;
                Logger.LogException(error, "Failed to load article '{1}' !",
                    article.Title);
            }
        }

        private void OnWebBrowserControlLayoutUpdated(object sender, EventArgs e)
        {
            WebBrowserControl.Loaded -= OnWebBrowserControlLayoutUpdated;
            Dispatcher.BeginInvoke(() => { Reload(); });
        }

        private void OnWebBrowserControlNavigating(object sender, Microsoft.Phone.Controls.NavigatingEventArgs e)
        {
            if (_codeInitiatedNavigation)
            {
                UpdateVisualState(true);
                return;
            }
        }

        private void OnWebBrowserControlNavigated(object sender, System.Windows.Navigation.NavigationEventArgs e)
        {
            _codeInitiatedNavigation = false;
        }

        private void OnWebBrowserControlLoadCompleted(object sender, System.Windows.Navigation.NavigationEventArgs e)
        {
            _isLoading = false;
            UpdateVisualState(true);
        }

        private void OnPreviousButtonClick(object sender, EventArgs e)
        {
            if (_viewModel != null)
            {
                _viewModel.GoToPreviousArticle();
            }
        }

        private void OnNextButtonClick(object sender, EventArgs e)
        {
            if (_viewModel != null)
            {
                _viewModel.GoToNextArticle();
            }
        }
    }
}

