// -------------------------------  Copyright ---------------------------------
//	This software has been developped by France Telecom, FT/RD/RD/MAPS/DVC/MSF
//
//	Copyright (c) France Telecom 2009-2010
//
// COPYRIGHT    : This file is the property of FRANCE TELECOM.
//                It cannot be copied, used, or modified without obtaining
//                an authorization from the authors or a mandated
//                member of FRANCE TELECOM.
//                If such an authorization is provided, any modified version or
//                copy of the software has to contain this header.
//
// WARRANTIES   : This software is made available by the authors in the  hope
//                that it will be useful, but without any warranty.
//                France Telecom is not liable for any consequence related to the
//                use of the provided software.
//
// AUTHORS      : France Telecom  / RD / MAPS / DVC / MSF
// ----------------------------------------------------------------------------
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using Microsoft.Phone.Controls;
using OrangeLigue1.ViewModels;
using OrangeLigue1.Model;

namespace OrangeLigue1.Views
{
    public partial class RankingPage : PhoneApplicationPage
    {
        private RankingPageViewModel _viewModel = null;
        private int rank = 0;

        public RankingPage()
        {
            InitializeComponent();
        }

        protected override void OnNavigatedTo(System.Windows.Navigation.NavigationEventArgs e)
        {
 	        base.OnNavigatedTo(e);
            BackgroundActivityBeacon.Current.RegisterProgressBar(BackgroundActivityIndicator);
            if (_viewModel == null)
            {
                _viewModel = RankingPageViewModel.Instance;
                _viewModel.PropertyChanged += myPageViewModelHasChanged;
                DataContext = _viewModel;
            }
            RestoreTransientState();
		}
		
		protected override void OnNavigatedFrom(System.Windows.Navigation.NavigationEventArgs e)
        {
            base.OnNavigatedFrom(e);
            BackgroundActivityBeacon.Current.Unregister(BackgroundActivityIndicator);
            SaveTransientState();
        }
		
		#region Transient State

        private const string RankIdStateKey = "RankId";

        private void SaveTransientState()
        {
            if (_viewModel != null)
            {
                State[RankIdStateKey] = rank;
            }
        }
		
		private void RestoreTransientState()
        {
            if (_viewModel != null)
            {
                if (State.ContainsKey(RankIdStateKey))
                {
                    try { rank = Convert.ToInt32(State[RankIdStateKey]); }
                    catch (Exception) { }
                }
                else if (this.NavigationContext.QueryString.ContainsKey("rank"))
	            {
	                rank = Convert.ToInt32(this.NavigationContext.QueryString["rank"]);
	            }
                _viewModel.Rank = rank;
            }
        }

        #endregion
		
		void myPageViewModelHasChanged(object sender, System.ComponentModel.PropertyChangedEventArgs e)
        {
            string propname = e.PropertyName;

            if (e.PropertyName == "Rank")
			{
                if (rank != 0 && this.listBox.Items.Count >= rank)
				{
					Deployment.Current.Dispatcher.BeginInvoke( () => 
						{
							this.listBox.ScrollIntoView(this.listBox.Items.Last());
							this.listBox.UpdateLayout();
                            this.listBox.ScrollIntoView(this.listBox.Items[rank - 1]); 
						});//-1 for array bound exception
				}
			}
        }
    }
}
