﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using Microsoft.Phone.Controls;
using OrangeLigue1.ViewModels;

namespace OrangeLigue1.Views
{
    public partial class PivotOptionsCatalog : PhoneApplicationPage
    {
        private PivotOptionsCatalogViewModel _viewModel = null;

        public PivotOptionsCatalog()
        {
            InitializeComponent();          
        }


        // Executes when the user navigates to this page.
        protected override void OnNavigatedTo(System.Windows.Navigation.NavigationEventArgs e)
        {
            base.OnNavigatedTo(e);
            //string optionId = null;

            if (_viewModel == null)
            {
                _viewModel = PivotOptionsCatalogViewModel.Instance;
                //_viewModel.PropertyChanged += myPageViewModel_PropertyChanged;
                //_viewModel.ConnectionErrorRequestEnd += myPageViewModel_ConnectionErrorRequestEnd;
            }
            
			
            DataContext = _viewModel;
            
          /*
            if (NavigationContext.QueryString.ContainsKey("index"))
            {
                optionId = NavigationContext.QueryString["index"];
            }

            _viewModel.SetCurrentPivotItem(Convert.ToInt32(optionId));*/
        }

       

    }
}