﻿// -------------------------------  Copyright ---------------------------------
//	This software has been developped by France Telecom, FT/RD/RD/MAPS/DVC/MSF
//
//	Copyright (c) France Telecom 2009-2010
//
// COPYRIGHT    : This file is the property of FRANCE TELECOM.
//                It cannot be copied, used, or modified without obtaining
//                an authorization from the authors or a mandated
//                member of FRANCE TELECOM.
//                If such an authorization is provided, any modified version or
//                copy of the software has to contain this header.
//
// WARRANTIES   : This software is made available by the authors in the  hope
//                that it will be useful, but without any warranty.
//                France Telecom is not liable for any consequence related to the
//                use of the provided software.
//
// AUTHORS      : France Telecom  / RD / MAPS / DVC / MSF
// ----------------------------------------------------------------------------
using System;
using System.ComponentModel;
using System.Diagnostics;
using System.Windows;
using Microsoft.Phone.Controls;
using Microsoft.Phone.Info;
using OrangeLigue1.ViewModels;
using OrangeLigue1.Tools;
using System.Windows.Threading;
using System.Collections.Generic;
using Kawagoe.Threading;
using System.Windows.Media;
using Microsoft.Web.Media.SmoothStreaming;
using System.Linq;

namespace OrangeLigue1.Views
{
    public partial class MediaElementPage : PhoneApplicationPage
    {
        private Dictionary<string, object> _transientState = null;
        private MediaElementPageViewModel _viewModel = null;
        private bool _isOverlayVisible = true;

        public MediaElementPage()
        {
            InitializeComponent();

            LayoutUpdated += OnLayoutUpdated;

            VisualStateManager.GoToState(this, InitModeState.Name, false);
        }

        protected override void OnNavigatedTo(System.Windows.Navigation.NavigationEventArgs e)
        {
            base.OnNavigatedTo(e);
            _transientState = new Dictionary<string, object>(State);

            this.mediaElement.CurrentStateChanged += OnMediaElementCurrentStateChanged;

            App.CurrentApp.PreventUserIdleDetectionMode();

            MediaElementPageViewModel.IsOpened = true;
            ((App)Application.Current).RootFrame.Obscured += this.RootFrameOnObscured;
            ((App)Application.Current).RootFrame.Unobscured += this.RootFrameOnUnobscured;
        }

        private void RootFrameOnObscured(object sender, ObscuredEventArgs e)
        {
            System.Diagnostics.Debug.WriteLine("FullScreenPlayer: RootFrame obscured!");
        }

        private void RootFrameOnUnobscured(object sender, EventArgs e)
        {
            if (mediaElement != null && mediaElement.CurrentState == MediaElementState.Closed )
            {
                NavigationService.GoBack();
            }
            if (mediaElement != null && mediaElement.CurrentState == MediaElementState.Paused)
            {
                mediaElement.Play();
            }
        }

        private void OnLayoutUpdated(object sender, EventArgs e)
        {
            if (_transientState == null)
            {
                return;
            }
            try
            {
                if (_viewModel == null)
                {
                    _viewModel = new MediaElementPageViewModel(AppConfig.Current.AmpConfig);
                    _viewModel.PropertyChanged += OnViewModelPropertyChanged;
                    DataContext = _viewModel;
                }
                RestoreTransientState(_transientState);
                if (_viewModel != null)
                {
                    _viewModel.Reload();
                }
                StartProgressTimer();
                UpdateVisualState(false);
            }
            catch (Exception)
            {
            }
            _transientState = null;
        }

        protected override void OnNavigatedFrom(System.Windows.Navigation.NavigationEventArgs e)
        {
            base.OnNavigatedFrom(e);
            App.CurrentApp.AllowUserIdleDetectionMode();
            SaveTransientState();
            StopProgressTimer();

            mediaElement.CurrentStateChanged -= OnMediaElementCurrentStateChanged;

            if (_viewModel.IsMediaReady)
            {
                try
                {
                    if (_viewModel.IsSmoothStreaming)
                    {
                        smoothMediaElement.Stop();
                    }
                    else
                    {
                        mediaElement.Stop();
                    }
                }
                catch (Exception)
                {
                }
            }

            MediaElementPageViewModel.IsOpened = false;
        }

        #region Transient State

        private const string MediaUriStateKey = "MediaUri";
        private const string MediaWifiUriStateKey = "MediaWifiUri";
        private const string MediaTitleStateKey = "MediaTitle";
        private const string MediaIsLiveStateKey = "MediaIsLive";

        private void SaveTransientState()
        {
            if (_viewModel == null)
            {
                return;
            }

            if (_viewModel.CellularMediaUri != null)
            {
                State[MediaUriStateKey] = _viewModel.CellularMediaUri;
            }

            if (_viewModel.WifiMediaUri != null)
            {
                State[MediaWifiUriStateKey] = _viewModel.WifiMediaUri;
            }

            if (_viewModel.Title != null)
            {
                State[MediaTitleStateKey] = _viewModel.Title;
            }

            if (_viewModel.Live != null)
            {
                State[MediaIsLiveStateKey] = _viewModel.Live;
            }
        }

        private void RestoreTransientState(Dictionary<string, object> transientState)
        {
            if (_viewModel == null || transientState == null)
            {
                return;
            }

            if (transientState.ContainsKey(MediaUriStateKey))
            {
                try
                {
                    ObjectRepository.Store(MediaElementPageViewModel.MediaUriString,
                                           transientState[MediaUriStateKey].ToString());
                }
                catch (Exception)
                {
                }
            }

            if (transientState.ContainsKey(MediaWifiUriStateKey))
            {
                try
                {
                    ObjectRepository.Store(MediaElementPageViewModel.MediaWifiUriString,
                                           transientState[MediaWifiUriStateKey].ToString());
                }
                catch (Exception)
                {
                }
            }

            if (transientState.ContainsKey(MediaTitleStateKey))
            {
                try
                {
                    ObjectRepository.Store(MediaElementPageViewModel.MediaTitleString,
                                           transientState[MediaTitleStateKey].ToString());
                }
                catch (Exception)
                {
                }
            }

            if (transientState.ContainsKey(MediaIsLiveStateKey))
            {
                try
                {
                    ObjectRepository.Store(MediaElementPageViewModel.MediaLiveString,
                                           transientState[MediaIsLiveStateKey]);
                }
                catch (Exception)
                {
                }
            }
        }

        #endregion Transient State

        private void UpdateVisualState(bool useTransitions)
        {
            if (_viewModel == null)
            {
                return;
            }

            // Handle error states
            string errorMessage = null;
            if (_viewModel.IsGeoblocked)
            {
                errorMessage = Localization.GetString("Newscloud_MessageH");
            }
            else if (_viewModel.LastError != null)
            {
                // and contained into the LastError. And in this case check the HorizontalAlignement of the MessageTextBlock
                // TextBlock of the .xaml
                // errorMessage = _viewModel.LastError.Message;

                errorMessage = Localization.GetString("Newscloud_MessageT");
            }
            if (!string.IsNullOrEmpty(errorMessage))
            {
                MessageTextBlock.Text = errorMessage;
                VisualStateManager.GoToState(this, BufferReadyState.Name, useTransitions);
                VisualStateManager.GoToState(this, MessageModeState.Name, useTransitions);
                return;
            }

            if (_viewModel.IsMediaReady)
            {
                if (_viewModel.IsSmoothStreaming)
                {
                    // Update the state of the Play button
                    switch (smoothMediaElement.CurrentState)
                    {
                        case SmoothStreamingMediaElementState.Playing:
                            playPause.IsEnabled = true;
                            playPause.Content = ";";
                            break;
                        case SmoothStreamingMediaElementState.Paused:
                            playPause.IsEnabled = true;
                            playPause.Content = "4";
                            break;
                        default:
                            playPause.IsEnabled = false;
                            break;
                    }

                    // Update the buffer state
                    switch (smoothMediaElement.CurrentState)
                    {
                        case SmoothStreamingMediaElementState.Closed:
                        case SmoothStreamingMediaElementState.Opening:
                        case SmoothStreamingMediaElementState.Buffering:
                            VisualStateManager.GoToState(this, BufferingState.Name, useTransitions);
                            break;
                        default:
                            VisualStateManager.GoToState(this, BufferReadyState.Name, useTransitions);
                            break;
                    }

                }
                else
                {
                    // Update the state of the Play button
                    switch (mediaElement.CurrentState)
                    {
                        case MediaElementState.Playing:
                            playPause.IsEnabled = true;
                            playPause.Content = ";";
                            break;
                        case MediaElementState.Paused:
                            playPause.IsEnabled = true;
                            playPause.Content = "4";
                            break;
                        default:
                            playPause.IsEnabled = false;
                            break;
                    }

                    // Update the buffer state
                    switch (mediaElement.CurrentState)
                    {
                        case MediaElementState.Closed:
                        case MediaElementState.Opening:
                        case MediaElementState.Buffering:
                            VisualStateManager.GoToState(this, BufferingState.Name, useTransitions);
                            break;
                        default:
                            VisualStateManager.GoToState(this, BufferReadyState.Name, useTransitions);
                            break;
                    }
                }
            }

            // Update the overlay state
            if (_isOverlayVisible)
            {
                VisualStateManager.GoToState(this, OverlayVisibleState.Name, useTransitions);
            }
            else
            {
                VisualStateManager.GoToState(this, OverlayHiddenState.Name, useTransitions);
            }

            // Update the progress bar
            UpdateProgressBarState();

            // Update the player mode state
            if (_viewModel.MediaSource != null)
            {
                if (_viewModel.IsSmoothStreaming)
                {
                    VisualStateManager.GoToState(this, SmoothVideoModeState.Name, useTransitions);
                }
                else
                {
                    VisualStateManager.GoToState(this, VideoModeState.Name, useTransitions);
                }
            }
        }

        #region Progress Bar

        private static readonly TimeSpan ProgressTimerInterval = TimeSpan.FromSeconds(0.9);

        private DispatcherTimer _progressTimer = null;

        private void UpdateProgressBarState()
        {
            if (_viewModel == null || _viewModel.IsMediaReady == false)
            {
                return;
            }

            if (_viewModel.IsSmoothStreaming)
            {
                if (smoothMediaElement.CurrentState == SmoothStreamingMediaElementState.Stopped
                    || !smoothMediaElement.NaturalDuration.HasTimeSpan
                    || smoothMediaElement.NaturalDuration.TimeSpan.TotalSeconds == 0)
                {
                    progressBar.Value = 0;
                    progressBar.Maximum = 1;
                }
                else
                {
                    progressBar.Value = smoothMediaElement.Position.TotalSeconds;
                    progressBar.Maximum = smoothMediaElement.NaturalDuration.TimeSpan.TotalSeconds;
                }

            }
            else
            {

                if (mediaElement.CurrentState == MediaElementState.Stopped
                    || !mediaElement.NaturalDuration.HasTimeSpan
                    || mediaElement.NaturalDuration.TimeSpan.TotalSeconds == 0)
                {
                    progressBar.Value = 0;
                    progressBar.Maximum = 1;
                }
                else
                {
                    progressBar.Value = mediaElement.Position.TotalSeconds;
                    progressBar.Maximum = mediaElement.NaturalDuration.TimeSpan.TotalSeconds;
                }
            }
        }

        private void StartProgressTimer()
        {
            StopProgressTimer();
            try
            {
                _progressTimer = new DispatcherTimer();
                _progressTimer.Interval = ProgressTimerInterval;
                _progressTimer.Tick += new EventHandler(OnProgressTimerTick);
                _progressTimer.Start();
            }
            catch (Exception)
            {
            }
        }

        private void StopProgressTimer()
        {
            if (_progressTimer == null)
            {
                return;
            }
            try
            {
                _progressTimer.Stop();
            }
            catch (Exception)
            {
            }
            _progressTimer = null;
        }

        private void OnProgressTimerTick(object sender, EventArgs e)
        {
            if (sender != _progressTimer)
            {
                return;
            }
            UpdateProgressBarState();
        }

        #endregion

        #region Hide Overlay Timer

        private static readonly TimeSpan HideOverlayTimerDuration = TimeSpan.FromSeconds(1);

        private OneShotDispatcherTimer _hideOverlayTimer = null;

        private void StartHideOverlayTimer()
        {
            if (_hideOverlayTimer != null)
            {
                return;
            }
            _hideOverlayTimer = new OneShotDispatcherTimer();
            _hideOverlayTimer.Duration = HideOverlayTimerDuration;
            _hideOverlayTimer.Fired += OnHideOverlayTimerFired;
            _hideOverlayTimer.Start();
        }

        private void StopHideOverlayTimer()
        {
            if (_hideOverlayTimer == null)
            {
                return;
            }
            _hideOverlayTimer.Stop();
            _hideOverlayTimer = null;
        }

        private void OnHideOverlayTimerFired(object sender, EventArgs e)
        {
            if (sender != _hideOverlayTimer)
            {
                return;
            }
            _hideOverlayTimer = null;
            _isOverlayVisible = false;
            UpdateVisualState(true);
        }

        #endregion

        private void OnViewModelPropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            if (sender != _viewModel || _viewModel == null)
            {
                return;
            }
            switch (e.PropertyName)
            {
                case "IsMediaReady":
                    if (_viewModel.IsMediaReady)
                    {
                        if (_viewModel.IsSmoothStreaming)
                        {
                            if (_viewModel.MediaSource != null)
                            {
                                this.mediaElement.Visibility = Visibility.Collapsed;
                                this.smoothMediaElement.Visibility = Visibility.Visible;
                                this.smoothMediaElement.CookieContainer = _viewModel.cookieContainer;
                                this.smoothMediaElement.SmoothStreamingSource = _viewModel.MediaSource;
                            }
                        }
                        else
                        {
                            if (_viewModel.MediaSource != null)
                            {
                                this.smoothMediaElement.Visibility = Visibility.Collapsed;
                                this.mediaElement.Visibility = Visibility.Visible;
                                this.mediaElement.Source = _viewModel.MediaSource;
                            }
                        }

                    }
                    break;
                case "LastError":
                    if (_viewModel.LastError != null && _viewModel.IsMediaReady)
                    {
                        try
                        {
                            if (_viewModel.IsSmoothStreaming)
                            {
                                smoothMediaElement.Stop();
                            }
                            else
                            {
                                mediaElement.Stop();
                            }
                        }
                        catch (Exception)
                        {
                        }
                    }
                    break;
                case "MediaSource":
                    {
                        UpdateVisualState(true);
                    }
                    break;
                    

                case "Live":
                    if (_viewModel.Live != null)
                    {
                        if (_viewModel.Live == false)
                        {
                            this.playPause.Visibility = Visibility.Visible;
                            this.progressBar.Visibility = Visibility.Visible;
                        }
                    }
                    break;
            }
            UpdateVisualState(true);
        }

        private void OnMediaOpened(object sender, RoutedEventArgs e)
        {
            if (_viewModel == null)
            {
                return;
            }

            _viewModel.OnMediaStopPlaybackTimer();
            if (_viewModel.IsSmoothStreaming)
            {
                smoothMediaElement.Play();
            }
            else
            {
                mediaElement.Play();
            }
            StartHideOverlayTimer();
        }

        private void OnMediaEnded(object sender, RoutedEventArgs e)
        {
            if (_viewModel != null)
            {
                _viewModel.OnMediaStopPlaybackTimer();
            }

            Navigator.GoBack();
        }

        private void OnMediaFailed(object sender, System.Windows.ExceptionRoutedEventArgs e)
        {
            if (e.ErrorException == null)
            {
                return;
            }

            if (_viewModel != null)
            {
                _viewModel.OnMediaStopPlaybackTimer();
                if (_viewModel.MediaSource != null)
                {
                    Logger.LogException(e.ErrorException, "Error while playing media item at {0}!", _viewModel.MediaSource.ToString());
                }
                else
                {
                    Logger.LogException(e.ErrorException, "Error while playing media item no media source available");
                }
                _viewModel.LastError = e.ErrorException;
            }

            this.playPause.Visibility = Visibility.Collapsed;
            this.progressBar.Visibility = Visibility.Collapsed;
        }

        private void OnMediaElementCurrentStateChanged(object sender, RoutedEventArgs e)
        {
            UpdateVisualState(true);
        }

        private void OnToggleOverlayButtonClick(object sender, RoutedEventArgs e)
        {
            StopHideOverlayTimer();
            _isOverlayVisible = !_isOverlayVisible;
            UpdateVisualState(true);
        }

        private void OnPlayPauseButtonClick(object sender, RoutedEventArgs e)
        {
            if (_viewModel == null || _viewModel.IsMediaReady == false)
            {
                return;
            }

            if (_viewModel.IsSmoothStreaming)
            {
                switch (smoothMediaElement.CurrentState)
                {
                    case SmoothStreamingMediaElementState.Playing:
                        if (smoothMediaElement.CanPause)
                        {
                            smoothMediaElement.Pause();
                        }
                        else
                        {
                            smoothMediaElement.Stop();
                        }
                        break;
                    case SmoothStreamingMediaElementState.Paused:
                        smoothMediaElement.Play();
                        StartHideOverlayTimer();
                        break;
                }
            }
            else
            {

                switch (mediaElement.CurrentState)
                {
                    case MediaElementState.Playing:
                        if (mediaElement.CanPause)
                        {
                            mediaElement.Pause();
                        }
                        else
                        {
                            mediaElement.Stop();
                        }
                        break;
                    case MediaElementState.Paused:
                        mediaElement.Play();
                        StartHideOverlayTimer();
                        break;
                }
            }
        }

        private void smoothMediaElement_ManifestReady(object sender, EventArgs e)
        {
            var ssme = sender as SmoothStreamingMediaElement;

            if (ssme == null)
            {
                return;
            }

            // Restrict the tracks only when the multi reso is not supported.
            // Simply return here if IsMultiResolutionVideoSupported is true
            if (MediaCapabilities.IsMultiResolutionVideoSupported)
            {
                return;
            }

            // IIS OOB #19049: Windows mobile 7 v1 cannot support dynamic resolution change, so application will
            // select a band of tracks which all have the same resolution.
            foreach (var segment in ssme.ManifestInfo.Segments)
            {
                foreach (var streamInfo in segment.AvailableStreams)
                {
                    if (MediaStreamType.Video == streamInfo.Type)
                    {
                        var widestBand = new List<TrackInfo>();
                        var currentBand = new List<TrackInfo>();
                        ulong lastHeight = 0;
                        ulong lastWidth = 0;
                        ulong index = 0;

                        foreach (var track in streamInfo.AvailableTracks.OrderBy(trac => trac.Bitrate))
                        {
                            index += 1;

                            string strMaxWidth;
                            string strMaxHeight;
                            ulong ulMaxWidth = index; // If can't find width/height, choose only the top bitrate
                            ulong ulMaxHeight = index; // If can't find width/height, choose only the top bitrate

                            // v2 manifests require "MaxWidth", while v1 manifests used "Width".
                            if (track.Attributes.TryGetValue("MaxWidth", out strMaxWidth) ||
                                track.Attributes.TryGetValue("Width", out strMaxWidth))
                            {
                                ulong.TryParse(strMaxWidth, out ulMaxWidth);
                            }

                            if (track.Attributes.TryGetValue("MaxHeight", out strMaxHeight) ||
                                track.Attributes.TryGetValue("Height", out strMaxHeight))
                            {
                                ulong.TryParse(strMaxHeight, out ulMaxHeight);
                            }

                            if (ulMaxWidth != lastWidth ||
                                ulMaxHeight != lastHeight)
                            {
                                // Current band is now finished, check if it is the widest
                                // If same size, we prefer current band over prev widest, since it
                                // will be of higher bitrate
                                if (currentBand.Count >= widestBand.Count)
                                {
                                    // We have a new widest band
                                    widestBand = currentBand;
                                    currentBand = new List<TrackInfo>();
                                }
                            }

                            if (track.Bitrate > 1000000)
                            {
                                break;
                            }

                            // Current track always gets added to current band
                            currentBand.Add(track);
                            lastWidth = ulMaxWidth;
                            lastHeight = ulMaxHeight;
                        }

                        if (0 == widestBand.Count &&
                            0 == currentBand.Count)
                        {
                            // Lowest bitrate band is > 1Mbps
                            widestBand.Add(streamInfo.AvailableTracks[0]);
                        }
                        else if (currentBand.Count >= widestBand.Count)
                        {
                            // Need to check the last band which was constructed
                            Debug.Assert(currentBand.Count > 0);
                            widestBand = currentBand; // Winner by default
                        }

                        Debug.Assert(widestBand.Count >= 1);

                        //for an unknown reason OnManifestReady is called twice ...
                        try
                        {
                            streamInfo.RestrictTracks(widestBand);
                        }
                        catch
                        {
                        }
                    }
                }
            }
        }

    }
}

