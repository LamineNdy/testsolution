// -------------------------------  Copyright ---------------------------------
//	This software has been developped by France Telecom, FT/RD/RD/MAPS/DVC/MSF
//
//	Copyright (c) France Telecom 2009-2010
//
// COPYRIGHT    : This file is the property of FRANCE TELECOM.
//                It cannot be copied, used, or modified without obtaining
//                an authorization from the authors or a mandated
//                member of FRANCE TELECOM.
//                If such an authorization is provided, any modified version or
//                copy of the software has to contain this header.
//
// WARRANTIES   : This software is made available by the authors in the  hope
//                that it will be useful, but without any warranty.
//                France Telecom is not liable for any consequence related to the
//                use of the provided software.
//
// AUTHORS      : France Telecom  / RD / MAPS / DVC / MSF
// ----------------------------------------------------------------------------
using System;
using System.Net;
using System.Windows;
using Microsoft.Phone.Controls;
using OrangeLigue1.ViewModels;
using System.ComponentModel;
using OrangeLigue1.Model;
using AuthenticationManager;

namespace OrangeLigue1.Views
{
    public partial class AccessRightsPage : PhoneApplicationPage
    {
        private AccessRightsPageViewModel _viewModel = null;

        public AccessRightsPage()
        {
            InitializeComponent();
        }

        protected override void OnNavigatedTo(System.Windows.Navigation.NavigationEventArgs e)
        {
 	        base.OnNavigatedTo(e);
            BackgroundActivityBeacon.Current.RegisterProgressBar(BackgroundActivityIndicator);
            if (_viewModel == null)
            {
                _viewModel = new AccessRightsPageViewModel();
                _viewModel.ConnectionErrorRequestEnd += myPageViewModel_ConnectionErrorRequestEnd;
                _viewModel.PropertyChanged += myPageViewModelHasChanged;
            }
            DataContext = _viewModel;
            _viewModel.Refresh();
            VisualStateManager.GoToState(this, LoadingState.Name, false);
        }

        protected override void OnNavigatedFrom(System.Windows.Navigation.NavigationEventArgs e)
        {
            base.OnNavigatedFrom(e);
            BackgroundActivityBeacon.Current.Unregister(BackgroundActivityIndicator);
        }

        void myPageViewModelHasChanged(object sender, PropertyChangedEventArgs e)
        {
            string propname = e.PropertyName;

            if (String.Equals(propname, "MyPageIsLoaded"))
            {
                if (_viewModel.MyPageIsLoaded == true)
                {
                    VisualStateManager.GoToState(this, PageState.Name, false);
                }
            }
        }

        void myPageViewModel_ConnectionErrorRequestEnd(object sender, Tools.BackgroundRequestCompletedEventArgs e)
        {
            string userMessage = null;

            OLAuthenticationException oLAuthenticationException = null;

            if (e != null &&
                e.Error != null)
            {
                if (e.Error is OLAuthenticationException)
                {
                    oLAuthenticationException = e.Error as OLAuthenticationException;
                    if (oLAuthenticationException != null)
                    {
                        userMessage = oLAuthenticationException.ErrorMessage;
                    }
                }
            }

            WebException webException = null;

            if (userMessage == null &&
                e != null &&
                e.Error != null &&
                e.Error.InnerException != null)
            {
                webException = e.Error.InnerException as WebException;
            }

            if (webException != null && webException.Response != null && webException.Response is HttpWebResponse)
            {
                HttpWebResponse webResponse = (HttpWebResponse)(webException.Response);
                if (webResponse.StatusCode == HttpStatusCode.Forbidden)
                {
                    userMessage = Localization.GetString("Message_DisableWiFi");
                }
            }
            if (userMessage == null)
            {
                userMessage = Localization.GetString("Message_NoConnection");
            }
            CannotLaunchTextBlock.Text = userMessage;
            VisualStateManager.GoToState(this, CannotLaunchState.Name, false);
        }
    }
}
