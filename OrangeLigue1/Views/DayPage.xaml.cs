// -------------------------------  Copyright ---------------------------------
//	This software has been developped by France Telecom, FT/RD/RD/MAPS/DVC/MSF
//
//	Copyright (c) France Telecom 2009-2010
//
// COPYRIGHT    : This file is the property of FRANCE TELECOM.
//                It cannot be copied, used, or modified without obtaining
//                an authorization from the authors or a mandated
//                member of FRANCE TELECOM.
//                If such an authorization is provided, any modified version or
//                copy of the software has to contain this header.
//
// WARRANTIES   : This software is made available by the authors in the  hope
//                that it will be useful, but without any warranty.
//                France Telecom is not liable for any consequence related to the
//                use of the provided software.
//
// AUTHORS      : France Telecom  / RD / MAPS / DVC / MSF
// ----------------------------------------------------------------------------
using System;
using OrangeLigue1.ViewModels;
using OrangeLigue1.Model;

namespace OrangeLigue1.Views
{
    public partial class DayPage
    {
        private DayPageViewModel _viewModel;

        public DayPage()
        {
            InitializeComponent();
        }

        // Executes when the user navigates to this page.
        protected override void OnNavigatedTo(System.Windows.Navigation.NavigationEventArgs e)
        {
 	        base.OnNavigatedTo(e);
            BackgroundActivityBeacon.Current.RegisterProgressBar(BackgroundActivityIndicator);
            if (_viewModel == null)
            {
                _viewModel = DayPageViewModel.Instance;
                DataContext = _viewModel;
            }
            RestoreTransientState();
        }

        protected override void OnNavigatedFrom(System.Windows.Navigation.NavigationEventArgs e)
        {
            base.OnNavigatedFrom(e);
            BackgroundActivityBeacon.Current.Unregister(BackgroundActivityIndicator);
            SaveTransientState();
        }

        #region Transient State

        private const string DayIdStateKey = "DayId";

        private void SaveTransientState()
        {
            if (_viewModel != null && _viewModel.Day != 0)
            {
                State[DayIdStateKey] = _viewModel.Day;
            }
        }
		
		private void RestoreTransientState()
        {
            if (_viewModel != null)
            {
                string dayId = null;
                if (State.ContainsKey(DayIdStateKey))
                {
                    try { dayId = State[DayIdStateKey].ToString(); }
                    catch { }
                }
                else if (NavigationContext.QueryString.ContainsKey("day"))
                {
                    dayId = NavigationContext.QueryString["day"];
                }
                if (App.CurrentApp.MyLigue1.WholeCalendarUpdated || (_viewModel.MyMatchsOfTheHourList != null && _viewModel.MyMatchsOfTheHourList.Count == 0))
                {
                    App.CurrentApp.MyLigue1.WholeCalendarUpdated = false;
                    _viewModel.MyCalendarAction = App.CurrentApp.MyLigue1.MyWholeCalendarAction;
                }
                _viewModel.Day = Convert.ToInt32(dayId);
            }
        }

        #endregion

        private void OnPreviousButtonClick(object sender, EventArgs e)
        {
            if (_viewModel != null)
            {
                _viewModel.GoToPreviousDay();
            }
        }

        private void OnNextButtonClick(object sender, EventArgs e)
        {
            if (_viewModel != null)
            {
                _viewModel.GoToNextDay();
            }
        }
    }
}
