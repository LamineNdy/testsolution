﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Navigation;
using Microsoft.Phone.Controls;
using Microsoft.Phone.Shell;
using OrangeLigue1.ViewModels;

namespace OrangeLigue1.Views
{
    public partial class LegalPage : PhoneApplicationPage
    {
        private LegalPageViewModel _viewModel = null;

        public LegalPage()
        {
            if (_viewModel == null)
            {
                _viewModel = new LegalPageViewModel();
                DataContext = _viewModel;
            }

            InitializeComponent();
        }
    }
}