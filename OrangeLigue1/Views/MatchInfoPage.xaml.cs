// -------------------------------  Copyright ---------------------------------
//	This software has been developped by France Telecom, FT/RD/RD/MAPS/DVC/MSF
//
//	Copyright (c) France Telecom 2009-2010
//
// COPYRIGHT    : This file is the property of FRANCE TELECOM.
//                It cannot be copied, used, or modified without obtaining
//                an authorization from the authors or a mandated
//                member of FRANCE TELECOM.
//                If such an authorization is provided, any modified version or
//                copy of the software has to contain this header.
//
// WARRANTIES   : This software is made available by the authors in the  hope
//                that it will be useful, but without any warranty.
//                France Telecom is not liable for any consequence related to the
//                use of the provided software.
//
// AUTHORS      : France Telecom  / RD / MAPS / DVC / MSF
// ----------------------------------------------------------------------------
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using Microsoft.Phone.Controls;
using OrangeLigue1.ViewModels;
using Orange.SDK.Enablers.Ligue1;
using System.ComponentModel;
using OrangeLigue1.Model;
using OrangeLigue1.Tools;
using AuthenticationManager;

namespace OrangeLigue1.Views
{
    public partial class MatchInfoPage : PhoneApplicationPage
    {
        private MatchInfoPageViewModel _viewModel = null;

        public MatchInfoPage()
        {
            InitializeComponent();
        }

        protected override void OnNavigatedTo(System.Windows.Navigation.NavigationEventArgs e)
        {
			base.OnNavigatedTo(e);
            BackgroundActivityBeacon.Current.RegisterProgressBar(BackgroundActivityIndicator);
            VisualStateManager.GoToState(this, LoadingState.Name, false);
            this.LiveBarPanel.Visibility = Visibility.Collapsed;

            if (_viewModel == null)
            {
                _viewModel = MatchInfoPageViewModel.Instance;
                _viewModel.PropertyChanged += myPageViewModel_PropertyChanged;
                _viewModel.ConnectionErrorRequestEnd += myPageViewModel_ConnectionErrorRequestEnd;
            }
            DataContext = _viewModel;
			RestoreTransientState();
            _viewModel.Refresh();
        }

        protected override void OnNavigatedFrom(System.Windows.Navigation.NavigationEventArgs e)
        {
            _viewModel.StopTimer();
            base.OnNavigatedFrom(e);
            BackgroundActivityBeacon.Current.Unregister(BackgroundActivityIndicator);
            SaveTransientState();
        }
		
		#region Transient State

        private const string MatchIdStateKey = "MatchId";
		private const string LiveStatusStateKey = "LiveStatus";
        private const string CurrentOpenPageIndexStateKey = "CurrentOpenPageIndex";

        private void SaveTransientState()
        {
            if (_viewModel != null)
            {
                State[MatchIdStateKey] = _viewModel.MyMatchItemViewModel.MatchId;
                State[LiveStatusStateKey] = _viewModel.Live;
                State[CurrentOpenPageIndexStateKey] = PivotControl.SelectedIndex;
            }
        }
		
		private void RestoreTransientState()
        {
            if (_viewModel != null)
            {
                _viewModel.OnNavigatedTo();

                if (State.ContainsKey(MatchIdStateKey) && 
                    State.ContainsKey(LiveStatusStateKey) &&
                    State.ContainsKey(CurrentOpenPageIndexStateKey))
                {
                    try 
                    {
                        _viewModel.MyMatchItemViewModel.MatchId = Convert.ToInt32(State[MatchIdStateKey]);
                        _viewModel.Live = Convert.ToInt32(State[LiveStatusStateKey]);
                        PivotControl.SelectedIndex = Convert.ToInt32(State[CurrentOpenPageIndexStateKey]);
                    }
                    catch (Exception) { }
                }
                else if (this.NavigationContext.QueryString.ContainsKey("match") && this.NavigationContext.QueryString.ContainsKey("live"))
                {
                    string matchId = this.NavigationContext.QueryString["match"];
                    _viewModel.MyMatchItemViewModel.MatchId = Convert.ToInt32(matchId);

                    string liveStatus = this.NavigationContext.QueryString["live"];
                    _viewModel.Live = Convert.ToInt32(liveStatus);
                }

                if (_viewModel.Live == 1)
                {
                    this.LiveBarPanel.Visibility = Visibility.Visible;
                }
                else
                {
                    this.LiveBarPanel.Visibility = Visibility.Collapsed;
                }
            }
        }

        #endregion

        void myPageViewModel_PropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            string propname = e.PropertyName;

            if (String.Equals(propname, "MyPageIsLoaded"))
            {
                if (_viewModel.MyPageIsLoaded == true)
                {
                    VisualStateManager.GoToState(this, PageState.Name, false);
                }
            }
        }

        void myPageViewModel_ConnectionErrorRequestEnd(object sender, Tools.BackgroundRequestCompletedEventArgs e)
        {
            string userMessage = null;

            OLAuthenticationException oLAuthenticationException = null;

            if (e != null &&
                e.Error != null)
            {
                if (e.Error is OLAuthenticationException)
                {
                    oLAuthenticationException = e.Error as OLAuthenticationException;
                    if (oLAuthenticationException != null)
                    {
                        userMessage = oLAuthenticationException.ErrorMessage;
                    }
                }
            }

            WebException webException = null;

            if (userMessage == null &&
                e != null &&
                e.Error != null &&
                e.Error.InnerException != null)
            {
                webException = e.Error.InnerException as WebException;
            }

            if (webException != null && webException.Response != null && webException.Response is HttpWebResponse)
            {
                HttpWebResponse webResponse = (HttpWebResponse)(webException.Response);
                if (webResponse.StatusCode == HttpStatusCode.Forbidden)
                {
                    userMessage = Localization.GetString("Message_DisableWiFi");
                }
            }
            if (userMessage == null)
            {
                userMessage = Localization.GetString("Message_NoConnection");
            }
            CannotLaunchTextBlock.Text = userMessage;
            VisualStateManager.GoToState(this, CannotLaunchState.Name, false);
        }
    }
}
