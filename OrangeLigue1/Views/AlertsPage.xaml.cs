// -------------------------------  Copyright ---------------------------------
//	This software has been developped by France Telecom, FT/RD/RD/MAPS/DVC/MSF
//
//	Copyright (c) France Telecom 2009-2010
//
// COPYRIGHT    : This file is the property of FRANCE TELECOM.
//                It cannot be copied, used, or modified without obtaining
//                an authorization from the authors or a mandated
//                member of FRANCE TELECOM.
//                If such an authorization is provided, any modified version or
//                copy of the software has to contain this header.
//
// WARRANTIES   : This software is made available by the authors in the  hope
//                that it will be useful, but without any warranty.
//                France Telecom is not liable for any consequence related to the
//                use of the provided software.
//
// AUTHORS      : France Telecom  / RD / MAPS / DVC / MSF
// ----------------------------------------------------------------------------
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using Microsoft.Phone.Controls;
using OrangeLigue1.ViewModels;
using Microsoft.Phone.Shell;
using OrangeLigue1.Storage;

namespace OrangeLigue1.Views
{
    public partial class AlertPage : PhoneApplicationPage
    {
        private AlertsPageViewModel _viewModel = null;
        AlertsPageViewModel ViewModel { get; set; }

        public AlertPage()
        {
            InitializeComponent();
        }

        protected override void OnNavigatedTo(System.Windows.Navigation.NavigationEventArgs e)
        {
            if (_viewModel == null)
            {
                _viewModel = new AlertsPageViewModel();
                _viewModel.listBox = ClubsList;
            }

            this.DataContext = _viewModel;

            base.OnNavigatedTo(e);

            _viewModel.OnNavigatedTo();
        }

        protected override void OnNavigatedFrom(System.Windows.Navigation.NavigationEventArgs e)
        {
            _viewModel.OnNavigatedFrom();
            base.OnNavigatedFrom(e);
        }

        private void AmateurModeTap(object sender, System.Windows.Input.GestureEventArgs e)
        {
            _viewModel.AmateurModeSelected = true;
        }

        private void SupporterModeTap(object sender, System.Windows.Input.GestureEventArgs e)
        {
            _viewModel.AmateurModeSelected = false;
        }

        private void ValidateTap(object sender, EventArgs e)
        {
            _viewModel.OnValidate();
        }

        private void CancelTap(object sender, EventArgs e)
        {
            _viewModel.OnCancel();
        }

        private void ClubsListSelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            ListBox listBox = (ListBox)sender;
            _viewModel.ClubsListSelectionChanged(listBox);
        }
    }
}
