// -------------------------------  Copyright ---------------------------------
//	This software has been developped by France Telecom, FT/RD/RD/MAPS/DVC/MSF
//
//	Copyright (c) France Telecom 2009-2010
//
// COPYRIGHT    : This file is the property of FRANCE TELECOM.
//                It cannot be copied, used, or modified without obtaining
//                an authorization from the authors or a mandated
//                member of FRANCE TELECOM.
//                If such an authorization is provided, any modified version or
//                copy of the software has to contain this header.
//
// WARRANTIES   : This software is made available by the authors in the  hope
//                that it will be useful, but without any warranty.
//                France Telecom is not liable for any consequence related to the
//                use of the provided software.
//
// AUTHORS      : France Telecom  / RD / MAPS / DVC / MSF
// ----------------------------------------------------------------------------
using System;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using Orange.SDK.Enablers;
using Orange.SDK.Enablers.Ligue1;
using System.Runtime.Serialization;
using System.ComponentModel;
using OrangeLigue1.Storage;
using System.Collections.Generic;
using System.Xml.Serialization;
using System.Windows.Threading;
using OrangeLigue1.Tools;
using Microsoft.Phone.Tasks;

namespace OrangeLigue1.Model
{
    public class Ligue1
    {
        private const string DataStoreKey = "Ligue1";
        private const string MarketplaceAppIdUrlPrefix = "mkpc://";
        public static TimeSpan DefaultLifetime = TimeSpan.FromDays(1);
        private int matchesCounter = 0; 
		
        private Ligue1Enabler Ligue1BackEnd         { get; set; }
        private Ligue1Enabler NewsLigue1BackEnd     { get; set; }
        private Ligue1Enabler CalendarLigue1BackEnd { get; set; }

        private ConnectivityType connectivityType = ConnectivityWatcher.CurrentType;

        //default refresh values
        private static int FreqR    = 30;   //all matchs
        private static int FreqL    = 30;  //news & vod & teams


        /// <summary>
        /// The time interval for the refresh timer.
        /// </summary>
        private static TimeSpan refreshFreqRTimerInterval       = TimeSpan.FromSeconds(FreqR);
        private static TimeSpan refreshFreqLTimerInterval       = TimeSpan.FromSeconds(FreqL);

        public DispatcherTimer RefreshFreqRTimer
        {
            get;
            set;
        }

        private DispatcherTimer RefreshFreqLTimer
        {
            get;
            set;
        }

        public DispatcherTimer RefreshFreqLiveTimer
        {
            get;
            set;
        }

        [DataMember]
        public int CurrentDay
        {
            get;
            set;
        }

        [DataMember]
        public int CurrentTimer
        {
            get;
            set;
        }

        [DataMember]
        public InitinfoAction MyInitinfoAction
        {
            get;
            set;
        }

        [DataMember]
        public DynamicInitAction MyDynamicInitAction
        {
            get;
            set;
        }

        [DataMember]
        public LightSettingsAction MyLightSettingsAction
        {
            get;
            set;
        }

        [DataMember]
        public AllMatchAction MyAllMatchAction
        {
            get;
            set;
        }

        [XmlIgnore]
        public bool MyLightSettingsActionIsLoaded
        {
            get;
            private set;
        }

        [XmlIgnore]
        public bool MyManagerPass
        {
            get;
            private set;
        }

        [XmlIgnore]
        public bool MyInitinfoActionIsLoaded
        {
            get;
            private set;
        }

        [XmlIgnore]
        public bool MyDynamicInitActionIsLoaded
        {
            get;
            private set;
        }

        [XmlIgnore]
        public bool MyWordingActionIsLoaded
        {
            get;
            private set;
        }

        [XmlIgnore]
        public string MyLogoBaseUrl
        {
            get
            {
                string url = String.Empty;

                connectivityType = ConnectivityWatcher.CurrentType;

                url = MyLightSettingsAction.Teams.LogoBaseUrl;

                /* deprecated
                if (connectivityType == ConnectivityType.WiFi)
                {
                    url = MyLightSettingsAction.Teams.LogoBaseWifiUrl;
                }
                else
                {
                    url = MyLightSettingsAction.Teams.LogoBaseUrl;
                }
                
                if (url.Contains(AppConfig.Current.AmpConfig.ForbiddenPrefixUri))
                {
                    url = url.Replace(AppConfig.Current.AmpConfig.ForbiddenPrefixUri, AppConfig.Current.AmpConfig.NominalPrefixUri);
                }*/

                return url;
            }
        }

        [DataMember]
        public UserinfoAction MyUserinfoAction
        {
            get;
            set;
        }

        [DataMember]
        public WordingAction MyWordingAction
        {
            get;
            set;
        }

        [XmlIgnore]
        public DayInfo MyDayInfo
        {
            get;
            set;
        }
        
        [XmlIgnore]
        public CalendarDayInfo MyCalendarInfo
        {
            get;
            set;
        }

        [XmlIgnore]
        public DayNewsInfo MyDayNewsInfo
        {
            get;
            set;
        }

        [DataMember]
        public OptionCatalogAction MyOptionCatalogAction
        {
            get;
            set;
        }
        
        [DataMember]
        public OptionSubscribeAction MyOptionSubscribeAction
        {
            get;
            set;
        }

        [DataMember]
        public TeamsAction MyTeamsAction
        {
            get;
            set;
        }

        [DataMember]
        public CalendarAction MyDayCalendarAction
        {
            get;
            set;
        }

        [DataMember]
        public WholeCalendarAction MyWholeCalendarAction
        {
            get;
            set;
        }

        public bool WholeCalendarUpdated
        {
            get;
            set;
        }

        [DataMember]
        public DateTime? MyWholeCalendarTimestamp
        {
            get;
            set;
        }

        [DataMember]
        public DaynewsAction MyDayNewsAction
        {
            get;
            set;
        }

        [DataMember]
        public VoDAction MyVoDAction
        {
            get;
            set;
        }

        [DataMember]
        public bool AlertsAreActivated
        {
            get;
            set;
        }

        [DataMember]
        public PushSettingsAction MyPushSettingsAction
        {
            get;
            set;
        }

        //key = team id
        private Dictionary<int, TeamInfo> myTeamsInfo = new Dictionary<int, TeamInfo>();
        [XmlIgnore]
        public Dictionary<int, TeamInfo> MyTeamsInfo
        {
            get { return myTeamsInfo; }
            set { myTeamsInfo = value; }
        }

        private Dictionary<int, ParamInfo> myParamInfo = new Dictionary<int, ParamInfo>();
        [XmlIgnore]
        public Dictionary<int, ParamInfo> MyParamInfo
        {
            get { return myParamInfo; }
            set { myParamInfo = value; }
        }

        private Dictionary<int, MatchEventInfo> myInitInfoHighlight = new Dictionary<int, MatchEventInfo>();
        [XmlIgnore]
        public Dictionary<int, MatchEventInfo> MyInitInfoHighlight
        {
            get { return myInitInfoHighlight; }
            set { myInitInfoHighlight = value; }
        }

        [XmlIgnore]
        public int CurrentMatchId
        {
            get;
            set;
        }

        [XmlIgnore]
        public int LastEventId
        {
            get;
            set;
        }

        #region "Event/Args"
        public event EventHandler<DynamicInitAction>                    DynamicInitRequestEnd;
        public event EventHandler<InitinfoAction>                       InitinfoRequestEnd;
        public event EventHandler<LightSettingsAction>                  LightSettingsRequestEnd;
        public event EventHandler<UserinfoAction>                       UserinfoRequestEnd;
        public event EventHandler<WordingAction>                        WordingRequestEnd;
        public event EventHandler<OptionCatalogAction>                  OptionCatalogRequestEnd;
        public event EventHandler<OptionSubscribeAction>                OptionSubscribeRequestEnd;
        public event EventHandler<TeamsAction>                          TeamsRequestEnd;
        public event EventHandler<CalendarAction>                       DayCalendarRequestEnd;
        public event EventHandler<WholeCalendarAction>                  WholeCalendarRequestEnd;
        public event EventHandler<DaynewsAction>                        DayNewsRequestEnd;
        public event EventHandler<MatchAction>                          MatchRequestEnd;
        public event EventHandler<AllMatchAction>                       AllMatchRequestEnd;
        public event EventHandler<VoDAction>                            VodRequestEnd;
        public event EventHandler<BackgroundRequestCompletedEventArgs>  ConnectionErrorRequestEnd;
        public event EventHandler<PushSettingsAction>                   PushSettingsRequestEnd;
        #endregion

        public Ligue1()
        {
            Ligue1BackEnd = new Ligue1Enabler(AppConfig.Current.AmpConfig);
            Ligue1BackEnd.InitinfoRequestEnd        += Ligue1_InitinfoRequestEnd;
            Ligue1BackEnd.DynamicInitRequestEnd     += Ligue1BackEnd_DynamicInitRequestEnd;
            Ligue1BackEnd.LightSettingsRequestEnd   += Ligue1BackEnd_LightSettingsRequestEnd;
            Ligue1BackEnd.UserinfoRequestEnd        += Ligue1_UserinfoRequestEnd;
            Ligue1BackEnd.WordingRequestEnd         += Ligue1_WordingRequestEnd;
            Ligue1BackEnd.OptionCatalogRequestEnd   += Ligue1_OptionCatalogRequestEnd;
            Ligue1BackEnd.OptionSubscribeRequestEnd += Ligue1BackEnd_OptionSubscribeRequestEnd;
            Ligue1BackEnd.TeamsRequestEnd           += Ligue1_TeamsRequestEnd;
            Ligue1BackEnd.CalendarRequestEnd        += Ligue1_DayCalendarRequestEnd;
            //Ligue1BackEnd.WholeCalendarRequestEnd   += Ligue1_WholeCalendarRequestEnd;
            Ligue1BackEnd.NewsRequestEnd            += Ligue1_NewsRequestEnd;
            Ligue1BackEnd.MatchRequestEnd           += Ligue1_MatchRequestEnd;
            Ligue1BackEnd.AllMatchRequestEnd        += Ligue1_AllMatchRequestEnd;
            Ligue1BackEnd.VodRequestEnd             += Ligue1_VodRequestEnd;
            Ligue1BackEnd.ConnectionErrorRequestEnd += Ligue1_ConnectionErrorRequestEnd;
            Ligue1BackEnd.PushSettingsRequestEnd    += Ligue1_PushSettingsRequestEnd;

            NewsLigue1BackEnd = new Ligue1Enabler(AppConfig.Current.AmpConfig);
            NewsLigue1BackEnd.NewsRequestEnd += Ligue1_NewsContentRequestEnd;
            NewsLigue1BackEnd.ConnectionErrorRequestEnd += Ligue1_ConnectionErrorRequestEnd;

            CalendarLigue1BackEnd = new Ligue1Enabler(AppConfig.Current.AmpConfig);
            CalendarLigue1BackEnd.WholeCalendarRequestEnd += Ligue1_WholeCalendarRequestEnd;
            CalendarLigue1BackEnd.ConnectionErrorRequestEnd += Ligue1_ConnectionErrorRequestEnd;

            Messenger.Current.Register(App.ShutdownMessage, OnAppShutdown);

            PushNotificationsHandler h = PushNotificationsHandler.SharedInstance;

            MyInitinfoActionIsLoaded = false;
            MyDynamicInitActionIsLoaded = false;
        }


        /// <summary>
        /// Initiate a request to the enabler to get the latest Ligue1 data
        /// </summary>
        public void Initialize()
        {
            BackgroundActivityBeacon.Current.Hold();
            matchesCounter = 0;
            if (connectivityType == ConnectivityType.Unknown)
            {
                // wait for the connectivity type to be known
                Messenger.Current.Register(ConnectivityWatcher.CurrentTypeChangedMessage, OnConnectivityTypeChanged);
                return;
            }
            else
            {
                //Ligue1BackEnd.Initialize();
                Ligue1BackEnd.DynamicInitialize();
            }
        }

        public void OnConnectivityTypeChanged(object sender, string message, object param)
        {
            connectivityType = ConnectivityWatcher.CurrentType;

            //Ligue1BackEnd.Initialize();
            Ligue1BackEnd.DynamicInitialize();
            
        }

        public void RequestLightSettings()
        {
            BackgroundActivityBeacon.Current.Hold();
            Ligue1BackEnd.RequestLightSettings();
        }

        public void RequestDayNews()
        {
            BackgroundActivityBeacon.Current.Hold();
            Ligue1BackEnd.RequestDayNews(0);            //retrieve news titles
        }

        public void RequestUserInfo()
        {
            BackgroundActivityBeacon.Current.Hold();
            Ligue1BackEnd.RequestUserInfo();            //retrieve user info
        }

        public void RequestWording()
        {
            BackgroundActivityBeacon.Current.Hold();
            Ligue1BackEnd.RequestWording(); //retrieve popup wording
        }

        public void RequestOptionCatalog()
        {
            BackgroundActivityBeacon.Current.Hold();
            Ligue1BackEnd.RequestOptionCatalog();            //retrieve options catalog
        }

        public void RequestSubscribeOption(string optionId, string type)
        {
            BackgroundActivityBeacon.Current.Hold();
            Ligue1BackEnd.RequestSubscribeOption(optionId, type);     //subscribe to option            
        }

        public void RequestWholeCalendar()
        {
            BackgroundActivityBeacon.Current.Hold();
            if (MyWholeCalendarTimestamp.HasValue)
            {
                TimeSpan lifetime = TimeSpan.FromSeconds(300);
                DateTime timeout = MyWholeCalendarTimestamp.Value.Add(lifetime);
                if (timeout > DateTime.UtcNow)
                {
                    if (MyWholeCalendarAction != null)
                    {
                        if (WholeCalendarRequestEnd != null)
                        {
                            WholeCalendarRequestEnd(this, MyWholeCalendarAction);
                        }
                        BackgroundActivityBeacon.Current.Release();
                        return;
                    }
                }
            }
            
            CalendarLigue1BackEnd.RequestCalendar();
        }

        public void RequestCalendar(int day)
        {
            BackgroundActivityBeacon.Current.Hold();
            Ligue1BackEnd.RequestCalendar(new int[] {
                day
            });
        }

        public void RequestTeams()
        {
            BackgroundActivityBeacon.Current.Hold();
            Ligue1BackEnd.RequestTeams();               //retrieve teams ranking
        }

        public void RequestVoD()
        {
            BackgroundActivityBeacon.Current.Hold();
            Ligue1BackEnd.RequestVod();                 //retrieve vods
        }

        public void RequestMatch(int matchId)
        {
            BackgroundActivityBeacon.Current.Hold();
            Ligue1BackEnd.RequestMatch(matchId);        //retrieve match
        }

        public void RequestPushSettings(Ligue1Enabler.NotifSettingsAction action, Ligue1Enabler.NotifSettingsSubscription subscription, List<string> teamsId)
        {
            BackgroundActivityBeacon.Current.Hold();
           Ligue1BackEnd.RequestNotifSettings(action, subscription, teamsId);
        }

        public void RequestAllMatch(int matchId = 0, int eventId = 0)
        {
            BackgroundActivityBeacon.Current.Hold();
            Ligue1BackEnd.RequestAllMatch(matchId, eventId, true /*get_videos*/);
        }

        #region REQUESTEND FCT

        void Ligue1_WordingRequestEnd(object sender, WordingAction e)
        {
            this.MyWordingAction = e;

            if (WordingRequestEnd != null)
            {
                WordingRequestEnd(this, e);
            }
            MyWordingActionIsLoaded = true;
            BackgroundActivityBeacon.Current.Release();
        }

        void Ligue1BackEnd_DynamicInitRequestEnd(object sender, DynamicInitAction e)
        {
            this.MyDynamicInitAction = e;
            Ligue1BackEnd.Initialize();

            if (DynamicInitRequestEnd != null)
            {
                DynamicInitRequestEnd(this, e);
           }

            BackgroundActivityBeacon.Current.Release();

            MyDynamicInitActionIsLoaded = true;
        }

        void Ligue1BackEnd_LightSettingsRequestEnd(object sender, LightSettingsAction e)
        {
            this.MyLightSettingsAction = e;
            
            if (e.ManagePass == "0")
                MyManagerPass = false;
            else
                MyManagerPass = true;

            //fill data (TeamsInfo) into dictionary items
            RestoreTeamsInfo();
            RequestData();

            //launch timers
           // StartRefreshFreqLTimer();
           // StartRefreshFreqRTimer();
            MyInitinfoActionIsLoaded = true;
            MyLightSettingsActionIsLoaded = true;

            if (LightSettingsRequestEnd != null)
            {
                LightSettingsRequestEnd(this, e);
            }

            BackgroundActivityBeacon.Current.Release();

        }

        void Ligue1_InitinfoRequestEnd(object sender, InitinfoAction e)
        {
            this.MyInitinfoAction = e;
            int count = 0;
            bool lightsettings = false;
            bool wording = false;
            foreach (ParamInfo param in e.InitInfo.Param)
            {
                if (!MyParamInfo.ContainsKey(param.Value))
                {
                    count++;
                    MyParamInfo.Add(param.Value, param);
                    if (param.Name == "settingslight_id")
                    {
                        lightsettings = true;
                        this.RequestLightSettings();
                    }
                    else if (param.Name == "wording_id")
                    {
                        wording = true;
                        this.RequestWording();
                    }
                }
            }

            if(lightsettings == false)
            {
                MyInitinfoActionIsLoaded = true;
                MyLightSettingsActionIsLoaded = true;

                if (MyLightSettingsAction.ManagePass == "0")
                    MyManagerPass = false;
                else
                    MyManagerPass = true;

                RestoreTeamsInfo();
                RequestData();
            }
            if(wording == false)
                MyWordingActionIsLoaded = true;
            //this.MyDayInfo = e.Matches;
            //this.MyDayNewsInfo = e.DayNews;

            /*CurrentDay = Convert.ToInt32(e.Matches.Day); // used to be: e.Date.Day
            
            foreach(ParamInfo param in MyDayInfo.Params)
            {
                if(param.Status.Equals("hight_t"))
                {
                    CurrentTimer = param.Id;
                    break;
                }
            }

            refreshFreqRTimerInterval = TimeSpan.FromSeconds(CurrentTimer);
            refreshFreqLTimerInterval = TimeSpan.FromSeconds(CurrentTimer);
            refreshFreqLiveTimerInterval = TimeSpan.FromSeconds(CurrentTimer);

            /*if (e.Update != null)
            {
                if(e.Update.FreqR > FreqR)
                    FreqR = e.Update.FreqR;
                if (e.Update.FreqL > FreqL)
                    FreqR = e.Update.FreqR;
                if (e.Update.FreqLive > FreqLive)
                    FreqLive = e.Update.FreqLive;

                refreshFreqRTimerInterval = TimeSpan.FromSeconds(FreqR);
                refreshFreqLTimerInterval = TimeSpan.FromSeconds(FreqL);
                refreshFreqLiveTimerInterval = TimeSpan.FromSeconds(FreqLive);
            }

            if (MyInitinfo != null && !MyInitinfo.ContainsKey(MyInitinfoAction.InitInfo.Params.Id))
            {
                this.RequestLightSettings();
            }
            else
            {
                RequestData();
            }
            */

            

            //MyInitinfoActionIsLoaded = true;

            if (InitinfoRequestEnd != null)
            {
                InitinfoRequestEnd(this, e);
            }

            BackgroundActivityBeacon.Current.Release();
        }

        void Ligue1_UserinfoRequestEnd(object sender, UserinfoAction e)
        {
            this.MyUserinfoAction = e;
            
            this.RequestOptionCatalog();
            
            if(UserinfoRequestEnd != null)
            {
                UserinfoRequestEnd(this, e);
            }

            BackgroundActivityBeacon.Current.Release();
        }

        /// <summary>
        /// handles the response data from the options/catalog request to retrive the options list that can be subscribed by the use 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        void Ligue1_OptionCatalogRequestEnd(object sender, OptionCatalogAction e)
        {
            this.MyOptionCatalogAction = e;

            if (OptionCatalogRequestEnd != null)
            {
                OptionCatalogRequestEnd(this, e);
            }

            BackgroundActivityBeacon.Current.Release();
        }

        /// <summary>
        /// handles the response data from the subscribe request 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        void Ligue1BackEnd_OptionSubscribeRequestEnd(object sender, OptionSubscribeAction e)
        {
            this.MyOptionSubscribeAction = e;

            if (OptionSubscribeRequestEnd != null)
            {
                OptionSubscribeRequestEnd(this, e);
            }

            BackgroundActivityBeacon.Current.Release();
        }

        void Ligue1_TeamsRequestEnd(object sender, TeamsAction e)
        {
            this.MyTeamsAction = e;

            //fill data (TeamsInfo) previously retrieved with initinfo
            PopulateTeamsInfo();

            if (TeamsRequestEnd != null)
            {
                TeamsRequestEnd(this, e);
            }

            BackgroundActivityBeacon.Current.Release();
        }

        void Ligue1_DayCalendarRequestEnd(object sender, CalendarAction e)
        {
            this.MyDayCalendarAction = e;
            if (DayCalendarRequestEnd != null)
            {
                DayCalendarRequestEnd(this, e);
            }

            BackgroundActivityBeacon.Current.Release();
        }

        void Ligue1_WholeCalendarRequestEnd(object sender, WholeCalendarAction e)
        {
            MyWholeCalendarTimestamp = DateTime.UtcNow;
            this.MyWholeCalendarAction = e;
            WholeCalendarUpdated = true;
            if (WholeCalendarRequestEnd != null)
            {
                WholeCalendarRequestEnd(this, e);
            }

            BackgroundActivityBeacon.Current.Release();
        }

        void Ligue1_NewsRequestEnd(object sender, DaynewsAction e)
        {
            this.MyDayNewsInfo = e.DayNews;
            //List<int> ids = null;
            /*
            if (e.DayNews != null &&
               e.DayNews.NewsList != null &&
               e.DayNews.NewsList.Count > 0)
            {
                e.DayNews.NewsList.Reverse();
            }*/
            this.MyDayNewsAction = e;
            if (DayNewsRequestEnd != null)
            {
                DayNewsRequestEnd(this, e);
            }

           /* if (e.DayNews != null &&
                e.DayNews.NewsList != null &&
                e.DayNews.NewsList.Count > 0)
            {
                ids = new List<int>();

                foreach (NewsInfo info in e.DayNews.NewsList)
                {
                    ids.Insert(0, info.Id);
                }

                //NewsLigue1BackEnd.RequestDayNews(ids.ToArray());
                ids = null;
            }
            else
            {
                if (DayNewsRequestEnd != null)
                {
                    DayNewsRequestEnd(this, e);
                }
            }*/

            BackgroundActivityBeacon.Current.Release();
        }

        void Ligue1_NewsContentRequestEnd(object sender, DaynewsAction e)
        {
            /*
            if (e.DayNews != null &&
                e.DayNews.NewsList != null &&
                e.DayNews.NewsList.Count > 0)
            {
                e.DayNews.NewsList.Reverse();
            }*/
            this.MyDayNewsAction = e;
            if (DayNewsRequestEnd != null)
            {
                DayNewsRequestEnd(this, e);
            }
            BackgroundActivityBeacon.Current.Release();
        }

        void Ligue1_MatchRequestEnd(object sender, MatchAction e)
        {
            if (MatchRequestEnd != null)
            {
                MatchRequestEnd(this, e);
            }
            BackgroundActivityBeacon.Current.Release();
        }

        void Ligue1_AllMatchRequestEnd(object sender, AllMatchAction e)
        {
            if (matchesCounter == 0)
            {
                if (myInitInfoHighlight != null)
                    myInitInfoHighlight.Clear();

                foreach (MatchInfo match in e.Matches.Matches)
                {
                    foreach (MatchEventInfo ev in match.Events)
                    {
                        if (myInitInfoHighlight.ContainsKey(ev.Id) == false)
                            myInitInfoHighlight.Add(ev.Id, ev);
                    }

                }
                matchesCounter = 1;
            }

            this.MyDayInfo = e.Matches;
            this.MyAllMatchAction = e;
            CurrentDay = Convert.ToInt32(e.Matches.Day); // used to be: e.Date.Day

            foreach (ParamInfo param in e.MatchInfo.Param)
            {
                if (!MyParamInfo.ContainsKey(param.Value))
                {
                    MyParamInfo.Add(param.Value, param);
                }

            }
            foreach (ParamInfo param in e.MatchInfo.Param)
            {
                if (param.Name.Equals("hight_t"))
                {
                    CurrentTimer = param.Value;
                    break;
                }
            }

            refreshFreqRTimerInterval = TimeSpan.FromSeconds(CurrentTimer);
            refreshFreqLTimerInterval = TimeSpan.FromSeconds(CurrentTimer);
            //refreshFreqLiveTimerInterval = TimeSpan.FromSeconds(CurrentTimer);

            //launch timers
            StartRefreshFreqLTimer();
            DispatcherManager timerHandler = DispatcherManager.Current;
            if (!timerHandler.RefreshFreqLiveTimerIsStarted())
            {
                StartRefreshFreqRTimer();
            }            

            if (AllMatchRequestEnd != null)
            {
                AllMatchRequestEnd(this, e);
            }
            BackgroundActivityBeacon.Current.Release();
        }

        void Ligue1_VodRequestEnd(object sender, VoDAction e)
        {
            this.MyVoDAction = e;
            if (VodRequestEnd != null)
            {
                VodRequestEnd(this, e);
            }
            BackgroundActivityBeacon.Current.Release();
        }

        void Ligue1_ConnectionErrorRequestEnd(object sender, BackgroundRequestCompletedEventArgs e)
        {
            if (ConnectionErrorRequestEnd != null)
            {
                ConnectionErrorRequestEnd(this, e);
            }
            BackgroundActivityBeacon.Current.Release();
        }

        void Ligue1_PushSettingsRequestEnd(object sender, PushSettingsAction e)
        {
            this.MyPushSettingsAction = e;
            if (PushSettingsRequestEnd != null)
            {
                PushSettingsRequestEnd(this, e);
            }
            BackgroundActivityBeacon.Current.Release();
        }

        #endregion
        /// <summary>
        /// Starts a new refresh timer, stopping the current one if it exists.
        /// </summary>
        public void StartRefreshFreqRTimer()
        {
            if (RefreshFreqRTimer != null)
            {
                RefreshFreqRTimer.Stop();
                RefreshFreqRTimer = null;
            }
            RefreshFreqRTimer = new DispatcherTimer();
            RefreshFreqRTimer.Interval = refreshFreqRTimerInterval;
            RefreshFreqRTimer.Tick += new EventHandler(OnRefreshFreqRTimerTick);
            RefreshFreqRTimer.Start();
        }

        private void StartRefreshFreqLTimer()
        {
            if (RefreshFreqLTimer != null)
            {
                RefreshFreqLTimer.Stop();
                RefreshFreqLTimer = null;
            }
            RefreshFreqLTimer = new DispatcherTimer();
            RefreshFreqLTimer.Interval = refreshFreqLTimerInterval;
            RefreshFreqLTimer.Tick += new EventHandler(OnRefreshFreqLTimerTick);
            RefreshFreqLTimer.Start();
        }
        /*
        public void StartRefreshFreqLiveTimer()
        {
            if (RefreshFreqLiveTimer != null)
            {
                RefreshFreqLiveTimer.Stop();
                RefreshFreqLiveTimer = null;
            }
            RefreshFreqLiveTimer = new DispatcherTimer();
            RefreshFreqLiveTimer.Interval = refreshFreqLiveTimerInterval;
            RefreshFreqLiveTimer.Tick += new EventHandler(OnRefreshFreqLiveTimerTick);
            RefreshFreqLiveTimer.Start();
        }*/

        public void StopRefreshFreqRTimer()
        {
            if (RefreshFreqRTimer != null)
            {
                RefreshFreqRTimer.Stop();
                RefreshFreqRTimer = null;
            }
        }

        public void StopRefreshFreqLiveTimer()
        {
            if (RefreshFreqLiveTimer != null)
            {
                RefreshFreqLiveTimer.Stop();
                RefreshFreqLiveTimer = null;
            }
        }
        /*
        public bool RefreshFreqLiveTimerIsStarted()
        {
            if (RefreshFreqLiveTimer != null)
            {
                return RefreshFreqLiveTimer.IsEnabled;
            }
            else
            {
                return false;
            }
        }*/

        /// <summary>
        /// Called when the refresh freqR timer ticks.
        /// </summary>
        private void OnRefreshFreqRTimerTick(object sender, EventArgs e)
        {
            DispatcherTimer timer = sender as DispatcherTimer;
            if (timer == null)
            {
                return;
            }
            if (timer != RefreshFreqRTimer)
            {
                return;
            }
            this.RequestAllMatch();
        }

        /// <summary>
        /// Called when the refresh freqL timer ticks.
        /// </summary>
        private void OnRefreshFreqLTimerTick(object sender, EventArgs e)
        {
            DispatcherTimer timer = sender as DispatcherTimer;
            if (timer == null)
            {
                return;
            }
            if (timer != RefreshFreqLTimer)
            {
                return;
            }
            this.RequestDayNews();
            this.RequestCalendar(CurrentDay);
            this.RequestTeams();
            this.RequestVoD();
        }

        /// <summary>
        /// Called when the refresh freqLive timer ticks.
        /// </summary>
        /*
        private void OnRefreshFreqLiveTimerTick(object sender, EventArgs e)
        {
            DispatcherTimer timer = sender as DispatcherTimer;
            if (timer == null)
            {
                return;
            }
            if (timer != RefreshFreqLiveTimer)
            {
                return;
            }
            //prevent exception in RequestAllMatch InvalidArgumentException
            if (CurrentMatchId == 0 && LastEventId != 0)
            {
                LastEventId = 0;
            }
            if (CurrentMatchId != 0 && LastEventId == 0)
            {
                CurrentMatchId = 0;
            }
            this.RequestAllMatch(CurrentMatchId, LastEventId);
        }*/

        /// <summary>
        /// Listens to <see cref="App.OnAppShutdown"/>.
        /// </summary>
        public void OnAppShutdown(object sender, string message, object param)
        {
            Save();
        }

        /// <summary>
        /// Wrapper for saving the data of the object instance in the datastore
        /// </summary>
        public void Save()
        {
            DependencyContainer.Resolve<IDataStore>().Save(DataStoreKey, this);
        }

        /// <summary>
        /// Create a new instance of the class from the data serialized in the datastore of the application
        /// </summary>
        /// <returns>a new instance of the class</returns>
        public static Ligue1 Restore()
        {
            try
            {
                Ligue1 ret = DependencyContainer.Resolve<IDataStore>().Load(DataStoreKey, typeof(Ligue1)) as Ligue1;
                if (ret == null)
                {
                    ret = new Ligue1();
                }
                return ret;
            }
            catch (Exception)
            {
                return new Ligue1();
            }
        }

        public void RequestData()
        {
            //Launch requests after initinfo
            //Launch requests in section appareance order
            this.RequestUserInfo();
            //this.RequestWording();
            this.RequestCalendar(CurrentDay);   //retrieve calendar of the current day
            this.RequestTeams();                //retrieve the ranking & the stats of each team
            this.RequestVoD();                  //retrieve vodss
        }


        /// <summary>
        /// Retrieve logos from Teams Info
        /// </summary>
        public void RestoreTeamsInfo()
        {
            if (MyTeamsInfo != null)
            {
                MyTeamsInfo.Clear();
            }
            
            if (MyLightSettingsAction == null || MyLightSettingsAction.Teams == null || MyLightSettingsAction.Teams.Teams == null)
            {
                return;
            }
            foreach (TeamInfo teamInfo in MyLightSettingsAction.Teams.Teams)
            {
                if (!MyTeamsInfo.ContainsKey(teamInfo.Id))
                {
                    MyTeamsInfo.Add(teamInfo.Id, teamInfo);
                }
            }
        }

        public void RestoreParamInfo()
        {
            if (MyParamInfo != null)
            {
                MyParamInfo.Clear();
            }

            if (MyAllMatchAction == null || MyInitinfoAction == null)
            {
                return;
            }
            foreach (ParamInfo paramInfo in MyInitinfoAction.InitInfo.Param)
            {
                if (!MyParamInfo.ContainsKey(paramInfo.Value))
                {
                    MyParamInfo.Add(paramInfo.Value, paramInfo);
                }
            }
            foreach (ParamInfo paramInfo in MyAllMatchAction.MatchInfo.Param)
            {
                if (!MyParamInfo.ContainsKey(paramInfo.Value))
                {
                    MyParamInfo.Add(paramInfo.Value, paramInfo);
                }
            }
        }
        

        /// <summary>
        /// Retrieve stats from Teams Info
        /// </summary>
        public void PopulateTeamsInfo()
        {
            if (MyTeamsAction == null || MyTeamsAction.Teams == null || MyTeamsAction.Teams.Teams == null)
            {
                return;
            }
            foreach (TeamInfo teamInfo in MyTeamsAction.Teams.Teams)
            {
                if(MyTeamsInfo.ContainsKey(teamInfo.Id))
                    MyTeamsInfo[teamInfo.Id].Stats = teamInfo.Stats;
            }
        }

        /// <summary>
        /// Opens the specified URL retrieved from AMP.
        /// </summary>
        public void OpenUrl(string url)
        {
            try
            {
                if (url.StartsWith(MarketplaceAppIdUrlPrefix))
                {
                    string applicationId = url.Substring(MarketplaceAppIdUrlPrefix.Length).Trim();
                    MarketplaceDetailTask task = new MarketplaceDetailTask();
                    task.ContentType = MarketplaceContentType.Applications;
                    task.ContentIdentifier = applicationId;
                    task.Show();
                }
                else
                {
                    new WebBrowserTask { Uri = new Uri(url, UriKind.Absolute) }.Show();
                }
            }
            catch (Exception error)
            {
                Logger.LogException(error, "Failed to open AMP URL '{0}'!", url);
            }
        }
    }

    /// <summary>
    /// Stores the Configuration Parameters for the AMP Enabler
    /// </summary>
    public class AmpClientConfig
    {
        public AmpClientConfig()
        {
        }

        public string AppVersion
        {
            get;
            set;
        }

        public string NominalServerUri
        {
            get;
            set;
        }

        public string CellularOnlyServerUri
        {
            get;
            set;
        }

        public string NominalPrefixUri
        {
            get;
            set;
        }

        public string CellularOnlyPrefixUri
        {
            get;
            set;
        }

        public string ForbiddenPrefixUri
        {
            get;
            set;
        }

        public string DeprecatedPrefixUri
        {
            get;
            set;
        }

        public string AllMatchString
        {
            get;
            set;
        }

        public string CalendarString
        {
            get;
            set;
        }

        public string DayNewsString
        {
            get;
            set;
        }

        public string InitInfoString
        {
            get;
            set;
        }

        public string DynamicInit
        {
            get;
            set;
        }

        public string LightSettingsString
        {
            get;
            set;
        }

        public string MatchString
        {
            get;
            set;
        }

        public string UserInfoString
        {
            get;
            set;
        }

        public string WordingString
        {
            get;
            set;
        }

        public string OptionCatalogString
        {
            get;
            set;
        }

        public string OptionSubscribeString
        {
            get;
            set;
        }

        public string TeamsString
        {
            get;
            set;
        }

        public string VodString
        {
            get;
            set;
        }

        public string NotifSettings
        {
            get;
            set;
        }
    }
}

