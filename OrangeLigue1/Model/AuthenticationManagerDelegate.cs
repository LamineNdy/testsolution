// -------------------------------  Copyright ---------------------------------
//	This software has been developped by France Telecom, FT/RD/RD/MAPS/DVC/MSF
//
//	Copyright (c) France Telecom 2009-2010
//
// COPYRIGHT    : This file is the property of FRANCE TELECOM.
//                It cannot be copied, used, or modified without obtaining
//                an authorization from the authors or a mandated
//                member of FRANCE TELECOM.
//                If such an authorization is provided, any modified version or
//                copy of the software has to contain this header.
//
// WARRANTIES   : This software is made available by the authors in the  hope
//                that it will be useful, but without any warranty.
//                France Telecom is not liable for any consequence related to the
//                use of the provided software.
//
// AUTHORS      : France Telecom  / RD / MAPS / DVC / MSF
// ----------------------------------------------------------------------------
using System;
using System.Collections.Generic;
using System.Xml.Serialization;
using AuthenticationManager;
using OrangeLigue1.Storage;
using OrangeLigue1.Tools;

namespace OrangeLigue1.Model
{
    /// <summary>
    /// The delegate for the authentication manager.
    /// </summary>
    public class AuthenticationManagerDelegate : IOLAuthenticationManagerDelegate
    {
        private const string DataStoreKey = "AuthenticationManagerDelegate";
        private const string DefaultToken = "";

        private static AuthenticationManagerDelegate _singleton = null;

        /// <summary>
        /// The current authentication manager delegate.
        /// </summary>
        public static AuthenticationManagerDelegate Current
        {
            get
            {
                if (_singleton == null)
                {
                    _singleton = Load();
                }
                return _singleton;
            }
        }

        /// <summary>
        /// Loads the authentication manager delegate from the data store, creating a new one if needed.
        /// </summary>
        private static AuthenticationManagerDelegate Load()
        {
            try
            {
                IDataStore dataStore = DependencyContainer.Resolve<IDataStore>();
                AuthenticationManagerDelegate delegateObject = dataStore.Load(DataStoreKey, typeof(AuthenticationManagerDelegate))
                    as AuthenticationManagerDelegate;
                if (delegateObject == null)
                {
                    delegateObject = new AuthenticationManagerDelegate();
                }
                else
                {
                    delegateObject.NeedsSaving = false;
                }
                return delegateObject;
            }
            catch (Exception error)
            {
                Logger.LogException(error, "Error while loading authentication manager delegate from data store!");
                return new AuthenticationManagerDelegate();
            }
        }

        private string _token = DefaultToken;
        private bool _isInitialized = false;

        /// <summary>
        /// Initializes a new <see cref="AuthenticationManagerDelegate"/> instance.
        /// You should not have to call this constructor directly; use <see cref="AuthenticationManagerDelegate.Current"/> instead.
        /// </summary>
        public AuthenticationManagerDelegate()
        {
            NeedsSaving = true;
            Messenger.Current.Register(App.ShutdownMessage, OnAppShutdown);
        }

        /// <summary>
        /// The current value of the authentication token.
        /// </summary>
        public string Token
        {
            get
            {
                return _token;
            }
            set
            {
                if (value == null)
                {
                    value = DefaultToken;
                }
                if (_token == value)
                {
                    return;
                }
                _token = value;
                NeedsSaving = true;
            }
        }

        /// <summary>
        /// Set to <c>true</c> once the delegate object has been modified and needs saving.
        /// </summary>
        [XmlIgnore]
        private bool NeedsSaving
        {
            get;
            set;
        }

        /// <summary>
        /// Initialize the authentication manager, if not already initialized.
        /// </summary>
        public void Initialize()
        {
            if (_isInitialized)
            {
                return;
            }

            AppConfig.AuthenticationManagerConfig config = AppConfig.Current.AuthenticationManager;
            if (config == null)
            {
                return;
            }
            if (string.IsNullOrEmpty(config.ApplicationId) && string.IsNullOrEmpty(config.ApplicationSecret))
            {
                return;
            }

            try
            {
                Dictionary<string, string> initParams = new Dictionary<string, string>();
                initParams[OLAuthenticationManager.TOKEN_APPLICATION_ID] = config.ApplicationId;
                initParams[OLAuthenticationManager.TOKEN_APPLICATION_SECRET] = config.ApplicationSecret;
                if (!OLAuthenticationManager.defaultAuthenticationManager().initialize(
                    this, OLAuthenticationManager.Mode.uSSOG4, initParams))
                {
                    Logger.Log("Failed to initialize authentication manager!");
                    return;
                }
                _isInitialized = true;
            }
            catch (Exception error)
            {
                Logger.LogException(error, "Error while initializing authentication manager!");
                _isInitialized = false;
            }
        }

        /// <summary>
        /// Shuts down the authentication manager, if initialized.
        /// </summary>
        public void Shutdown()
        {
            if (!_isInitialized)
            {
                return;
            }
            try
            {
                if (!OLAuthenticationManager.defaultAuthenticationManager().shutdown())
                {
                    Logger.Log("Failed to shut down authentication manager!");
                }
                if (!OLAuthenticationManager.releaseAuthenticationManager())
                {
                    Logger.Log("Failed to release authentication manager!");
                }
            }
            catch (Exception error)
            {
                Logger.LogException(error, "Error while shutting down authentication manager!");
            }
            _isInitialized = false;

        }

        /// <summary>
        /// Implements <see cref="IOLAuthenticationManagerDelegate.stateChanged"/>.
        /// </summary>
        public void stateChanged(OLAuthenticationManager.State oldState, OLAuthenticationManager.State newState)
        {
        }

        /// <summary>
        /// Implements <see cref="IOLAuthenticationManagerDelegate.configChanged"/>.
        /// </summary>
        public void configChanged(string configKey, string configValue)
        {
            if (configKey == OLAuthenticationManager.TOKEN_LAST_TOKEN)
            {
                Token = (configValue != null ? configValue : "");
            }
        }

        /// <summary>
        /// Listens to <see cref="App.ShutdownMessage"/>.
        /// </summary>
        public void OnAppShutdown(object sender, string message, object param)
        {
            Save();
        }

        /// <summary>
        /// Saves the delegate object to the data store.
        /// </summary>
        private void Save()
        {
            if (!NeedsSaving)
            {
                return;
            }
            try
            {
                IDataStore dataStore = DependencyContainer.Resolve<IDataStore>();
                dataStore.Save(DataStoreKey, this);
                NeedsSaving = false;
            }
            catch (Exception error)
            {
                Logger.LogException(error, "Error while saving authentication manager delegate to data store!");
            }
        }
    }
}
