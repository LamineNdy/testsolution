// -------------------------------  Copyright ---------------------------------
//	This software has been developped by France Telecom, FT/RD/RD/MAPS/DVC/MSF
//
//	Copyright (c) France Telecom 2009-2010
//
// COPYRIGHT    : This file is the property of FRANCE TELECOM.
//                It cannot be copied, used, or modified without obtaining
//                an authorization from the authors or a mandated
//                member of FRANCE TELECOM.
//                If such an authorization is provided, any modified version or
//                copy of the software has to contain this header.
//
// WARRANTIES   : This software is made available by the authors in the  hope
//                that it will be useful, but without any warranty.
//                France Telecom is not liable for any consequence related to the
//                use of the provided software.
//
// AUTHORS      : France Telecom  / RD / MAPS / DVC / MSF
// ----------------------------------------------------------------------------
using System;
using System.Collections.Generic;
using System.Threading;
using System.Windows;
using System.Windows.Controls;
using OrangeLigue1.Tools;
using Microsoft.Phone.Controls;

namespace OrangeLigue1.Model
{
    public class BackgroundActivityBeacon
    {
        /// <summary>
        /// The message sent through <see cref="Messenger"/> when the background activity beacon is started.
        /// </summary>
        public const string StartedMessage = "BackgroundActivityBeacon.StartedMessage";

        /// <summary>
        /// The message sent through <see cref="Messenger"/> when the background activity beacon is stopped.
        /// </summary>
        public const string StoppedMessage = "BackgroundActivityBeacon.StoppedMessage";

        private static BackgroundActivityBeacon _singleton = null;

        /// <summary>
        /// The current background activity beacon.
        /// </summary>
        public static BackgroundActivityBeacon Current
        {
            get
            {
                if (_singleton == null)
                {
                    _singleton = new BackgroundActivityBeacon();
                }
                return _singleton;
            }
        }

        private int _counter = 0;
        private readonly List<WeakReference> _listeners = new List<WeakReference>();
        private readonly object _listenersLock = new object();
        private readonly Dictionary<WeakReference, OwnerStatus> _owners = new Dictionary<WeakReference, OwnerStatus>();
        private readonly object _ownersLock = new object();

        /// <summary>
        /// Initializes a new <see cref="BackgroundActivityBeacon"/> instance.
        /// </summary>
        private BackgroundActivityBeacon()
        {
        }

        /// <summary>
        /// Indicates whether the beacon is started or not.
        /// </summary>
        public bool IsStarted
        {
            get
            {
                return (_counter > 0);
            }
        }

        /// <summary>
        /// Signals the beacon that some background activity has started.
        /// </summary>
        /// <remarks>
        /// The caller is responsible for calling <see cref="Release"/> once the background activity has stopped.
        /// </remarks>
        public void Hold()
        {
            Hold(null);
        }

        /// <summary>
        /// Signals the beacon that some background activity has started for the following owner.
        /// </summary>
        public void Hold(object owner)
        {
            if (owner != null)
            {
                lock (_ownersLock)
                {
                    CleanupOwners();
                    WeakReference ownerRef = null;
                    foreach (WeakReference reference in _owners.Keys)
                    {
                        if (reference.IsAlive && reference.Target == owner)
                        {
                            ownerRef = reference;
                            break;
                        }
                    }
                    if (ownerRef == null)
                    {
                        ownerRef = new WeakReference(owner);
                    }
                    if (_owners.ContainsKey(ownerRef))
                    {
                        _owners[ownerRef].activityCounter += 1;
                    }
                    else
                    {
                        _owners[ownerRef] = new OwnerStatus() { isEnabled = true, activityCounter = 1 };
                    }
                    if (!_owners[ownerRef].isEnabled)
                    {
                        return;
                    }
                }
            }

            AddToCounter(1);
        }

        /// <summary>
        /// Signals the beacon that a background activity has stopped.
        /// </summary>
        /// <remarks>
        /// The start of the background activity must previously have been signalled with <see cref="Hold"/>.
        /// </remarks>
        public void Release()
        {
            Release(null);
        }

        /// <summary>
        /// Signals the beacon that a background activity for the specified owner has stopped.
        /// </summary>
        public void Release(object owner)
        {
            if (owner != null)
            {
                lock (_ownersLock)
                {
                    WeakReference ownerRef = null;
                    foreach (WeakReference reference in _owners.Keys)
                    {
                        if (reference.IsAlive && reference.Target == owner)
                        {
                            ownerRef = reference;
                            break;
                        }
                    }
                    if (ownerRef != null)
                    {
                        _owners[ownerRef].activityCounter -= 1;
                        if (!_owners[ownerRef].isEnabled)
                        {
                            return;
                        }
                        CleanupOwners();
                    }
                }
            }

            SubstractFromCounter(1);
        }

        /// <summary>
        /// Ignores all background activity for the specified owner.
        /// </summary>
        public void DisableActivityForOwner(object owner)
        {
            if (owner == null)
            {
                return;
            }
            lock (_ownersLock)
            {
                foreach (WeakReference reference in _owners.Keys)
                {
                    if (reference.IsAlive && reference.Target == owner)
                    {
                        if (_owners[reference].isEnabled)
                        {
                            _owners[reference].isEnabled = false;
                            SubstractFromCounter(_owners[reference].activityCounter);
                        }
                        return;
                    }
                }
            }
        }

        /// <summary>
        /// Ensure that background activity for the specified owner is taken into account.
        /// </summary>
        public void EnableActivityForOwner(object owner)
        {
            if (owner == null)
            {
                return;
            }
            lock (_ownersLock)
            {
                foreach (WeakReference reference in _owners.Keys)
                {
                    if (reference.IsAlive && reference.Target == owner)
                    {
                        if (!_owners[reference].isEnabled)
                        {
                            _owners[reference].isEnabled = true;
                            AddToCounter(_owners[reference].activityCounter);
                        }
                        return;
                    }
                }
            }
        }

        private void AddToCounter(int value)
        {
            if (value == 0)
            {
                return;
            }
            int newCounterValue = Interlocked.Add(ref _counter, value);
            if (newCounterValue - value <= 0 && newCounterValue >= 1)
            {
                SendStartedMessage();
            }
        }

        private void SubstractFromCounter(int value)
        {
            if (value == 0)
            {
                return;
            }
            int newCounterValue = Interlocked.Add(ref _counter, -value);
            if (newCounterValue + value >= 0 && newCounterValue <= 0)
            {
                SendStoppedMessage();
            }
        }

        /// <summary>
        /// Sends the <see cref="StartedMessage"/>.
        /// </summary>
        private void SendStartedMessage()
        {
            lock (_listenersLock)
            {
                CleanupListeners();
                foreach (WeakReference listenerRef in _listeners)
                {
                    if (!listenerRef.IsAlive)
                    {
                        continue;
                    }
                    if (listenerRef.Target is PerformanceProgressBar)
                    {
                        PerformanceProgressBar progressBar = (PerformanceProgressBar)(listenerRef.Target);
                        progressBar.Dispatcher.BeginInvoke(() => { progressBar.Visibility = Visibility.Visible; });
                    }
                }
            }

            Deployment.Current.Dispatcher.BeginInvoke(() =>
            {
                Messenger.Current.Send(this, StartedMessage);
            });
        }

        /// <summary>
        /// Sends the <see cref="StoppedMessage"/>.
        /// </summary>
        private void SendStoppedMessage()
        {
            lock (_listenersLock)
            {
                CleanupListeners();
                foreach (WeakReference listenerRef in _listeners)
                {
                    if (!listenerRef.IsAlive)
                    {
                        continue;
                    }
                    if (listenerRef.Target is PerformanceProgressBar)
                    {
                        PerformanceProgressBar progressBar = (PerformanceProgressBar)(listenerRef.Target);
                        progressBar.Dispatcher.BeginInvoke(() => { progressBar.Visibility = Visibility.Collapsed; });
                    }
                }
            }

            Deployment.Current.Dispatcher.BeginInvoke(() =>
            {
                Messenger.Current.Send(this, StoppedMessage);
            });
        }

        /// <summary>
        /// Registers the specified progress bar so that its visibility is tied to the status of the beacon.
        /// </summary>
        /// <remarks>
        /// The progress bar is registered as a weak reference.
        /// </remarks>
        public void RegisterProgressBar(PerformanceProgressBar progressBar)
        {
            if (progressBar == null)
            {
                return;
            }
            lock (_listenersLock)
            {
                foreach (WeakReference listenerRef in _listeners)
                {
                    if (listenerRef.IsAlive && listenerRef.Target == progressBar)
                    {
                        return;
                    }
                }
                _listeners.Add(new WeakReference(progressBar));
            }
            progressBar.Dispatcher.BeginInvoke(() =>
            {
                if (IsStarted)
                {
                    progressBar.IsIndeterminate = true;
                    progressBar.Visibility = Visibility.Visible;
                }
                else
                {
                    progressBar.Visibility = Visibility.Collapsed;
                    progressBar.IsIndeterminate = false;
                }
            });
        }

        /// <summary>
        /// Unregisters the specified listener object.
        /// </summary>
        public void Unregister(object listener)
        {
            lock (_listenersLock)
            {
                WeakReference listenerReference = null;
                foreach (WeakReference reference in _listeners)
                {
                    if (reference.IsAlive && reference.Target == listener)
                    {
                        listenerReference = reference;
                        break;
                    }
                }
                if (listenerReference != null)
                {
                    _listeners.Remove(listenerReference);
                }
            }
        }

        private void CleanupListeners()
        {
            lock (_listenersLock)
            {
                List<WeakReference> deadReferences = null;
                foreach (WeakReference reference in _listeners)
                {
                    if (!reference.IsAlive)
                    {
                        if (deadReferences == null)
                        {
                            deadReferences = new List<WeakReference>();
                        }
                        deadReferences.Add(reference);
                        continue;
                    }
                }
                if (deadReferences == null)
                {
                    return;
                }
                foreach (WeakReference deadReference in deadReferences)
                {
                    _listeners.Remove(deadReference);
                }
            }
        }

        private void CleanupOwners()
        {
            lock (_ownersLock)
            {
                List<WeakReference> deadReferences = null;
                foreach (WeakReference reference in _owners.Keys)
                {
                    if (!reference.IsAlive)
                    {
                        if (deadReferences == null)
                        {
                            deadReferences = new List<WeakReference>();
                        }
                        deadReferences.Add(reference);
                    }
                }
                if (deadReferences == null)
                {
                    return;
                }
                foreach (WeakReference deadReference in deadReferences)
                {
                    if (_owners[deadReference].isEnabled)
                    {
                        SubstractFromCounter(_owners[deadReference].activityCounter);
                    }
                    _owners.Remove(deadReference);
                }
            }
        }

        private class OwnerStatus
        {
            public bool isEnabled;
            public int activityCounter;
        }
    }
}

