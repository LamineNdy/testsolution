// -------------------------------  Copyright ---------------------------------
//	This software has been developped by France Telecom, FT/RD/RD/MAPS/DVC/MSF
//
//	Copyright (c) France Telecom 2009-2010
//
// COPYRIGHT    : This file is the property of FRANCE TELECOM.
//                It cannot be copied, used, or modified without obtaining
//                an authorization from the authors or a mandated
//                member of FRANCE TELECOM.
//                If such an authorization is provided, any modified version or
//                copy of the software has to contain this header.
//
// WARRANTIES   : This software is made available by the authors in the  hope
//                that it will be useful, but without any warranty.
//                France Telecom is not liable for any consequence related to the
//                use of the provided software.
//
// AUTHORS      : France Telecom  / RD / MAPS / DVC / MSF
// ----------------------------------------------------------------------------
using System;
using Microsoft.Phone.Controls;
using OrangeLigue1.Tools;

namespace OrangeLigue1
{
    /// <summary>
    /// The interface implemented by page navigators.
    /// </summary>
    public interface IPageNavigator
    {
        /// <summary>
        /// Navigates to the page at the specified URI.
        /// Does nothing if <paramref name="pageUri"/> is <c>null</c>.
        /// </summary>
        void GoTo(Uri pageUri);

        /// <summary>
        /// Navigates back in the page stack.
        /// Does nothing if there is no previous page.
        /// </summary>
        void GoBack();
    }

    /// <summary>
    /// Provides for navigating the page stack.
    /// </summary>
    public class Navigator
    {
        private static IPageNavigator PageNavigator
        {
            get
            {
                return DependencyContainer.Resolve<IPageNavigator>();
            }
        }

        /// <summary>
        /// Navigates to the page at the specified URI.
        /// Does nothing if <paramref name="pageUri"/> is <c>null</c>.
        /// </summary>
        public static void GoToPage(Uri pageUri)
        {
            PageNavigator.GoTo(pageUri);
        }

        /// <summary>
        /// Navigates back in the page stack.
        /// Does nothing if there is no previous page.
        /// </summary>
        public static void GoBack()
        {
            PageNavigator.GoBack();
        }
    }

    /// <summary>
    /// Implements an <see cref="IPageNavigator"/> on top of a PhoneApplicationFrame.
    /// </summary>
    public class PhonePageNavigator : IPageNavigator
    {
        public PhonePageNavigator()
        {
        }

        public PhoneApplicationFrame Frame
        {
            get;
            set;
        }

        public void GoTo(Uri pageUri)
        {
            if (pageUri == null)
            {
                return;
            }
            if (Frame == null)
            {
                return;
            }
            Frame.Navigate(pageUri);
        }

        public void GoBack()
        {
            if (Frame == null || Frame.CanGoBack == false)
            {
                return;
            }
            Frame.GoBack();
        }
    }

    /// <summary>
    /// Implements a dummy <see cref="IPageNavigator"/> that does absolutely nothing.
    /// </summary>
    public class DummyPageNavigator : IPageNavigator
    {
        public DummyPageNavigator()
        {
        }

        public void GoTo(Uri pageUri)
        {
        }

        public void GoBack()
        {
        }
    }
}

