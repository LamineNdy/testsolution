// -------------------------------  Copyright ---------------------------------
//	This software has been developped by France Telecom, FT/RD/RD/MAPS/DVC/MSF
//
//	Copyright (c) France Telecom 2009-2010
//
// COPYRIGHT    : This file is the property of FRANCE TELECOM.
//                It cannot be copied, used, or modified without obtaining
//                an authorization from the authors or a mandated
//                member of FRANCE TELECOM.
//                If such an authorization is provided, any modified version or
//                copy of the software has to contain this header.
//
// WARRANTIES   : This software is made available by the authors in the  hope
//                that it will be useful, but without any warranty.
//                France Telecom is not liable for any consequence related to the
//                use of the provided software.
//
// AUTHORS      : France Telecom  / RD / MAPS / DVC / MSF
// ----------------------------------------------------------------------------
using System;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using Orange.SDK;
using Orange.SDK.Enablers.Ligue1;
using Orange.SDK.Tools;
using OrangeLigue1.Model;
using System.Collections.ObjectModel;
using System.Windows.Threading;
using OrangeLigue1.Tools;

namespace OrangeLigue1.ViewModels
{
    public class MatchInfoPageViewModel : ViewModel
    {
        #region "Singleton definition"

        private static MatchInfoPageViewModel _instance;
        public static MatchInfoPageViewModel Instance
        {
            get
            {
                if (_instance == null)
                {
                    _instance = new MatchInfoPageViewModel();
                }
                return _instance;
            }
        }
        #endregion

        #region "Event/Args"
        public event EventHandler<BackgroundRequestCompletedEventArgs> ConnectionErrorRequestEnd;
        #endregion

        /// <summary>
        /// Returns the URI of the MatchInfo page
        /// </summary>
        public static Uri GetUri(int id, int live=0)
        {
            return new Uri(string.Format("/Views/MatchInfoPage.xaml?match={0}&live={1}", id, live), UriKind.Relative);
        }

        private Ligue1 myLigue1;
        public Ligue1 MyLigue1
        {
            get { return myLigue1; }
            set { myLigue1 = value; OnPropertyChanged("MyLigue1"); }
        }

        private MatchItemViewModel myMatchItemViewModel;
        public MatchItemViewModel MyMatchItemViewModel
        {
            get { return myMatchItemViewModel; }
            set { myMatchItemViewModel = value; OnPropertyChanged("MyMatchItemViewModel"); }
        }

        private MatchAction myMatchAction;
        public MatchAction MyMatchAction
        {
            get { return myMatchAction; }
            set { myMatchAction = value; OnPropertyChanged("MyMatchAction"); }
        }

        private PivotCommentsSectionViewModel myPivotCommentsSectionViewModel;
        public PivotCommentsSectionViewModel MyPivotCommentsSectionViewModel
        {
            get { return myPivotCommentsSectionViewModel; }
            set { myPivotCommentsSectionViewModel = value; OnPropertyChanged("MyPivotCommentsSectionViewModel"); }
        }

        private PivotHighlightsSectionViewModel myPivotHighlightsSectionViewModel;
        public PivotHighlightsSectionViewModel MyPivotHighlightsSectionViewModel
        {
            get { return myPivotHighlightsSectionViewModel; }
            set { myPivotHighlightsSectionViewModel = value; OnPropertyChanged("MyPivotHighlightsSectionViewModel"); }
        }

        private PivotStatsSectionViewModel myPivotStatsSectionViewModel;
        public PivotStatsSectionViewModel MyPivotStatsSectionViewModel
        {
            get { return myPivotStatsSectionViewModel; }
            set { myPivotStatsSectionViewModel = value; OnPropertyChanged("MyPivotStatsSectionViewModel"); }
        }

        private ObservableCollection<int> localTeamRedCards = new ObservableCollection<int>();
        public ObservableCollection<int> LocalTeamRedCards
        {
            get { return localTeamRedCards; }
            set { localTeamRedCards = value; OnPropertyChanged("LocalTeamRedCards"); }
        }

        private ObservableCollection<int> visitorTeamRedCards = new ObservableCollection<int>();
        public ObservableCollection<int> VisitorTeamRedCards
        {
            get { return visitorTeamRedCards; }
            set { visitorTeamRedCards = value; OnPropertyChanged("VisitorTeamRedCards"); }
        }

        private FontFamily localTeamFontFamily;
        public FontFamily LocalTeamFontFamily
        {
            get { return localTeamFontFamily; }
            set { localTeamFontFamily = value; OnPropertyChanged("LocalTeamFontFamily"); }
        }

        private FontFamily visitorTeamFontFamily;
        public FontFamily VisitorTeamFontFamily
        {
            get { return visitorTeamFontFamily; }
            set { visitorTeamFontFamily = value; OnPropertyChanged("VisitorTeamFontFamily"); }
        }

        private int live;
        public int Live
        {
            get { return live; }
            set { live = value; OnPropertyChanged("Live"); }
        }

        private int status;
        public int Status
        {
            get { return status; }
            set { status = value; OnPropertyChanged("Status"); }
        }

        private string liveUri;
        public string LiveUri
        {
            get { return liveUri; }
            set { liveUri = value; OnPropertyChanged("LiveUri"); }
        }

        private string liveUriWifi;
        public string LiveUriWifi
        {
            get { return liveUriWifi; }
            set { liveUriWifi = value; OnPropertyChanged("LiveUriWifi"); }
        }

        private bool myPageIsLoaded = false;
        public bool MyPageIsLoaded
        {
            get { return myPageIsLoaded; }
            set { myPageIsLoaded = value; OnPropertyChanged("MyPageIsLoaded"); }
        }

        private MatchInfoPageViewModel()
        {
            StartLiveCommand = new DelegateCommand<object>(OnStartLive);

            myLigue1 = App.CurrentApp.MyLigue1;

            if (myLigue1 != null)
            {
                myLigue1.MatchRequestEnd += myMatchRequestEnd;
                myLigue1.ConnectionErrorRequestEnd += myLigue1_ConnectionErrorRequestEnd;
            }

            myMatchItemViewModel = new MatchItemViewModel();
            myPivotCommentsSectionViewModel = new PivotCommentsSectionViewModel();
            myPivotHighlightsSectionViewModel = new PivotHighlightsSectionViewModel();
            myPivotStatsSectionViewModel = new PivotStatsSectionViewModel();

            LocalTeamFontFamily = App.CurrentApp.Resources["PhoneFontFamilyNormal"] as FontFamily;
            VisitorTeamFontFamily = App.CurrentApp.Resources["PhoneFontFamilyNormal"] as FontFamily;
        }

        void myMatchRequestEnd(object sender, MatchAction e)
        {       
            myPivotCommentsSectionViewModel.MyMatchAction = e;
            myPivotHighlightsSectionViewModel.MyMatchAction = e;
            myPivotStatsSectionViewModel.MyMatchAction = e;

            MatchInfo matchInfo = null;
            
            if(e.Matches != null &&
               e.Matches != null &&
               e.Matches.Count == 1)
            {
                matchInfo = e.Matches[0];
            }

            if (matchInfo == null)
            {
                return;
            }

            FontFamily localTeamFontFamily = App.CurrentApp.Resources["PhoneFontFamilyNormal"] as FontFamily;
            FontFamily visitorTeamFontFamily = App.CurrentApp.Resources["PhoneFontFamilyNormal"] as FontFamily;
            LogoInfo localTeamlogoInfo = null;
            LogoInfo visitorTeamlogoInfo = null;

            myMatchItemViewModel.MatchId = matchInfo.Id;
            myMatchItemViewModel.Status = matchInfo.Status;

            myMatchItemViewModel.LocalTeamScore = matchInfo.Teams.LocalTeam.Score.ToString();
            myMatchItemViewModel.VisitorTeamScore = matchInfo.Teams.VisitorTeam.Score.ToString();

            myMatchItemViewModel.LocalTeamName = App.CurrentApp.MyLigue1.MyTeamsInfo[matchInfo.Teams.LocalTeam.Id].City;
            myMatchItemViewModel.VisitorTeamName = App.CurrentApp.MyLigue1.MyTeamsInfo[matchInfo.Teams.VisitorTeam.Id].City;

            myMatchItemViewModel.LocalTeamShortName = App.CurrentApp.MyLigue1.MyTeamsInfo[matchInfo.Teams.LocalTeam.Id].ShortName;
            myMatchItemViewModel.VisitorTeamShortName = App.CurrentApp.MyLigue1.MyTeamsInfo[matchInfo.Teams.VisitorTeam.Id].ShortName;

            if (matchInfo.Teams.LocalTeam.Score > matchInfo.Teams.VisitorTeam.Score)
            {
                localTeamFontFamily = App.CurrentApp.Resources["PhoneFontFamilySemiBold"] as FontFamily;
            }
            else if (matchInfo.Teams.LocalTeam.Score < matchInfo.Teams.VisitorTeam.Score)
            {
                visitorTeamFontFamily = App.CurrentApp.Resources["PhoneFontFamilySemiBold"] as FontFamily;
            }

            LocalTeamFontFamily = localTeamFontFamily;
            VisitorTeamFontFamily = visitorTeamFontFamily;

            if (App.CurrentApp.MyLigue1.MyTeamsInfo != null &&
                App.CurrentApp.MyLigue1.MyTeamsInfo.ContainsKey(matchInfo.Teams.LocalTeam.Id) &&
                App.CurrentApp.MyLigue1.MyTeamsInfo[matchInfo.Teams.LocalTeam.Id].Logos.Count > 0)
            {
                localTeamlogoInfo = App.CurrentApp.MyLigue1.MyTeamsInfo[matchInfo.Teams.LocalTeam.Id].Logos[0];
            }

            if (App.CurrentApp.MyLigue1.MyTeamsInfo != null &&
                App.CurrentApp.MyLigue1.MyTeamsInfo.ContainsKey(matchInfo.Teams.VisitorTeam.Id) &&
                App.CurrentApp.MyLigue1.MyTeamsInfo[matchInfo.Teams.VisitorTeam.Id].Logos.Count > 0)
            {
                visitorTeamlogoInfo = App.CurrentApp.MyLigue1.MyTeamsInfo[matchInfo.Teams.VisitorTeam.Id].Logos[0];
            }

            if (App.CurrentApp.MyLigue1.MyLightSettingsAction != null &&
                    App.CurrentApp.MyLigue1.MyLightSettingsAction.Teams != null &&
                    localTeamlogoInfo != null)
            {
                myMatchItemViewModel.LocalTeamLogo44Uri = App.CurrentApp.MyLigue1.MyLogoBaseUrl + "/" + localTeamlogoInfo.Name + "_44." + localTeamlogoInfo.Ext;
            }

            if (App.CurrentApp.MyLigue1.MyLightSettingsAction != null &&
                App.CurrentApp.MyLigue1.MyLightSettingsAction.Teams != null &&
                visitorTeamlogoInfo != null)
            {
                myMatchItemViewModel.VisitorTeamLogo44Uri = App.CurrentApp.MyLigue1.MyLogoBaseUrl + "/" + visitorTeamlogoInfo.Name + "_44." + visitorTeamlogoInfo.Ext;
            }

            if (matchInfo.Teams.LocalTeam.Stats != null &&
                matchInfo.Teams.VisitorTeam.Stats != null)
            {
                localTeamRedCards.Clear();
                for (int i = 0; i < matchInfo.Teams.LocalTeam.Stats.RedCards; i++)
                {
                    localTeamRedCards.Add(0);
                }
                OnPropertyChanged("LocalTeamRedCards");

                visitorTeamRedCards.Clear();
                for (int i = 0; i < matchInfo.Teams.VisitorTeam.Stats.RedCards; i++)
                {
                    visitorTeamRedCards.Add(0);
                }
                OnPropertyChanged("VisitorTeamRedCards");
            }

            if (matchInfo.Videos != null &&
                matchInfo.Videos.Videos != null)
            {
                foreach (VideoInfo videoInfo in matchInfo.Videos.Videos)
                {
                    if (String.Equals(videoInfo.Type, "L"))
                    {
                        liveUri = videoInfo.Link;
                        liveUriWifi = videoInfo.LinkWifi;
                    }
                }
            }

            myLigue1.CurrentMatchId = matchInfo.Id;
            DispatcherManager timerHandler = DispatcherManager.Current;
            timerHandler.CurrentMatchId = matchInfo.Id;
            if (!timerHandler.RefreshFreqLiveTimerIsStarted())
            {
                timerHandler.StartRefreshFreqLiveTimer();
            }

            if (matchInfo.Events != null &&
                matchInfo.Events.Count > 0)
            {
                myLigue1.LastEventId = matchInfo.Events[0].Id;
            }
            //myLigue1.StartRefreshFreqLiveTimer();

            MyPageIsLoaded = true;
        }

        public void MyAllMatchActionRequestEnd(object sender, AllMatchAction e)
        {
            if (e.Matches != null &&
                e.Matches.Matches != null &&
                e.Matches.Matches.Count > 0)
            {
                MatchInfo matchInfo = null;

                foreach (MatchInfo item in e.Matches.Matches)
                {
                    if (item.Id == myMatchItemViewModel.MatchId)
                    {
                        matchInfo = item;
                        break;
                    }
                }

                if (matchInfo == null)
                {
                    return;
                }

                int localTeamScore = 0;
                int visitorTeamScore = 0;

                FontFamily localTeamFontFamily = App.CurrentApp.Resources["PhoneFontFamilyNormal"] as FontFamily;
                FontFamily visitorTeamFontFamily = App.CurrentApp.Resources["PhoneFontFamilyNormal"] as FontFamily;

                bool found = false;
                //best effort
                //1. Multiplex
                foreach (MatchItemViewModel item in PanoramaViewModel.Instance.MyPanoramaMultiplexSectionViewModel.MyMatchsList)
                {
                    if (item.MatchId == matchInfo.Id)
                    {
                        localTeamScore = Convert.ToInt32(item.LocalTeamScore);
                        myMatchItemViewModel.LocalTeamScore = localTeamScore.ToString();
						visitorTeamScore = Convert.ToInt32(item.VisitorTeamScore);
                        myMatchItemViewModel.VisitorTeamScore = visitorTeamScore.ToString();
                        found = true;
                        break;
                    }
                }
                //2. Calendar
                if (found == false)
                {
                    foreach (MatchsOfTheHourViewModel matchsOfTheHour in PanoramaViewModel.Instance.MyPanoramaCalendarSectionViewModel.MyMatchsOfTheHourList)
                    {
                        if (matchsOfTheHour.MyMatchsList != null)
                        {
                            foreach (MatchItemViewModel item in matchsOfTheHour.MyMatchsList)
                            {
                                if (item.MatchId == matchInfo.Id)
                                {
                                    localTeamScore = Convert.ToInt32(item.LocalTeamScore);
                                    myMatchItemViewModel.LocalTeamScore = localTeamScore.ToString();
                                    visitorTeamScore = Convert.ToInt32(item.VisitorTeamScore);
                                    myMatchItemViewModel.VisitorTeamScore = visitorTeamScore.ToString();
                                    found = true;
                                    break;
                                }
                            }
                            if (found == true)
                            {
                                break;
                            }
                        }
                    }
                }

                if (localTeamScore > visitorTeamScore)
                {
                    localTeamFontFamily = App.CurrentApp.Resources["PhoneFontFamilySemiBold"] as FontFamily;
                }
                else if (localTeamScore < visitorTeamScore)
                {
                    visitorTeamFontFamily = App.CurrentApp.Resources["PhoneFontFamilySemiBold"] as FontFamily;
                }

                LocalTeamFontFamily = localTeamFontFamily;
                VisitorTeamFontFamily = visitorTeamFontFamily;

                if (matchInfo.Teams.LocalTeam.Stats != null &&
                    matchInfo.Teams.VisitorTeam.Stats != null)
                {
                    localTeamRedCards.Clear();
                    for (int i = 0; i < matchInfo.Teams.LocalTeam.Stats.RedCards; i++)
                    {
                        localTeamRedCards.Add(0);
                    }
                    OnPropertyChanged("LocalTeamRedCards");

                    visitorTeamRedCards.Clear();
                    for (int i = 0; i < matchInfo.Teams.VisitorTeam.Stats.RedCards; i++)
                    {
                        visitorTeamRedCards.Add(0);
                    }
                    OnPropertyChanged("VisitorTeamRedCards");
                }

                if (matchInfo.Events != null &&
                    matchInfo.Events.Count > 0)
                {
                    myPivotCommentsSectionViewModel.MyMatchInfo = matchInfo;
                    myPivotHighlightsSectionViewModel.MyMatchInfo = matchInfo;
                    myLigue1.LastEventId = matchInfo.Events[0].Id;
                }
                myPivotStatsSectionViewModel.MyMatchInfo = matchInfo;
            }
        }

        void myLigue1_ConnectionErrorRequestEnd(object sender, BackgroundRequestCompletedEventArgs e)
        {
            if (ConnectionErrorRequestEnd != null)
            {
                ConnectionErrorRequestEnd(this, e);
            }
        }

        #region Commands
        public void Refresh()
        {
            myLigue1.RequestMatch(myMatchItemViewModel.MatchId);
        }

        public ICommand StartLiveCommand { get; set; }

        public void OnStartLive(object param)
        {
            if (liveUri != null || liveUriWifi != null)
            {
                if (Microsoft.Devices.Environment.DeviceType != Microsoft.Devices.DeviceType.Emulator)
                {
                    ObjectRepository.Store(MediaElementPageViewModel.MediaWifiUriString, liveUriWifi);
                    ObjectRepository.Store(MediaElementPageViewModel.MediaUriString, liveUri);
                    ObjectRepository.Store(MediaElementPageViewModel.MediaTitleString, MyMatchItemViewModel.LocalTeamName + " - " + MyMatchItemViewModel.VisitorTeamName);
                    ObjectRepository.Store(MediaElementPageViewModel.MediaLiveString, true);

                    Navigator.GoToPage(MediaElementPageViewModel.GetUri());
                }
            }
        }
        #endregion

        #region wording
        public string LoadingText
        {
            get
            {
                return Localization.GetString("MainPage_LoadingText");
            }
        }

        public string CommentsLabel
        {
            get
            {
                return Localization.GetString("CommentsLabel");
            }
        }

        public string HighLightsLabel
        {
            get
            {
                return Localization.GetString("HighLightsLabel");
            }
        }

        public string StatsLabel
        {
            get
            {
                return Localization.GetString("StatsLabel");
            }
        }

        public string WatchLiveLabel
        {
            get
            {
                return Localization.GetString("WatchLiveLabel");
            }
        }
        #endregion

        public void OnNavigatedTo()
        {
            if (myLigue1.RefreshFreqRTimer != null && myLigue1.RefreshFreqRTimer.IsEnabled)
            {
                myLigue1.StopRefreshFreqRTimer();
            }
        }
        public void StopTimer()
        {
            DispatcherManager timerHandler = DispatcherManager.Current;
            if (timerHandler.RefreshFreqLiveTimerIsStarted())
            {
                timerHandler.StopRefreshFreqLiveTimer();
            }
            if (myLigue1.RefreshFreqRTimer == null)
            {
                    myLigue1.StartRefreshFreqRTimer();
            }
        }
    }
}

