// -------------------------------  Copyright ---------------------------------
//	This software has been developped by France Telecom, FT/RD/RD/MAPS/DVC/MSF
//
//	Copyright (c) France Telecom 2009-2010
//
// COPYRIGHT    : This file is the property of FRANCE TELECOM.
//                It cannot be copied, used, or modified without obtaining
//                an authorization from the authors or a mandated
//                member of FRANCE TELECOM.
//                If such an authorization is provided, any modified version or
//                copy of the software has to contain this header.
//
// WARRANTIES   : This software is made available by the authors in the  hope
//                that it will be useful, but without any warranty.
//                France Telecom is not liable for any consequence related to the
//                use of the provided software.
//
// AUTHORS      : France Telecom  / RD / MAPS / DVC / MSF
// ----------------------------------------------------------------------------

using System;
using System.Net;
using System.Text;
using System.Windows;
using System.Windows.Threading;
using System.Xml.Linq;
using Orange.SDK;
using Orange.SDK.Enablers;
using OrangeLigue1.Model;
using OrangeLigue1.OrangeSDK;
using OrangeLigue1.Tools;
using Microsoft.Phone.Info;
using Kawagoe.Controls;
using System.Windows.Controls.Primitives;
using System.IO.IsolatedStorage;
using AuthenticationManager;
using System.Collections.Generic;

namespace OrangeLigue1.ViewModels
{
    /// <summary>
    /// Remote Media Element Page View Model
    /// </summary>
    public class MediaElementPageViewModel : ViewModel
    {
        public static string MediaUriString = "mediaUri";
        public static string MediaWifiUriString = "mediaWifiUri";
        public static string MediaTitleString = "mediaTitle";
        public static string MediaLiveString = "mediaIsLive";
        private string m_CurrentSetCookie = "";
        private IsolatedStorageSettings m_oUserSettings;
        public static bool IsOpened;

        public bool IsSmoothStreaming { get; set; }

        private static readonly TimeSpan PlaybackTimerInterval = TimeSpan.FromSeconds(30);
        private readonly AmpClientConfig config;
        private readonly ConnectivityType connectivityType = ConnectivityWatcher.CurrentType;

        private string _cellularMediaUri;
        private CookieContainer _cookieContainer;
        private bool _isGeoblocked;
        private bool _isMediaReady;
        private Exception _lastError;
        private bool? _live;
        private Uri _mediaSource;
        private string _mediaUri;
        private string _title;
        private BackgroundWebRequest _webRequest;
        private string _wifiMediaUri;
        private string elapsedTime = String.Empty;
        private string encoding = "ISO-8859-1";
        private string remainingTime = String.Empty;

        public MediaElementPageViewModel(AmpClientConfig config)
        {
            this.config = config;
            myLigue1 = App.CurrentApp.MyLigue1;
        }


        private Ligue1 myLigue1;
        public Ligue1 MyLigue1
        {
            get { return myLigue1; }
            set { myLigue1 = value; OnPropertyChanged("MyLigue1"); }
        }

        /// <summary>
        /// String representation of the tp2.orange.fr cellular link
        /// </summary>
        public string CellularMediaUri
        {
            get { return _cellularMediaUri; }
            set
            {
                if (_cellularMediaUri == value)
                {
                    return;
                }
                if (value != null)
                {
                    _cellularMediaUri = value;
                    OnPropertyChanged("CellularMediaUri");
                }
            }
        }

        /// <summary>
        /// CookieContainer properties, needed by the MedialElement to reuse cookie following several redirect
        /// </summary>
        public CookieContainer cookieContainer
        {
            get { return _cookieContainer; }
            set
            {
                if (_cookieContainer != value)
                    _cookieContainer = value;
            }
        }


        /// <summary>
        /// String representation of the ampfr wifi link
        /// </summary>
        public string WifiMediaUri
        {
            get { return _wifiMediaUri; }
            set
            {
                if (_wifiMediaUri == value)
                {
                    return;
                }
                if (value != null)
                {
                    _wifiMediaUri = value;
                    OnPropertyChanged("WifiMediaUri");
                }
            }
        }

        public bool IsMediaReady
        {
            get { return _isMediaReady; }
            set
            {
                if (_isMediaReady == value)
                {
                    return;
                }
                _isMediaReady = value;
                OnPropertyChanged("IsMediaReady");
            }
        }

        public string ElaspsedTime
        {
            get { return elapsedTime; }
            set
            {
                elapsedTime = value;
                OnPropertyChanged("ElaspsedTime");
            }
        }

        public string RemainingTime
        {
            get { return remainingTime; }
            set
            {
                remainingTime = value;
                OnPropertyChanged("RemainingTime");
            }
        }

        /// <summary>
        /// String representation of the tp2.orange.fr link
        /// </summary>
        public string MediaUri
        {
            get { return _mediaUri; }
            set
            {
                if (_mediaUri == value)
                {
                    return;
                }
                if (value != null)
                {
                    _mediaUri = value;
                    OnPropertyChanged("MediaUri");
                }
            }
        }

        /// <summary>
        /// Uri of the media to be read by the media element
        /// </summary>
        public Uri MediaSource
        {
            get { return _mediaSource; }
            set { _mediaSource = value; }
        }

        public String Title
        {
            get { return _title; }
            set
            {
                if (_title == value)
                {
                    return;
                }
                if (value != null)
                {
                    _title = value;
                    OnPropertyChanged("Title");
                }
            }
        }

        public bool? Live
        {
            get { return _live; }
            set
            {
                if (_live == value)
                {
                    return;
                }
                _live = value;
                OnPropertyChanged("Live");
            }
        }

        public bool IsGeoblocked
        {
            get { return _isGeoblocked; }
            set
            {
                if (_isGeoblocked == value)
                {
                    return;
                }
                _isGeoblocked = value;
                OnPropertyChanged("IsGeoblocked");
            }
        }

        public Exception LastError
        {
            get { return _lastError; }
            set
            {
                if (_lastError == value)
                {
                    return;
                }
                _lastError = value;
                OnPropertyChanged("LastError");
            }
        }

        public static Uri GetUri()
        {
            return new Uri(string.Format("/Views/MediaElementPage.xaml"), UriKind.Relative);
        }

        private void Reset()
        {
            StopPlaybackTimer();
            MediaSource = null;
            IsMediaReady = false;
            IsSmoothStreaming = false;
            Title = null;
            CellularMediaUri = null;
            WifiMediaUri = null;
            MediaUri = null;
            Live = null;
            IsGeoblocked = false;
            LastError = null;
        }

        public void Reload()
        {
            Reset();

            string mediaUriString = null;

            if (connectivityType == ConnectivityType.Unknown)
            {
                // wait for the connectivity type to be known
                Messenger.Current.Register(ConnectivityWatcher.CurrentTypeChangedMessage, OnConnectivityTypeChanged);
                return;
            }

            try
            {
                /*
                if (connectivityType == ConnectivityType.WiFi)
                {
                    mediaUriString = ObjectRepository.Get(MediaWifiUriString) as string;
                    if (!String.IsNullOrEmpty(mediaUriString))
                    {
                        _wifiMediaUri =
                            _mediaUri = mediaUriString.Replace(config.ForbiddenPrefixUri, config.NominalPrefixUri);
                        ObjectRepository.Remove(MediaWifiUriString);
                        LoadInternalData();
                    }
                }
                else if (connectivityType == ConnectivityType.Cellular)
                {*/
                    mediaUriString = ObjectRepository.Get(MediaElementPageViewModel.MediaUriString) as string;
                    if (!String.IsNullOrEmpty(mediaUriString))
                    {
                        _cellularMediaUri = _mediaUri = mediaUriString;
                        ObjectRepository.Remove(MediaUriString);
                        LoadInternalData();
                    }
                //}
                if (String.IsNullOrEmpty(_mediaUri))
                {
                    throw new Exception("Unable to retrieve media uri stream");
                }
            }
            catch (Exception error)
            {
                NotifyError(error);
            }
        }

        private void OnConnectivityTypeChanged(object sender, string message, object param)
        {
            Reload();
        }

        private void LoadInternalData()
        {
            try
            {
                Live = ObjectRepository.Get(MediaLiveString) as bool?;

                if (Live != null)
                {
                    ObjectRepository.Remove(MediaLiveString);
                }

                Title = ObjectRepository.Get(MediaTitleString) as string;

                if (!String.IsNullOrEmpty(Title))
                {
                    ObjectRepository.Remove(MediaTitleString);
                }

                if (_mediaUri != null) //title can be null here
                {
                    var nominalUri = new Uri(_mediaUri, UriKind.Absolute);

                    CreateAndStartRequest(nominalUri);
                }
                else if (MediaSource != null && !String.IsNullOrEmpty(Title) && Live != null)
                {
                    Deployment.Current.Dispatcher.BeginInvoke(() =>
                                                              {
                                                                  OnPropertyChanged("MediaSource");
                                                                  StartPlaybackTimer();
                                                              });
                }
                else throw new Exception("Unable to retrieve enough data to play the stream");
            }
            catch (Exception error)
            {
                Deployment.Current.Dispatcher.BeginInvoke(() => NotifyError(error));
            }
        }

        private void CreateAndStartRequest(Uri nominalUri)
        {
            _webRequest = BackgroundWebRequest.CreateAuthenticated(nominalUri, null /*no fallback uri*/);
            /*
            if (String.Equals(App.CurrentApp.CurrentConfigurationName, "OrangeFR_Integration"))
            {
                _webRequest.Headers["User-Agent"] = App.CurrentApp.CurrentUserAgent; //"NativeHost"
            }
            else if (String.Equals(App.CurrentApp.CurrentConfigurationName, "OrangeFR_Development"))
            {
                _webRequest.Headers["User-Agent"] = Ligue1Enabler.SampleNativeHostUAString;
            }
            if (_cookieContainer == null)
            {
                _cookieContainer = new CookieContainer();
            }*/
            //_webRequest.Headers["X_AMP_MID"] = DeviceExtendedProperties.GetValue("DeviceUniqueId").ToString();
            _webRequest.Headers["User-Agent"] = App.CurrentApp.CurrentUserAgent;
            _webRequest.Headers["X_AMP_UA"] = App.CurrentApp.CurrentUserAgent;
            if (ConnectivityWatcher.CurrentType == ConnectivityType.WiFi)
            {
                _webRequest.Headers["X_AMP_BEARER"] = "1";
            }
            else
            {
                _webRequest.Headers["X_AMP_BEARER"] = "0";
            }
            if (!string.IsNullOrEmpty(getCookie()))
            {
                _webRequest.Headers["Cookie"] = m_CurrentSetCookie;
            }
            if (_cookieContainer == null)
            {
                _cookieContainer = new CookieContainer();
            }
            _webRequest.AllowAutoRedirect = false;
            _webRequest.CookieContainer = _cookieContainer;
            _webRequest.Method = "GET";
            _webRequest.Completed += OnBackgroundWebRequestCompleted;

            Logger.Log("CreateAndStartRequest {0}", nominalUri.ToString());
            _webRequest.Start(null);
        }

        private string getCookie()
        {
            m_oUserSettings = IsolatedStorageSettings.ApplicationSettings;
            try
            {
                m_CurrentSetCookie = m_oUserSettings[OLAuthenticationManager.USSO_LAST_SETCOOKIE_VALUE].ToString();
            }
            catch (KeyNotFoundException)
            {
                m_CurrentSetCookie = "";
            }
            return m_CurrentSetCookie;
        }

        private void OnBackgroundWebRequestCompleted(object sender, BackgroundRequestCompletedEventArgs e)
        {
            var request = sender as BackgroundWebRequest;

            string responseString = null;

            if (request == null)
            {
                return;
            }
            try
            {
                string lastLocation = null;
                bool isManifest = false;
                bool isMp4 = false;

                if (e.Error != null ||
                    e.Cancelled)
                {
                    throw new Exception(e.Error.ToString());
                }

                if (e.StatusCode == HttpStatusCode.Found)
                {
                    var location = e.Headers["Location"];

                    isManifest = location.ToLower().Contains(".isml/manifest");
                    isMp4 = location.ToLower().Contains(".mp4");

                    if (!isManifest && !isMp4)
                    {
                        CreateAndStartRequest(new Uri(location));
                        return;
                    }

                    lastLocation = location;
                }

                if (!String.IsNullOrEmpty(lastLocation))
                {
                    if (isMp4 == true)
                    {
                        Logger.Log("Mp4 started at uri {0}", lastLocation);
                        Deployment.Current.Dispatcher.BeginInvoke(() =>
                                                                  {
                                                                      IsSmoothStreaming = false; 
                                                                      // HERE we know that we have a valid .mp4 Progressive Download Url 
                                                                      MediaSource = new Uri(lastLocation);
                                                                      IsMediaReady = true;
                                                                      StartPlaybackTimer();
                                                                  });

                    }
                    else
                    {
                        if (isManifest == true)
                        {
                            Logger.Log("Manifest started at uri {0}", lastLocation);
                            Deployment.Current.Dispatcher.BeginInvoke(() =>
                                                                {
                                                                    IsSmoothStreaming = true; 
                                                                    // HERE we have a Manifest Url
                                                                    MediaSource = new Uri(lastLocation);
                                                                    IsMediaReady = true;
                                                                    StartPlaybackTimer();
                                                                });

                        }
                    }

                    return;

                }

                responseString = BuildString(e.Data, encoding);
                throw new Exception(responseString);

            }
            catch (Exception error)
            {
                Logger.Log("OnBackgroundWebRequestCompleted  Exception {0}", error.Message);
 
                //RemoteMediaElementException rmeException = null;
                //string backEndMessage = null;
                string message = null;
                XDocument xDoc = null;

                if (!String.IsNullOrEmpty(responseString))
                {
                    try
                    {
                        string tempResponseString = responseString;

                        // Temporary workaround waiting backend to fix this
                        // the bakcend send &eacute in xml entities.
                        //if (responseString.Contains("&eacute"))
                        //{
                        //    tempResponseString = HttpUtility.HtmlDecode(responseString);
                        //}

                        xDoc = XDocument.Parse(tempResponseString);
                    }
                    catch (Exception)
                    {
                        xDoc = null;
                    }
                }

                if (xDoc != null)
                {
                    foreach (XElement el in xDoc.Descendants())
                    {
                        if (el.Name != null &&
                           el.Name.LocalName != null &&
                           String.Equals(el.Name.LocalName, "code"))
                        {
                            if (el.Value == "010")
                            {
                                if (myLigue1.MyUserinfoAction.L1Compliant == 1)
                                    message = myLigue1.MyWordingAction.Elements[0].Value;
                                else
                                    message = myLigue1.MyWordingAction.Elements[1].Value;
                                break;
                            }                           
                        }
                    }
                    if (message == null)
                    {
                        foreach (XElement el in xDoc.Descendants())
                        {
                            if (el.Name != null &&
                               el.Name.LocalName != null &&
                               String.Equals(el.Name.LocalName, "description"))
                            {
                                message = el.Value;
                            }
                        }
                    }
                }

                if (!string.IsNullOrEmpty(message))
                {
                    Deployment.Current.Dispatcher.BeginInvoke(() =>
                    {
                        MessageBoxResult m = MessageBox.Show(message);
                        if (m == MessageBoxResult.OK)
                            Navigator.GoBack();
                    });
                }
                else
                {
                    Navigator.GoBack();

                    /*

                    if (!String.IsNullOrEmpty(backEndMessage))
                    {
                        rmeException = new RemoteMediaElementException(backEndMessage, error);
                    }

                    if (rmeException != null)
                    {
                        Deployment.Current.Dispatcher.BeginInvoke(() => NotifyError(rmeException));
                    }
                    else
                    {
                        Deployment.Current.Dispatcher.BeginInvoke(() => NotifyError(error));
                    }*/
                }
            }
        }

        /// <summary>
        /// Builds a string instance from the specified data.
        /// Returns <c>null</c> if the data cannot be read.
        /// </summary>
        public static string BuildString(byte[] data, string encoding)
        {
            if (data == null || data.Length == 0)
            {
                return null;
            }
            try
            {
                return Encoding.GetEncoding(encoding).GetString(data, 0, data.Length);
            }
            catch (Exception error)
            {
                Logger.DebugTrace("Failed to build string: {0}", error);
                return null;
            }
        }

        private void NotifyError(Exception error)
        {
            if (error == null)
            {
                return;
            }
            Logger.DebugTrace("Notifying player error: {0} {1}", error.GetType().Name, error.Message);
            Reset();
            if (error is RemoteMediaElementException)
            {
                if (error.InnerException is WebException)
                {
                    var webError = (WebException) error;
                    if (webError.Response != null && webError.Response is HttpWebResponse)
                    {
                        var webResponse = (HttpWebResponse) webError.Response;
                        if (webResponse.StatusCode == HttpStatusCode.Forbidden)
                        {
                            IsGeoblocked = true;
                            return;
                        }
                    }
                }
            }
            else if (error.InnerException is WebException)
            {
                var webError = (WebException) error;
                if (webError.Response != null && webError.Response is HttpWebResponse)
                {
                    var webResponse = (HttpWebResponse) webError.Response;
                    if (webResponse.StatusCode == HttpStatusCode.Forbidden)
                    {
                        IsGeoblocked = true;
                        return;
                    }
                }
            }
            LastError = error;
        }

        public void OnMediaStopPlaybackTimer()
        {
            StopPlaybackTimer();
            _cookieContainer = null;
        }

        #region Playback Timer

        private DispatcherTimer _playbackTimer;


        private void StartPlaybackTimer()
        {
            StopPlaybackTimer();
            _playbackTimer = new DispatcherTimer();
            _playbackTimer.Interval = PlaybackTimerInterval;
            _playbackTimer.Tick += OnPlaybackTimerTick;
            _playbackTimer.Start();
        }

        private void StopPlaybackTimer()
        {
            if (_playbackTimer == null)
            {
                return;
            }
            DispatcherTimer timer = _playbackTimer;
            _playbackTimer = null;
            try
            {
                timer.Stop();
            }
            catch (Exception)
            {
            }
        }

        private void OnPlaybackTimerTick(object sender, EventArgs e)
        {
            if (sender != _playbackTimer)
            {
                return;
            }
            StopPlaybackTimer();
            Logger.DebugTrace("Playback timout elapsed!");
            NotifyError(new TimeoutException());
        }

        #endregion
    }
}