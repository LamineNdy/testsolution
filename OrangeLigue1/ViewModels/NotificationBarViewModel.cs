// -------------------------------  Copyright ---------------------------------
//	This software has been developped by France Telecom, FT/RD/RD/MAPS/DVC/MSF
//
//	Copyright (c) France Telecom 2009-2010
//
// COPYRIGHT    : This file is the property of FRANCE TELECOM.
//                It cannot be copied, used, or modified without obtaining
//                an authorization from the authors or a mandated
//                member of FRANCE TELECOM.
//                If such an authorization is provided, any modified version or
//                copy of the software has to contain this header.
//
// WARRANTIES   : This software is made available by the authors in the  hope
//                that it will be useful, but without any warranty.
//                France Telecom is not liable for any consequence related to the
//                use of the provided software.
//
// AUTHORS      : France Telecom  / RD / MAPS / DVC / MSF
// ----------------------------------------------------------------------------
using System;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using Orange.SDK;
using Orange.SDK.Enablers.Ligue1;
using System.Collections.ObjectModel;
using Orange.SDK.Tools;
using System.Windows.Threading;
using System.Runtime.CompilerServices;
using OrangeLigue1.Tools;
using System.Collections.Generic;
using Microsoft.Devices;
using OrangeLigue1.Model;

namespace OrangeLigue1.ViewModels
{
    public class NotificationBarViewModel : ViewModel
    {
        private DispatcherTimer notificationTimer = null;
        private static TimeSpan refreshNotificationTimer = TimeSpan.FromSeconds(5);

        private DispatcherTimer closeNotificationBarTimer = null;
        private static TimeSpan refreshCloseNotificationBarTimer = TimeSpan.FromSeconds(1);
        private static TimeSpan vibrateTimer = TimeSpan.FromMilliseconds(250);

        #region "Singleton definition"

        private static NotificationBarViewModel _instance;
        public static NotificationBarViewModel Instance
        {
            get
            {
                if (_instance == null)
                {
                    _instance = new NotificationBarViewModel();
                }
                return _instance;
            }
        }
        #endregion

        private NotificationBarViewModel()
        {
            PanoramaViewModel.Instance.AllMatchRequestEnd += myAllMatchActionRequestEnd;

            OpenCommand = new DelegateCommand<object>((p) => { Open(); });
            CloseCommand = new DelegateCommand<object>((p) => { Close(); });

            StartNotificationTimer();
        }

        private AllMatchAction myAllMatchAction;
        public AllMatchAction MyAllMatchAction
        {
            get { return myAllMatchAction; }
            set
            {
                myAllMatchAction = value;
                if (myAllMatchAction != null)
                {
                    UpdatesPropertiesFromAllMatchAction();
                }
            }
        }

        /// <summary>
        /// List of highlights item
        /// </summary>
        private ObservableCollection<HighlightViewModel> myHighlightsList = new ObservableCollection<HighlightViewModel>();

        /// <summary>
        /// Map of highlights item
        /// </summary>
        private Dictionary<int, HighlightViewModel> myHighlightsDictionary = new Dictionary<int, HighlightViewModel>();

        private HighlightViewModel currentItem;
        public HighlightViewModel CurrentItem
        {
            get { return currentItem; }
            set { currentItem = value; OnPropertyChanged("CurrentItem"); }
        }

        private void LightSettingsRequestEnd(object sender, LightSettingsAction e)
        {
            App.CurrentApp.MyLigue1.LightSettingsRequestEnd -= LightSettingsRequestEnd;
            this.UpdatesPropertiesFromAllMatchAction();
        }

        private void UpdatesPropertiesFromAllMatchAction()
        {
            if (App.CurrentApp.MyLigue1.MyLightSettingsActionIsLoaded == false)
            {
                App.CurrentApp.MyLigue1.LightSettingsRequestEnd += LightSettingsRequestEnd;
                return;
            }

            foreach (MatchInfo matchInfo in myAllMatchAction.Matches.Matches)
            {
                foreach (MatchEventInfo eventInfo in matchInfo.Events)
                {
                    if (eventInfo.Type != 0 && eventInfo.Type != 3) // != comment && != action
                    {
                        string localTeamName = null;
                        string visitorTeamName = null;
                        string localTeamLogo64Uri = null;
                        string visitorTeamLogo64Uri = null;
                        int localTeamScore = 0;
                        int visitorTeamScore = 0;
                        int live = 0;
                        FontFamily localTeamFontFamily = App.CurrentApp.Resources["PhoneFontFamilyNormal"] as FontFamily;
                        FontFamily visitorTeamFontFamily = App.CurrentApp.Resources["PhoneFontFamilyNormal"] as FontFamily;

                        bool found = false;
                        //best effort
                        //1. Multiplex
                        foreach (MatchItemViewModel item in PanoramaViewModel.Instance.MyPanoramaMultiplexSectionViewModel.MyMatchsList)
                        {
                            if (item.MatchId == matchInfo.Id)
                            {
                                localTeamScore = Convert.ToInt32(item.LocalTeamScore);
                                visitorTeamScore = Convert.ToInt32(item.VisitorTeamScore);
                                localTeamName = item.LocalTeamName;
                                visitorTeamName = item.VisitorTeamName;
                                localTeamLogo64Uri = item.LocalTeamLogo64Uri;
                                visitorTeamLogo64Uri = item.VisitorTeamLogo64Uri;
                                live = item.Live;
                                found = true;
                                break;
                            }
                        }
                        //2. Calendar
                        if (found == false)
                        {
                            foreach (MatchsOfTheHourViewModel matchsOfTheHour in PanoramaViewModel.Instance.MyPanoramaCalendarSectionViewModel.MyMatchsOfTheHourList)
                            {
                                if (matchsOfTheHour.MyMatchsList != null)
                                {
                                    foreach (MatchItemViewModel item in matchsOfTheHour.MyMatchsList)
                                    {
                                        if (item.MatchId == matchInfo.Id)
                                        {
                                            localTeamScore = Convert.ToInt32(item.LocalTeamScore);
                                            visitorTeamScore = Convert.ToInt32(item.VisitorTeamScore);
                                            localTeamName = item.LocalTeamName;
                                            visitorTeamName = item.VisitorTeamName;
                                            localTeamLogo64Uri = item.LocalTeamLogo64Uri;
                                            visitorTeamLogo64Uri = item.VisitorTeamLogo64Uri;
                                            live = item.Live;
                                            found = true;
                                            break;
                                        }
                                    }
                                    if (found == true)
                                    {
                                        break;
                                    }
                                }
                            }
                        }

                        if (localTeamScore > visitorTeamScore)
                        {
                            localTeamFontFamily = App.CurrentApp.Resources["PhoneFontFamilySemiBold"] as FontFamily;
                        }
                        else if (localTeamScore < visitorTeamScore)
                        {
                            visitorTeamFontFamily = App.CurrentApp.Resources["PhoneFontFamilySemiBold"] as FontFamily;
                        }


                        if (myHighlightsDictionary.ContainsKey(eventInfo.Id) == false && App.CurrentApp.MyLigue1.MyInitInfoHighlight.ContainsKey(eventInfo.Id) == false)
                        {
                           // if (myHighlightsList.Count < AppConfig.Current.MaxAlertsNumber)
                           // {
                                myHighlightsList.Insert(0, new HighlightViewModel()
                                {
                                    Match = new MatchItemViewModel()
                                    {
                                        MatchId = matchInfo.Id,
                                        LocalTeamScore = localTeamScore.ToString(),
                                        VisitorTeamScore = visitorTeamScore.ToString(),
                                        LocalTeamName = localTeamName,
                                        VisitorTeamName = visitorTeamName,
                                        LocalTeamLogo64Uri = localTeamLogo64Uri,
                                        VisitorTeamLogo64Uri = visitorTeamLogo64Uri,
                                        LocalTeamFontFamily = localTeamFontFamily,
                                        VisitorTeamFontFamily = visitorTeamFontFamily,
                                        Status = matchInfo.Status,
                                        Live = live
                                    },
                                    EventInfo = eventInfo
                                });
                                myHighlightsDictionary.Add(
                                    eventInfo.Id,
                                    null
                                );
                            //}
                        }
                    }
                }
            }
        }

        #region command
        public ICommand OpenCommand { get; private set; }
        public ICommand CloseCommand { get; private set; }

        [MethodImpl(MethodImplOptions.Synchronized)]
        private void Open()
        {
            Close();
            if (currentItem != null &&
                currentItem.Match != null &&
                currentItem.EventInfo != null)
            {
                if (currentItem.EventInfo.Type == 1 ||
                    currentItem.EventInfo.Type == 2)
                {
                    {
                        //NotPlayed = 0,
                        //Playing = 1,
                        //Finished = 2
                        int live = 0;
                        if (currentItem.Match.Status == 0)
                        {
                            return;
                        }
                        else if (currentItem.Match.Status == 1)
                        {
                            live = currentItem.Match.Live;
                        }
                        Navigator.GoToPage(MatchInfoPageViewModel.GetUri(currentItem.Match.MatchId, live));
                    }
                }
                else if (currentItem.EventInfo.Type == 4)
                {
                    if (Microsoft.Devices.Environment.DeviceType != Microsoft.Devices.DeviceType.Emulator)
                    {
                        ObjectRepository.Store(MediaElementPageViewModel.MediaWifiUriString, currentItem.EventInfo.Video.LinkWifi);
                        ObjectRepository.Store(MediaElementPageViewModel.MediaUriString, currentItem.EventInfo.Video.Link);
                        ObjectRepository.Store(MediaElementPageViewModel.MediaTitleString, currentItem.Title);
                        ObjectRepository.Store(MediaElementPageViewModel.MediaLiveString, false);

                        Navigator.GoToPage(MediaElementPageViewModel.GetUri());
                    }
                }
            }
        }

        [MethodImpl(MethodImplOptions.Synchronized)]
        private void Close()
        {
            if (currentItem != null)
            {
                VisualStateManager.GoToState(App.CurrentApp.notificationBar, App.CurrentApp.notificationBar.ClosedNotificationBar.Name, true);
                StartCloseNotificationBarTimer();
            }
        }
        #endregion

        //
        // NotificationTimer
        //

        private void StartNotificationTimer()
        {
            if (notificationTimer != null)
            {
                notificationTimer.Stop();
                notificationTimer = null;
            }
            notificationTimer = new DispatcherTimer();
            notificationTimer.Interval = refreshNotificationTimer;
            notificationTimer.Tick += new EventHandler(OnRefreshNotificationTimerTick);
            notificationTimer.Start();
        }

        private void StopNotificationTimer()
        {
            if (notificationTimer != null)
            {
                notificationTimer.Stop();
                notificationTimer = null;
            }
        }

        /// <summary>
        /// Called when the refresh notification timer ticks.
        /// </summary>
        [MethodImpl(MethodImplOptions.Synchronized)]
        private void OnRefreshNotificationTimerTick(object sender, EventArgs e)
        {
            DispatcherTimer timer = sender as DispatcherTimer;
            if (timer == null)
            {
                return;
            }
            if (timer != notificationTimer)
            {
                return;
            }

            if (currentItem != null)
            {
                VisualStateManager.GoToState(App.CurrentApp.notificationBar, App.CurrentApp.notificationBar.ClosedNotificationBar.Name, true);
                StartCloseNotificationBarTimer();
                return;
            }

            currentItem = null;

            if (myHighlightsList.Count > 0)
            {
                if (MediaElementPageViewModel.IsOpened == false)
                {
                    CurrentItem = myHighlightsList[0];
                    myHighlightsList.Remove(currentItem);

                    App.CurrentApp.popup.IsOpen = true;
                    VisualStateManager.GoToState(App.CurrentApp.notificationBar, App.CurrentApp.notificationBar.OpenedNotificationBar.Name, true);
                    //VibrateController.Default.Start(vibrateTimer);
                }
                else
                {
                    CurrentItem = myHighlightsList[0];
                    myHighlightsList.Remove(currentItem);

                    //VibrateController.Default.Start(vibrateTimer);
                }
            }
        }

        //
        // CloseNotificationBarTimer
        //

        private void StartCloseNotificationBarTimer()
        {
            if (closeNotificationBarTimer != null)
            {
                closeNotificationBarTimer.Stop();
                closeNotificationBarTimer = null;
            }
            closeNotificationBarTimer = new DispatcherTimer();
            closeNotificationBarTimer.Interval = refreshCloseNotificationBarTimer;
            closeNotificationBarTimer.Tick += new EventHandler(OnRefreshCloseNotificationBarTimerTick);
            closeNotificationBarTimer.Start();
        }

        [MethodImpl(MethodImplOptions.Synchronized)]
        private void StopCloseNotificationBarTimer()
        {
            if (closeNotificationBarTimer != null)
            {
                closeNotificationBarTimer.Stop();
                closeNotificationBarTimer = null;
            }

            App.CurrentApp.popup.IsOpen = false;

            if (myHighlightsList.Count > 0)
            {
                myHighlightsList.Remove(currentItem);
                currentItem = null;
            }
        }

        /// <summary>
        /// Called when the refresh notification timer ticks.
        /// </summary>
        private void OnRefreshCloseNotificationBarTimerTick(object sender, EventArgs e)
        {
            DispatcherTimer timer = sender as DispatcherTimer;
            if (timer == null)
            {
                return;
            }
            if (timer != closeNotificationBarTimer)
            {
                return;
            }

            StopCloseNotificationBarTimer();
        }

        void myAllMatchActionRequestEnd(object sender, AllMatchAction e)
        {
            this.MyAllMatchAction = e;
        }

        #region wording
        public string AlertMenuLabel
        {
            get
            {
                return Localization.GetString("AlertMenuLabel");
            }
        }

        public string CloseLabel
        {
            get
            {
                return Localization.GetString("CloseLabel");
            }
        }
        #endregion
    }
}

