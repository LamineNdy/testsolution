// -------------------------------  Copyright ---------------------------------
//	This software has been developped by France Telecom, FT/RD/RD/MAPS/DVC/MSF
//
//	Copyright (c) France Telecom 2009-2010
//
// COPYRIGHT    : This file is the property of FRANCE TELECOM.
//                It cannot be copied, used, or modified without obtaining
//                an authorization from the authors or a mandated
//                member of FRANCE TELECOM.
//                If such an authorization is provided, any modified version or
//                copy of the software has to contain this header.
//
// WARRANTIES   : This software is made available by the authors in the  hope
//                that it will be useful, but without any warranty.
//                France Telecom is not liable for any consequence related to the
//                use of the provided software.
//
// AUTHORS      : France Telecom  / RD / MAPS / DVC / MSF
// ----------------------------------------------------------------------------
using System;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using Orange.SDK;
using Orange.SDK.Tools;

namespace OrangeLigue1.ViewModels
{
    public class AboutPageViewModel : ViewModel
    {

        public AboutPageViewModel()
        {
            LaunchLegalCommand = new DelegateCommand<object>(OnLaunchLegal, CanLaunchLegal);
        }
        #region wording
        public string ApplicationTitleLabel
        {
            get
            {
                return Localization.GetString("ApplicationTitleLabel");
            }
        }

        public string ApplicationVersion
        {
            get
            {
                string appVersionNumber = App.VersionNumber;
                if (appVersionNumber.EndsWith("9999"))
                {
                    appVersionNumber = AppConfig.Current.AmpConfig.AppVersion;
                }
                return appVersionNumber;
            }
        }

        public string AboutMenuLabel
        {
            get
            {
                return Localization.GetString("AboutMenuLabel");
            }
        }

        public string AboutPropertyLabel
        {
            get
            {
                return Localization.GetString("AboutPropertyLabel");
            }
        }

        public string AboutLegalLabel
        {
            get
            {
                return Localization.GetString("AboutLegalLabel");
            }
        }

        public string AboutQuestionLabel
        {
            get
            {
                return Localization.GetString("AboutQuestionLabel");
            }
        }       
        #endregion

        public static Uri GetUri()
        {
            return new Uri("/Views/AboutPage.xaml", UriKind.Relative);
        }


        #region Command
        public ICommand LaunchLegalCommand { get; set; }

        public void OnLaunchLegal(object param)
        {
            Navigator.GoToPage(LegalPageViewModel.GetUri());            
        }
        public bool CanLaunchLegal(object param)
        {          
            return true;
        }

        #endregion
    }
}

