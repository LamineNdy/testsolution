// -------------------------------  Copyright ---------------------------------
//	This software has been developped by France Telecom, FT/RD/RD/MAPS/DVC/MSF
//
//	Copyright (c) France Telecom 2009-2010
//
// COPYRIGHT    : This file is the property of FRANCE TELECOM.
//                It cannot be copied, used, or modified without obtaining
//                an authorization from the authors or a mandated
//                member of FRANCE TELECOM.
//                If such an authorization is provided, any modified version or
//                copy of the software has to contain this header.
//
// WARRANTIES   : This software is made available by the authors in the  hope
//                that it will be useful, but without any warranty.
//                France Telecom is not liable for any consequence related to the
//                use of the provided software.
//
// AUTHORS      : France Telecom  / RD / MAPS / DVC / MSF
// ----------------------------------------------------------------------------
using System;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using Orange.SDK;
using System.Collections.ObjectModel;
using Orange.SDK.Enablers.Ligue1;
using OrangeLigue1.Tools;
using OrangeLigue1.Model;
using Orange.SDK.Enablers;
using System.Xml.Linq;
using System.Collections.Generic;
using Orange.SDK.Tools;

namespace OrangeLigue1.ViewModels
{
    public class PanoramaMultiplexSectionViewModel : ViewModel
    {
        public PanoramaMultiplexSectionViewModel()
        {
            OpenAccessRightsCommand = new DelegateCommand<object>(OnOpenAccessRightsCommand);
        }

        //initinfo
        private DayInfo myDayInfo;
        public DayInfo MyDayInfo
        {
            get { return myDayInfo; }
            set
            {
                myDayInfo = value;
                if (myDayInfo != null)
                {
                    UpdatesPropertiesFromDayInfo();
                    OnPropertyChanged("MultiplexIsInProgress");
                    OnPropertyChanged("MyMatchsList");
                }
            }
        }

        //allmatch
        private AllMatchAction myAllMatchAction;
        public AllMatchAction MyAllMatchAction
        {
            get { return myAllMatchAction; }
            set
            {
                myAllMatchAction = value;
                if (myAllMatchAction != null)
                {
                    UpdatesPropertiesFromAllMatchAction();
                    OnPropertyChanged("MultiplexIsInProgress");
                    OnPropertyChanged("MyMatchsList");
                }
            }
        }

        /// <summary>
        /// List of Matchs
        /// </summary>
        private ObservableCollection<MatchItemViewModel> myMatchsList = new ObservableCollection<MatchItemViewModel>();
        public ObservableCollection<MatchItemViewModel> MyMatchsList
        {
            get { return myMatchsList; }
            set
            {
                myMatchsList = value;
                OnPropertyChanged("MyMatchsList");
            }
        }

        public MatchItemViewModel CurrentListItem
        {
            get { return null; }
            set
            {
                if (value != null)
                {
                    SelectionChangedCommand(value);
                    OnPropertyChanged("CurrentListItem");
                }
            }
        }

        public string MultiplexNoMatchLabel { get; set; }

        private bool noMoreMatchesToday;
        public bool NoMoreMatchesToday
        {
            get
            {
                return noMoreMatchesToday;
            }
            set
            {
                noMoreMatchesToday = value;
                OnPropertyChanged("NoMoreMatchsToday");
            }
        }

        public bool MultiplexIsInProgress
        {
            get 
            {
                if (myDayInfo != null)
                {
                    if (myDayInfo.Matches.Count > 0)
                    {
                        return true;
                    }
                    else
                    {
                        return false;
                    }
                }
                return false;
            }
        }

        public void CheckMultiplexStatus()
        {
            OnPropertyChanged("MultiplexIsInProgress");
        }

        private void UpdatesPropertiesFromDayInfo()
        {
            if (myDayInfo != null &&
                myDayInfo.Matches != null &&
                myDayInfo.Matches.Count > 0)
            {
                myMatchsList.Clear();

                foreach (MatchInfo matchInfo in myDayInfo.Matches)
                {
                    //NotPlayed = 0,
                    //Playing = 1,
                    //Finished = 2
                    if (matchInfo.Status == 1)
                    {
                        int localTeamScore = matchInfo.Teams.LocalTeam.Score;
                        int visitorTeamScore = matchInfo.Teams.VisitorTeam.Score;
                        FontFamily localTeamFontFamily = App.CurrentApp.Resources["PhoneFontFamilyNormal"] as FontFamily;
                        FontFamily visitorTeamFontFamily = App.CurrentApp.Resources["PhoneFontFamilyNormal"] as FontFamily;
                        LogoInfo localTeamlogoInfo = null;
                        LogoInfo visitorTeamlogoInfo = null;
                        string localTeamName = null;
                        string visitorTeamName = null;
                        string localTeamShortName = null;
                        string visitorTeamShortName = null;
                        string localTeamLogo64Uri = null;
                        string visitorTeamLogo64Uri = null;
                        string liveUri = null;
                        string liveUriWifi = null;

                        if (localTeamScore > visitorTeamScore)
                        {
                            localTeamFontFamily = App.CurrentApp.Resources["PhoneFontFamilySemiBold"] as FontFamily;
                        }
                        else if (localTeamScore < visitorTeamScore)
                        {
                            visitorTeamFontFamily = App.CurrentApp.Resources["PhoneFontFamilySemiBold"] as FontFamily;
                        }

                        if (App.CurrentApp.MyLigue1.MyTeamsInfo != null)
                        {
                            if (App.CurrentApp.MyLigue1.MyTeamsInfo.ContainsKey(matchInfo.Teams.LocalTeam.Id) &&
                            App.CurrentApp.MyLigue1.MyTeamsInfo[matchInfo.Teams.LocalTeam.Id].Logos.Count > 0)
                            {
                                localTeamlogoInfo = App.CurrentApp.MyLigue1.MyTeamsInfo[matchInfo.Teams.LocalTeam.Id].Logos[0];
                            }

                            if (App.CurrentApp.MyLigue1.MyTeamsInfo.ContainsKey(matchInfo.Teams.VisitorTeam.Id) &&
                                App.CurrentApp.MyLigue1.MyTeamsInfo[matchInfo.Teams.VisitorTeam.Id].Logos.Count > 0)
                            {
                                visitorTeamlogoInfo = App.CurrentApp.MyLigue1.MyTeamsInfo[matchInfo.Teams.VisitorTeam.Id].Logos[0];
                            }

                            if (App.CurrentApp.MyLigue1.MyTeamsInfo.ContainsKey(matchInfo.Teams.LocalTeam.Id))
                            {
                                localTeamName = App.CurrentApp.MyLigue1.MyTeamsInfo[matchInfo.Teams.LocalTeam.Id].City;
                                localTeamShortName = App.CurrentApp.MyLigue1.MyTeamsInfo[matchInfo.Teams.LocalTeam.Id].ShortName;
                            }

                            if (App.CurrentApp.MyLigue1.MyTeamsInfo.ContainsKey(matchInfo.Teams.LocalTeam.Id))
                            {
                                visitorTeamName = App.CurrentApp.MyLigue1.MyTeamsInfo[matchInfo.Teams.VisitorTeam.Id].City;
                                visitorTeamShortName = App.CurrentApp.MyLigue1.MyTeamsInfo[matchInfo.Teams.VisitorTeam.Id].ShortName;
                            }
                        }

                        if (App.CurrentApp.MyLigue1.MyLightSettingsAction != null)
                        {
                            if (App.CurrentApp.MyLigue1.MyLightSettingsAction.Teams != null &&
                               localTeamlogoInfo != null)
                            {
                                localTeamLogo64Uri = App.CurrentApp.MyLigue1.MyLogoBaseUrl + "/" + localTeamlogoInfo.Name + "_64." + localTeamlogoInfo.Ext; //App.CurrentApp.MyLigue1.MyLogoBaseUrl + localTeamlogoInfo.Name + "_64." + localTeamlogoInfo.Ext;
                            }

                            if (App.CurrentApp.MyLigue1.MyLightSettingsAction.Teams != null &&
                                visitorTeamlogoInfo != null)
                            {
                                visitorTeamLogo64Uri = App.CurrentApp.MyLigue1.MyLogoBaseUrl + "/" + visitorTeamlogoInfo.Name + "_64." + visitorTeamlogoInfo.Ext;
                            }
                        }

                        if (matchInfo.Videos != null &&
                            matchInfo.Videos.Videos != null)
                        {
                            foreach (VideoInfo videoInfo in matchInfo.Videos.Videos)
                            {
                                if (String.Equals(videoInfo.Type, "L"))
                                {
                                    liveUri = videoInfo.Link;
                                    liveUriWifi = videoInfo.LinkWifi;
                                }
                            }
                        }

                        myMatchsList.Add(new MatchItemViewModel()
                        {
                            MatchId = matchInfo.Id,
                            LocalTeamScore = matchInfo.Teams.LocalTeam.Score.ToString(),
                            VisitorTeamScore = matchInfo.Teams.VisitorTeam.Score.ToString(),
                            LocalTeamName = localTeamName,
                            VisitorTeamName = visitorTeamName,
                            LocalTeamShortName = localTeamShortName,
                            VisitorTeamShortName = visitorTeamShortName,
                            LocalTeamLogo64Uri = localTeamLogo64Uri,
                            VisitorTeamLogo64Uri = visitorTeamLogo64Uri,
                            LocalTeamFontFamily = localTeamFontFamily,
                            VisitorTeamFontFamily = visitorTeamFontFamily,
                            Status = matchInfo.Status,
                            Live = matchInfo.Status,
                            LiveUri = liveUri,
                            LiveUriWifi = liveUriWifi
                        });
                    }
                }
            }
        }

        private void UpdatesPropertiesFromAllMatchAction()
        {
            foreach (MatchInfo matchInfo in myAllMatchAction.Matches.Matches)
            {
                //NotPlayed = 0,
                //Playing = 1,
                //Finished = 2
                if (matchInfo.Status == 1)
                {
                    int localTeamScore = -1; 
                    int visitorTeamScore = -1;
                    bool matchExists = false;
                    bool scoresNeedToBeUpdated = false;
                    FontFamily localTeamFontFamily = App.CurrentApp.Resources["PhoneFontFamilyNormal"] as FontFamily;
                    FontFamily visitorTeamFontFamily = App.CurrentApp.Resources["PhoneFontFamilyNormal"] as FontFamily;

                    if (matchInfo.Teams.LocalTeam.Id != 0 &&
                        matchInfo.Teams.VisitorTeam.Id != 0)
                    {
                        localTeamScore = matchInfo.Teams.LocalTeam.Score;
                        visitorTeamScore = matchInfo.Teams.VisitorTeam.Score;
                        scoresNeedToBeUpdated = true;
                    }
                    else
                    {
                        break;
                    }

                    if (scoresNeedToBeUpdated == true)
                    {
                        if (localTeamScore > visitorTeamScore)
                        {
                            localTeamFontFamily = App.CurrentApp.Resources["PhoneFontFamilySemiBold"] as FontFamily;
                        }
                        else if (localTeamScore < visitorTeamScore)
                        {
                            visitorTeamFontFamily = App.CurrentApp.Resources["PhoneFontFamilySemiBold"] as FontFamily;
                        }

                        //match already exists and needs to be updated
                        foreach (MatchItemViewModel item in myMatchsList)
                        {
                            if (item.MatchId == matchInfo.Id)
                            {
                                item.LocalTeamScore = localTeamScore.ToString();
                                item.LocalTeamFontFamily = localTeamFontFamily;
                                item.VisitorTeamScore = visitorTeamScore.ToString();
                                item.VisitorTeamFontFamily = visitorTeamFontFamily;
                                matchExists = true;
                                break;
                            }
                        }
                    }

                    //match does not exist and needs to be created
                    if (matchExists == false)
                    {
                        LogoInfo localTeamlogoInfo = null;
                        LogoInfo visitorTeamlogoInfo = null;
                        string localTeamName = null;
                        string visitorTeamName = null;
                        string localTeamShortName = null;
                        string visitorTeamShortName = null;
                        string localTeamLogo64Uri = null;
                        string visitorTeamLogo64Uri = null;
                        int live = 0;
                        string liveUri = null;
                        string liveUriWifi = null;

                        if (localTeamScore == -1)
                        {
                            localTeamScore = 0;
                        }
                        if (visitorTeamScore == -1)
                        {
                            visitorTeamScore = 0;
                        }

                        if (App.CurrentApp.MyLigue1.MyTeamsInfo != null)
                        {
                            if (App.CurrentApp.MyLigue1.MyTeamsInfo.ContainsKey(matchInfo.Teams.LocalTeam.Id) &&
                            App.CurrentApp.MyLigue1.MyTeamsInfo[matchInfo.Teams.LocalTeam.Id].Logos.Count > 0)
                            {
                                localTeamlogoInfo = App.CurrentApp.MyLigue1.MyTeamsInfo[matchInfo.Teams.LocalTeam.Id].Logos[0];
                            }

                            if (App.CurrentApp.MyLigue1.MyTeamsInfo.ContainsKey(matchInfo.Teams.VisitorTeam.Id) &&
                                App.CurrentApp.MyLigue1.MyTeamsInfo[matchInfo.Teams.VisitorTeam.Id].Logos.Count > 0)
                            {
                                visitorTeamlogoInfo = App.CurrentApp.MyLigue1.MyTeamsInfo[matchInfo.Teams.VisitorTeam.Id].Logos[0];
                            }

                            if (App.CurrentApp.MyLigue1.MyTeamsInfo.ContainsKey(matchInfo.Teams.LocalTeam.Id))
                            {
                                localTeamName = App.CurrentApp.MyLigue1.MyTeamsInfo[matchInfo.Teams.LocalTeam.Id].City;
                                localTeamShortName = App.CurrentApp.MyLigue1.MyTeamsInfo[matchInfo.Teams.LocalTeam.Id].ShortName;
                            }

                            if (App.CurrentApp.MyLigue1.MyTeamsInfo.ContainsKey(matchInfo.Teams.LocalTeam.Id))
                            {
                                visitorTeamName = App.CurrentApp.MyLigue1.MyTeamsInfo[matchInfo.Teams.VisitorTeam.Id].City;
                                visitorTeamShortName = App.CurrentApp.MyLigue1.MyTeamsInfo[matchInfo.Teams.VisitorTeam.Id].ShortName;
                            }
                        }

                        if (App.CurrentApp.MyLigue1.MyLightSettingsAction != null) //used to be MyInitinfoAction
                        {
                            if (App.CurrentApp.MyLigue1.MyLightSettingsAction.Teams != null &&
                               localTeamlogoInfo != null)
                            {
                                localTeamLogo64Uri = App.CurrentApp.MyLigue1.MyLogoBaseUrl + "/" + localTeamlogoInfo.Name + "_64." + localTeamlogoInfo.Ext;
                            }

                            if (App.CurrentApp.MyLigue1.MyLightSettingsAction.Teams != null &&
                                visitorTeamlogoInfo != null)
                            {
                                visitorTeamLogo64Uri = App.CurrentApp.MyLigue1.MyLogoBaseUrl + "/" + visitorTeamlogoInfo.Name + "_64." + visitorTeamlogoInfo.Ext;
                            }

                            if (PanoramaViewModel.Instance.MyLigue1.MyInitinfoAction.Matches != null &&
                                PanoramaViewModel.Instance.MyLigue1.MyInitinfoAction.Matches.Matches != null &&
                                PanoramaViewModel.Instance.MyLigue1.MyInitinfoAction.Matches.Matches.Count > 0)
                            {
                                if (myMatchsList.Count == 0)
                                {
                                    //start the muliplex
                                    MessageBox.Show(Localization.GetString("MultiplexHasStartedLabel"));
                                }

                                //retrieve live status
                                foreach (MatchInfo item in PanoramaViewModel.Instance.MyLigue1.MyInitinfoAction.Matches.Matches)
                                {
                                    if (matchInfo.Id == item.Id)
                                    {
                                        live = item.Status;
                                        break;
                                    }
                                }

                                if (matchInfo.Videos != null &&
                                    matchInfo.Videos.Videos != null)
                                {
                                    foreach (VideoInfo videoInfo in matchInfo.Videos.Videos)
                                    {
                                        if (String.Equals(videoInfo.Type, "L"))
                                        {
                                            liveUri = videoInfo.Link;
                                            liveUriWifi = videoInfo.LinkWifi;
                                        }
                                    }
                                }

                                myMatchsList.Add(new MatchItemViewModel()
                                {
                                    MatchId = matchInfo.Id,
                                    LocalTeamScore = matchInfo.Teams.LocalTeam.Score.ToString(),
                                    VisitorTeamScore = matchInfo.Teams.VisitorTeam.Score.ToString(),
                                    LocalTeamName = localTeamName,
                                    VisitorTeamName = visitorTeamName,
                                    LocalTeamShortName = localTeamShortName,
                                    VisitorTeamShortName = visitorTeamShortName,
                                    LocalTeamLogo64Uri = localTeamLogo64Uri,
                                    VisitorTeamLogo64Uri = visitorTeamLogo64Uri,
                                    LocalTeamFontFamily = localTeamFontFamily,
                                    VisitorTeamFontFamily = visitorTeamFontFamily,
                                    Status = matchInfo.Status,
                                    Live = live,
                                    LiveUri = liveUri,
                                    LiveUriWifi = liveUriWifi
                                });
                            }
                        }
                    }
                }
                else if(matchInfo.Status == 2)
                {
                    ObservableCollection<MatchItemViewModel> tempList = new ObservableCollection<MatchItemViewModel>();

                    //clone myMatchsList into tempList
                    foreach (MatchItemViewModel item in myMatchsList)
                    {
                        tempList.Add(item);
                    }

                    //remove match from myMatchsList
                    foreach (MatchItemViewModel item in tempList)
                    {
                        if (matchInfo.Id == item.MatchId)
                        {
                            if (myMatchsList.Contains(item))
                            {
                                myMatchsList.Remove(item);
                            }
                            break;
                        }
                    }
                }
            }
        }

        private void SelectionChangedCommand(MatchItemViewModel item)
        {
            //NotPlayed = 0,
            //Playing = 1,
            //Finished = 2
            int live = 0;
            if (item.Status == 0)
            {
                return;
            }
            else if (item.Status == 1)
            {
                live = item.Live;
            }
            Navigator.GoToPage(MatchInfoPageViewModel.GetUri(item.MatchId, live));
        }

        public void DayCalendarReceived(CalendarAction calendar)
        {
            if (MultiplexIsInProgress)
            {
                return;
            }

            List<CalendarDayInfo> matchs = calendar.Matches;
            if (matchs != null && matchs.Count == 1)
            {
                if (matchs[0].Day == App.CurrentApp.MyLigue1.CurrentDay)
                {
                    MatchInfo futureMatch = this.FutureMatch(matchs[0].Matches);

                    if (futureMatch != null)
                    {
                        MultiplexNoMatchLabel = FutureMatchLabel(futureMatch);
                        NoMoreMatchesToday = true;
                        return;
                    }

                    if (App.CurrentApp.MyLigue1.CurrentDay == 38)
                    {
                        MultiplexNoMatchLabel = Localization.GetString("MultiplexEndOfSeason");
                        NoMoreMatchesToday = true;
                    }
                    else
                    {
                        App.CurrentApp.MyLigue1.RequestCalendar(App.CurrentApp.MyLigue1.CurrentDay + 1);
                    }
                    return;
                }
                else if (matchs[0].Day == App.CurrentApp.MyLigue1.CurrentDay + 1)
                {
                    MatchInfo futureMatch = this.FutureMatch(matchs[0].Matches);

                    if (futureMatch != null)
                    {
                        MultiplexNoMatchLabel = FutureMatchLabel(futureMatch);
                        NoMoreMatchesToday = true;
                        return;
                    }
                    else
                    {
                        MultiplexNoMatchLabel = Localization.GetString("MultiplexEndOfSeason"); //Localization.GetString("MultiplexUnknownFutureMatch");
                        NoMoreMatchesToday = true;
                    }
                    return;
                }
            }

            MultiplexNoMatchLabel = "Service indisponible";
            NoMoreMatchesToday = true;
        }

        private string FutureMatchLabel(MatchInfo match)
        {
            string dateString = match.Date + " " + match.Hour;
            DateTime date;
            DateTime.TryParse(dateString, out date);

            string result = String.Format(Localization.GetString("MultiplexFutureMatch"), date.ToString("dddd"), date.ToShortDateString(), date.ToShortTimeString());

            return result;
        }

        private MatchInfo FutureMatch(List<MatchInfo> matchs)
        {
            MatchInfo futureMatch = null;
            DateTime futureMatchDate = DateTime.MaxValue;
            DateTime now = DateTime.Now;

            foreach (MatchInfo match in matchs)
            {
                string dateString = match.Date + " " + match.Hour;

                DateTime date;
                bool parsingSucceed = DateTime.TryParse(dateString, out date);

                if (parsingSucceed && date > now && date < futureMatchDate)
                {
                    futureMatch = match;
                    futureMatchDate = date;
                }
            }

            return futureMatch;
        }

        #region Command
        public ICommand OpenAccessRightsCommand { get; set; }

        public void OnOpenAccessRightsCommand(object param)
        {
            Navigator.GoToPage(AccessRightsPageViewModel.GetUri());
        }
        #endregion
    }
}

