// -------------------------------  Copyright ---------------------------------
//	This software has been developped by France Telecom, FT/RD/RD/MAPS/DVC/MSF
//
//	Copyright (c) France Telecom 2009-2010
//
// COPYRIGHT    : This file is the property of FRANCE TELECOM.
//                It cannot be copied, used, or modified without obtaining
//                an authorization from the authors or a mandated
//                member of FRANCE TELECOM.
//                If such an authorization is provided, any modified version or
//                copy of the software has to contain this header.
//
// WARRANTIES   : This software is made available by the authors in the  hope
//                that it will be useful, but without any warranty.
//                France Telecom is not liable for any consequence related to the
//                use of the provided software.
//
// AUTHORS      : France Telecom  / RD / MAPS / DVC / MSF
// ----------------------------------------------------------------------------
using System;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using Orange.SDK;
using System.Collections.ObjectModel;

namespace OrangeLigue1.ViewModels
{
    public class CalendarItemViewModel : ViewModel
    {
        private int day;
        public int Day
        {
            get { return day; }
            set
            {
                day = value;
                OnPropertyChanged("Day");
            }
        }

        public DayItemViewModel CurrentListItem
        {
            get { return null; }
            set
            {
                if (value != null)
                {
                    CalendarPageViewModel.Instance.SelectedDay = value.Day;
                    SelectionChangedCommand(value.Day);
                    OnPropertyChanged("CurrentListItem");
                }
            }
        }

        private SolidColorBrush dayLabelColor;
        public SolidColorBrush DayLabelColor
        {
            get { return dayLabelColor; }
            set { dayLabelColor = value; OnPropertyChanged("DayLabelColor"); }
        }

        /// <summary>
        /// List of days
        /// </summary>
        private ObservableCollection<DayItemViewModel> myDaysList = new ObservableCollection<DayItemViewModel>();
        public ObservableCollection<DayItemViewModel> MyDaysList
        {
            get { return myDaysList; }
            set
            {
                myDaysList = value;
                OnPropertyChanged("MyDaysList");
            }
        }

        #region command
        private void SelectionChangedCommand(int day)
        {
            Navigator.GoToPage(DayPageViewModel.GetUri(day));
        }
        #endregion

        #region wording
        public string DayNumberLabel
        {
            get
            {
                if (day == 1)
                {
                    return day.ToString() + "ère " + Localization.GetString("DayLabel");
                }
                else
                {
                    return day.ToString() + "e " + Localization.GetString("DayLabel");
                }
            }
        }
        #endregion
    }
}

