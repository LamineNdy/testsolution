// -------------------------------  Copyright ---------------------------------
//	This software has been developped by France Telecom, FT/RD/RD/MAPS/DVC/MSF
//
//	Copyright (c) France Telecom 2009-2010
//
// COPYRIGHT    : This file is the property of FRANCE TELECOM.
//                It cannot be copied, used, or modified without obtaining
//                an authorization from the authors or a mandated
//                member of FRANCE TELECOM.
//                If such an authorization is provided, any modified version or
//                copy of the software has to contain this header.
//
// WARRANTIES   : This software is made available by the authors in the  hope
//                that it will be useful, but without any warranty.
//                France Telecom is not liable for any consequence related to the
//                use of the provided software.
//
// AUTHORS      : France Telecom  / RD / MAPS / DVC / MSF
// ----------------------------------------------------------------------------
using System;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using Orange.SDK;
using Orange.SDK.Enablers.Ligue1;
using System.Collections.ObjectModel;
using System.Collections.Generic;

namespace OrangeLigue1.ViewModels
{
    public class RankingPageViewModel : ViewModel
    {
        #region "Singleton definition"

        private static RankingPageViewModel _instance;
        public static RankingPageViewModel Instance
        {
            get
            {
                if (_instance == null)
                {
                    _instance = new RankingPageViewModel();
                }
                return _instance;
            }
        }
        #endregion

        private RankingPageViewModel()
        {
            myTeamsAction = App.CurrentApp.MyLigue1.MyTeamsAction;
            App.CurrentApp.MyLigue1.TeamsRequestEnd += new EventHandler<TeamsAction>(myTeamsRequestEnd);
            UpdatesPropertiesFromTeamsAction();
        }

        /// <summary>
        /// Returns the URI of the Ranking page
        /// </summary>
        public static Uri GetUri(int rank)
        {
            return new Uri(string.Format("/Views/RankingPage.xaml?rank={0}", rank), UriKind.Relative);
        }

        private TeamsAction myTeamsAction;
        public TeamsAction MyTeamsAction
        {
            get { return myTeamsAction; }
            set
            {
                myTeamsAction = value;
                if (myTeamsAction != null)
                {
                    UpdatesPropertiesFromTeamsAction();
                    OnPropertyChanged("MyRankedTeamsList");
                }
            }
        }

        private int rank;
        public int Rank
        {
            get { return rank; }
            set { rank = value; OnPropertyChanged("Rank"); }
        }

        /// <summary>
        /// List of Ranked teams
        /// </summary>
        private ObservableCollection<RankingSectionItemViewModel> myRankedTeamsList = new ObservableCollection<RankingSectionItemViewModel>();
        public ObservableCollection<RankingSectionItemViewModel> MyRankedTeamsList
        {
            get { return myRankedTeamsList; }
            set
            {
                myRankedTeamsList = value;
                OnPropertyChanged("MyRankedTeamsList");
            }
        }

        /// <summary>
        /// Fill Ranked teams list property
        /// </summary>
        void UpdatesPropertiesFromTeamsAction()
        {
            myRankedTeamsList.Clear();

            if (App.CurrentApp.MyLigue1.MyTeamsInfo != null)
            {
                SolidColorBrush whiteColor = new SolidColorBrush(Colors.White);
                SolidColorBrush greenColor = new SolidColorBrush(Color.FromArgb(255, 0, 255, 0));
                SolidColorBrush darkgreenColor = new SolidColorBrush(Color.FromArgb(255, 0, 150, 15));
                SolidColorBrush darkgrayColor = new SolidColorBrush(Color.FromArgb(255, 110, 120, 120));
                SolidColorBrush lightgrayColor = new SolidColorBrush(Color.FromArgb(255, 170, 170, 170));
                SolidColorBrush redColor = new SolidColorBrush(Colors.Red);
                SolidColorBrush color;

                List<RankingSectionItemViewModel> rankingList = new List<RankingSectionItemViewModel>();

                foreach (KeyValuePair<int, TeamInfo> kvp in App.CurrentApp.MyLigue1.MyTeamsInfo)
	            {
                    TeamInfo teamInfo = kvp.Value;
                    LogoInfo logoInfo = null;

                    if (teamInfo != null &&
                        teamInfo.Logos != null &&
                        teamInfo.Logos.Count > 0)
                    {
                        logoInfo = teamInfo.Logos[0];
                    }

                    string logoUri = null;

                    if (App.CurrentApp.MyLigue1.MyLightSettingsAction != null &&
                        App.CurrentApp.MyLigue1.MyLightSettingsAction.Teams != null &&
                        logoInfo != null)
                    {
                        logoUri = App.CurrentApp.MyLigue1.MyLogoBaseUrl + "/" + logoInfo.Name + "_64." + logoInfo.Ext; //App.CurrentApp.MyLigue1.MyLogoBaseUrl + logoInfo.Name + "_64." + logoInfo.Ext;
                    }

                    color = whiteColor;

                    if (teamInfo.Rank <= 3) 
                    {
                        color = lightgrayColor;
                    }
                    else if (teamInfo.Rank == 4)
                    {
                        color = darkgrayColor;
                    }
                    else if (teamInfo.Rank >= 18)
                    {
                        color = redColor;
                    }

                    rankingList.Add(new RankingSectionItemViewModel()
                    {
                        Id = teamInfo.Id,
                        Rank = teamInfo.Rank,
                        Name = teamInfo.City,
                        Points = teamInfo.Stats.Points,
                        PlayedMatchs = teamInfo.Stats.PlayedMatchs,
                        WonMatchs = teamInfo.Stats.WonMatchs,
                        DrawMatchs = teamInfo.Stats.DrawMatchs,
                        LostMatchs = teamInfo.Stats.LostMatchs,
                        ScoredGoals = teamInfo.Stats.ScoredGoals,
                        ConcededGoals = teamInfo.Stats.ConcededGoals,
                        DiffGoals = teamInfo.Stats.GoalsDelta,
                        LogoUri = logoUri,
                        Color = color
                    });
                }

                rankingList.Sort(delegate(RankingSectionItemViewModel r1, RankingSectionItemViewModel r2) { return r1.Rank.CompareTo(r2.Rank); });

                foreach (RankingSectionItemViewModel item in rankingList)
                {
                    myRankedTeamsList.Add(item);
                }
            }
        }

        void myTeamsRequestEnd(object sender, TeamsAction e)
        {
            MyTeamsAction = e;
        }

        #region wording
        public string RankingPageTitleLabel
        {
            get
            {
                return Localization.GetString("RankingPageTitleLabel");
            }
        }

        public string PointsShortLabel
        {
            get
            {
                return Localization.GetString("PointsShortLabel");
            }
        }

        public string PlayedMatchsShortLabel
        {
            get
            {
                return Localization.GetString("PlayedMatchsShortLabel");
            }
        }

        public string WonMatchsShortLabel
        {
            get
            {
                return Localization.GetString("WonMatchsShortLabel");
            }
        }

        public string DrawMatchsShortLabel
        {
            get
            {
                return Localization.GetString("DrawMatchsShortLabel");
            }
        }

        public string LostMatchsShortLabel
        {
            get
            {
                return Localization.GetString("LostMatchsShortLabel");
            }
        }

        public string ScoredGoalsShortLabel
        {
            get
            {
                return Localization.GetString("ScoredGoalsShortLabel");
            }
        }

        public string ConcededGoalsShortLabel
        {
            get
            {
                return Localization.GetString("ConcededGoalsShortLabel");
            }
        }

        public string DiffGoalsShortLabel
        {
            get
            {
                return Localization.GetString("DiffGoalsShortLabel");
            }
        }
        #endregion
    }
}

