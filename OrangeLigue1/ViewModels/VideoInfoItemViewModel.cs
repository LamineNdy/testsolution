// -------------------------------  Copyright ---------------------------------
//	This software has been developped by France Telecom, FT/RD/RD/MAPS/DVC/MSF
//
//	Copyright (c) France Telecom 2009-2010
//
// COPYRIGHT    : This file is the property of FRANCE TELECOM.
//                It cannot be copied, used, or modified without obtaining
//                an authorization from the authors or a mandated
//                member of FRANCE TELECOM.
//                If such an authorization is provided, any modified version or
//                copy of the software has to contain this header.
//
// WARRANTIES   : This software is made available by the authors in the  hope
//                that it will be useful, but without any warranty.
//                France Telecom is not liable for any consequence related to the
//                use of the provided software.
//
// AUTHORS      : France Telecom  / RD / MAPS / DVC / MSF
// ----------------------------------------------------------------------------
using System;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using Orange.SDK;
using Orange.SDK.Tools;
using System.Windows.Media.Imaging;
using Kawagoe.Storage;

namespace OrangeLigue1.ViewModels
{
    public class VideoInfoItemViewModel : ViewModel
    {
        private string title;
        public string Title
        {
            get { return title; }
            set { title = value; OnPropertyChanged("Title"); }
        }

        private string link;
        public string Link
        {
            get { return link; }
            set { link = value; OnPropertyChanged("Link"); }
        }

        private string linkWifi;
        public string LinkWifi
        {
            get { return linkWifi; }
            set { linkWifi = value; OnPropertyChanged("LinkWifi"); }
        }

        private string thumbnailUri;
        public string ThumbnailUri
        {
            get { return thumbnailUri; }
            set { thumbnailUri = value; OnPropertyChanged("Thumbnail"); }
        }

        public ImageSource Thumbnail
        {
            get
            {
                return ImageCache.Default.Get(thumbnailUri);
            }
        }
    }
}

