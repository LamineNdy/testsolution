// -------------------------------  Copyright ---------------------------------
//	This software has been developped by France Telecom, FT/RD/RD/MAPS/DVC/MSF
//
//	Copyright (c) France Telecom 2009-2010
//
// COPYRIGHT    : This file is the property of FRANCE TELECOM.
//                It cannot be copied, used, or modified without obtaining
//                an authorization from the authors or a mandated
//                member of FRANCE TELECOM.
//                If such an authorization is provided, any modified version or
//                copy of the software has to contain this header.
//
// WARRANTIES   : This software is made available by the authors in the  hope
//                that it will be useful, but without any warranty.
//                France Telecom is not liable for any consequence related to the
//                use of the provided software.
//
// AUTHORS      : France Telecom  / RD / MAPS / DVC / MSF
// ----------------------------------------------------------------------------
using System;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using Orange.SDK;
using System.Collections.ObjectModel;
using Orange.SDK.Enablers.Ligue1;
using Orange.SDK.Tools;
using System.Globalization;
using OrangeLigue1.Tools;
using OrangeLigue1.Model;

namespace OrangeLigue1.ViewModels
{
    public class CalendarPageViewModel : ViewModel
    {
        #region "Singleton definition"

        private static CalendarPageViewModel _instance;
        public static CalendarPageViewModel Instance
        {
            get
            {
                if (_instance == null)
                {
                    _instance = new CalendarPageViewModel();
                }
                return _instance;
            }
        }
        #endregion

        #region "Event/Args"
        public event EventHandler<BackgroundRequestCompletedEventArgs> ConnectionErrorRequestEnd;
        #endregion

        private Ligue1 myLigue1;
        public Ligue1 MyLigue1
        {
            get { return myLigue1; }
            set { myLigue1 = value; OnPropertyChanged("MyLigue1"); }
        }
        
        private CalendarPageViewModel()
        {
            myLigue1 = App.CurrentApp.MyLigue1;
            MyCalendarAction = myLigue1.MyWholeCalendarAction;
            if (myLigue1 != null)
            {
                myLigue1.WholeCalendarRequestEnd += myWholeCalendarActionRequestEnds;
                myLigue1.ConnectionErrorRequestEnd += myLigue1_ConnectionErrorRequestEnd;
            }
        }

        /// <summary>
        /// Returns the URI of the Calendar page
        /// </summary>
        public static Uri GetUri(int day)
        {
            return new Uri(string.Format("/Views/CalendarPage.xaml?day={0}", day), UriKind.Relative);
        }

        private WholeCalendarAction myCalendarAction;
        public WholeCalendarAction MyCalendarAction
        {
            get { return myCalendarAction; }
            set
            {
                if (value != null)
                {
                    this.myCalendarAction = value;
                    UpdatesPropertiesFromCalendarAction();
                    OnPropertyChanged("MyCalendarItemsList");
                }
            }
        }

        private int selectedDay;
        public int SelectedDay
        {
            get { return selectedDay; }
            set { selectedDay = value; OnPropertyChanged("SelectedDay"); }
        }

        public CalendarItemViewModel CurrentListItem
        {
            get { return null; }
            set
            {
                if (value != null)
                {
                    SelectedDay = value.Day;
                    SelectionChangedCommand(selectedDay);
                    OnPropertyChanged("CurrentListItem");
                }
            }
        }

        /// <summary>
        /// List of Calendar items
        /// </summary>
        private ObservableCollection<CalendarItemViewModel> myCalendarItemsList = new ObservableCollection<CalendarItemViewModel>();
        public ObservableCollection<CalendarItemViewModel> MyCalendarItemsList
        {
            get { return myCalendarItemsList; }
            set
            {
                myCalendarItemsList = value;
                OnPropertyChanged("MyCalendarItemsList");
            }
        }
		
		private bool myPageIsLoaded = false;
        public bool MyPageIsLoaded
        {
            get { return myPageIsLoaded; }
            set { myPageIsLoaded = value; OnPropertyChanged("MyPageIsLoaded"); }
        }

        /// <summary>
        /// Fill MyCalendarItemsList property
        /// </summary>
        void UpdatesPropertiesFromCalendarAction()
        {
            myCalendarItemsList.Clear();

            if (myCalendarAction != null)
            {
                SolidColorBrush whiteColor = new SolidColorBrush(Colors.White);
                SolidColorBrush grayColor = new SolidColorBrush(Colors.Gray);
                SolidColorBrush color;

                foreach (CalendarDayInfo dayInfo in myCalendarAction.Matches)
                {
                    ObservableCollection<DayItemViewModel> calendarItemsList = new ObservableCollection<DayItemViewModel>();
                    string date = "";
                    color = grayColor;
                    DateTime dateTime = DateTime.MinValue;
                    if (dayInfo.Matches != null &&
                        dayInfo.Matches.Count > 0)
                    {
                        date = dayInfo.Matches[0].Date;
                        dateTime = DateTime.Parse(date).ToLocalTime();
                    }
                    calendarItemsList.Add(new DayItemViewModel()
                    {
                        Day = dayInfo.Day,
                        DayName = dateTime
                    }); //add first date

                    foreach (MatchInfo matchInfo in dayInfo.Matches)
                    {
                        if (String.Equals(date, matchInfo.Date) == false)
                        {
                            dateTime = DateTime.Parse(matchInfo.Date, CultureInfo.InvariantCulture).ToUniversalTime();
                            calendarItemsList.Add(new DayItemViewModel()
                            {
                                Day = dayInfo.Day,
                                DayName = dateTime
                            });
                        }
                        date = matchInfo.Date;
                    }
                    if (App.CurrentApp.MyLigue1.CurrentDay == dayInfo.Day)
                    {
                        color = whiteColor;
                    }
                    myCalendarItemsList.Add(new CalendarItemViewModel()
                    {
                        Day = dayInfo.Day,
                        MyDaysList = calendarItemsList,
                        DayLabelColor = color
                    });
                }
            }
        }

        void myLigue1_ConnectionErrorRequestEnd(object sender, BackgroundRequestCompletedEventArgs e)
        {
            if (ConnectionErrorRequestEnd != null)
            {
                ConnectionErrorRequestEnd(this, e);
            }
        }

        void myWholeCalendarActionRequestEnds(object sender, WholeCalendarAction e)
        {
            this.MyCalendarAction = e;

            MyPageIsLoaded = true;
        }

		#region Command
        public void Refresh()
        {
            myLigue1.RequestWholeCalendar();
        }

        private void SelectionChangedCommand(int day)
        {
            Navigator.GoToPage(DayPageViewModel.GetUri(day));
        }
        #endregion

        #region wording
        public string ApplicationTitleLabel
        {
            get
            {
                return Localization.GetString("ApplicationTitleLabel");
            }
        }

        public string CalendarLabel
        {
            get
            {
                return Localization.GetString("CalendarLabel");
            }
        }
        public string LoadingText
        {
            get
            {
                return Localization.GetString("MainPage_LoadingText");
            }
        }
        #endregion
    }
}

