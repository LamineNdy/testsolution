// -------------------------------  Copyright ---------------------------------
//	This software has been developped by France Telecom, FT/RD/RD/MAPS/DVC/MSF
//
//	Copyright (c) France Telecom 2009-2010
//
// COPYRIGHT    : This file is the property of FRANCE TELECOM.
//                It cannot be copied, used, or modified without obtaining
//                an authorization from the authors or a mandated
//                member of FRANCE TELECOM.
//                If such an authorization is provided, any modified version or
//                copy of the software has to contain this header.
//
// WARRANTIES   : This software is made available by the authors in the  hope
//                that it will be useful, but without any warranty.
//                France Telecom is not liable for any consequence related to the
//                use of the provided software.
//
// AUTHORS      : France Telecom  / RD / MAPS / DVC / MSF
// ----------------------------------------------------------------------------
using System;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using Orange.SDK;
using Orange.SDK.Tools;
using System.Collections.ObjectModel;

namespace OrangeLigue1.ViewModels
{
    public class PanoramaAboutSectionViewModel : ViewModel
    {
        private ObservableCollection<AboutActionItemViewModel> aboutActionList;
        public ObservableCollection<AboutActionItemViewModel> AboutActionList
        {
            get { return aboutActionList; }
            set { aboutActionList = value; OnPropertyChanged("AboutActionList"); }
        }

        public PanoramaAboutSectionViewModel()
        {
            aboutActionList = new ObservableCollection<AboutActionItemViewModel>();
            aboutActionList.Add(new AboutActionItemViewModel() 
            {
                ActionLabel = Localization.GetString("AlertMenuLabel"),
                ActionCommand = new DelegateCommand<object>(p => { Navigator.GoToPage(AlertsPageViewModel.GetUri()); }) 
            });
            aboutActionList.Add(new AboutActionItemViewModel() 
            {
                ActionLabel = Localization.GetString("AccessRightsMenuLabel"),
                ActionCommand = new DelegateCommand<object>(p => { Navigator.GoToPage(AccessRightsPageViewModel.GetUri()); }) 
            });
            aboutActionList.Add(new AboutActionItemViewModel()
            {
                ActionLabel = "", //EMPTY ACTION
                ActionCommand = new DelegateCommand<object>(p => { })
            });
            aboutActionList.Add(new AboutActionItemViewModel()
            {
                ActionLabel = Localization.GetString("AboutMenuLabel"),
                ActionCommand = new DelegateCommand<object>(p => { Navigator.GoToPage(AboutPageViewModel.GetUri()); })
            });
        }
    }
}

