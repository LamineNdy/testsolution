// -------------------------------  Copyright ---------------------------------
//	This software has been developped by France Telecom, FT/RD/RD/MAPS/DVC/MSF
//
//	Copyright (c) France Telecom 2009-2010
//
// COPYRIGHT    : This file is the property of FRANCE TELECOM.
//                It cannot be copied, used, or modified without obtaining
//                an authorization from the authors or a mandated
//                member of FRANCE TELECOM.
//                If such an authorization is provided, any modified version or
//                copy of the software has to contain this header.
//
// WARRANTIES   : This software is made available by the authors in the  hope
//                that it will be useful, but without any warranty.
//                France Telecom is not liable for any consequence related to the
//                use of the provided software.
//
// AUTHORS      : France Telecom  / RD / MAPS / DVC / MSF
// ----------------------------------------------------------------------------
using System;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using Orange.SDK;
using System.Windows.Media.Imaging;
using OrangeLigue1.Storage;
using Kawagoe.Storage;

namespace OrangeLigue1.ViewModels
{
    public class RankingSectionItemViewModel : ViewModel
    {
        private int id;
        public int Id
        {
            get { return id; }
            set { id = value; OnPropertyChanged("Id"); }
        }

        private int rank; 
        public int Rank 
        { 
            get { return rank; }
            set { rank = value; OnPropertyChanged("Rank"); } 
        }

        private SolidColorBrush color;
        public SolidColorBrush Color
        {
            get { return color; }
            set { color = value; OnPropertyChanged("Color"); }
        }

        private string name;
        public string Name 
        { 
            get { return name; } 
            set { name = value; OnPropertyChanged("Name"); }
        }

        private int points;
        public int Points 
        { 
            get { return points; }
            set { points = value; OnPropertyChanged("Points"); }
        }

        private int playedMatchs;
        public int PlayedMatchs 
        { 
            get { return playedMatchs; }
            set { playedMatchs = value; OnPropertyChanged("PlayedMatchs"); }
        }

        private int wonMatchs;
        public int WonMatchs
        {
            get { return wonMatchs; }
            set { wonMatchs = value; OnPropertyChanged("WonMatchs"); }
        }

        private int drawMatchs;
        public int DrawMatchs
        {
            get { return drawMatchs; }
            set { drawMatchs = value; OnPropertyChanged("DrawMatchs"); }
        }

        private int lostMatchs;
        public int LostMatchs
        {
            get { return lostMatchs; }
            set { lostMatchs = value; OnPropertyChanged("LostMatchs"); }
        }

        private int scoredGoals;
        public int ScoredGoals
        {
            get { return scoredGoals; }
            set { scoredGoals = value; OnPropertyChanged("ScoredGoals"); }
        }

        private int concededGoals;
        public int ConcededGoals
        {
            get { return concededGoals; }
            set { concededGoals = value; OnPropertyChanged("ConcededGoals"); }
        }

        private int diffGoals;
        public int DiffGoals
        {
            get { return diffGoals; }
            set { diffGoals = value; OnPropertyChanged("DiffGoals"); }
        }

        private string logoUri;
        public string LogoUri
        {
            get { return logoUri; }
            set { logoUri = value; OnPropertyChanged("Logo"); }
        }

        public ImageSource Logo
        {
            get
            {
                return Ligue1ImageCache.Default.Get(name + "_64", logoUri);
            }
        }

        public string PointsAndMatchs
        {
            get { return Points.ToString() + " " + PointsLabel + " en " + PlayedMatchs.ToString() + " " + PlayedMatchsLabel; }
        }

        #region wording
        public string PointsLabel
        {
            get
            {
                return Localization.GetString("PointsLabel");
            }
        }

        public string PlayedMatchsLabel
        {
            get
            {
                return Localization.GetString("PlayedMatchsLabel");
            }
        }
        #endregion
    }
}

