// -------------------------------  Copyright ---------------------------------
//	This software has been developped by France Telecom, FT/RD/RD/MAPS/DVC/MSF
//
//	Copyright (c) France Telecom 2009-2010
//
// COPYRIGHT    : This file is the property of FRANCE TELECOM.
//                It cannot be copied, used, or modified without obtaining
//                an authorization from the authors or a mandated
//                member of FRANCE TELECOM.
//                If such an authorization is provided, any modified version or
//                copy of the software has to contain this header.
//
// WARRANTIES   : This software is made available by the authors in the  hope
//                that it will be useful, but without any warranty.
//                France Telecom is not liable for any consequence related to the
//                use of the provided software.
//
// AUTHORS      : France Telecom  / RD / MAPS / DVC / MSF
// ----------------------------------------------------------------------------
using System;
using Orange.SDK;
using System.Windows;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using Orange.SDK.Enablers.Ligue1;
using System.Windows.Controls;
using System.Windows.Input;
using Orange.SDK.Tools;
using Orange.SDK.Enablers;

namespace OrangeLigue1.ViewModels
{
    public class AlertsPageViewModel : ViewModel
    {
        private bool alertsAreActivated;
        private bool amateurModeSelected;
        private ObservableCollection<AlertsClub> alertClubs;
        private ObservableCollection<AlertsClub> selectedClubs = new ObservableCollection<AlertsClub>();
        private bool notifSettingsRetrieved;
        private bool initialAlertsAreActivated;
        private bool initialAmateurModeSelected;
        private List<string> initialTeamsIds;
        public ListBox listBox { get; set; }

        public AlertsPageViewModel()
        {
            
        }

        public void OnNavigatedFrom()
        {
            App.CurrentApp.MyLigue1.PushSettingsRequestEnd -= PushSettingsRequestEnd;
        }

        public void OnNavigatedTo()
        {
            alertsAreActivated = false;
            notifSettingsRetrieved = false;

            List<TeamInfo> lists = App.CurrentApp.MyLigue1.MyLightSettingsAction.Teams.Teams;
            ObservableCollection<AlertsClub> clubs = new ObservableCollection<AlertsClub>();
            foreach (TeamInfo teamInfo in lists)
            {
                AlertsClub club = new AlertsClub { name = teamInfo.Name, teamId = teamInfo.Id.ToString() };
                clubs.Add(club);
            }
            AlertClubs = clubs;

            App.CurrentApp.MyLigue1.PushSettingsRequestEnd += PushSettingsRequestEnd;

            App.CurrentApp.MyLigue1.RequestPushSettings(Ligue1Enabler.NotifSettingsAction.NotifSettingsActionGet, 0, null);

            refresh();
        }

        private void PushSettingsRequestEnd(object sender, PushSettingsAction e)
        {
            int respStatus = e.RespStatus;
            if (respStatus <= 0)
            {
                PushSettingsInfos infos = e.Infos;
                if (infos != null)
                {
                    int action = infos.action;

                    if (action == 2)
                    {
                        initialAlertsAreActivated = true;
                        initialAmateurModeSelected = false;
                    }
                    else if (action == 1)
                    {
                        initialAlertsAreActivated = true;
                        initialAmateurModeSelected = true;
                    }
                    else
                    {
                        initialAlertsAreActivated = false;
                        initialAmateurModeSelected = true;
                    }

                    string[] teamsIds = infos.teams.Split(';');
                    initialTeamsIds = new List<string>();
                    for (int i = 0; i < teamsIds.Length; i++)
                    {
                        string teamId = teamsIds[i];
                        initialTeamsIds.Add(teamId);
                    }

                    notifSettingsRetrieved = true;

                    refresh();
                }
            }
            else
            {
                string message = e.Message;

                if (string.IsNullOrEmpty(message))
                    message = Localization.GetString("GenericError");

                MessageBox.Show(message);
                Navigator.GoBack();
            }
        }

        public void refresh()
        {
            AlertsAreActivated = initialAlertsAreActivated;
            AmateurModeSelected = initialAmateurModeSelected;

            if (notifSettingsRetrieved)
            {
                bool clubShouldBeDisabled = initialTeamsIds.Count >= 2;

                ObservableCollection<object> selectedItems = (ObservableCollection<object>)listBox.SelectedItems;
                selectedItems.Clear();

                foreach (AlertsClub club in AlertClubs)
                {
                    club.Selected = initialTeamsIds.Contains(club.teamId);
                    club.Enabled = !clubShouldBeDisabled || club.Selected;
                    if (club.Selected)
                    {
                        selectedItems.Add(club);
                    }
                }
            }
        }

        public void OnValidate()
        {
            if (notifSettingsRetrieved)
            {
                List<string> teamsId = new List<string>();
                Ligue1Enabler.NotifSettingsSubscription subscription = Ligue1Enabler.NotifSettingsSubscription.NotifSettingsSubscriptionDeactivated;

                if (AlertsAreActivated && AmateurModeSelected)
                {
                    subscription = Ligue1Enabler.NotifSettingsSubscription.NotifSettingsSubscriptionLightMode;
                }
                else if (AlertsAreActivated && SupporterModeSelected)
                {
                    subscription = Ligue1Enabler.NotifSettingsSubscription.NotifSettingsSubscriptionExpertMode;

                    foreach (AlertsClub club in AlertClubs)
                    {
                        if (club.Selected)
                        {
                            teamsId.Add(club.teamId);
                        }
                    }
                }

                App.CurrentApp.MyLigue1.RequestPushSettings(Ligue1Enabler.NotifSettingsAction.NotifSettingsActionSet, subscription, teamsId);

                Navigator.GoBack();
            }
        }

        public void OnCancel()
        {
            if (notifSettingsRetrieved)
            {
                refresh();
            }
        }

        public void ClubsListSelectionChanged(ListBox list)
        {
            ObservableCollection<object> selectedItems = (ObservableCollection<object>)list.SelectedItems;

            foreach (AlertsClub item in selectedItems)
            {
                if (!selectedClubs.Contains(item))
                {
                    item.Selected = true;
                }
            }
            foreach (AlertsClub item in selectedClubs)
            {
                if (!selectedItems.Contains(item))
                {
                    item.Selected = false;
                }
            }
            selectedClubs.Clear();
            foreach (AlertsClub item in selectedItems)
            {
                selectedClubs.Add(item);
            }

            OnPropertyChanged("AlertsModeSupporterTitle");

            if (selectedItems.Count == 2)
            {
                foreach (AlertsClub club in alertClubs)
                {
                    if (!selectedClubs.Contains(club))
                    {
                        club.Enabled = false;
                    }
                }
            }
            else if (selectedItems.Count == 1)
            {
                foreach (AlertsClub club in alertClubs)
                {
                    club.Enabled = true;
                }
            }
        }

        #region DataBindings
        public bool AlertsAreActivated
        {
            get
            {
                return alertsAreActivated;
            }
            set
            {
                alertsAreActivated = value;
                OnPropertyChanged("LoadingTextBlockVisible");
                OnPropertyChanged("LayoutAlertsModeVisible");
                OnPropertyChanged("LayoutAlertActivationVisible");
                OnPropertyChanged("ActivationStatusLabel");
                OnPropertyChanged("AlertsAreActivated");
            }
        }

        public bool LoadingAppBarVisible
        {
            get
            {
                return this.notifSettingsRetrieved ? false : true;
            }
        }

        public Visibility LoadingTextBlockVisible
        {
            get
            {
                return this.notifSettingsRetrieved ? Visibility.Collapsed : Visibility.Visible;
            }
        }

        public Visibility LayoutAlertActivationVisible
        {
            get
            {
                return this.notifSettingsRetrieved ? Visibility.Visible : Visibility.Collapsed;
            }
        }

        public Visibility LayoutAlertsModeVisible
        {
            get
            {
                return this.AlertsAreActivated ? Visibility.Visible : Visibility.Collapsed;
            }
        }

        public ObservableCollection<AlertsClub> AlertClubs
        {
            get
            {
                return alertClubs;
            }
            set
            {
                alertClubs = value;
                OnPropertyChanged("AlertClubs");
            }
        }

        public bool AmateurModeSelected
        {
            get
            {
                return amateurModeSelected;
            }
            set
            {
                amateurModeSelected = value;
                OnPropertyChanged("AmateurModeSelected");
                OnPropertyChanged("SupporterModeSelected");
            }
        }

        public bool SupporterModeSelected
        {
            get
            {
                return !amateurModeSelected;
            }
        }
        #endregion

        #region wording
        public string ApplicationTitleLabel
        {
            get
            {
                return Localization.GetString("ApplicationTitleLabel");
            }
        }

        public string AlertMenuLabel
        {
            get
            {
                return Localization.GetString("AlertMenuTitle");
            }
        }

        public string ActivateAlertsLabel
        {
            get
            {
                return Localization.GetString("ActivateAlertsLabel");
            }
        }

        public string ActivationStatusLabel
        {
            get
            {
                if (AlertsAreActivated == true)
                {
                    return Localization.GetString("ActivatedLabel");
                }
                else
                {
                    return Localization.GetString("UnactivatedLabel");
                }
            }
        }

        public string AlertsSwitchSubtitle
        {
            get
            {
                return Localization.GetString("AlertsSwitchSubtitle");
            }
        }

        public string AlertsModeAmateurTitle
        {
            get
            {
                return Localization.GetString("AlertsModeAmateurTitle");
            }
        }

        public string AlertsModeAmateurSubtitle
        {
            get
            {
                return Localization.GetString("AlertsModeAmateurSubtitle");
            }
        }

        public string AlertsModeSupporterTitle
        {
            get
            {
                return Localization.GetString("AlertsModeSupporterTitle") + " (" + selectedClubs.Count.ToString() + "/2)";
            }
        }

        public string AlertsModeSupporterSubtitle
        {
            get
            {
                return Localization.GetString("AlertsModeSupporterSubtitle");
            }
        }

        public string AlertsLoadingText
        {
            get
            {
                return Localization.GetString("AlertsLoadingText");
            }
        }

        #endregion

        public static Uri GetUri()
        {
            return new Uri("/Views/AlertsPage.xaml", UriKind.Relative);
        }
    }

    public class AlertsClub : ViewModel
    {
        public string teamId { get; set; }
        public string name { get; set; }

        private bool selected = false;
        public bool Selected
        {
            get
            {
                return selected;
            }
            set
            {
                selected = value;
                OnPropertyChanged("Selected");
            }
        }

        private bool enabled = true;
        public bool Enabled
        {
            get
            {
                return enabled;
            }
            set
            {
                enabled = value;
                OnPropertyChanged("Enabled");
                OnPropertyChanged("Opacity");
            }
        }

        public double Opacity
        {
            get
            {
                return enabled ? 1 : 0.5;
            }
        }
    }
}

