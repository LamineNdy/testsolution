// -------------------------------  Copyright ---------------------------------
//	This software has been developped by France Telecom, FT/RD/RD/MAPS/DVC/MSF
//
//	Copyright (c) France Telecom 2009-2010
//
// COPYRIGHT    : This file is the property of FRANCE TELECOM.
//                It cannot be copied, used, or modified without obtaining
//                an authorization from the authors or a mandated
//                member of FRANCE TELECOM.
//                If such an authorization is provided, any modified version or
//                copy of the software has to contain this header.
//
// WARRANTIES   : This software is made available by the authors in the  hope
//                that it will be useful, but without any warranty.
//                France Telecom is not liable for any consequence related to the
//                use of the provided software.
//
// AUTHORS      : France Telecom  / RD / MAPS / DVC / MSF
// ----------------------------------------------------------------------------
using System;
using System.Windows.Input;
using Microsoft.Phone.Tasks;
using Orange.SDK;
using Orange.SDK.Tools;
using OrangeLigue1.Model;
using System.Windows.Media.Imaging;
using System.Globalization;
using Orange.SDK.Enablers.Ligue1;

namespace OrangeLigue1.ViewModels
{
    public class NewsInfoPageViewModel : ViewModel
    {
        /// <summary>
        /// Initializes a new <see cref="NewsInfoPageViewModel"/> instance.
        /// </summary>
        public NewsInfoPageViewModel()
        {
            myDaynewsAction = App.CurrentApp.MyLigue1.MyDayNewsAction;
        }

        private DaynewsAction myDaynewsAction;
        public DaynewsAction MyDaynewsAction
        {
            get { return myDaynewsAction; }
            set
            {
                myDaynewsAction = value;
                if (myDaynewsAction != null)
                {
                    OnPropertyChanged("MyInitwidgetAction");
                }
            }
        }

        /// <summary>
        /// Returns the URI of the Article page for the specified article in the specified category.
        /// </summary>
        public static Uri GetUri(int id)
        {
            return new Uri(string.Format("/Views/NewsInfoPage.xaml?article={0}", id), UriKind.Relative);
        }

        public NewsInfo GetNewsInfoFromNewsId(string id)
        {
            foreach (NewsInfo newsInfo in myDaynewsAction.DayNews.NewsList)
            {
                if (String.Equals(newsInfo.Id.ToString(), id))
                {
                    return newsInfo;
                }
            }
            return null;
        }

		public DateTime TimeStamp
		{
			get
            {
                if (CurrentNewsInfo == null)
                {
                    return DateTime.Now;
                }
                return DateTime.Parse(_currentNewsInfo.Date + " " + _currentNewsInfo.Hour, CultureInfo.InvariantCulture).ToUniversalTime();
			}
		}

        private NewsInfo _currentNewsInfo;
        public NewsInfo CurrentNewsInfo
        {
            get { return _currentNewsInfo; }
            set
            {
                _currentNewsInfo = value; 
                OnPropertyChanged("CurrentNewsInfo"); 
                OnPropertyChanged("PageIndicator");
                OnPropertyChanged("TimeStamp");
            }
        }

		/// <summary>
        /// The zero-based index of the article in the category, or -1 if unknown.
        /// </summary>
        public int IndexOfArticle
        {
            get
            {
                if (_currentNewsInfo == null)
                {
                    return -1;
                }
                int indexOfNewsInfo = myDaynewsAction.DayNews.NewsList.IndexOf(_currentNewsInfo);
                if (indexOfNewsInfo < 0 || indexOfNewsInfo >= myDaynewsAction.DayNews.NewsList.Count)
                {
                    return -1;
                }
                return indexOfNewsInfo;
            }
        }
		
		/// <summary>
        /// Indicates the position of the article in its category.
        /// </summary>
        public string PageIndicator
		{
			get
			{
                if (_currentNewsInfo == null)
				{
                    return "";
				}
                int indexOfArticle = IndexOfArticle;
                if (indexOfArticle < 0)
				{
                    return "";
				}
                return string.Format("{0}/{1}", (indexOfArticle + 1), myDaynewsAction.DayNews.NewsList.Count);
			}
		}

        public String NewsInfoPageTitle
        {
            get
            {
                return Localization.GetString("NewsInfoPageTitle");
            }
        }

        /// <summary>
        /// Opens the previous article.
        /// Does nothing if there is no previous article.
        /// </summary>
        public void GoToPreviousArticle()
        {
            if (_currentNewsInfo == null)
            {
                return;
            }
            int articleCount = myDaynewsAction.DayNews.NewsList.Count;
            if (articleCount <= 1)
            {
                return;
            }
            int indexOfArticle = IndexOfArticle;
            int nextIndex;
            if (indexOfArticle <= 0)
            {
                nextIndex = articleCount - 1;
            }
            else
            {
                nextIndex = Math.Min(indexOfArticle - 1, articleCount - 1);
            }
            CurrentNewsInfo = myDaynewsAction.DayNews.NewsList[nextIndex];
        }

        /// <summary>
        /// Opens the next article.
        /// Does nothing if there is no next article.
        /// </summary>
        public void GoToNextArticle()
        {
            if (_currentNewsInfo == null)
            {
                return;
            }
            int articleCount = myDaynewsAction.DayNews.NewsList.Count;
            if (articleCount <= 1)
            {
                return;
            }
            int indexOfArticle = IndexOfArticle;
            int nextIndex;
            if (indexOfArticle < 0 || indexOfArticle >= articleCount - 1)
            {
                nextIndex = 0;
            }
            else
            {
                nextIndex = indexOfArticle + 1;
            }
            CurrentNewsInfo = myDaynewsAction.DayNews.NewsList[nextIndex];
        }
    }
}

