// -------------------------------  Copyright ---------------------------------
//	This software has been developped by France Telecom, FT/RD/RD/MAPS/DVC/MSF
//
//	Copyright (c) France Telecom 2009-2010
//
// COPYRIGHT    : This file is the property of FRANCE TELECOM.
//                It cannot be copied, used, or modified without obtaining
//                an authorization from the authors or a mandated
//                member of FRANCE TELECOM.
//                If such an authorization is provided, any modified version or
//                copy of the software has to contain this header.
//
// WARRANTIES   : This software is made available by the authors in the  hope
//                that it will be useful, but without any warranty.
//                France Telecom is not liable for any consequence related to the
//                use of the provided software.
//
// AUTHORS      : France Telecom  / RD / MAPS / DVC / MSF
// ----------------------------------------------------------------------------
using System;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using Orange.SDK;
using System.Collections.ObjectModel;
using OrangeLigue1.Model;
using Orange.SDK.Enablers.Ligue1;
using Orange.SDK.Tools;
using System.Collections.Generic;
using System.Diagnostics;

namespace OrangeLigue1.ViewModels
{
    public class PanoramaRankingSectionViewModel : ViewModel
    {
        private TeamsAction myTeamsAction;
        public TeamsAction MyTeamsAction
        {
            get { return myTeamsAction; }
            set
            {
                myTeamsAction = value;
                if (myTeamsAction != null)
                {
                    UpdatesPropertiesFromTeamsAction();
                    OnPropertyChanged("MyRankedTeamsList");
                }
            }
        }

        /// <summary>
        /// List of Ranked teams
        /// </summary>
        private ObservableCollection<RankingSectionItemViewModel> myRankedTeamsList = new ObservableCollection<RankingSectionItemViewModel>();
        public ObservableCollection<RankingSectionItemViewModel> MyRankedTeamsList
        {
            get { return myRankedTeamsList; }
            set
            {
                myRankedTeamsList = value;
                OnPropertyChanged("MyRankedTeamsList");
            }
        }

        public RankingSectionItemViewModel CurrentListItem
        {
            get { return null; }
            set
            {
                if (value != null)
                {
                    SelectionChangedCommand(value);
                    OnPropertyChanged("CurrentListItem");
                }
            }
        }

        /// <summary>
        /// Fill Ranked teams list property
        /// </summary>
        void UpdatesPropertiesFromTeamsAction()
        {
            myRankedTeamsList.Clear();

            if (App.CurrentApp.MyLigue1.MyTeamsInfo != null)
            {
                SolidColorBrush whiteColor = new SolidColorBrush(Colors.White);
                SolidColorBrush greenColor = new SolidColorBrush(Color.FromArgb(255, 0, 255, 0));
                SolidColorBrush darkgreenColor = new SolidColorBrush(Color.FromArgb(255, 0, 150, 15));
                SolidColorBrush darkgrayColor = new SolidColorBrush(Color.FromArgb(255, 110, 120, 120));
                SolidColorBrush lightgrayColor = new SolidColorBrush(Color.FromArgb(255, 170, 170, 170));
                SolidColorBrush redColor = new SolidColorBrush(Colors.Red);
                SolidColorBrush color;

                List<RankingSectionItemViewModel> rankingList = new List<RankingSectionItemViewModel>();

                foreach (KeyValuePair<int, TeamInfo> kvp in App.CurrentApp.MyLigue1.MyTeamsInfo)
	            {
                    TeamInfo teamInfo = kvp.Value;
                    LogoInfo logoInfo = null;

                    if (teamInfo != null &&
                        teamInfo.Logos != null &&
                        teamInfo.Logos.Count > 0)
                    {
                        logoInfo = teamInfo.Logos[0];
                    }

                    string logoUri = null;

                    if (App.CurrentApp.MyLigue1.MyLightSettingsAction != null &&
                        App.CurrentApp.MyLigue1.MyLightSettingsAction.Teams != null &&
                        logoInfo != null)
                    {
                        logoUri = App.CurrentApp.MyLigue1.MyLogoBaseUrl + "/" + logoInfo.Name + "_64." + logoInfo.Ext;
                        //Debug.WriteLine(logoUri);
                    }

                    color = whiteColor;

                    if (teamInfo.Rank <= 3)
                    {
                        color = lightgrayColor;
                    }
                    else if (teamInfo.Rank == 4)
                    {
                        color = darkgrayColor;
                    }
                    else if (teamInfo.Rank >= 18)
                    {
                        color = redColor;
                    }

                    rankingList.Add(new RankingSectionItemViewModel()
                    {
                        Id = teamInfo.Id,
                        Rank = teamInfo.Rank,
                        Name = teamInfo.City,
                        Points = teamInfo.Stats.Points,
                        PlayedMatchs = teamInfo.Stats.PlayedMatchs,
                        LogoUri = logoUri,
                        Color = color
                    });
                }

                rankingList.Sort(delegate(RankingSectionItemViewModel r1, RankingSectionItemViewModel r2) { return r1.Rank.CompareTo(r2.Rank); });

                foreach (RankingSectionItemViewModel item in rankingList)
                {
                    myRankedTeamsList.Add(item);
                }
            }
        }

        #region Command

        private void SelectionChangedCommand(RankingSectionItemViewModel item)
        {
            Navigator.GoToPage(RankingPageViewModel.GetUri(item.Rank));
        }

        #endregion
    }
}

