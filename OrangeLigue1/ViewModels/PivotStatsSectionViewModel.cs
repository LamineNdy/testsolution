// -------------------------------  Copyright ---------------------------------
//	This software has been developped by France Telecom, FT/RD/RD/MAPS/DVC/MSF
//
//	Copyright (c) France Telecom 2009-2010
//
// COPYRIGHT    : This file is the property of FRANCE TELECOM.
//                It cannot be copied, used, or modified without obtaining
//                an authorization from the authors or a mandated
//                member of FRANCE TELECOM.
//                If such an authorization is provided, any modified version or
//                copy of the software has to contain this header.
//
// WARRANTIES   : This software is made available by the authors in the  hope
//                that it will be useful, but without any warranty.
//                France Telecom is not liable for any consequence related to the
//                use of the provided software.
//
// AUTHORS      : France Telecom  / RD / MAPS / DVC / MSF
// ----------------------------------------------------------------------------
using System;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using Orange.SDK;
using Orange.SDK.Enablers.Ligue1;
using System.Windows.Media.Imaging;
using OrangeLigue1.Storage;

namespace OrangeLigue1.ViewModels
{
    public class PivotStatsSectionViewModel : ViewModel
    {
        private MatchAction myMatchAction;
        public MatchAction MyMatchAction
        {
            get { return myMatchAction; }
            set
            {
                myMatchAction = value;
                if (myMatchAction != null)
                {
                    UpdatesPropertiesFromMatchAction();
                }
            }
        }

        private MatchInfo myMatchInfo;
        public MatchInfo MyMatchInfo
        {
            get { return myMatchInfo; }
            set
            {
                myMatchInfo = value;
                if (myMatchInfo != null)
                {
                    UpdatesPropertiesFromMatchInfo();
                }
            }
        }

        //
        //  local team name
        //
        private string localTeamName;
        public string LocalTeamName
        {
            get { return localTeamName; }
            set { localTeamName = value; OnPropertyChanged("LocalTeamName"); }
        }

        //
        //  visiror team name
        //
        private string visitorTeamName;
        public string VisitorTeamName
        {
            get { return visitorTeamName; }
            set { visitorTeamName = value; OnPropertyChanged("VisitorTeamName"); }
        }

        //
        //  possession
        //
        private int localTeamPossession;
        public int LocalTeamPossession
        {
            get { return localTeamPossession; }     
            set { localTeamPossession = value; OnPropertyChanged("LocalTeamPossession"); }
        }

        private int visitorTeamPossession;
        public int VisitorTeamPossession
        {
            get { return visitorTeamPossession; }
            set { visitorTeamPossession = value; OnPropertyChanged("VisitorTeamPossession"); }
        }

        //
        //  corners
        //
        private int localTeamCorners;
        public int LocalTeamCorners
        {
            get { return localTeamCorners; }
            set { localTeamCorners = value; OnPropertyChanged("LocalTeamCorners"); }
        }

        private int visitorTeamCorners;
        public int VisitorTeamCorners
        {
            get { return visitorTeamCorners; }
            set { visitorTeamCorners = value; OnPropertyChanged("VisitorTeamCorners"); }
        }

        //
        //  fouls
        //
        private int localTeamFouls;
        public int LocalTeamFouls
        {
            get { return localTeamFouls; }
            set { localTeamFouls = value; OnPropertyChanged("LocalTeamFouls"); }
        }

        private int visitorTeamFouls;
        public int VisitorTeamFouls
        {
            get { return visitorTeamFouls; }
            set { visitorTeamFouls = value; OnPropertyChanged("VisitorTeamFouls"); }
        }


        //
        //  shots in
        //
        private int localTeamShotsIn;
        public int LocalTeamShotsIn
        {
            get { return localTeamShotsIn; }
            set { localTeamShotsIn = value; OnPropertyChanged("LocalTeamShotsIn"); }
        }

        private int visitorTeamShotsIn;
        public int VisitorTeamShotsIn
        {
            get { return visitorTeamShotsIn; }
            set { visitorTeamShotsIn = value; OnPropertyChanged("VisitorTeamShotsIn"); }
        }

        //
        //  shots out
        //
        private int localTeamShotsOut;
        public int LocalTeamShotsOut
        {
            get { return localTeamShotsOut; }
            set { localTeamShotsOut = value; OnPropertyChanged("LocalTeamShotsOut"); }
        }

        private int visitorTeamShotsOut;
        public int VisitorTeamShotsOut
        {
            get { return visitorTeamShotsOut; }
            set { visitorTeamShotsOut = value; OnPropertyChanged("VisitorTeamShotsOut"); }
        }

        //
        //  offsides
        //
        private int localTeamOffsides;
        public int LocalTeamOffsides
        {
            get { return localTeamOffsides; }
            set { localTeamOffsides = value; OnPropertyChanged("LocalTeamOffsides"); }
        }

        private int visitorTeamOffsides;
        public int VisitorTeamOffsides
        {
            get { return visitorTeamOffsides; }
            set { visitorTeamOffsides = value; OnPropertyChanged("VisitorTeamOffsides"); }
        }

        //
        //  free kicks
        //
        private int localTeamFreeKicks;
        public int LocalTeamFreeKicks
        {
            get { return localTeamFreeKicks; }
            set { localTeamFreeKicks = value; OnPropertyChanged("LocalTeamFreeKicks"); }
        }

        private int visitorTeamFreeKicks;
        public int VisitorTeamFreeKicks
        {
            get { return visitorTeamFreeKicks; }
            set { visitorTeamFreeKicks = value; OnPropertyChanged("VisitorTeamFreeKicks"); }
        }

        //
        //  yellow cards
        //
        private int localTeamYellowCards;
        public int LocalTeamYellowCards
        {
            get { return localTeamYellowCards; }
            set { localTeamYellowCards = value; OnPropertyChanged("LocalTeamYellowCards"); }
        }

        private int visitorTeamYellowCards;
        public int VisitorTeamYellowCards
        {
            get { return visitorTeamYellowCards; }
            set { visitorTeamYellowCards = value; OnPropertyChanged("VisitorTeamYellowCards"); }
        }

        //
        //  red cards
        //
        private int localTeamRedCards;
        public int LocalTeamRedCards
        {
            get { return localTeamRedCards; }
            set { localTeamRedCards = value; OnPropertyChanged("LocalTeamRedCards"); }
        }

        private int visitorTeamRedCards;
        public int VisitorTeamRedCards
        {
            get { return visitorTeamRedCards; }
            set { visitorTeamRedCards = value; OnPropertyChanged("VisitorTeamRedCards"); }
        }

        private string localTeamLogo64Uri;
        public string LocalTeamLogo64Uri
        {
            get { return localTeamLogo64Uri; }
            set { localTeamLogo64Uri = value; OnPropertyChanged("LocalTeam64Logo"); }
        }

        public ImageSource LocalTeam64Logo
        {
            get
            {
                return Ligue1ImageCache.Default.Get(localTeamName + "_64" , localTeamLogo64Uri);
            }
        }

        private string visitorTeamLogo64Uri;
        public string VisitorTeamLogo64Uri
        {
            get { return visitorTeamLogo64Uri; }
            set { visitorTeamLogo64Uri = value; OnPropertyChanged("VisitorTeam64Logo"); }
        }

        public ImageSource VisitorTeam64Logo
        {
            get
            {
                return Ligue1ImageCache.Default.Get(visitorTeamName + "_64", visitorTeamLogo64Uri);
            }
        }

        private void UpdatesPropertiesFromMatchAction()
        {
            if (myMatchAction.Matches != null &&
                myMatchAction.Matches.Count == 1 &&
                myMatchAction.Matches[0].Teams != null &&
                myMatchAction.Matches[0].Teams.LocalTeam != null &&
                myMatchAction.Matches[0].Teams.VisitorTeam != null)
            {
                MatchInfo matchInfo = myMatchAction.Matches[0];

                if (App.CurrentApp.MyLigue1.MyTeamsInfo.ContainsKey(matchInfo.Teams.LocalTeam.Id))
                {
                    localTeamName = App.CurrentApp.MyLigue1.MyTeamsInfo[matchInfo.Teams.LocalTeam.Id].City;
                }

                if (App.CurrentApp.MyLigue1.MyTeamsInfo.ContainsKey(matchInfo.Teams.LocalTeam.Id))
                {
                    visitorTeamName = App.CurrentApp.MyLigue1.MyTeamsInfo[matchInfo.Teams.VisitorTeam.Id].City;
                }

                LocalTeamPossession = Convert.ToInt32(matchInfo.Teams.LocalTeam.Stats.PossessionPercentage);
                VisitorTeamPossession = Convert.ToInt32(matchInfo.Teams.VisitorTeam.Stats.PossessionPercentage);
                LocalTeamCorners = matchInfo.Teams.LocalTeam.Stats.Corners;
                VisitorTeamCorners = matchInfo.Teams.VisitorTeam.Stats.Corners;
                LocalTeamFouls = matchInfo.Teams.LocalTeam.Stats.Fouls;
                VisitorTeamFouls = matchInfo.Teams.VisitorTeam.Stats.Fouls;
                LocalTeamShotsIn = matchInfo.Teams.LocalTeam.Stats.ShotsIn;
                VisitorTeamShotsIn = matchInfo.Teams.VisitorTeam.Stats.ShotsIn;
                LocalTeamShotsOut = matchInfo.Teams.LocalTeam.Stats.ShotsOut;
                VisitorTeamShotsOut = matchInfo.Teams.VisitorTeam.Stats.ShotsOut;
                LocalTeamFreeKicks = matchInfo.Teams.LocalTeam.Stats.FreeKicks;
                VisitorTeamFreeKicks = matchInfo.Teams.VisitorTeam.Stats.FreeKicks;
                LocalTeamYellowCards = matchInfo.Teams.LocalTeam.Stats.YellowCards;
                VisitorTeamYellowCards = matchInfo.Teams.VisitorTeam.Stats.YellowCards;
                LocalTeamRedCards = matchInfo.Teams.LocalTeam.Stats.RedCards;
                VisitorTeamRedCards = matchInfo.Teams.VisitorTeam.Stats.RedCards;
                LocalTeamOffsides = matchInfo.Teams.LocalTeam.Stats.OffSides;
                VisitorTeamOffsides = matchInfo.Teams.VisitorTeam.Stats.OffSides;

                LogoInfo localTeamlogoInfo = App.CurrentApp.MyLigue1.MyTeamsInfo[matchInfo.Teams.LocalTeam.Id].Logos[0]; //FIXEME test here
                LogoInfo vistorTeamlogoInfo = App.CurrentApp.MyLigue1.MyTeamsInfo[matchInfo.Teams.VisitorTeam.Id].Logos[0]; //FIXEME test here
                LocalTeamLogo64Uri = App.CurrentApp.MyLigue1.MyLogoBaseUrl + "/" + localTeamlogoInfo.Name + "_64." + localTeamlogoInfo.Ext;
                VisitorTeamLogo64Uri = App.CurrentApp.MyLigue1.MyLogoBaseUrl + "/" + vistorTeamlogoInfo.Name + "_64." + vistorTeamlogoInfo.Ext;
            }
        }

        private void UpdatesPropertiesFromMatchInfo()
        {
            if (myMatchInfo.Teams != null &&
                myMatchInfo.Teams.LocalTeam != null &&
                myMatchInfo.Teams.VisitorTeam != null &&
                myMatchInfo.Teams.LocalTeam.Stats != null &&
                myMatchInfo.Teams.VisitorTeam.Stats != null)
            {
                LocalTeamPossession = Convert.ToInt32(myMatchInfo.Teams.LocalTeam.Stats.PossessionPercentage);
                VisitorTeamPossession = Convert.ToInt32(myMatchInfo.Teams.VisitorTeam.Stats.PossessionPercentage);
                LocalTeamCorners = myMatchInfo.Teams.LocalTeam.Stats.Corners;
                VisitorTeamCorners = myMatchInfo.Teams.VisitorTeam.Stats.Corners;
                LocalTeamFouls = myMatchInfo.Teams.LocalTeam.Stats.Fouls;
                VisitorTeamFouls = myMatchInfo.Teams.VisitorTeam.Stats.Fouls;
                LocalTeamShotsIn = myMatchInfo.Teams.LocalTeam.Stats.ShotsIn;
                VisitorTeamShotsIn = myMatchInfo.Teams.VisitorTeam.Stats.ShotsIn;
                LocalTeamShotsOut = myMatchInfo.Teams.LocalTeam.Stats.ShotsOut;
                VisitorTeamShotsOut = myMatchInfo.Teams.VisitorTeam.Stats.ShotsOut;
                LocalTeamFreeKicks = myMatchInfo.Teams.LocalTeam.Stats.FreeKicks;
                VisitorTeamFreeKicks = myMatchInfo.Teams.VisitorTeam.Stats.FreeKicks;
                LocalTeamYellowCards = myMatchInfo.Teams.LocalTeam.Stats.YellowCards;
                VisitorTeamYellowCards = myMatchInfo.Teams.VisitorTeam.Stats.YellowCards;
                LocalTeamRedCards = myMatchInfo.Teams.LocalTeam.Stats.RedCards;
                VisitorTeamRedCards = myMatchInfo.Teams.VisitorTeam.Stats.RedCards;
                LocalTeamOffsides = myMatchInfo.Teams.LocalTeam.Stats.OffSides;
                VisitorTeamOffsides = myMatchInfo.Teams.VisitorTeam.Stats.OffSides;
            }
        }

        #region wording
        public string ShotInAndShoutOutLabel
        {
            get
            {
                return Localization.GetString("ShotInAndShoutOutLabel");
            }
        }

        public string PossessionLabel
        {
            get
            {
                return Localization.GetString("PossessionLabel");
            }
        }

        public string CornersLabel
        {
            get
            {
                return Localization.GetString("CornersLabel");
            }
        }

        public string FoulsLabel
        {
            get
            {
                return Localization.GetString("FoulsLabel");
            }
        }

        public string OffsidesLabel
        {
            get
            {
                return Localization.GetString("OffsidesLabel");
            }
        }

        public string YellowCardsLabel
        {
            get
            {
                return Localization.GetString("YellowCardsLabel");
            }
        }

        public string RedCardsLabel
        {
            get
            {
                return Localization.GetString("RedCardsLabel");
            }
        }

        public string FreeKicksLabel
        {
            get
            {
                return Localization.GetString("FreeKicksLabel");
            }
        }
        #endregion
    }
}

