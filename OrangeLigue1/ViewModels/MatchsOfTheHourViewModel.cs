// -------------------------------  Copyright ---------------------------------
//	This software has been developped by France Telecom, FT/RD/RD/MAPS/DVC/MSF
//
//	Copyright (c) France Telecom 2009-2010
//
// COPYRIGHT    : This file is the property of FRANCE TELECOM.
//                It cannot be copied, used, or modified without obtaining
//                an authorization from the authors or a mandated
//                member of FRANCE TELECOM.
//                If such an authorization is provided, any modified version or
//                copy of the software has to contain this header.
//
// WARRANTIES   : This software is made available by the authors in the  hope
//                that it will be useful, but without any warranty.
//                France Telecom is not liable for any consequence related to the
//                use of the provided software.
//
// AUTHORS      : France Telecom  / RD / MAPS / DVC / MSF
// ----------------------------------------------------------------------------
using System;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using Orange.SDK;
using System.Collections.ObjectModel;
using Orange.SDK.Enablers.Ligue1;
using System.Diagnostics;

namespace OrangeLigue1.ViewModels
{
    public class MatchsOfTheHourViewModel : ViewModel
    {
        private DateTime now = DateTime.Now;
        /// <summary>
        /// List of Matchs
        /// </summary>
        private ObservableCollection<MatchInfo> myMatchInfoList = new ObservableCollection<MatchInfo>();
        public ObservableCollection<MatchInfo> MyMatchInfoList
        {
            get { return myMatchInfoList; }
            set
            {
                myMatchInfoList = value;
                now = DateTime.Now;
                UpdatesPropertiesFromMatchInfo();
                OnPropertyChanged("MyMatchsList");
            }
        }

        public MatchItemViewModel CurrentListItem
        {
            get { return null; }
            set
            {
                if (value != null)
                {
                    SelectionChangedCommand(value);
                    OnPropertyChanged("CurrentListItem");
                }
            }
        }

        private DateTime date;
        public DateTime Date
        {
            get { return date; }
            set { date = value; OnPropertyChanged("Date"); }
        }

        private string hour;
        public string Hour
        {
            get 
            {
                DateTime now = DateTime.UtcNow;
                if (date < now && date.AddHours(2) > now)
                {
                    return "";
                }
                return hour; 
            }
            set { hour = value; OnPropertyChanged("Hour"); }
        }

        /// <summary>
        /// List of Matchs
        /// </summary>
        private ObservableCollection<MatchItemViewModel> myMatchsList = new ObservableCollection<MatchItemViewModel>();
        public ObservableCollection<MatchItemViewModel> MyMatchsList
        {
            get { return myMatchsList; }
            set
            {
                myMatchsList = value;
                OnPropertyChanged("MyMatchsList");
            }
        }

        /// <summary>
        /// Fill Matchs list property
        /// </summary>
        void UpdatesPropertiesFromMatchInfo()
        {
            myMatchsList.Clear();

            if (App.CurrentApp.MyLigue1 != null &&
                App.CurrentApp.MyLigue1.MyTeamsInfo != null)
            {
                foreach (MatchInfo matchInfo in myMatchInfoList)
                {
                    //NotPlayed = 0,
                    //Playing = 1,
                    //Finished = 2
                    int localTeamScore = matchInfo.Teams.LocalTeam.Score;
                    int visitorTeamScore = matchInfo.Teams.VisitorTeam.Score;
                    FontFamily localTeamFontFamily = App.CurrentApp.Resources["PhoneFontFamilyNormal"] as FontFamily;
                    FontFamily visitorTeamFontFamily = App.CurrentApp.Resources["PhoneFontFamilyNormal"] as FontFamily;
                    LogoInfo localTeamlogoInfo = null;
                    LogoInfo visitorTeamlogoInfo = null;
                    string localTeamName = null;
                    string visitorTeamName = null;
                    string localTeamLogo64Uri = null;
                    string visitorTeamLogo64Uri = null;

                    if (localTeamScore > visitorTeamScore)
                    {
                        localTeamFontFamily = App.CurrentApp.Resources["PhoneFontFamilySemiBold"] as FontFamily;
                    }
                    else if (localTeamScore < visitorTeamScore)
                    {
                        visitorTeamFontFamily = App.CurrentApp.Resources["PhoneFontFamilySemiBold"] as FontFamily;
                    }

                    if (App.CurrentApp.MyLigue1.MyTeamsInfo != null &&
                        App.CurrentApp.MyLigue1.MyTeamsInfo.ContainsKey(matchInfo.Teams.LocalTeam.Id) &&
                        App.CurrentApp.MyLigue1.MyTeamsInfo[matchInfo.Teams.LocalTeam.Id].Logos.Count > 0)
                    {
                        localTeamlogoInfo = App.CurrentApp.MyLigue1.MyTeamsInfo[matchInfo.Teams.LocalTeam.Id].Logos[0];
                    }

                    if (App.CurrentApp.MyLigue1.MyTeamsInfo != null &&
                        App.CurrentApp.MyLigue1.MyTeamsInfo.ContainsKey(matchInfo.Teams.VisitorTeam.Id) &&
                        App.CurrentApp.MyLigue1.MyTeamsInfo[matchInfo.Teams.VisitorTeam.Id].Logos.Count > 0)
                    {
                        visitorTeamlogoInfo = App.CurrentApp.MyLigue1.MyTeamsInfo[matchInfo.Teams.VisitorTeam.Id].Logos[0];
                    }

                    if (App.CurrentApp.MyLigue1.MyTeamsInfo != null &&
                        App.CurrentApp.MyLigue1.MyTeamsInfo.ContainsKey(matchInfo.Teams.LocalTeam.Id))
                    {
                        localTeamName = App.CurrentApp.MyLigue1.MyTeamsInfo[matchInfo.Teams.LocalTeam.Id].City;
                    }

                    if (App.CurrentApp.MyLigue1.MyTeamsInfo != null &&
                        App.CurrentApp.MyLigue1.MyTeamsInfo.ContainsKey(matchInfo.Teams.LocalTeam.Id))
                    {
                        visitorTeamName = App.CurrentApp.MyLigue1.MyTeamsInfo[matchInfo.Teams.VisitorTeam.Id].City;
                    }

                    if (App.CurrentApp.MyLigue1.MyLightSettingsAction != null &&
                        App.CurrentApp.MyLigue1.MyLightSettingsAction.Teams != null &&
                        localTeamlogoInfo != null)
                    {
                        localTeamLogo64Uri = App.CurrentApp.MyLigue1.MyLogoBaseUrl + "/" + localTeamlogoInfo.Name + "_64." + localTeamlogoInfo.Ext;
                        Debug.WriteLine(localTeamLogo64Uri);
                    }

                    if (App.CurrentApp.MyLigue1.MyLightSettingsAction != null &&
                        App.CurrentApp.MyLigue1.MyLightSettingsAction.Teams != null &&
                        visitorTeamlogoInfo != null)
                    {
                        visitorTeamLogo64Uri = App.CurrentApp.MyLigue1.MyLogoBaseUrl + "/" + visitorTeamlogoInfo.Name + "_64." + visitorTeamlogoInfo.Ext;
                        Debug.WriteLine(visitorTeamLogo64Uri);
                    }

                    myMatchsList.Add(new MatchItemViewModel()
                    {
                        MatchId = matchInfo.Id,
                        LocalTeamScore = matchInfo.Teams.LocalTeam.Score.ToString(),
                        VisitorTeamScore = matchInfo.Teams.VisitorTeam.Score.ToString(),
                        LocalTeamName = localTeamName,
                        VisitorTeamName = visitorTeamName,
                        LocalTeamLogo64Uri = localTeamLogo64Uri,
                        VisitorTeamLogo64Uri = visitorTeamLogo64Uri,
                        LocalTeamFontFamily = localTeamFontFamily,
                        VisitorTeamFontFamily = visitorTeamFontFamily,
                        Status = matchInfo.Status,
                        Live = matchInfo.Status //matchInfo.Live
                    });
                }
            }
        }

        private void SelectionChangedCommand(MatchItemViewModel item)
        {
            //NotPlayed = 0,
            //Playing = 1,
            //Finished = 2
            int live = 0;
            if (item.Status == 0)
            {
                return;
            }
            else if (item.Status == 1)
            {
                live = item.Live;
            }
            Navigator.GoToPage(MatchInfoPageViewModel.GetUri(item.MatchId, live));
        }
    }
}

