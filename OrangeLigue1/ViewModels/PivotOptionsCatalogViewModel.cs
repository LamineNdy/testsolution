﻿using System;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using Orange.SDK;
using Orange.SDK.Enablers.Ligue1;
using System.Collections.ObjectModel;
using System.Windows.Threading;
using Orange.SDK.Tools;
using OrangeLigue1.Model;
using OrangeLigue1.Tools;
using System.Collections.Generic;
using System.Windows.Controls.Primitives;
using OrangeLigue1.Views;
using Kawagoe.Controls;

namespace OrangeLigue1.ViewModels
{
    public class PivotOptionsCatalogViewModel : ViewModel
    {
        private Popup _popup;

        #region "Singleton definition"
        private static PivotOptionsCatalogViewModel _instance;
        public static PivotOptionsCatalogViewModel Instance
        {
            get
            {
                if (_instance == null)
                {
                    _instance = new PivotOptionsCatalogViewModel();
                }
                return _instance;
            }
        }
        #endregion

        //private string passStatus = "";
        //private string optionsStatus = "";

        public PivotOptionsCatalogViewModel()
        {
            myLigue1 = App.CurrentApp.MyLigue1;

            LaunchCGUCommand = new DelegateCommand<object>(OnLaunchCGU, CanLaunchCGU);
            LaunchSubCommand = new DelegateCommand<object>(OnLaunchSub, CanLaunchSub);

            if (myLigue1 != null)
            {
                myLigue1.OptionSubscribeRequestEnd += myLigue1_OptionSubscribeRequestEnd;
            }
        }

        void myLigue1_OptionSubscribeRequestEnd(object sender, OptionSubscribeAction e)
        {
            MessagePopup popup = new MessagePopup();
            if (e.RespStatus > 0)
            {
                if (string.IsNullOrEmpty(e.Message))
                    popup.Message = Localization.GetString("GenericError");
                else
                    popup.Message = e.Message;
            }
            else
            {
                popup.Message = e.ConfirmMessage;              
            }

            popup.AddButton("OK", null, null);
            popup.Open();
        }


        #region GETTERS/SETTERS
        private Ligue1 myLigue1;
        public Ligue1 MyLigue1
        {
            get { return myLigue1; }
            set { myLigue1 = value; OnPropertyChanged("MyLigue1"); }
        }

        private bool isEnabled;
        public bool IsEnabled
        {
            get { return isEnabled; }
            set
            {
                isEnabled = value;
                OnPropertyChanged("IsEnabled");
            }
        }

        private Visibility isVisible;
        public Visibility IsVisible
        {
            get { return isVisible; }
            set
            {
                isVisible = value;
                OnPropertyChanged("IsVisible");
            }
        }

        private AccessRightsItemViewModel currentOption;
        public AccessRightsItemViewModel CurrentOption
        {
            get { return currentOption; }
            set
            {
                currentOption = value;
                OnPropertyChanged("CurrentOption");
            }
        }

        private int pivotIndex;
        public int PivotIndex
        {
            get { return pivotIndex; }
            set
            {
                pivotIndex = value;
                OnPropertyChanged("PivotIndex");
            }
        }

        private ObservableCollection<AccessRightsItemViewModel> allOptions = new ObservableCollection<AccessRightsItemViewModel>();
        public ObservableCollection<AccessRightsItemViewModel> AllOptions
        {
            get { return allOptions; }
            set
            {
                allOptions = value;
                SetCurrentPivotItem();
                OnPropertyChanged("AllOptions");
            }
        }

        #endregion

        public void SetCurrentPivotItem()
        {
            //if (index != -1)
            //    CurrentOption = AllOptions[index];

            foreach (AccessRightsItemViewModel item in AllOptions)
            {
                if (item.SubscriptionEnum == 1)
                    IsEnabled = true;
                else
                {
                    if(string.IsNullOrEmpty(item.SubscriptionStatus))
                        IsVisible = Visibility.Collapsed;
                    IsEnabled = false;
                }
            }
        }

        private void OpenCgu()
        {
            MessagePopup popup = new MessagePopup();
            popup.Title = Localization.GetString("ConditionsLabel") +" " +currentOption.Name;
            popup.Message = currentOption.Legal;
            popup.AddButton("OK",null, null);
            popup.Open();
        }

        private void SubscribeToOption(string id, string type)
        {
            myLigue1.RequestSubscribeOption(id, type);
        }


        #region wording
        public string ConditionsLabel
        {
            get
            {
                return Localization.GetString("ConditionsLabel");
            }
        }

        public string OptionsPivotTitle
        {
            get
            {
                return Localization.GetString("OptionsPivotTitle");
            }
        }
        #endregion

        public static Uri GetUri()
        {
            return new Uri("/Views/PivotOptionsCatalog.xaml", UriKind.Relative);
        }

        #region Command
        public ICommand LaunchCGUCommand { get; set; }
        public ICommand LaunchSubCommand { get; set; }

        public void OnLaunchCGU(object param)
        {
            if (currentOption.Legal == "")
            {
                return;
            }

            OpenCgu();
        }
        public bool CanLaunchCGU(object param)
        {
            if (currentOption.Legal != null && currentOption.Legal != "")
            {
                return true;
            }
            return false;
        }

        public void OnLaunchSub(object param)
        {
            if (string.IsNullOrEmpty(currentOption.Id) && string.IsNullOrEmpty(currentOption.Type))
            {
                return;
            }

            SubscribeToOption(currentOption.Id, currentOption.Type);
        }


        public bool CanLaunchSub(object param)
        {
            if (!string.IsNullOrEmpty(currentOption.Id) && !string.IsNullOrEmpty(currentOption.Type))
            {
                return true;
            }
            return false;
        }

        #endregion
    }
}
