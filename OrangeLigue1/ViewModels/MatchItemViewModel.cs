// -------------------------------  Copyright ---------------------------------
//	This software has been developped by France Telecom, FT/RD/RD/MAPS/DVC/MSF
//
//	Copyright (c) France Telecom 2009-2010
//
// COPYRIGHT    : This file is the property of FRANCE TELECOM.
//                It cannot be copied, used, or modified without obtaining
//                an authorization from the authors or a mandated
//                member of FRANCE TELECOM.
//                If such an authorization is provided, any modified version or
//                copy of the software has to contain this header.
//
// WARRANTIES   : This software is made available by the authors in the  hope
//                that it will be useful, but without any warranty.
//                France Telecom is not liable for any consequence related to the
//                use of the provided software.
//
// AUTHORS      : France Telecom  / RD / MAPS / DVC / MSF
// ----------------------------------------------------------------------------
using System;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using Orange.SDK;
using System.Windows.Media.Imaging;
using Orange.SDK.Enablers.Ligue1;
using OrangeLigue1.Storage;

namespace OrangeLigue1.ViewModels
{
    public class MatchItemViewModel : ViewModel
    {
        private int matchId;
        public int MatchId
        {
            get { return matchId; }
            set { matchId = value; OnPropertyChanged("MatchId"); }
        }

        private string localTeamScore;
        public string LocalTeamScore
        {
            get 
            {
                if (status == 0)
                {
                    return "";
                }
                return localTeamScore; 
            }
            set { localTeamScore = value; OnPropertyChanged("LocalTeamScore"); }
        }

        private FontFamily localTeamFontFamily;
        public FontFamily LocalTeamFontFamily
        {
            get { return localTeamFontFamily; }
            set { localTeamFontFamily = value; OnPropertyChanged("LocalTeamFontFamily"); }
        }

        private string visitorTeamScore;
        public string VisitorTeamScore
        {
            get
            {
                if (status == 0)
                {
                    return "";
                }
                return visitorTeamScore;
            }
            set { visitorTeamScore = value; OnPropertyChanged("VisitorTeamScore"); }
        }

        private string localTeamName;
        public string LocalTeamName 
        {
            get { return localTeamName; }
            set { localTeamName = value; OnPropertyChanged("LocalTeamName"); }
        }

        private string localTeamShortName;
        public string LocalTeamShortName
        {
            get { return localTeamShortName; }
            set { localTeamShortName = value; OnPropertyChanged("LocalTeamShortName"); }
        }

        private string visitorTeamName;
        public string VisitorTeamName 
        { 
            get { return visitorTeamName; }
            set { visitorTeamName = value; OnPropertyChanged("VisitorTeamName"); } 
        }

        private string visitorTeamShortName;
        public string VisitorTeamShortName
        {
            get { return visitorTeamShortName; }
            set { visitorTeamShortName = value; OnPropertyChanged("VisitorTeamShortName"); }
        }

        private FontFamily visitorTeamFontFamily;
        public FontFamily VisitorTeamFontFamily
        {
            get { return visitorTeamFontFamily; }
            set { visitorTeamFontFamily = value; OnPropertyChanged("VisitorTeamFontFamily"); }
        }

        private string localTeamLogo64Uri;
        public string LocalTeamLogo64Uri
        {
            get { return localTeamLogo64Uri; }
            set { localTeamLogo64Uri = value; OnPropertyChanged("LocalTeam64Logo"); }
        }

        public ImageSource LocalTeam64Logo
        {
            get
            {
                return Ligue1ImageCache.Default.Get(localTeamName + "_64", localTeamLogo64Uri);
            }
        }

        private string visitorTeamLogo64Uri;
        public string VisitorTeamLogo64Uri
        {
            get { return visitorTeamLogo64Uri; }
            set { visitorTeamLogo64Uri = value; OnPropertyChanged("VisitorTeam64Logo"); }
        }

        public ImageSource VisitorTeam64Logo
        {
            get
            {
                return Ligue1ImageCache.Default.Get(visitorTeamName+"_64", visitorTeamLogo64Uri);
            }
        }

        private string localTeamLogo44Uri;
        public string LocalTeamLogo44Uri
        {
            get { return localTeamLogo44Uri; }
            set { localTeamLogo44Uri = value; OnPropertyChanged("LocalTeam44Logo"); }
        }

        public ImageSource LocalTeam44Logo
        {
            get
            {
                return Ligue1ImageCache.Default.Get(localTeamName + "_44", localTeamLogo44Uri);
            }
        }

        private string visitorTeamLogo44Uri;
        public string VisitorTeamLogo44Uri
        {
            get { return visitorTeamLogo44Uri; }
            set { visitorTeamLogo44Uri = value; OnPropertyChanged("VisitorTeam44Logo"); }
        }

        public ImageSource VisitorTeam44Logo
        {
            get
            {
                return Ligue1ImageCache.Default.Get(visitorTeamName + "_44", visitorTeamLogo44Uri);
            }
        }

        private int status;
        public int Status
        {
            get { return status; }
            set { status = value; OnPropertyChanged("Status"); OnPropertyChanged("Enabled"); }
        }

        public bool Enabled
        {
            get { return status != 0; }
        }

        private int live;
        public int Live
        {
            get { return live; }
            set { live = value; OnPropertyChanged("Live"); }
        }

        public Visibility LiveImageVisibility
        {
            get
            {
                if (live == 1 && status == 1)
                {
                    return Visibility.Visible;
                }
                else
                {
                    return Visibility.Collapsed;
                }
            }
        }

        private string liveUri;
        public string LiveUri
        {
            get { return liveUri; }
            set { liveUri = value; OnPropertyChanged("LiveUri"); }
        }

        private string liveUriWifi;
        public string LiveUriWifi
        {
            get { return liveUriWifi; }
            set { liveUriWifi = value; OnPropertyChanged("LiveUriWifi"); }
        }
    }
}

