// -------------------------------  Copyright ---------------------------------
//	This software has been developped by France Telecom, FT/RD/RD/MAPS/DVC/MSF
//
//	Copyright (c) France Telecom 2009-2010
//
// COPYRIGHT    : This file is the property of FRANCE TELECOM.
//                It cannot be copied, used, or modified without obtaining
//                an authorization from the authors or a mandated
//                member of FRANCE TELECOM.
//                If such an authorization is provided, any modified version or
//                copy of the software has to contain this header.
//
// WARRANTIES   : This software is made available by the authors in the  hope
//                that it will be useful, but without any warranty.
//                France Telecom is not liable for any consequence related to the
//                use of the provided software.
//
// AUTHORS      : France Telecom  / RD / MAPS / DVC / MSF
// ----------------------------------------------------------------------------
using System;
using Orange.SDK;
using Orange.SDK.Enablers.Ligue1;
using OrangeLigue1.Model;
using System.ComponentModel;
using OrangeLigue1.Tools;

namespace OrangeLigue1.ViewModels
{
    public class PanoramaViewModel : ViewModel
    {
        #region "Singleton definition"

        private static PanoramaViewModel _instance;
        public static PanoramaViewModel Instance
        {
            get
            {
                if (_instance == null)
                {
                    _instance = new PanoramaViewModel();
                }
                return _instance;
            }
        }
        #endregion

        #region "Event/Args"
        public event EventHandler<AllMatchAction>                       AllMatchRequestEnd;
        public event EventHandler<UserinfoAction>                       UserinfoRequestEnd;
        public event EventHandler<WordingAction>                        WordingRequestEnd;
        public event EventHandler                                       PanoramaIsLoaded;
        public event EventHandler                                       MagFootIsLoaded;
        public event EventHandler<BackgroundRequestCompletedEventArgs>  ConnectionErrorRequestEnd;
        #endregion

        private Ligue1 myLigue1;
        public Ligue1 MyLigue1
        {
            get { return myLigue1;} 
            set { myLigue1 = value; OnPropertyChanged("MyLigue1"); }
        }

        private PanoramaMultiplexSectionViewModel myPanoramaMultiplexSectionViewModel;
        public PanoramaMultiplexSectionViewModel MyPanoramaMultiplexSectionViewModel
        {
            get { return myPanoramaMultiplexSectionViewModel; }
            set { myPanoramaMultiplexSectionViewModel = value; OnPropertyChanged("MyPanoramaMultiplexSectionViewModel"); }
        }
        private PanoramaNewsSectionViewModel myPanoramaNewsSectionViewModel;
        public PanoramaNewsSectionViewModel MyPanoramaNewsSectionViewModel
        {
            get { return myPanoramaNewsSectionViewModel; }
            set { myPanoramaNewsSectionViewModel = value; OnPropertyChanged("MyPanoramaNewsSectionViewModel"); }
        }

        private PanoramaCalendarSectionViewModel myPanoramaCalendarSectionViewModel;
        public PanoramaCalendarSectionViewModel MyPanoramaCalendarSectionViewModel
        {
            get { return myPanoramaCalendarSectionViewModel; }
            set { myPanoramaCalendarSectionViewModel = value; OnPropertyChanged("MyPanoramaCalendarSectionViewModel"); }
        }

        private PanoramaRankingSectionViewModel myPanoramaRankingSectionViewModel;
        public PanoramaRankingSectionViewModel MyPanoramaRankingSectionViewModel
        {
            get { return myPanoramaRankingSectionViewModel; }
            set { myPanoramaRankingSectionViewModel = value; OnPropertyChanged("MyPanoramaRankingSectionViewModel"); }
        }

        private PanoramaAboutSectionViewModel myPanoramaAboutSectionViewModel;
        public PanoramaAboutSectionViewModel MyPanoramaAboutSectionViewModel
        {
            get { return myPanoramaAboutSectionViewModel; }
            set { myPanoramaAboutSectionViewModel = value; OnPropertyChanged("MyPanoramaAboutSectionViewModel"); }
        }

        private PanoramaSummariesSectionViewModel myPanoramaSummariesSectionViewModel;
        public PanoramaSummariesSectionViewModel MyPanoramaSummariesSectionViewModel
        {
            get { return myPanoramaSummariesSectionViewModel; }
            set { myPanoramaSummariesSectionViewModel = value; OnPropertyChanged("MyPanoramaSummariesSectionViewModel"); }
        }

        private PanoramaViewModel()
        {

        }

        public void Initialize()
        {
            myLigue1 = App.CurrentApp.MyLigue1;

            if (myLigue1 != null)
            {
                //myLigue1.DynamicInitRequestEnd
                myLigue1.InitinfoRequestEnd += myInitInfoRequestEnd;
                myLigue1.TeamsRequestEnd += myTeamsRequestEnd;
                myLigue1.UserinfoRequestEnd += myLigue1_UserinfoRequestEnd;
                myLigue1.WordingRequestEnd += myLigue1_WordingRequestEnd;
                myLigue1.DayNewsRequestEnd += myDayNewsRequestEnd;
                myLigue1.DayCalendarRequestEnd += myDayCalendarRequestEnd;
                myLigue1.VodRequestEnd += myVodRequestEnd;
                myLigue1.AllMatchRequestEnd += myAllMatchActionRequestEnd;
                myLigue1.LightSettingsRequestEnd += myLigue1_LightSettingsRequestEnd;
                myLigue1.ConnectionErrorRequestEnd += myConnectionErrorRequestEnd;

                myPanoramaMultiplexSectionViewModel = new PanoramaMultiplexSectionViewModel { MyDayInfo = myLigue1.MyDayInfo, MultiplexNoMatchLabel = Localization.GetString("MultiplexLoading")};
                if (myPanoramaMultiplexSectionViewModel != null)
                {
                    myPanoramaMultiplexSectionViewModel.PropertyChanged += myPanoramaMultiplexSectionViewModel_PropertyChanged;
                }

                myPanoramaNewsSectionViewModel = new PanoramaNewsSectionViewModel { MyDayNewsAction = myLigue1.MyDayNewsAction };
                myPanoramaCalendarSectionViewModel = new PanoramaCalendarSectionViewModel { MyCalendarAction = myLigue1.MyDayCalendarAction };
                myPanoramaRankingSectionViewModel = new PanoramaRankingSectionViewModel { MyTeamsAction = myLigue1.MyTeamsAction };
                myPanoramaSummariesSectionViewModel = new PanoramaSummariesSectionViewModel { MyVoDAction = myLigue1.MyVoDAction };
                myPanoramaAboutSectionViewModel = new PanoramaAboutSectionViewModel();

                if (myPanoramaCalendarSectionViewModel != null)
                {
                    AllMatchRequestEnd += myPanoramaCalendarSectionViewModel.MyAllMatchActionRequestEnd;
                }
                AllMatchRequestEnd += MatchInfoPageViewModel.Instance.MyAllMatchActionRequestEnd;
                AllMatchRequestEnd += DayPageViewModel.Instance.MyAllMatchActionRequestEnd;
            }
        }

        void myLigue1_WordingRequestEnd(object sender, WordingAction e)
        {
            if (WordingRequestEnd != null)
            {
                WordingRequestEnd(this, e);
            }
        }

        void myLigue1_UserinfoRequestEnd(object sender, UserinfoAction e)
        {
            if (UserinfoRequestEnd != null)
            {
                UserinfoRequestEnd(this, e);
            }
        }

        void myLigue1_LightSettingsRequestEnd(object sender, LightSettingsAction e)
        {
            myPanoramaMultiplexSectionViewModel.MyDayInfo = myLigue1.MyDayInfo;
        }

        void myPanoramaMultiplexSectionViewModel_PropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            /*
            if (myPanoramaMultiplexSectionViewModel.MultiplexIsInProgress)
            {
                if (myLigue1.RefreshFreqRTimer != null && myLigue1.RefreshFreqRTimer.IsEnabled)
                {
                    myLigue1.StopRefreshFreqRTimer();
                    myLigue1.StartRefreshFreqLiveTimer();
                }
            }
            else
            {
                if (myLigue1.RefreshFreqLiveTimer != null && myLigue1.RefreshFreqLiveTimer.IsEnabled)
                {
                    myLigue1.StopRefreshFreqLiveTimer();
                    myLigue1.StartRefreshFreqRTimer();
                }
            }*/
        }

        void myInitInfoRequestEnd(object sender, InitinfoAction e)
        {
            //myPanoramaMultiplexSectionViewModel.MyDayInfo = e.Matches; deprecated 
            if (PanoramaIsLoaded != null)
            {
                PanoramaIsLoaded(this, null);
            }
            //OnPropertyChanged("CalendarTitleLabel"); //update calendar section title with the current day number
        }

        void myTeamsRequestEnd(object sender, TeamsAction e)
        {
            myPanoramaRankingSectionViewModel.MyTeamsAction = e;
        }

        void myDayNewsRequestEnd(object sender, DaynewsAction e)
        {
            myPanoramaNewsSectionViewModel.MyDayNewsAction = e;
            if (MagFootIsLoaded != null)
            {
                MagFootIsLoaded(this, null);
            }
        }

        void myDayCalendarRequestEnd(object sender, CalendarAction e)
        {
            myPanoramaCalendarSectionViewModel.MyCalendarAction = e;
            myPanoramaMultiplexSectionViewModel.DayCalendarReceived(e);
        }

        void myVodRequestEnd(object sender, VoDAction e)
        {
            myPanoramaSummariesSectionViewModel.MyVoDAction = e;
        }

        void myAllMatchActionRequestEnd(object sender, AllMatchAction e)
        {
            if(myLigue1.MyLightSettingsActionIsLoaded)
                myPanoramaMultiplexSectionViewModel.MyDayInfo = e.Matches;
            //OnPropertyChanged("CalendarTitleLabel"); //update calendar section title with the current day number

            //send event to notification bar / calendar / day page after updating multiplex view model
            if (e.Matches != null &&
                 e.Matches.Matches != null &&
                 e.Matches.Matches.Count > 0)
            {
                myPanoramaMultiplexSectionViewModel.MyAllMatchAction = e;
                if (AllMatchRequestEnd != null)
                {
                    AllMatchRequestEnd(this, e);
                }
            }
            OnPropertyChanged("CalendarTitleLabel");
        }

        void myConnectionErrorRequestEnd(object sender, BackgroundRequestCompletedEventArgs e)
        {
            if (ConnectionErrorRequestEnd != null)
            {
                ConnectionErrorRequestEnd(this, e);
            }
        }

        #region wording
        public string LoadingText
        {
            get
            {
                return Localization.GetString("MainPage_LoadingText");
            }
        }

        public string ApplicationTitleLabel
        {
            get
            {
                return Localization.GetString("ApplicationTitleLabel");
            }
        }

        public string MultiplexTitleLabel
        {
            get
            {
                return Localization.GetString("MultiplexTitleLabel");
            }
        }

        public string NewsTitleLabel
        {
            get
            {
                return Localization.GetString("NewsTitleLabel");
            }
        }

        public string CalendarTitleLabel
        {
            get
            {

                int day = 0;

                if (MyLigue1 != null)
                {
                    day = MyLigue1.CurrentDay;
                }

                if (day == 1)
                {
                    return day.ToString() + "ère " + Localization.GetString("DayLabel");
                }
                return day.ToString() + "e " + Localization.GetString("DayLabel");
            }
        }

        public string RankingTitleLabel
        {
            get
            {
                return Localization.GetString("RankingTitleLabel");
            }
        }

        public string SummariesTitleLabel
        {
            get
            {
                return Localization.GetString("SummariesTitleLabel");
            }
        }

        public string AlertMenuLabel
        {
            get
            {
                return Localization.GetString("AlertMenuLabel");
            }
        }

        public string AccessRightsMenuLabel
        {
            get
            {
                return Localization.GetString("AccessRightsMenuLabel");
            }
        }

        public string AboutMenuLabel
        {
            get
            {
                return Localization.GetString("AboutMenuLabel");
            }
        }

        public string CloseLabel
        {
            get
            {
                return Localization.GetString("CloseLabel");
            }
        }
        #endregion

        /// <summary>
        /// Retrieves the URI of the background image of the panorama.
        /// </summary>
        public Uri BackgroundImageUri
        {
            get
            {
                return new Uri("/Assets/ligue1_op1.jpg", UriKind.Relative);
            }
        }
    }
}

