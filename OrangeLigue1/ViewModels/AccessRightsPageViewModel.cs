// -------------------------------  Copyright ---------------------------------
//	This software has been developped by France Telecom, FT/RD/RD/MAPS/DVC/MSF
//
//	Copyright (c) France Telecom 2009-2010
//
// COPYRIGHT    : This file is the property of FRANCE TELECOM.
//                It cannot be copied, used, or modified without obtaining
//                an authorization from the authors or a mandated
//                member of FRANCE TELECOM.
//                If such an authorization is provided, any modified version or
//                copy of the software has to contain this header.
//
// WARRANTIES   : This software is made available by the authors in the  hope
//                that it will be useful, but without any warranty.
//                France Telecom is not liable for any consequence related to the
//                use of the provided software.
//
// AUTHORS      : France Telecom  / RD / MAPS / DVC / MSF
// ----------------------------------------------------------------------------
using System;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using Orange.SDK;
using Orange.SDK.Enablers.Ligue1;
using System.Collections.ObjectModel;
using System.Windows.Threading;
using Orange.SDK.Tools;
using OrangeLigue1.Model;
using OrangeLigue1.Tools;
using System.Collections.Generic;

namespace OrangeLigue1.ViewModels
{
    public class AccessRightsPageViewModel : ViewModel
    {
        #region "Event/Args"
        public event EventHandler<BackgroundRequestCompletedEventArgs> ConnectionErrorRequestEnd;
        #endregion
        private PivotOptionsCatalogViewModel _viewModel = null;
        private string passStatus = "";
        private int passEnum;
        private string optionsStatus = "";
        private int optionEnum;

        private Ligue1 myLigue1;
        public Ligue1 MyLigue1
        {
            get { return myLigue1; }
            set { myLigue1 = value; OnPropertyChanged("MyLigue1"); }
        }

        private PivotOptionsCatalogViewModel myPivotOptionsCatalogViewModel;
        public PivotOptionsCatalogViewModel MyPivotOptionsCatalogViewModel
        {
            get { return myPivotOptionsCatalogViewModel; }
            set { myPivotOptionsCatalogViewModel = value; OnPropertyChanged("MyPivotOptionsCatalogViewModel"); }
        }

        public AccessRightsPageViewModel()
        {
            myLigue1 = App.CurrentApp.MyLigue1;
            _viewModel = PivotOptionsCatalogViewModel.Instance;

            myUserinfoAction = myLigue1.MyUserinfoAction;
            if (myLigue1 != null)
            {
                myLigue1.UserinfoRequestEnd += myUserinfoRequestEnds;
                myLigue1.ConnectionErrorRequestEnd += myLigue1_ConnectionErrorRequestEnd;
                myLigue1.OptionCatalogRequestEnd += myLigue1_OptionCatalogRequestEnd;
            }
        }

        private UserinfoAction myUserinfoAction;
        public UserinfoAction MyUserinfoAction
        {
            get { return myUserinfoAction; }
            set
            {
                myUserinfoAction = value;
                if (myUserinfoAction != null)
                {
                    UpdatesPropertiesFromUserinfoAction();
                }
            }
        }

        private OptionCatalogAction myOptionCatalogAction;
        public OptionCatalogAction MyOptionCatalogAction
        {
            get { return myOptionCatalogAction; }
            set
            {
                myOptionCatalogAction = value;
                if (myOptionCatalogAction != null)
                {
                    UpdatesPropertiesFromOptionCatalogAction();
                }
            }
        }

        private AccessRightsItemViewModel optionItem;
        public AccessRightsItemViewModel OptionItem
        {
            get { return optionItem; }
            set
            {
                optionItem = value;
                if (optionItem != null)
                {
                    GoToOptionsPivot(Options.IndexOf(optionItem));
                }
            }
        }

        private void GoToOptionsPivot(int index)
        {
            _viewModel.CurrentOption = optionItem;
            Navigator.GoToPage(PivotOptionsCatalogViewModel.GetUri());
        }

        private string message;
        public string Message
        {
            get { return message; }
            set { message = value; OnPropertyChanged("Message"); }
        }

        private ObservableCollection<AccessRightsItemViewModel> options = new ObservableCollection<AccessRightsItemViewModel>();
        public ObservableCollection<AccessRightsItemViewModel> Options
        {
            get { return options; }
            set
            {
                options = value;
                OnPropertyChanged("Options");
            }
        }

        private bool myPageIsLoaded = false;
        public bool MyPageIsLoaded
        {
            get { return myPageIsLoaded; }
            set { myPageIsLoaded = value; OnPropertyChanged("MyPageIsLoaded"); }
        }
		
		private void UpdatesPropertiesFromUserinfoAction()
        {
            if (myUserinfoAction.Message != null)
            {
                Message = myUserinfoAction.Message;                
            }
            if (myUserinfoAction.L1CompliantMessage != null)
            {
                optionsStatus = myUserinfoAction.L1CompliantMessage;
                optionEnum = myUserinfoAction.L1Compliant;
            }
            if (myUserinfoAction.DayPassCompliantMessage != null)
            {
                passStatus = myUserinfoAction.DayPassCompliantMessage;
                passEnum = myUserinfoAction.DayPassCompliant;
            }
        }

        private void UpdatesPropertiesFromOptionCatalogAction()
        {
            options.Clear();
            if (myOptionCatalogAction.Options != null)
            {
                //Message = myUserinfoAction.Message;
                List<AccessRightsItemViewModel> optionsList = new List<AccessRightsItemViewModel>();
                foreach (OptionInfo info in myOptionCatalogAction.Options)
                {
                    if (info.Id == "OptFoot500MoI")
                    {
                        optionsList.Add(new AccessRightsItemViewModel()
                        {
                            Id = info.Id,
                            Name = info.Name,
                            Desc = info.Desc,
                            Price = Localization.GetString("PriceOption").Replace("X", info.Price),
                            ShortDesc = Localization.GetString("PriceOption").Replace("X", info.Price) + "\n" + Localization.GetString("ShortOptionDescription"),
                            Legal = info.Legal.Trim(),
                            SubscriptionStatus = optionsStatus,
                            SubscriptionEnum = optionEnum,
                            Type = "0"
                        });
                    }
                }
                foreach (AccessRightsItemViewModel item in optionsList)
                {
                    options.Add(item);
                }
            }
            if (myLigue1.MyManagerPass)
            {
                if (myOptionCatalogAction.Pass != null)
                {
                    //Message = myUserinfoAction.Message;
                    List<AccessRightsItemViewModel> passList = new List<AccessRightsItemViewModel>();
                    foreach (PassInfo info in myOptionCatalogAction.Pass)
                    {
                        passList.Add(new AccessRightsItemViewModel()
                        {
                            Id = info.Id,
                            Name = info.Name,
                            Desc = info.Desc,
                            Price = Localization.GetString("PricePass").Replace("X", info.Price),
                            ShortDesc = Localization.GetString("PricePass").Replace("X", info.Price) + "\n" + Localization.GetString("ShortPassDescription"),
                            Legal = info.Legal.Trim(),
                            SubscriptionStatus = passStatus,
                            SubscriptionEnum = passEnum,
                            Type = "1"
                        });
                    }
                    foreach (AccessRightsItemViewModel item in passList)
                    {
                        options.Add(item);
                    }
                }
            }

            _viewModel.AllOptions = options;
            
        }

        void myLigue1_ConnectionErrorRequestEnd(object sender, BackgroundRequestCompletedEventArgs e)
        {
            if (ConnectionErrorRequestEnd != null)
            {
                ConnectionErrorRequestEnd(this, e);
            }
        }

        #region Commands


        public void Refresh()
        {
            myLigue1.RequestUserInfo();
        }
        #endregion

        void myUserinfoRequestEnds(object sender, UserinfoAction e)
        {
            this.MyUserinfoAction = e;
           // MyPageIsLoaded = true;
        }

        void myLigue1_OptionCatalogRequestEnd(object sender, OptionCatalogAction e)
        {
            this.MyOptionCatalogAction = e;
            //myPivotOptionsCatalogViewModel.AllOptions = options;
            MyPageIsLoaded = true;
        }

        #region wording
        public string LoadingText
        {
            get
            {
                return Localization.GetString("MainPage_LoadingText");
            }
        }
        public string ApplicationTitleLabel
        {
            get
            {
                return Localization.GetString("ApplicationTitleLabel");
            }
        }

        public string AccessRightsLabel
        {
            get
            {
                return Localization.GetString("AccessRightsLabel");
            }
        }
        #endregion

        public static Uri GetUri()
        {
            return new Uri("/Views/AccessRightsPage.xaml", UriKind.Relative);
        }
    }
}

