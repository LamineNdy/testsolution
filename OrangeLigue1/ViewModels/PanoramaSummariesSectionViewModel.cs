// -------------------------------  Copyright ---------------------------------
//	This software has been developped by France Telecom, FT/RD/RD/MAPS/DVC/MSF
//
//	Copyright (c) France Telecom 2009-2010
//
// COPYRIGHT    : This file is the property of FRANCE TELECOM.
//                It cannot be copied, used, or modified without obtaining
//                an authorization from the authors or a mandated
//                member of FRANCE TELECOM.
//                If such an authorization is provided, any modified version or
//                copy of the software has to contain this header.
//
// WARRANTIES   : This software is made available by the authors in the  hope
//                that it will be useful, but without any warranty.
//                France Telecom is not liable for any consequence related to the
//                use of the provided software.
//
// AUTHORS      : France Telecom  / RD / MAPS / DVC / MSF
// ----------------------------------------------------------------------------
using System;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using Orange.SDK;
using System.Collections.ObjectModel;
using Orange.SDK.Enablers.Ligue1;
using OrangeLigue1.Tools;

namespace OrangeLigue1.ViewModels
{
    public class PanoramaSummariesSectionViewModel : ViewModel
    {
        private VoDAction myVoDAction;
        public VoDAction MyVoDAction
        {
            get { return myVoDAction; }
            set 
            {
                myVoDAction = value;
                if (myVoDAction != null)
                {
                    UpdatesPropertiesFromVoDAction();
                    OnPropertyChanged("MyVideos");
                    OnPropertyChanged("DayNumberLabel");
                }
            }
        }

        public VideoInfoItemViewModel CurrentListItem
        {
            get { return null; }
            set
            {
                if (value != null)
                {
                    SelectionChangedCommand(value.Link, value.LinkWifi, value.Title);
                    OnPropertyChanged("CurrentListItem");
                }
            }
        }
        
        /// <summary>
        /// List of Vod
        /// </summary>
        private ObservableCollection<VideoInfoItemViewModel> myVideos = new ObservableCollection<VideoInfoItemViewModel>();
        public ObservableCollection<VideoInfoItemViewModel> MyVideos
        {
            get { return myVideos; }
            set
            {
                myVideos = value;
                OnPropertyChanged("MyVideos");
            }
        }

        /// <summary>
        /// Fill myVideos list property
        /// </summary>
        void UpdatesPropertiesFromVoDAction()
        {
            myVideos.Clear();

            int day = App.CurrentApp.MyLigue1.CurrentDay;

            if (myVoDAction != null &&
                myVoDAction.Videos != null)
            {
                foreach (VideoInfoList list in myVoDAction.Videos)
                {
                    if (list.Videos != null)
                    {
                        foreach (VideoInfo videoInfo in list.Videos)
                        {
                            // display only the videos of type R (other videos are part of the live stream)
                            if (videoInfo.Type == "R")
                            {
                                myVideos.Add(new VideoInfoItemViewModel()
                                {
                                    Title = videoInfo.Title,
                                    Link = videoInfo.Link,
                                    LinkWifi = videoInfo.LinkWifi,
                                    ThumbnailUri = videoInfo.Image
                                });
                            }
                        }
                    }
                }
            }
        }

        #region wording
        public string DayNumberLabel
        {
            get
            {

                int day = 0;

                if (App.CurrentApp.MyLigue1 != null)
                {
                    day = App.CurrentApp.MyLigue1.CurrentDay;
                }

                if (day == 1)
                {
                    return day.ToString() + "ère " + Localization.GetString("DayLabel");
                }
                else
                {
                    return day.ToString() + "e " + Localization.GetString("DayLabel");
                }
            }
        }
        #endregion

        private void SelectionChangedCommand(string link, string wifiLink, string title)
        {
            if (String.IsNullOrEmpty(link))
            {
                return;
            }

            if (Microsoft.Devices.Environment.DeviceType != Microsoft.Devices.DeviceType.Emulator)
            {
                ObjectRepository.Store(MediaElementPageViewModel.MediaWifiUriString, wifiLink);
                ObjectRepository.Store(MediaElementPageViewModel.MediaUriString, link);
                ObjectRepository.Store(MediaElementPageViewModel.MediaTitleString, title);
                ObjectRepository.Store(MediaElementPageViewModel.MediaLiveString, false);

                Navigator.GoToPage(MediaElementPageViewModel.GetUri());
            }
        }
    }
}

