// -------------------------------  Copyright ---------------------------------
//	This software has been developped by France Telecom, FT/RD/RD/MAPS/DVC/MSF
//
//	Copyright (c) France Telecom 2009-2010
//
// COPYRIGHT    : This file is the property of FRANCE TELECOM.
//                It cannot be copied, used, or modified without obtaining
//                an authorization from the authors or a mandated
//                member of FRANCE TELECOM.
//                If such an authorization is provided, any modified version or
//                copy of the software has to contain this header.
//
// WARRANTIES   : This software is made available by the authors in the  hope
//                that it will be useful, but without any warranty.
//                France Telecom is not liable for any consequence related to the
//                use of the provided software.
//
// AUTHORS      : France Telecom  / RD / MAPS / DVC / MSF
// ----------------------------------------------------------------------------
using System;
using System.Windows.Input;
using Orange.SDK;
using Orange.SDK.Enablers.Ligue1;
using System.Collections.ObjectModel;
using Orange.SDK.Tools;
using OrangeLigue1.Tools;
using System.Globalization;

namespace OrangeLigue1.ViewModels
{
    public class PanoramaNewsSectionViewModel : ViewModel
    {
        public PanoramaNewsSectionViewModel()
        {
            LaunchJDFCommand = new DelegateCommand<object>(OnLaunchJDF, CanLaunchJDF);
        }

        private DaynewsAction myDayNewsAction;
        public DaynewsAction MyDayNewsAction
        {
            get { return myDayNewsAction; }
            set
            {
                myDayNewsAction = value;
                if (myDayNewsAction != null)
                {
                    UpdatesPropertiesFromDayNewsAction();
                    OnPropertyChanged("MyNewsList"); 
                }
            }
        }

        public NewsInfoItemViewModel CurrentListItem
        {
            get { return null; }
            set
            {
                if (value != null)
                {
                    SelectionChangedCommand(value.Id);
                    OnPropertyChanged("CurrentListItem");
                }
            }
        }

        /// <summary>
        /// List of NewsInfo
        /// </summary>
        private ObservableCollection<NewsInfoItemViewModel> myNewsList = new ObservableCollection<NewsInfoItemViewModel>();
        public ObservableCollection<NewsInfoItemViewModel> MyNewsList
        {
            get { return myNewsList; }
            set {
                    myNewsList = value;
                    OnPropertyChanged("MyNewsList"); 
                }
        }

        /// <summary>
        /// Fill NewsInfo list property
        /// </summary>
        void UpdatesPropertiesFromDayNewsAction()
        {
            myNewsList.Clear();

            if (myDayNewsAction != null &&
                myDayNewsAction.DayNews != null &&
                myDayNewsAction.DayNews.NewsList != null)
            {
                foreach (NewsInfo info in myDayNewsAction.DayNews.NewsList)
                {
                    myNewsList.Add(new NewsInfoItemViewModel
                                   {
                        Id = info.Id,
                        TimeStamp = DateTime.Parse(info.Date + " " + info.Hour, CultureInfo.InvariantCulture).ToUniversalTime(),
                        Title = info.Title,
                        Text = info.Text
                    });
                }
            }
        }

        public bool MagFootIsAvailable
        {
            get
            {
                return myDayNewsAction != null &&
                       myDayNewsAction.DayNews != null &&
                       (myDayNewsAction.DayNews.MagUrl != null ||
                        myDayNewsAction.DayNews.MagWifiUrl != null);
            }
        }

        #region Command
        public ICommand LaunchJDFCommand { get; set; }

        public void OnLaunchJDF(object param)
        {
            if (MagFootIsAvailable == false)
            {
                return;
            }

            if (Microsoft.Devices.Environment.DeviceType != Microsoft.Devices.DeviceType.Emulator)
            {
                ObjectRepository.Store(MediaElementPageViewModel.MediaWifiUriString, myDayNewsAction.DayNews.MagWifiUrl);
                ObjectRepository.Store(MediaElementPageViewModel.MediaUriString, myDayNewsAction.DayNews.MagUrl);
                ObjectRepository.Store(MediaElementPageViewModel.MediaTitleString, Localization.GetString("JournalDuFootLabel"));
                ObjectRepository.Store(MediaElementPageViewModel.MediaLiveString, false);

                Navigator.GoToPage(MediaElementPageViewModel.GetUri());
            }
        }

        private void SelectionChangedCommand(int id)
        {
            Navigator.GoToPage(NewsInfoPageViewModel.GetUri(id));
        }

        public bool CanLaunchJDF(object param)
        {
            if (myDayNewsAction != null &&
                myDayNewsAction.DayNews != null &&
                myDayNewsAction.DayNews.MagUrl != null)
            {
                return true;
            }
            return false;
        }

        #endregion
    }
}

