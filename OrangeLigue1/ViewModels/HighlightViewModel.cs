// -------------------------------  Copyright ---------------------------------
//	This software has been developped by France Telecom, FT/RD/RD/MAPS/DVC/MSF
//
//	Copyright (c) France Telecom 2009-2010
//
// COPYRIGHT    : This file is the property of FRANCE TELECOM.
//                It cannot be copied, used, or modified without obtaining
//                an authorization from the authors or a mandated
//                member of FRANCE TELECOM.
//                If such an authorization is provided, any modified version or
//                copy of the software has to contain this header.
//
// WARRANTIES   : This software is made available by the authors in the  hope
//                that it will be useful, but without any warranty.
//                France Telecom is not liable for any consequence related to the
//                use of the provided software.
//
// AUTHORS      : France Telecom  / RD / MAPS / DVC / MSF
// ----------------------------------------------------------------------------
using System;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using Orange.SDK;
using System.Windows.Media.Imaging;
using Orange.SDK.Enablers.Ligue1;
using Orange.SDK.Tools;

namespace OrangeLigue1.ViewModels
{
    public class HighlightViewModel : ViewModel
    {
        private MatchItemViewModel match;
        public MatchItemViewModel Match
        {
            get { return match; }
            set { match = value; OnPropertyChanged("Match"); }
        }

        private int minute;
        public int Minute
        {
            get { return minute; }
            set { minute = value; OnPropertyChanged("Minute"); }
        }

        private MatchEventInfo eventInfo;
        public MatchEventInfo EventInfo
        {
            get { return eventInfo; }
            set 
            { 
                eventInfo = value;
                if (eventInfo != null)
                {
                    UpdatePropertiesFromEventInfo();
                    OnPropertyChanged("EventInfo");
                }
            }
        }

        private void UpdatePropertiesFromEventInfo()
        {
            minute = eventInfo.Minute;

            if (eventInfo.Type == 0 && eventInfo.Info != null)
            {
                if (String.Equals(eventInfo.Info.Type, "goal"))
                {
                    imageUri = "/Assets/goal.png";
                    OnPropertyChanged("Image");
                    title = Localization.GetString("GoalLabel");
                    OnPropertyChanged("Title");
                }
                else if (String.Equals(eventInfo.Info.Type, "substitution"))
                {
                    imageUri = "/Assets/substitution.png";
                    OnPropertyChanged("Image");
                    title = Localization.GetString("SubstitutionLabel");
                    OnPropertyChanged("Title");
                }
                else if (String.Equals(eventInfo.Info.Type, "red_card"))
                {
                    imageUri = "/Assets/red_card.png";
                    OnPropertyChanged("Image");
                    title = Localization.GetString("RedCardLabel");
                    OnPropertyChanged("Title");
                }
                comment = eventInfo.Info.Text;
                OnPropertyChanged("Comment");
            }
            else if (eventInfo.Type == 1)
            {
                imageUri = "/Assets/goal.png";
                OnPropertyChanged("Image");
                title = App.CurrentApp.MyLigue1.MyTeamsInfo[eventInfo.Info.TeamId].ShortName;
                OnPropertyChanged("Title");
                comment = Localization.GetString("GoalLabel") + " " + Localization.GetString("OfLabel") + " " + eventInfo.Info.Player;
                OnPropertyChanged("Comment");
            }
            else if (eventInfo.Type == 2)
            {
                imageUri = "/Assets/red_card.png";
                OnPropertyChanged("Image");
                title = App.CurrentApp.MyLigue1.MyTeamsInfo[eventInfo.Info.TeamId].ShortName;
                OnPropertyChanged("Title");
                comment = Localization.GetString("RedCardLabel") + " " + Localization.GetString("ForLabel") + " " + eventInfo.Info.Player;
                OnPropertyChanged("Comment");
            }
            else if (eventInfo.Type == 4)
            {
                imageUri = "/Assets/play.png";
                OnPropertyChanged("Image");
                if (eventInfo.Video != null &&
                    eventInfo.Video.Title != null)
                {
                    title = eventInfo.Video.Title;
                    OnPropertyChanged("Title");
                }
            }
        }

        private string title;
        public string Title
        {
            get { return title; }
            set { title = value; OnPropertyChanged("Title"); }
        }
		
        private string comment;
        public string Comment
        {
            get { return comment; }
            set { comment = value; OnPropertyChanged("Comment"); }
        }

        private string imageUri;
        public string ImageUri
        {
            get { return imageUri; }
            set { imageUri = value; OnPropertyChanged("Image"); }
        }

        private ImageSource image = null;
        public ImageSource Image
        {
            get
            {
                if (image == null && !String.IsNullOrEmpty(imageUri))
                {
                    Image = new BitmapImage(new Uri(imageUri, UriKind.Relative));
                }
                return image;
            }
            set
            {
                if (object.ReferenceEquals(image, value))
                {
                    return;
                }
                image = value;
                OnPropertyChanged("Image");
            }
        }
    }
}

