// -------------------------------  Copyright ---------------------------------
//	This software has been developped by France Telecom, FT/RD/RD/MAPS/DVC/MSF
//
//	Copyright (c) France Telecom 2009-2010
//
// COPYRIGHT    : This file is the property of FRANCE TELECOM.
//                It cannot be copied, used, or modified without obtaining
//                an authorization from the authors or a mandated
//                member of FRANCE TELECOM.
//                If such an authorization is provided, any modified version or
//                copy of the software has to contain this header.
//
// WARRANTIES   : This software is made available by the authors in the  hope
//                that it will be useful, but without any warranty.
//                France Telecom is not liable for any consequence related to the
//                use of the provided software.
//
// AUTHORS      : France Telecom  / RD / MAPS / DVC / MSF
// ----------------------------------------------------------------------------
using System;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using Orange.SDK;
using Orange.SDK.Tools;
using Orange.SDK.Enablers.Ligue1;
using System.Collections.ObjectModel;
using System.Globalization;

namespace OrangeLigue1.ViewModels
{
    public class DayPageViewModel : ViewModel
    {
        #region "Singleton definition"

        private static DayPageViewModel _instance;
        public static DayPageViewModel Instance
        {
            get
            {
                if (_instance == null)
                {
                    _instance = new DayPageViewModel();
                }
                return _instance;
            }
        }
        #endregion

        private int day = 0;
        public int Day
        {
            get { return day; }
            set
            {
                System.Diagnostics.Debug.WriteLine("DAY :" + value);
                day = value;
                UpdatesPropertiesFromCalendarAction();
                OnPropertyChanged("DayNumberLabel");
            }
        }

        /// <summary>
        /// Returns the URI of the Day page
        /// </summary>
        public static Uri GetUri(int id)
        {
            return new Uri(string.Format("/Views/DayPage.xaml?day={0}", id), UriKind.Relative);
        }

        private WholeCalendarAction myCalendarAction;
        public WholeCalendarAction MyCalendarAction
        {
            get { return myCalendarAction; }
            set { myCalendarAction = value; }
        }

        private AllMatchAction myAllMatchAction;
        public AllMatchAction MyAllMatchAction
        {
            get { return myAllMatchAction; }
            set
            {
                myAllMatchAction = value;
                if (myAllMatchAction != null)
                {
                    UpdatesPropertiesFromAllMatchAction();
                    OnPropertyChanged("MyMatchsOfTheHourList");
                }
            }
        }

        /// <summary>
        /// List of matchs of the hour
        /// </summary>
        private ObservableCollection<MatchsOfTheHourViewModel> myMatchsOfTheHourList = new ObservableCollection<MatchsOfTheHourViewModel>();
        public ObservableCollection<MatchsOfTheHourViewModel> MyMatchsOfTheHourList
        {
            get { return myMatchsOfTheHourList; }
            set
            {
                myMatchsOfTheHourList = value;
                OnPropertyChanged("MyMatchsOfTheHourList");
            }
        }

        /// <summary>
        /// Fill MyMatchsOfTheHourList property
        /// </summary>
        void UpdatesPropertiesFromCalendarAction()
        {
            myMatchsOfTheHourList.Clear();

            if (myCalendarAction != null &&
                myCalendarAction.Matches != null &&
                myCalendarAction.Matches.Count > 0 && myCalendarAction.Matches.Count >= day &&
                myCalendarAction.Matches[day - 1] != null && //-1 for array
                myCalendarAction.Matches[day - 1].Matches != null && //-1 for array
                myCalendarAction.Matches[day - 1].Matches.Count > 0 && //-1 for array
                App.CurrentApp.MyLigue1 != null &&
                App.CurrentApp.MyLigue1.MyTeamsInfo != null)
            {
                string date = myCalendarAction.Matches[day - 1].Matches[0].Date;
                string hour = myCalendarAction.Matches[day - 1].Matches[0].Hour;

                ObservableCollection<MatchInfo> matchInfoOfTheHourList = new ObservableCollection<MatchInfo>();

                foreach (MatchInfo matchInfo in myCalendarAction.Matches[day - 1].Matches) //-1 for array
                {
                    if (String.Equals(hour, matchInfo.Hour) == false || String.Equals(date, matchInfo.Date) == false)
                    {
                        myMatchsOfTheHourList.Add(new MatchsOfTheHourViewModel()
                        {
                            Date = DateTime.Parse(date + " " + hour, CultureInfo.InvariantCulture).ToUniversalTime(),
                            Hour = hour,
                            MyMatchInfoList = matchInfoOfTheHourList
                        });
                        matchInfoOfTheHourList.Clear();
                        matchInfoOfTheHourList.Add(matchInfo);
                    }
                    else
                    {
                        matchInfoOfTheHourList.Add(matchInfo);
                    }

                    date = matchInfo.Date;
                    hour = matchInfo.Hour;
                }
                if (matchInfoOfTheHourList.Count != 0)
                {
                    myMatchsOfTheHourList.Add(new MatchsOfTheHourViewModel()
                    {
                        Date = DateTime.Parse(date + " " + hour).ToLocalTime(),
                        Hour = hour,
                        MyMatchInfoList = matchInfoOfTheHourList
                    });
                }
            }
        }

        private void UpdatesPropertiesFromAllMatchAction()
        {
            foreach (MatchInfo matchInfo in myAllMatchAction.Matches.Matches)
            {
                //NotPlayed = 0,
                //Playing = 1,
                //Finished = 2
                if (matchInfo.Status == 1)
                {
                    int localTeamScore = -1;
                    int visitorTeamScore = -1;
                    bool scoresNeedToBeUpdated = false;
                    FontFamily localTeamFontFamily = App.CurrentApp.Resources["PhoneFontFamilyNormal"] as FontFamily;
                    FontFamily visitorTeamFontFamily = App.CurrentApp.Resources["PhoneFontFamilyNormal"] as FontFamily;

                    if (matchInfo.Teams.LocalTeam.Id != 0 &&
                        matchInfo.Teams.VisitorTeam.Id != 0)
                    {
                        localTeamScore = matchInfo.Teams.LocalTeam.Score;
                        visitorTeamScore = matchInfo.Teams.VisitorTeam.Score;
                        scoresNeedToBeUpdated = true;
                    }

                    if (scoresNeedToBeUpdated == true)
                    {
                        if (localTeamScore > visitorTeamScore)
                        {
                            localTeamFontFamily = App.CurrentApp.Resources["PhoneFontFamilySemiBold"] as FontFamily;
                        }
                        else if (localTeamScore < visitorTeamScore)
                        {
                            visitorTeamFontFamily = App.CurrentApp.Resources["PhoneFontFamilySemiBold"] as FontFamily;
                        }

                        if (myMatchsOfTheHourList.Count > 0)
                        {
                            //match already exists and needs to be updated
                            foreach (MatchsOfTheHourViewModel matchsOfTheHour in myMatchsOfTheHourList)
                            {
                                bool found = false;
                                if (matchsOfTheHour.MyMatchsList != null)
                                {
                                    foreach (MatchItemViewModel item in matchsOfTheHour.MyMatchsList)
                                    {
                                        if (item.MatchId == matchInfo.Id)
                                        {
                                            item.LocalTeamScore = localTeamScore.ToString();
                                            item.LocalTeamFontFamily = localTeamFontFamily;
                                            item.VisitorTeamScore = visitorTeamScore.ToString();
                                            item.VisitorTeamFontFamily = visitorTeamFontFamily;
                                            found = true;
                                            break;
                                        }
                                    }
                                    if (found == true)
                                    {
                                        break;
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }

        public void MyAllMatchActionRequestEnd(object sender, AllMatchAction e)
        {
            this.MyAllMatchAction = e;
        }

        #region wording
        public string ApplicationTitleLabel
        {
            get
            {
                return Localization.GetString("ApplicationTitleLabel");
            }
        }

        public string DayNumberLabel
        {
            get
            {
                if (day == 1)
                {
                    return day.ToString() + "ère " + Localization.GetString("DayLabel");
                }
                else
                {
                    return day.ToString() + "e " + Localization.GetString("DayLabel");
                }
            }
        }
        #endregion

        /// <summary>
        /// Opens the previous article.
        /// Does nothing if there is no previous article.
        /// </summary>
        public void GoToPreviousDay()
        {
            if (myCalendarAction != null &&
                myCalendarAction.Matches != null)
            {
                if (day <= 1)
                {
                    Day = myCalendarAction.Matches.Count;
                }
                else
                {
                    Day--;
                }
            }
        }

        /// <summary>
        /// Opens the next article.
        /// Does nothing if there is no next article.
        /// </summary>
        public void GoToNextDay()
        {
            if (myCalendarAction != null &&
                myCalendarAction.Matches != null)
            {
                if (day >= myCalendarAction.Matches.Count)
                {
                    Day = 1;
                }
                else
                {
                    Day++;
                }
            }
        }
    }
}

