// -------------------------------  Copyright ---------------------------------
//	This software has been developped by France Telecom, FT/RD/RD/MAPS/DVC/MSF
//
//	Copyright (c) France Telecom 2009-2010
//
// COPYRIGHT    : This file is the property of FRANCE TELECOM.
//                It cannot be copied, used, or modified without obtaining
//                an authorization from the authors or a mandated
//                member of FRANCE TELECOM.
//                If such an authorization is provided, any modified version or
//                copy of the software has to contain this header.
//
// WARRANTIES   : This software is made available by the authors in the  hope
//                that it will be useful, but without any warranty.
//                France Telecom is not liable for any consequence related to the
//                use of the provided software.
//
// AUTHORS      : France Telecom  / RD / MAPS / DVC / MSF
// ----------------------------------------------------------------------------
using System;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using Orange.SDK;
using Orange.SDK.Enablers.Ligue1;
using System.Collections.ObjectModel;
using OrangeLigue1.Tools;
using System.Collections.Generic;

namespace OrangeLigue1.ViewModels
{
    public class PivotHighlightsSectionViewModel : ViewModel
    {
        private MatchAction myMatchAction;
        public MatchAction MyMatchAction
        {
            get { return myMatchAction; }
            set 
            { 
                myMatchAction = value;
                if (myMatchAction != null)
                {
                    UpdatesPropertiesFromMatchAction();
                    OnPropertyChanged("MyHightLightsList");
                    OnPropertyChanged("NoDataVisibility"); 
                }
            }
        }

        private MatchInfo myMatchInfo;
        public MatchInfo MyMatchInfo
        {
            get { return myMatchInfo; }
            set
            {
                myMatchInfo = value;
                if (myMatchInfo != null)
                {
                    UpdatesPropertiesFromMatchInfo();
                    OnPropertyChanged("MyHighlightsList");
                    OnPropertyChanged("NoDataVisibility"); 
                }
            }
        }

        /// <summary>
        /// List of highlights item
        /// </summary>
        private ObservableCollection<HighlightViewModel> myHighlightsList = new ObservableCollection<HighlightViewModel>();
        public ObservableCollection<HighlightViewModel> MyHighlightsList
        {
            get { return myHighlightsList; }
            set
            {
                myHighlightsList = value;
                OnPropertyChanged("MyHightLightsList");
            }
        }

        /// <summary>
        /// Map of highlights item
        /// </summary>
        private Dictionary<int, HighlightViewModel> myHighlightsDictionary = new Dictionary<int, HighlightViewModel>();

        public HighlightViewModel CurrentListItem
        {
            get { return null; }
            set
            {
                if (value != null)
                {
                    if (value.EventInfo.Type == 4)
                    {
                        SelectionChangedCommand(value.EventInfo.Video.Link, value.EventInfo.Video.Title);
                        OnPropertyChanged("CurrentListItem");
                    }
                }
            }
        }

        public Visibility NoDataVisibility
        {
            get
            {
                if (myHighlightsList.Count == 0)
                {
                    return Visibility.Visible;
                }
                return Visibility.Collapsed;
            }
        }

        public string NoDataText
        {
            get
            {
                return Localization.GetString("NoHighlightAvailableLabel");
            }
        }
        
        private void UpdatesPropertiesFromMatchAction()
        {
            myHighlightsList.Clear();

            if (myMatchAction.Matches != null &&
               myMatchAction.Matches.Count == 1 &&
               myMatchAction.Matches[0].Events != null)
            {
                //myMatchAction.Matches[0].Events.Reverse();
                foreach (MatchEventInfo eventInfo in myMatchAction.Matches[0].Events)
                {
                    if (eventInfo.Type != 0 && eventInfo.Type != 3) // != comment && != action
                    {
                        if (eventInfo.Type == 4)
                        {
                            if (!string.IsNullOrEmpty(eventInfo.Video.Title) && !string.IsNullOrEmpty(eventInfo.Video.Link))
                            {
                                myHighlightsList.Add(new HighlightViewModel()
                                {
                                    EventInfo = eventInfo
                                });
                                if (myHighlightsDictionary != null && (myHighlightsDictionary.ContainsKey(eventInfo.Id) == false))
                                {
                                    myHighlightsDictionary.Add(
                                       eventInfo.Id,
                                       null
                                   );
                                }
                            }
                        }
                        else
                        {
                            myHighlightsList.Add(new HighlightViewModel()
                            {
                                EventInfo = eventInfo
                            });
                            if (myHighlightsDictionary != null && (myHighlightsDictionary.ContainsKey(eventInfo.Id) == false))
                            {
                                myHighlightsDictionary.Add(
                                   eventInfo.Id,
                                   null
                               );
                            }
                        }
                    }
                }
            }
        }

        private void UpdatesPropertiesFromMatchInfo()
        {
            //myMatchInfo.Events.Reverse();
            foreach (MatchEventInfo eventInfo in myMatchInfo.Events)
            {
                if (eventInfo.Type != 0 && eventInfo.Type != 3) // != comment && != action
                {
                    if (eventInfo.Type == 4)
                    {
                        if (!string.IsNullOrEmpty(eventInfo.Video.Title) && !string.IsNullOrEmpty(eventInfo.Video.Link))
                        {
                            if (myHighlightsDictionary.ContainsKey(eventInfo.Id) == false)
                            {
                                myHighlightsList.Insert(0, new HighlightViewModel()
                                {
                                    EventInfo = eventInfo
                                });
                                myHighlightsDictionary.Add(
                                    eventInfo.Id,
                                    null
                                );
                            }
                            else
                            {
                                //update comments if needed
                                foreach (HighlightViewModel item in myHighlightsList)
                                {
                                    if (item.EventInfo.Id == eventInfo.Id)
                                    {
                                        item.EventInfo = eventInfo;
                                        break;
                                    }
                                }
                            }
                        }

                    }
                    else
                    {
                        if (myHighlightsDictionary.ContainsKey(eventInfo.Id) == false)
                        {
                            myHighlightsList.Insert(0, new HighlightViewModel()
                            {
                                EventInfo = eventInfo
                            });
                            myHighlightsDictionary.Add(
                                eventInfo.Id,
                                null
                            );
                        }
                        else
                        {
                            //update comments if needed
                            foreach (HighlightViewModel item in myHighlightsList)
                            {
                                if (item.EventInfo.Id == eventInfo.Id)
                                {
                                    item.EventInfo = eventInfo;
                                    break;
                                }
                            }
                        }
                    }
                }
            }
        }

        private void SelectionChangedCommand(string videoLink, string videoTitle)
        {
            if (String.IsNullOrEmpty(videoLink))
            {
                return;
            }

            if (Microsoft.Devices.Environment.DeviceType != Microsoft.Devices.DeviceType.Emulator)
            {
                ObjectRepository.Store(MediaElementPageViewModel.MediaUriString, videoLink);
                ObjectRepository.Store(MediaElementPageViewModel.MediaTitleString, videoTitle);
                ObjectRepository.Store(MediaElementPageViewModel.MediaLiveString, false);

                Navigator.GoToPage(MediaElementPageViewModel.GetUri());
            }
        }
    }
}

