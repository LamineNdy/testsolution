﻿using System;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using Orange.SDK;
using System.Windows.Media.Imaging;
using OrangeLigue1.Storage;
using Kawagoe.Storage;

namespace OrangeLigue1.ViewModels
{
    public class AccessRightsItemViewModel : ViewModel
    {
        static int _scaleFactor = -1;

        private string name;
        public string Name
        {
            get { return name; }
            set { name = value; OnPropertyChanged("Name"); }
        }

        private string id;
        public string Id
        {
            get { return id; }
            set { id = value; OnPropertyChanged("Id"); }
        }

        private string shortDesc;
        public string ShortDesc
        {
            get { return shortDesc; }
            set { shortDesc = value; OnPropertyChanged("ShortDesc"); }
        }

        private string desc;
        public string Desc
        {
            get { return desc; }
            set { desc = value; OnPropertyChanged("Desc"); }
        }

        private string price;
        public string Price
        {
            get { return price; }
            set { price = value; OnPropertyChanged("Price"); }
        }

        private string legal;
        public string Legal
        {
            get { return legal; }
            set { legal = value; OnPropertyChanged("Legal"); }
        }

        private string subscriptionStatus;
        public string SubscriptionStatus
        {
            get { return subscriptionStatus; }
            set { subscriptionStatus = value; OnPropertyChanged("SubscriptionStatus"); }
        }

        private int subscriptionEnum;
        public int SubscriptionEnum
        {
            get { return subscriptionEnum; }
            set { subscriptionEnum = value; OnPropertyChanged("SubscriptionEnum"); }
        }

        private string type;
        public string Type
        {
            get { return type; }
            set { type = value; OnPropertyChanged("Type"); }
        }

        private int txtHeight;
        public int TxtHeight
        {
            get {
                if (Id == "OptFoot500MoI")
                {
                    if (ScaleFactor() == 160) //Lumia 920
                        txtHeight = 450;
                    else if (ScaleFactor() == 150) //HTC 8X
                        txtHeight = 460;
                    else if (ScaleFactor() == 100) //Lumia 820
                        txtHeight = 500;
                }
                else
                {
                    if (ScaleFactor() == 160) //Lumia 920
                        txtHeight = 460;
                    else if (ScaleFactor() == 150) //HTC 8X
                        txtHeight = 470;
                    else if (ScaleFactor() == 100) //Lumia 820
                        txtHeight = 480;
                }
                return txtHeight; 
            }
            set
            {
                txtHeight = value;
                OnPropertyChanged("TxtHeight"); 
            }
        }

        public static int ScaleFactor()
        {
            if (_scaleFactor == -1)
            {
                if (Environment.OSVersion.Version.Major < 8)
                {
                    _scaleFactor = 100;
                }
                else
                {
                    var scaleFactorProperty = Application.Current.Host.Content.GetType().GetProperty("ScaleFactor");
                    if (scaleFactorProperty != null)
                        _scaleFactor = (int)scaleFactorProperty.GetValue(Application.Current.Host.Content, null);
                }

            }
            return _scaleFactor;            
        }
    }
}
