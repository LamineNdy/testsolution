﻿using Orange.SDK;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace OrangeLigue1.ViewModels
{
    public class LegalPageViewModel : ViewModel
    {
        public LegalPageViewModel()
        {
        }

        public string EditorLabel
        {
            get
            {
                return "Orange France SA \n RCS Créteil 428 706 097 \nSiège social: 1 avenue Nelson Mandela \n94745 Arcueil cedex \nN° de téléphone : 01.55.22.22.22\n";
            }
        }

        public string PublicatorLabel
        {
            get
            {
                return "Stéphane Richard\n";
            }
        }

        public string HostLabel
        {
            get
            {
                return "Orange France SA \nRCS Créteil 428 706 097 \nSiège social: 1 avenue Nelson Mandela \n94745 Arcueil cedex \nN° de téléphone : 01.55.22.22.22\n";
            }
        }

        public string ContactLabel
        {
            get
            {
                return "Pour tout renseignement, vous pouvez contacter votre service client habituel.\n";
            }
        }

        public string OwnerLabel
        {
            get
            {
                return "Orange France";
            }
        }   

        public static Uri GetUri()
        {
            return new Uri("/Views/LegalPage.xaml", UriKind.Relative);
        }
    }
}
