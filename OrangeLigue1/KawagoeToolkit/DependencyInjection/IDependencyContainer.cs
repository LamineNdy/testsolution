﻿// Copyright 2010 Andreas Saudemont (andreas.saudemont@gmail.com)
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//    http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

namespace Kawagoe.DependencyInjection
{
    /// <summary>
    /// The interface of dependency containers.
    /// </summary>
    public interface IDependencyContainer
    {
        /// <summary>
        /// Resolves the specified type.
        /// </summary>
        /// <typeparam name="T">The type to resolve.</typeparam>
        /// <returns>The resolved instance of <paramref name="T"/>, or <code>default(T)</code> if
        /// <paramref name="T"/> is not registered in the container.</returns>
        T Resolve<T>();
    }
}
