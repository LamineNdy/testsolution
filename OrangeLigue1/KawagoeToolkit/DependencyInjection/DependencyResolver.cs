﻿// Copyright 2010 Andreas Saudemont (andreas.saudemont@gmail.com)
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//    http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

using System;

namespace Kawagoe.DependencyInjection
{
    /// <summary>
    /// The entry-point class for dependency injection.
    /// </summary>
    public class DependencyResolver
    {
        /// <summary>
        /// The dependency container used by the resolver.
        /// </summary>
        public static IDependencyContainer Container
        {
            get;
            set;
        }

        /// <summary>
        /// Resolves the specified type.
        /// </summary>
        /// <typeparam name="T">The type to resolve.</typeparam>
        /// <returns>The resolved instance of <paramref name="T"/>, or <code>default(T)</code> if
        /// <paramref name="T"/> is not registered in the container.</returns>
        public static T Resolve<T>()
        {
            IDependencyContainer container = Container;
            if (container == null)
            {
                throw new InvalidOperationException("the dependency container is not set");
            }
            return container.Resolve<T>();
        }

        private DependencyResolver()
        {
        }
    }
}
