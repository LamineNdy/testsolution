﻿// Copyright 2010 Andreas Saudemont (andreas.saudemont@gmail.com)
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//    http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

using System;
using System.Collections.Generic;
using System.Reflection;

namespace Kawagoe.DependencyInjection
{
    /// <summary>
    /// Implements a basic dependency container.
    /// </summary>
    public class BasicDependencyContainer : IDependencyContainer
    {
        private readonly Dictionary<Type, object> _registeredObjects = new Dictionary<Type, object>();

        /// <summary>
        /// Initializes a new <see cref="BasicDependencyContainer"/> instance.
        /// </summary>
        public BasicDependencyContainer()
        {
        }

        /// <summary>
        /// Registers the specified type mapping within the container.
        /// </summary>
        /// <typeparam name="TFrom">The source type of the mapping.</typeparam>
        /// <typeparam name="TTo">The type <paramref name="TFrom"/> will resolve to.</typeparam>
        /// <exception cref="InvalidOperationException"><paramref name="TTo"/> is an interface, an abstract class,
        /// or has no public parameterless constructor.</exception>
        public void RegisterType<TFrom, TTo>() where TTo : TFrom
        {
            Type resolvedType = typeof(TTo);
            if (resolvedType.IsInterface || resolvedType.IsAbstract)
            {
                throw new InvalidOperationException("TTo cannot be instantiated");
            }
            ConstructorInfo ctorInfo = resolvedType.GetConstructor(new Type[] { });
            if (ctorInfo == null || !ctorInfo.IsPublic)
            {
                throw new InvalidOperationException("TTo has no public parameterless constructor");
            }
            lock (this)
            {
                _registeredObjects[typeof(TFrom)] =  resolvedType;
            }
        }

        /// <summary>
        /// Registers the specified instance mapping within the container.
        /// </summary>
        /// <typeparam name="T">The source type of the mapping.</typeparam>
        /// <param name="obj">The object <paramref name="TFrom"/> will resolve to.</param>
        /// <exception cref="InvalidOperationException"><paramref name="obj"/> is <c>null</c>
        /// or is not an instance of <paramref name="T"/>.</exception>
        public void RegisterInstance<T>(object obj)
        {
            if (obj == null)
            {
                throw new InvalidOperationException("obj cannot be null");
            }
            if (!(obj is T))
            {
                throw new InvalidOperationException("obj must be an instance of T");
            }
            lock (this)
            {
                _registeredObjects[typeof(T)] = obj;
            }
        }

        /// <summary>
        /// Implements <see cref="IDependencyContainer.Resolve"/>.
        /// </summary>
        public T Resolve<T>()
        {
            Type type = typeof(T);
            object obj;
            lock (this)
            {
                if (!_registeredObjects.ContainsKey(type))
                {
                    return default(T);
                }
                obj = _registeredObjects[type];
            }
            if (obj is Type)
            {
                obj = ((Type)obj).GetConstructor(new Type[] { }).Invoke(null);
            }
            return (T)obj;
        }
    }
}
