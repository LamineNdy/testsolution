// -------------------------------  Copyright ---------------------------------
//	This software has been developped by France Telecom, FT/RD/RD/MAPS/DVC/MSF
//
//	Copyright (c) France Telecom 2009-2010
//
// COPYRIGHT    : This file is the property of FRANCE TELECOM.
//                It cannot be copied, used, or modified without obtaining
//                an authorization from the authors or a mandated
//                member of FRANCE TELECOM.
//                If such an authorization is provided, any modified version or
//                copy of the software has to contain this header.
//
// WARRANTIES   : This software is made available by the authors in the  hope
//                that it will be useful, but without any warranty.
//                France Telecom is not liable for any consequence related to the
//                use of the provided software.
//
// AUTHORS      : France Telecom  / RD / MAPS / DVC / MSF
// ----------------------------------------------------------------------------

using System;
using System.IO.IsolatedStorage;
using System.Reflection;
using System.Text;
using System.Text.RegularExpressions;
using System.Windows;
using System.Windows.Media;
using System.Windows.Navigation;
using Microsoft.Phone.Controls;
using Microsoft.Phone.Shell;
using Microsoft.Phone.Tasks;
using OrangeLigue1.Model;
using OrangeLigue1.Storage;
using OrangeLigue1.Tools;
using OrangeLigue1.Views;
using OrangeLigue1.ViewModels;
using Microsoft.Phone.Notification;
using System.Windows.Threading;

namespace OrangeLigue1
{
    public partial class App : Application
    {
        /// <summary>
        /// The message sent when the transient state of the application is being saved.
        /// </summary>
        public const string SavingTransientStateMessage = "App.SavingTransientStateMessage";
        public static HttpNotificationChannel CurrentChannel { get; set; }
        /// <summary>
        /// The message sent when the application is shutting down.
        /// </summary>
        public const string ShutdownMessage = "App.ShutdownMessage";

        private static readonly TimeSpan BackgroundRequestsStopDelay = TimeSpan.FromSeconds(0);
        private const string LogFileName = "logs.txt";

        /// <summary>
        /// Set to <c>true</c> for production builds, to <c>false</c> otherwise.
        /// </summary>
        public const bool IsProductionBuild = true; 

        public static App CurrentApp
        {
            get { return (App)Application.Current; }
        }

        private void SetThemeResources()
        {
            (this.Resources["PhoneAccentBrush"] as SolidColorBrush).Color = Color.FromArgb(0xFF, 0xFF, 0x66, 0x00);
            (this.Resources["PhoneForegroundBrush"] as SolidColorBrush).Color = Color.FromArgb(0xFF, 0xFF, 0xFF, 0xFF);
            (this.Resources["PhoneBackgroundBrush"] as SolidColorBrush).Color = Color.FromArgb(0xFF, 0x00, 0x00, 0x00);
            (this.Resources["PhoneContrastBackgroundBrush"] as SolidColorBrush).Color = Color.FromArgb(0xFF, 0xFF, 0xFF, 0xFF);
            (this.Resources["PhoneContrastForegroundBrush"] as SolidColorBrush).Color = Color.FromArgb(0xFF, 0x00, 0x00, 0x00);
            (this.Resources["PhoneDisabledBrush"] as SolidColorBrush).Color = Color.FromArgb(0x66, 0xFF, 0xFF, 0xFF);
            (this.Resources["PhoneSubtleBrush"] as SolidColorBrush).Color = Color.FromArgb(0x99, 0xFF, 0xFF, 0xFF);
            (this.Resources["PhoneBorderBrush"] as SolidColorBrush).Color = Color.FromArgb(0xBF, 0xFF, 0xFF, 0xFF);
            (this.Resources["PhoneSemitransparentBrush"] as SolidColorBrush).Color = Color.FromArgb(0xAA, 0x00, 0x00, 0x00);
            (this.Resources["PhoneChromeBrush"] as SolidColorBrush).Color = Color.FromArgb(0xFF, 0x1F, 0x1F, 0x1F);
            (this.Resources["PhoneTextBoxBrush"] as SolidColorBrush).Color = Color.FromArgb(0xBF, 0xFF, 0xFF, 0xFF);
            (this.Resources["PhoneTextCaretBrush"] as SolidColorBrush).Color = Color.FromArgb(0xFF, 0x00, 0x00, 0x00);
            (this.Resources["PhoneTextBoxForegroundBrush"] as SolidColorBrush).Color = Color.FromArgb(0xFF, 0x00, 0x00, 0x00);
            (this.Resources["PhoneTextBoxEditBackgroundBrush"] as SolidColorBrush).Color = Color.FromArgb(0xFF, 0xFF, 0xFF, 0xFF);
            (this.Resources["PhoneTextBoxEditBorderBrush"] as SolidColorBrush).Color = Color.FromArgb(0xFF, 0xFF, 0xFF, 0xFF);
            (this.Resources["PhoneTextBoxReadOnlyBrush"] as SolidColorBrush).Color = Color.FromArgb(0x77, 0x00, 0x00, 0x00);
            (this.Resources["PhoneTextBoxSelectionForegroundBrush"] as SolidColorBrush).Color = Color.FromArgb(0xFF, 0xFF, 0xFF, 0xFF);
            (this.Resources["PhoneRadioCheckBoxBrush"] as SolidColorBrush).Color = Color.FromArgb(0xBF, 0xFF, 0xFF, 0xFF);
            (this.Resources["PhoneRadioCheckBoxDisabledBrush"] as SolidColorBrush).Color = Color.FromArgb(0x66, 0xFF, 0xFF, 0xFF);
            (this.Resources["PhoneRadioCheckBoxCheckBrush"] as SolidColorBrush).Color = Color.FromArgb(0xFF, 0x00, 0x00, 0x00);
            (this.Resources["PhoneRadioCheckBoxCheckDisabledBrush"] as SolidColorBrush).Color = Color.FromArgb(0x66, 0x00, 0x00, 0x00);
            (this.Resources["PhoneRadioCheckBoxPressedBrush"] as SolidColorBrush).Color = Color.FromArgb(0xFF, 0xFF, 0xFF, 0xFF);
            (this.Resources["PhoneRadioCheckBoxPressedBorderBrush"] as SolidColorBrush).Color = Color.FromArgb(0xFF, 0xFF, 0xFF, 0xFF);
        }

        private void AcquirePushChannel()
        {
            PushNotificationsHandler pushHandler = PushNotificationsHandler.SharedInstance;
            pushHandler.AcquirePushChannel();

        }

        /// <summary>
        /// The name of the application, as indicated by the application's assembly.
        /// </summary>
        public static string Name
        {
            get
            {
                return "Ligue1Orange";
                /*
                try
                {
                    return Regex.Match(Assembly.GetExecutingAssembly().FullName, "([^,]*)").Groups[1].Value;
                }
                catch (Exception)
                {
                    return "unknown";
                }*/
            }
        }

        /// <summary>
        /// The version number of the application, as indicated by the application's assembly.
        /// </summary>
        public static string VersionNumber
        {
            get
            {
                try
                {
                   return Regex.Match(Assembly.GetExecutingAssembly().FullName, "Version=([^ ,]+)").Groups[1].Value;
                }
                catch (Exception)
                {
                    return "unknown";
                }
            }
        }
		
		/// <summary>
        /// The version number of the application formatted as MAJOR.MINOR.
        /// </summary>
        public static string ShortVersionNumber
        {
            get
            {
                try
                {
                    System.Text.RegularExpressions.Match match = Regex.Match(VersionNumber, "([0-9]+\\.[0-9]+)");
                    if (match.Groups.Count >= 2)
                    {
                        return match.Groups[1].Value;
                    }
                }
                catch (Exception) { }
                return "unknown";
            }
        }

        /// <summary>
        /// Set to <c>true</c> when the application was launched, to <c>false</c> when it was activated.
        /// </summary>
        public bool WasLaunched
        {
            get
            {
                return !WasActivated;
            }
        }

        /// <summary>
        /// Set to <c>true</c> when the application was activated, to <c>false</c> when it was launched.
        /// </summary>
        public bool WasActivated
        {
            get;
            private set;
        }

        // Easy access to the root frame
        public PhoneApplicationFrame RootFrame { get; private set; }

        // Constructor
        public App()
        {
            WasActivated = false;

            // Global handler for uncaught exceptions. 
            // Note that exceptions thrown by ApplicationBarItem.Click will not get caught here.
            UnhandledException += Application_UnhandledException;

            // Standard Silverlight initialization
            InitializeComponent();

            //override phone's theme
            this.SetThemeResources();

            // Phone-specific initialization
            InitializePhoneApplication();

            RootFrame.Navigated += OnFirstRootFrameNavigated;
        }
	
        public Ligue1 MyLigue1
        {
            get;
            set;
        }

        public NotificationBar notificationBar
        {
            get;
            set;
        }
                
        public System.Windows.Controls.Primitives.Popup popup
        {
            get;
            set;
        }
		
		#region Application Initialization and Shutdown

        // Code to execute when the application is launching (eg, from Start)
        // This code will not execute when the application is reactivated
        private void Application_Launching(object sender, LaunchingEventArgs e)
        {
            AcquirePushChannel();
            System.Diagnostics.Debug.WriteLine("Application launching.");
            InitializePhase1();
        }

        // Code to execute when the application is activated (brought to foreground)
        // This code will not execute when the application is first launched
        private void Application_Activated(object sender, ActivatedEventArgs e)
        {
            WasActivated = true;
            System.Diagnostics.Debug.WriteLine("Application activated.");
            InitializePhase1();
        }

        // Code to execute when the application is deactivated (sent to background)
        // This code will not execute when the application is closing
        private void Application_Deactivated(object sender, DeactivatedEventArgs e)
        {
            System.Diagnostics.Debug.WriteLine("Application deactived.");
            Shutdown(true);
        }

        // Code to execute when the application is closing (eg, user hit Back)
        // This code will not execute when the application is deactivated
        private void Application_Closing(object sender, ClosingEventArgs e)
        {
            System.Diagnostics.Debug.WriteLine("Application closing.");
            Shutdown(false);
        }

        private void OnFirstRootFrameNavigated(object sender, NavigationEventArgs e)
        {
            RootFrame.Navigated -= OnFirstRootFrameNavigated;
            InitializePhase2();
        }

        /// <summary>
        /// Performs the first phase of the application initialization when the application is launched
        /// or activated.
        /// </summary>
        private void InitializePhase1()
        {
            // Prevent the application from being deactivated when the device is locked while the debugger is attached
            if (!IsProductionBuild && System.Diagnostics.Debugger.IsAttached)
            {
                PhoneApplicationService.Current.ApplicationIdleDetectionMode = IdleDetectionMode.Disabled;
            }

            SimpleDependencyResolver dependencyResolver = new SimpleDependencyResolver();
            DependencyContainer.Initialize(dependencyResolver);
            SetupDebuggingAides();
            PersistentDataStore dataStore = new PersistentDataStore(CurrentConfigurationName);
            if (MustClearStorage)
            {
                MustClearStorage = false;
                dataStore.DeleteAll();
                Ligue1ImageCache.Default.Clear();
            }
            dependencyResolver.RegisterInstance<IDataStore>(dataStore);
            dependencyResolver.RegisterInstance<IPageNavigator>(new PhonePageNavigator() { Frame = RootFrame });
        }

        /// <summary>
        /// Performs the second phase of the application initialization after the first occurence
        /// of the RootFrame.Navigated event.
        /// </summary>
        private void InitializePhase2()
        {
			PreloadAssemblies();
			
			AuthenticationManagerDelegate.Current.Initialize();
			
            MyLigue1 = Ligue1.Restore();
            MyLigue1.RestoreTeamsInfo();
            MyLigue1.RestoreParamInfo();
            MyLigue1.PopulateTeamsInfo();
            MyLigue1.Initialize();
            PanoramaViewModel.Instance.Initialize();

            notificationBar = new NotificationBar();
            popup = new System.Windows.Controls.Primitives.Popup();
            popup.Child = notificationBar;
		}
		
		/// <summary>
        /// Preloads the assemblies used by the application in order to reduce UI latency
        /// later when these assemblies will be used.
        /// </summary>
        private void PreloadAssemblies()
        {

        }

        /// <summary>
        /// Sets up the application's debugging aides.
        /// </summary>
        private void SetupDebuggingAides()
        {
            if (IsProductionBuild)
            {
                if (System.Diagnostics.Debugger.IsAttached)
                {
                    Logger.ActualLogger = new DebuggerLogger();
                }
                return;
            }

            PersistentLogger logger = new PersistentLogger(LogFileName);
            if (WasLaunched)
            {
                string previousLogs = logger.GetAll();
                logger.ClearAll();
                if (string.IsNullOrEmpty(previousLogs) == false)
                {
                    try
                    {
                        MessageBoxResult result = MessageBox.Show(
                            "It looks like something went wrong during the previous run of the application.\n"
                            + "Do you want to help improve quality by sending debug info to the development team?",
                            "Help Improve Quality?",
                            MessageBoxButton.OKCancel);
                        if (result == MessageBoxResult.OK)
                        {
                            EmailComposeTask task = new EmailComposeTask();
                            task.To = "patrick.monfort@orange.com";
                            task.Subject = "Orange Ligue 1 Logs";
                            task.Body = previousLogs;
                            task.Show();
                        }
                    }
                    catch (Exception)
                    {
                        // ignore
                    }
                }
            }
            Logger.ActualLogger = logger;

#if DEBUG
            Application.Current.Host.Settings.EnableFrameRateCounter = true;
#endif
        }

        private void Shutdown(bool saveTransientState)
        {
            Logger.DebugTrace("Shutting down application...");

            if (saveTransientState)
            {
                Logger.DebugTrace("Sending saving transient state message...");
                Messenger.Current.Send(this, SavingTransientStateMessage);
                Logger.DebugTrace("Saving transient state message sent.");
            }

            Logger.DebugTrace("Sending app shutdown message...");
            Messenger.Current.Send(this, ShutdownMessage);
            Logger.DebugTrace("App shutdown message sent.");

            BackgroundWebRequestQueue.StopAll(BackgroundRequestsStopDelay);

            AuthenticationManagerDelegate.Current.Shutdown();

            IDataStore dataStore;
            try
            {
                dataStore = DependencyContainer.Resolve<IDataStore>();
            }
            catch (InvalidOperationException)
            {
                dataStore = null;
            }
            if (dataStore != null)
            {
                dataStore.Close();
            }

            DependencyContainer.Uninitialize();

            Logger.DebugTrace("Application has been shut down.");
        }

        #endregion

        // Code to execute if a navigation fails
        void RootFrame_NavigationFailed(object sender, NavigationFailedEventArgs e)
        {
            if (System.Diagnostics.Debugger.IsAttached)
            {
                // A navigation has failed; break into the debugger
                System.Diagnostics.Debugger.Break();
            }
        }

        // Code to execute on Unhandled Exceptions
        private void Application_UnhandledException(object sender, ApplicationUnhandledExceptionEventArgs e)
        {
            e.Handled = true;
            if (!App.IsProductionBuild)
            {
                Logger.LogException(e.ExceptionObject, "Unhandled exception (stack trace follows)!");
                if (!string.IsNullOrEmpty(e.ExceptionObject.StackTrace))
                {
                    string[] stackTraceLines = e.ExceptionObject.StackTrace.Split(new string[] { "\r\n" }, StringSplitOptions.RemoveEmptyEntries);
                    foreach (string stackTraceLine in stackTraceLines)
                    {
                        Logger.Log(stackTraceLine);
                    }
                }
            }
            if (System.Diagnostics.Debugger.IsAttached)
            {
                System.Diagnostics.Debugger.Break();
            }
        }

        /// <summary>
        /// Returns <c>true</c> if the resource with the specified URI exists in the application,
        /// <c>false</c> otherwise.
        /// </summary>
        public bool ResourceExists(Uri resourceUri)
        {
            if (resourceUri == null || resourceUri.IsAbsoluteUri)
            {
                return false;
            }
            try
            {
                string resourceUriString = resourceUri.ToString();
                if (resourceUriString.StartsWith("/"))
                {
                    resourceUri = new Uri(resourceUriString.Substring(1, resourceUriString.Length - 1), UriKind.Relative);
                }
                if (Application.GetResourceStream(resourceUri) != null)
                {
                    return true;
                }
            }
            catch (Exception) { }
            return false;
        }

        #region Application Configuration

        private const string UserAgentSettingsKey = "App.UserAgent";

        public string DefaultUserAgent
        {
            get
            {
                StringBuilder userAgent = new StringBuilder();
                userAgent.Append(Name)
                    .Append("_WP8")
                    .Append('_').Append(ShortVersionNumber)
                    .Append('_').Append(DeviceInfo.Manufacturer)
                    .Append('_').Append(DeviceInfo.Name.Replace("_", "").Replace(" ", ""))
                    //.Append("_Windows Phone OS ")
                    .Append("_")
                    .Append(Environment.OSVersion.Version.Major)
                    .Append('.')
                    .Append(Environment.OSVersion.Version.Minor);
                return userAgent.ToString();
            }
        }

        public string iPhoneUserAgent
        {
            get
            {
                return "Mozilla/5.0 (Ligue1AACOrangeTVPlayer4iPhone) OrangeAppliL1/2.0";
            }
        }

        /// <summary>
        /// The default user-agent value for HTTP requests sent by the application.
        /// </summary>
        public string CurrentUserAgent
        {
            get
            {
                if (IsProductionBuild)
                {
                    return DefaultUserAgent;
                }
                try
                {
                    IsolatedStorageSettings appSettings = IsolatedStorageSettings.ApplicationSettings;
                    if (appSettings == null || appSettings.Contains(UserAgentSettingsKey) == false)
                    {
                        return DefaultUserAgent;
                    }
                    string userAgent = (string)appSettings[UserAgentSettingsKey];
                    if (string.IsNullOrEmpty(userAgent))
                    {
                        return DefaultUserAgent;
                    }
                    return userAgent;
                }
                catch (Exception)
                {
                    return DefaultUserAgent;
                }
            }
            set
            {
                if (IsProductionBuild)
                {
                    throw new InvalidOperationException();
                }
                try
                {
                    IsolatedStorageSettings appSettings = IsolatedStorageSettings.ApplicationSettings;
                    if (string.IsNullOrEmpty(value))
                    {
                        if (!appSettings.Contains(UserAgentSettingsKey))
                        {
                            return;
                        }
                        appSettings.Remove(UserAgentSettingsKey);
                    }
                    else
                    {
                        if (appSettings.Contains(UserAgentSettingsKey) && (string)appSettings[UserAgentSettingsKey] == value)
                        {
                            return;
                        }
                        appSettings[UserAgentSettingsKey] = value;
                    }
                    appSettings[MustClearStorageSettingsKey] = true;
                    appSettings.Save();
                    MessageBox.Show("The new settings will be applied the next time you launch the application.", "Settings Changed", MessageBoxButton.OK);
                }
                catch (Exception) { }
            }
        }

        public string DefaultConfigurationName
        {
            get
            {
                return "OrangeFR_Integration";
            }
        }

        private const string ConfigurationNameSettingsKey = "App.ConfigurationName";

        public string CurrentConfigurationName
        {
            get
            {
                if (IsProductionBuild)
                {
                    return DefaultConfigurationName;
                }
                try
                {
                    IsolatedStorageSettings appSettings = IsolatedStorageSettings.ApplicationSettings;
                    if (appSettings == null || appSettings.Contains(ConfigurationNameSettingsKey) == false)
                    {
                        return DefaultConfigurationName;
                    }
                    string configurationName = (string)appSettings[ConfigurationNameSettingsKey];
                    if (string.IsNullOrEmpty(configurationName))
                    {
                        return DefaultConfigurationName;
                    }
                    return configurationName;
                }
                catch (Exception)
                {
                    return DefaultConfigurationName;
                }
            }
            set
            {
                if (IsProductionBuild)
                {
                    throw new InvalidOperationException();
                }
                try
                {
                    IsolatedStorageSettings appSettings = IsolatedStorageSettings.ApplicationSettings;
                    if (string.IsNullOrEmpty(value))
                    {
                        if (!appSettings.Contains(ConfigurationNameSettingsKey))
                        {
                            return;
                        }
                        appSettings.Remove(ConfigurationNameSettingsKey);
                    }
                    else
                    {
                        if (appSettings.Contains(ConfigurationNameSettingsKey) && (string)appSettings[ConfigurationNameSettingsKey] == value)
                        {
                            return;
                        }
                        appSettings[ConfigurationNameSettingsKey] = value;
                    }
                    appSettings[MustClearStorageSettingsKey] = true;
                    appSettings.Save();
                    MessageBox.Show("The new settings will be applied the next time you launch the application.", "Settings Changed", MessageBoxButton.OK);
                }
                catch (Exception) { }
            }
        }

        private const string MustClearStorageSettingsKey = "App.MustClearStorage";

        public bool MustClearStorage
        {
            get
            {
                if (IsProductionBuild)
                {
                    return false;
                }
                try
                {
                    IsolatedStorageSettings appSettings = IsolatedStorageSettings.ApplicationSettings;
                    if (appSettings == null || appSettings.Contains(MustClearStorageSettingsKey) == false)
                    {
                        return false;
                    }
                    return (bool)appSettings[MustClearStorageSettingsKey];
                }
                catch (Exception)
                {
                    return false;
                }
            }
            set
            {
                if (IsProductionBuild)
                {
                    return;
                }
                try
                {
                    IsolatedStorageSettings appSettings = IsolatedStorageSettings.ApplicationSettings;
                    if (!value)
                    {
                        if (appSettings.Contains(MustClearStorageSettingsKey))
                        {
                            appSettings.Remove(MustClearStorageSettingsKey);
                            appSettings.Save();
                        }
                    }
                    else
                    {
                        appSettings[MustClearStorageSettingsKey] = value;
                        appSettings.Save();
                        MessageBox.Show("The new settings will be applied the next time you launch the application.", "Settings Changed", MessageBoxButton.OK);
                    }
                }
                catch (Exception) { }
            }
        }

        #endregion

        #region UserIdleDetectionMode

        private int _userIdleDetectionCounter = 0;

        public void PreventUserIdleDetectionMode()
        {
            if (_userIdleDetectionCounter == 0)
            {
                PhoneApplicationService.Current.UserIdleDetectionMode = IdleDetectionMode.Disabled;
            }
            _userIdleDetectionCounter += 1;
        }

        public void AllowUserIdleDetectionMode()
        {
            _userIdleDetectionCounter -= 1;
            if (_userIdleDetectionCounter == 0)
            {
                PhoneApplicationService.Current.UserIdleDetectionMode = IdleDetectionMode.Enabled;
            }
        }

        #endregion

        #region Phone application initialization

        // Avoid double-initialization
        private bool phoneApplicationInitialized = false;

        // Do not add any additional code to this method
        private void InitializePhoneApplication()
        {
            if (phoneApplicationInitialized)
                return;

            // Create the frame but don't set it as RootVisual yet; this allows the splash
            // screen to remain active until the application is ready to render.
            RootFrame = new PhoneApplicationFrame();
            RootFrame.Navigated += CompleteInitializePhoneApplication;

            // Handle navigation failures
            RootFrame.NavigationFailed += RootFrame_NavigationFailed;

            // Ensure we don't initialize again
            phoneApplicationInitialized = true;
        }

        // Do not add any additional code to this method
        private void CompleteInitializePhoneApplication(object sender, NavigationEventArgs e)
        {
            // Set the root visual to allow the application to render
            if (RootVisual != RootFrame)
                RootVisual = RootFrame;

            // Remove this handler since it is no longer needed
            RootFrame.Navigated -= CompleteInitializePhoneApplication;
        }

        #endregion
    }
}
