// -------------------------------  Copyright ---------------------------------
//	This software has been developped by France Telecom, FT/RD/RD/MAPS/DVC/MSF
//
//	Copyright (c) France Telecom 2009-2010
//
// COPYRIGHT    : This file is the property of FRANCE TELECOM.
//                It cannot be copied, used, or modified without obtaining
//                an authorization from the authors or a mandated
//                member of FRANCE TELECOM.
//                If such an authorization is provided, any modified version or
//                copy of the software has to contain this header.
//
// WARRANTIES   : This software is made available by the authors in the  hope
//                that it will be useful, but without any warranty.
//                France Telecom is not liable for any consequence related to the
//                use of the provided software.
//
// AUTHORS      : France Telecom  / RD / MAPS / DVC / MSF
// ----------------------------------------------------------------------------
using System;
using System.Collections.Generic;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using Microsoft.Phone.Net.NetworkInformation;
using OrangeLigue1.Model;
using OrangeLigue1.Tools;
using OrangeLigue1.ViewModels;
using Orange.SDK.Enablers.Ligue1;
using Kawagoe.Controls;
using AuthenticationManager;

namespace OrangeLigue1
{
    public partial class MainPage : Microsoft.Phone.Controls.PhoneApplicationPage
    {
        private PanoramaViewModel _viewModel = null;
        private bool appVersionAlreadyChecked = false;
        private bool fairUse = false;
        private bool upgradePlan = false;
        private bool multiplexIsInProgress = false;

        public MainPage()
        {
            InitializeComponent();

            Panorama.Title = Localization.GetString("ApplicationTitleLabel");
            NewsSection.Header = Localization.GetString("NewsTitleLabel");
            RankingSection.Header = Localization.GetString("RankingTitleLabel");
            SummariesSection.Header = Localization.GetString("SummariesTitleLabel");
            LoadingTextBlock.Text = Localization.GetString("MainPage_LoadingText");
            MultiplexNoMatchsLabel.Text = Localization.GetString("MultiplexLoading");
            MultiplexNoMatchsTitle.Text = Localization.GetString("MultiplexNextMatchTitle");

            VisualStateManager.GoToState(this, LoadingState.Name, false);
        }

        protected override void OnNavigatedTo(System.Windows.Navigation.NavigationEventArgs e)
        {
            base.OnNavigatedTo(e);
            LayoutUpdated += CompletePageInitialization;
        }

        protected override void OnNavigatedFrom(System.Windows.Navigation.NavigationEventArgs e)
        {
            base.OnNavigatedFrom(e);
        }

		private void CompletePageInitialization(object sender, EventArgs e)
        {
            LayoutUpdated -= CompletePageInitialization;

            BackgroundActivityBeacon.Current.RegisterProgressBar(BackgroundActivityIndicator);

            if (_viewModel == null)
            {
                _viewModel = PanoramaViewModel.Instance;
                _viewModel.PanoramaIsLoaded += _viewModel_PanoramaIsLoaded;
                _viewModel.MagFootIsLoaded += _viewModel_PanoramaIsLoaded;
                _viewModel.UserinfoRequestEnd += _viewModel_UserinfoRequestEnd;
                _viewModel.WordingRequestEnd += _viewModel_WordingRequestEnd;
                _viewModel.ConnectionErrorRequestEnd += myPanoramaViewModel_ConnectionErrorRequestEnd;

                if (_viewModel.MyPanoramaMultiplexSectionViewModel != null)
                {
                    _viewModel.MyPanoramaMultiplexSectionViewModel.PropertyChanged += myPanoramaMultiplexSectionViewModelHasChanged;
                }

				DataContext = _viewModel;
            }    
            UpdateVisualState(true);
        }

        void _viewModel_WordingRequestEnd(object sender, WordingAction e)
        {
            if (fairUse)
            {
                displayPopup(e.Elements[3].Value);
                _viewModel.UserinfoRequestEnd -= _viewModel_UserinfoRequestEnd;
            }

            if (upgradePlan)
            {
                MessagePopup popup = new MessagePopup();
                popup.Message = e.Elements[2].Value;
                popup.AddCancelButton("Annuler", null, null);
                popup.AddButton("Plus d'infos", (senders, state) => OpenCGU(), null);
                popup.Open();
                _viewModel.UserinfoRequestEnd -= _viewModel_UserinfoRequestEnd;
            }
        }

        private void OpenCGU()
        {
            Navigator.GoToPage(AccessRightsPageViewModel.GetUri());
        }


        void _viewModel_UserinfoRequestEnd(object sender, UserinfoAction e)
        {
            if (e.FairUse == 1)
            {
                fairUse = true;
                if (_viewModel.MyLigue1.MyWordingAction != null)
                {
                    displayPopup(_viewModel.MyLigue1.MyWordingAction.Elements[3].Value);                    
                }
                _viewModel.UserinfoRequestEnd -= _viewModel_UserinfoRequestEnd;
            }
            else
                fairUse = false;

            if (e.UpgradePlan == 1)
            {
                upgradePlan = true;
                if (_viewModel.MyLigue1.MyWordingAction != null)
                {
                    MessagePopup popup = new MessagePopup();
                    popup.Message = _viewModel.MyLigue1.MyWordingAction.Elements[2].Value;
                    popup.AddCancelButton("Annuler", null, null);
                    popup.AddButton("Plus d'infos", (senders, state) => OpenCGU(), null);
                    popup.Open();
                }
                _viewModel.UserinfoRequestEnd -= _viewModel_UserinfoRequestEnd;
            }
            else
                upgradePlan = false;
            
        }

        void displayPopup(string message)
        {
            MessagePopup popup = new MessagePopup();
            popup.Message = message;
            popup.AddButton("OK", null, null);
            popup.Open();
            
        }
		
        void myPanoramaMultiplexSectionViewModelHasChanged(object sender, System.ComponentModel.PropertyChangedEventArgs e)
        {
            string propname = e.PropertyName;

            if (String.Equals(propname, "MultiplexIsInProgress"))
            {
                if ( _viewModel.MyPanoramaMultiplexSectionViewModel.MultiplexIsInProgress && multiplexIsInProgress == false)
                {
                    this.MultiplexListBox.Visibility = Visibility.Visible;
                    this.MultiplexNoMatchsLabel.Visibility = Visibility.Collapsed;
                    this.MultiplexNoMatchsTitle.Visibility = Visibility.Collapsed;
                    this.MultiplexNoMatchsPassButton.Visibility = Visibility.Collapsed;
                    multiplexIsInProgress = true;
                }
                else if (! _viewModel.MyPanoramaMultiplexSectionViewModel.MultiplexIsInProgress && multiplexIsInProgress == true)
                {
                    this.MultiplexListBox.Visibility = Visibility.Collapsed;
                    this.MultiplexNoMatchsLabel.Visibility = Visibility.Visible;
                    this.MultiplexNoMatchsTitle.Visibility = Visibility.Visible;
                    this.MultiplexNoMatchsPassButton.Visibility = Visibility.Visible;
                    this.MultiplexNoMatchsLabel.Text = _viewModel.MyPanoramaMultiplexSectionViewModel.MultiplexNoMatchLabel;
                    multiplexIsInProgress = false;
                }
            }
            else if (String.Equals(propname, "NoMoreMatchsToday"))
            {
                if (_viewModel.MyPanoramaMultiplexSectionViewModel.NoMoreMatchesToday)
                {
                    this.MultiplexListBox.Visibility = Visibility.Collapsed;
                    this.MultiplexNoMatchsLabel.Visibility = Visibility.Visible;
                    this.MultiplexNoMatchsTitle.Visibility = Visibility.Visible;
                    this.MultiplexNoMatchsPassButton.Visibility = Visibility.Visible;
                    this.MultiplexNoMatchsLabel.Text = _viewModel.MyPanoramaMultiplexSectionViewModel.MultiplexNoMatchLabel;
                }
                else
                {
                    this.MultiplexListBox.Visibility = Visibility.Visible;
                    this.MultiplexNoMatchsLabel.Visibility = Visibility.Collapsed;
                    this.MultiplexNoMatchsTitle.Visibility = Visibility.Collapsed;
                    this.MultiplexNoMatchsPassButton.Visibility = Visibility.Collapsed;
                }
            }
        }
		
		/// <summary>
        /// Updates the page's visual state depending on the current context.
        /// </summary>
        private void UpdateVisualState(bool useTransitions)
        {
            if (_viewModel == null)
            {
                return;
            }

            if (appVersionAlreadyChecked == false &&
                _viewModel.MyLigue1 != null &&
                _viewModel.MyLigue1.MyDynamicInitActionIsLoaded== true &&
                _viewModel.MyLigue1.MyDynamicInitAction != null &&
                _viewModel.MyLigue1.MyDynamicInitAction.Version != null)
            {
                DynamicInitAction initInfo = _viewModel.MyLigue1.MyDynamicInitAction;

                switch ((VersionStatus)initInfo.Version.Status)
                {
                    case VersionStatus.None:
                            break;
                        case VersionStatus.OptionalUpdate:
                            if (!appVersionAlreadyChecked)
                            {
                                NotifyOptionalAppUpdate(initInfo.Version.Message, initInfo.Version.Url);
                            }
                            break;
                        case VersionStatus.MandatoryUpdate:
                            GoToMandatoryAppUpdateState(initInfo.Version.Message, initInfo.Version.Url, true);
                            return;
                        case VersionStatus.EndOfLife:
                            GoToEndOfLifeState(initInfo.Version.Message, initInfo.Version.Url, true);
                            return;
                        case VersionStatus.Message:
                            if (!appVersionAlreadyChecked)
                            {
                                NotifyServiceMessage(initInfo.Version.Message, initInfo.Version.Url);
                            }
                            break;
                        default:
                            Logger.Log("Ignoring unexpected AMP version status {0} (message: {1})",
                                (int)initInfo.Version.Status, (initInfo.Version.Message ?? "(none)"));
                            break;
                    }
                    appVersionAlreadyChecked = true;
                }

            if (_viewModel.MyPanoramaMultiplexSectionViewModel != null)
            {
                _viewModel.MyPanoramaMultiplexSectionViewModel.CheckMultiplexStatus();
            }

            if (_viewModel.MyPanoramaNewsSectionViewModel != null &&
                _viewModel.MyPanoramaNewsSectionViewModel.MagFootIsAvailable == true)
            {
                VisualStateManager.GoToState(this, PanoramaStateMagFootActivated.Name, useTransitions);
            }
            else
            {
                VisualStateManager.GoToState(this, PanoramaState.Name, useTransitions);
            }
		}

        void _viewModel_PanoramaIsLoaded(object sender, EventArgs e)
        {
            UpdateVisualState(true);
        }

        void myPanoramaViewModel_ConnectionErrorRequestEnd(object sender, Tools.BackgroundRequestCompletedEventArgs e)
        {
            string userMessage = null;

            OLAuthenticationException oLAuthenticationException = null;

            if (e != null &&
                e.Error != null)
            {
                if (e.Error is OLAuthenticationException && DeviceNetworkInformation.IsWiFiEnabled)
                {
                    oLAuthenticationException = e.Error as OLAuthenticationException;
                    if (oLAuthenticationException != null)
                    {
                        if(oLAuthenticationException.ErrorCode == "AUTHENTICATION_ERROR")
                            userMessage = Localization.GetString("Message_DisableWiFi");
                        else
                            userMessage = oLAuthenticationException.ErrorMessage;
                    }
                }
            }

            WebException webException = null;

            if (userMessage == null &&
                e != null &&
                e.Error != null &&
                e.Error.InnerException != null)
            {
                webException = e.Error.InnerException as WebException;
            }

            if (webException != null && webException.Response != null && webException.Response is HttpWebResponse)
            {
                HttpWebResponse webResponse = (HttpWebResponse)(webException.Response);
                if (webResponse.StatusCode == HttpStatusCode.Forbidden)
                {
                    userMessage = Localization.GetString("Message_DisableWiFi");
                }
            }
            if (userMessage == null)
            {
                if (!DeviceNetworkInformation.IsCellularDataEnabled && !DeviceNetworkInformation.IsNetworkAvailable)
                    userMessage = Localization.GetString("Message_NoConnection");
                else if (!DeviceNetworkInformation.IsCellularDataEnabled && DeviceNetworkInformation.IsWiFiEnabled)
                    userMessage = Localization.GetString("Message_DisableWiFi");
                else
                    userMessage = Localization.GetString("Message_NoConnection");
            }
            CannotLaunchTextBlock.Text = userMessage;
            VisualStateManager.GoToState(this, CannotLaunchState.Name, false);
        }

        private void GoToMandatoryAppUpdateState(string message, string url, bool useTransitions)
        {
            if (!string.IsNullOrEmpty(message))
            {
                AppUpdateTextBlock.Text = message;
            }
            if (!string.IsNullOrEmpty(url))
            {
                AppUpdateMoreButton.Click += (sender, e) => App.CurrentApp.MyLigue1.OpenUrl(url);
            }
            else
            {
                AppUpdateMoreButton.Visibility = Visibility.Collapsed;
            }
            VisualStateManager.GoToState(this, MandatoryAppUpdateState.Name, useTransitions);
        }

        private void GoToEndOfLifeState(string message, string url, bool useTransitions)
        {
            if (!string.IsNullOrEmpty(message))
            {
                EndOfLifeTextBlock.Text = message;
            }
            if (!string.IsNullOrEmpty(url))
            {
                EndOfLifeMoreButton.Click += (sender, e) => App.CurrentApp.MyLigue1.OpenUrl(url);
            }
            else
            {
                EndOfLifeMoreButton.Visibility = Visibility.Collapsed;
            }
            VisualStateManager.GoToState(this, EndOfLifeState.Name, useTransitions);
        }

        private void NotifyOptionalAppUpdate(string message, string url)
        {
            MessagePopup popup = new MessagePopup();
            popup.Title = Localization.GetString("OptionalAppUpdateMessage_Title");
            if (!string.IsNullOrEmpty(message))
            {
                popup.Message = message;
            }
            else
            {
                popup.Message = Localization.GetString("OptionalAppUpdateMessage_DefaultMessage");
            }
            popup.AddCancelButton(Localization.GetString("OptionalAppUpdateMessage_DismissButton"), null, null);
            if (!string.IsNullOrEmpty(url))
            {
                popup.AddButton(
                    Localization.GetString("OptionalAppUpdateMessage_MoreButton"),
                    (sender, state) => App.CurrentApp.MyLigue1.OpenUrl(url),
                    null);
            }
            popup.Open();
        }

        private void NotifyServiceMessage(string message, string url)
        {
            if (string.IsNullOrEmpty(message))
            {
                return;
            }
            MessagePopup popup = new MessagePopup();
            popup.Title = Localization.GetString("ServiceMessage_Title");
            popup.Message = message;
            popup.AddCancelButton(Localization.GetString("ServiceMessage_DismissButton"), null, null);
            if (!string.IsNullOrEmpty(url))
            {
                popup.AddButton(
                    Localization.GetString("ServiceMessage_MoreButton"),
                    (sender, state) => App.CurrentApp.MyLigue1.OpenUrl(url),
                    null);
            }
            popup.Open();
        }


        private void LaunchJDFCommand(object sender, EventArgs e)
        {
            if (_viewModel.MyPanoramaNewsSectionViewModel != null)
            {
                if (_viewModel.MyPanoramaNewsSectionViewModel.CanLaunchJDF(sender))
                {
                    _viewModel.MyPanoramaNewsSectionViewModel.OnLaunchJDF(sender);
                }
            }
        }

    }
}