<?xml version="1.0" encoding="ISO-8859-1"?><richmediarequest module="sportL1"><appname>LIGUE1</appname><appversion>1.0</appversion><action name="initwidget" /></richmediarequest>

<?xml version="1.0" encoding="ISO-8859-1"?>
<richmediaresponse module="sportL1">
	<action name="initwidget">
		<respstatus>0</respstatus>
		<day>38</day>
		<season begin="2009" end="2010" logo=""/>
		<teams>
			<team id="31" short_name="OM" name="Olympique de Marseille" city="Marseille" rank="1">
				<stats points="78" mp="38" mw="23" md="9" ml="6" gs="69" gc="36" gd="33"/>
			</team>
			<team id="29" short_name="OL" name="Olympique Lyonnais" city="Lyon" rank="2">
				<stats points="72" mp="38" mw="20" md="12" ml="6" gs="64" gc="38" gd="26"/>
			</team>
			<team id="23" short_name="AJA" name="AJ Auxerre" city="Auxerre" rank="3">
				<stats points="71" mp="38" mw="20" md="11" ml="7" gs="42" gc="29" gd="13"/>
			</team>
			<team id="27" short_name="LOS" name="Lille OSC" city="Lille" rank="4">
				<stats points="70" mp="38" mw="21" md="7" ml="10" gs="72" gc="40" gd="32"/>
			</team>
			<team id="25" short_name="MTP" name="Montpellier HSC" city="Montpellier" rank="5">
				<stats points="69" mp="38" mw="20" md="9" ml="9" gs="50" gc="40" gd="10"/>
			</team>
			<team id="24" short_name="BOR" name="FC Girondins de Bordeaux" city="Bordeaux" rank="6">
				<stats points="64" mp="38" mw="19" md="7" ml="12" gs="58" gc="40" gd="18"/>
			</team>
			<team id="43" short_name="FCL" name="FC Lorient-Bretagne Sud" city="Lorient" rank="7">
				<stats points="58" mp="38" mw="16" md="10" ml="12" gs="54" gc="42" gd="12"/>
			</team>
			<team id="33" short_name="ASM" name="AS Monaco" city="Monaco" rank="8">
				<stats points="55" mp="38" mw="15" md="10" ml="13" gs="39" gc="45" gd="-6"/>
			</team>
			<team id="37" short_name="REN" name="Stade Rennais FC" city="Rennes" rank="9">
				<stats points="53" mp="38" mw="14" md="11" ml="13" gs="52" gc="41" gd="11"/>
			</team>
			<team id="42" short_name="VA" name="Valenciennes FC" city="Valenciennes" rank="10">
				<stats points="52" mp="38" mw="14" md="10" ml="14" gs="50" gc="50" gd="0"/>
			</team>
			<team id="26" short_name="RCL" name="RC Lens" city="Lens" rank="11">
				<stats points="48" mp="38" mw="12" md="12" ml="14" gs="40" gc="44" gd="-4"/>
			</team>
			<team id="28" short_name="ASN" name="A.S. Nancy Lorraine" city="Nancy" rank="12">
				<stats points="48" mp="38" mw="13" md="9" ml="16" gs="46" gc="53" gd="-7"/>
			</team>
			<team id="36" short_name="PSG" name="Paris Saint Germain" city="Paris" rank="13">
				<stats points="47" mp="38" mw="12" md="11" ml="15" gs="50" gc="46" gd="4"/>
			</team>
			<team id="41" short_name="TFC" name="Toulouse FC" city="Toulouse" rank="14">
				<stats points="47" mp="38" mw="12" md="11" ml="15" gs="36" gc="36" gd="0"/>
			</team>
			<team id="35" short_name="OGC" name="OGC Nice C�te d'Azur" city="Nice" rank="15">
				<stats points="44" mp="38" mw="11" md="11" ml="16" gs="41" gc="57" gd="-16"/>
			</team>
			<team id="38" short_name="SOC" name="FC Sochaux Montb�liard" city="Sochaux" rank="16">
				<stats points="41" mp="38" mw="11" md="8" ml="19" gs="28" gc="52" gd="-24"/>
			</team>
			<team id="39" short_name="STE" name="AS Saint-Etienne" city="Saint-Etienne" rank="17">
				<stats points="40" mp="38" mw="10" md="10" ml="18" gs="27" gc="45" gd="-18"/>
			</team>
			<team id="30" short_name="MUC" name="Le Mans U.C. 72" city="Le Mans" rank="18">
				<stats points="32" mp="38" mw="8" md="8" ml="22" gs="36" gc="59" gd="-23"/>
			</team>
			<team id="32" short_name="USB" name="US Boulogne CO" city="Boulogne" rank="19">
				<stats points="31" mp="38" mw="7" md="10" ml="21" gs="31" gc="62" gd="-31"/>
			</team>
			<team id="40" short_name="GRE" name="Grenoble Foot 38" city="Grenoble" rank="20">
				<stats points="23" mp="38" mw="5" md="8" ml="25" gs="31" gc="61" gd="-30"/>
			</team>
		</teams>
		<calendar>
			<match id="82565" status="2" date="2010/05/15" hour="21:00" day="38">
				<localTeam id="32" score="1"/>
				<visitorTeam id="37" score="0"/>
			</match>
			<match id="82573" status="2" date="2010/05/15" hour="21:00" day="38">
				<localTeam id="28" score="1"/>
				<visitorTeam id="42" score="1"/>
			</match>
			<match id="82572" status="2" date="2010/05/15" hour="21:00" day="38">
				<localTeam id="31" score="2"/>
				<visitorTeam id="40" score="0"/>
			</match>
			<match id="82571" status="2" date="2010/05/15" hour="21:00" day="38">
				<localTeam id="43" score="2"/>
				<visitorTeam id="27" score="1"/>
			</match>
			<match id="82570" status="2" date="2010/05/15" hour="21:00" day="38">
				<localTeam id="35" score="1"/>
				<visitorTeam id="39" score="1"/>
			</match>
			<match id="82569" status="2" date="2010/05/15" hour="21:00" day="38">
				<localTeam id="29" score="2"/>
				<visitorTeam id="30" score="0"/>
			</match>
			<match id="82568" status="2" date="2010/05/15" hour="21:00" day="38">
				<localTeam id="41" score="0"/>
				<visitorTeam id="33" score="0"/>
			</match>
			<match id="82567" status="2" date="2010/05/15" hour="21:00" day="38">
				<localTeam id="26" score="4"/>
				<visitorTeam id="24" score="3"/>
			</match>
			<match id="82566" status="2" date="2010/05/15" hour="21:00" day="38">
				<localTeam id="36" score="1"/>
				<visitorTeam id="25" score="3"/>
			</match>
			<match id="82574" status="2" date="2010/05/15" hour="21:00" day="38">
				<localTeam id="38" score="1"/>
				<visitorTeam id="23" score="2"/>
			</match>
		</calendar>
		<daynews>
			<news id="1836341" date="2010/05/19" hour="11:17" title="De Tavernost m�content">Interrog� par le quotidien Sud Ouest, Nicolas de Tavernost, pr�sident du directoire de M6 et principal actionnaire de Bordeaux, a jug� �inacceptable� le comportement de la F�d�ration fran�aise de football (FFF) concernant le dossier Laurent Blanc, appel� � devenir s�lectionneur apr�s Raymond Domenech. �Cela a contribu� � pourrir la deuxi�me partie de la saison des Girondins, a-t-il affirm�, nous attendons qu'il y ait r�paration par ceux qui ont commis une erreur.� Pour remplacer Laurent Blanc, le dirigeant de M6 souhaiterait un entra�neur �qui apporte de la bonne humeur dans cette �quipe.� Alors que les rumeurs annoncent une arriv�e �ventuelle d'Eric Gerets, Nicolas de Tavernost a imm�diatement d�menti. �C'est un tr�s bon entra�neur, mais on me dit qu'il n'est pas disponible�.</news>
			<news id="1836091" date="2010/05/19" hour="09:04" title="Alors, Roy ou Sanchez ?">Nice est encore au centre d'un nouvel imbroglio. La question de l'entra�neur pour la saison prochaine n'est toujours pas r�gl�e, alors qu'elle aurait d�j� d� l'�tre. Les d�cideurs du club n'arrivent tout simplement pas � s'entendre sur le nom du technicien � mettre en place. Il y a ceux qui sont favorables au maintien d'Eric Roy, le directeur sportif qui a fini la saison. Mais sa double casquette est un frein, d'autant plus qu'il n'est pas titulaire du dipl�me d'entra�neur, le fameux DEPF. Il faudrait donc un pr�te-nom...



Et puis, il y a ceux qui aimeraient voir arriver Daniel Sanchez, dont le nom circulait d�j� il y a un an. L'entra�neur de Tours s'impatiente d'ailleurs de voir les choses tra�ner. Il aimerait que la situation se d�cante tr�s vite. �Moi, je continue mon boulot � Tours, a-t-il expliqu� dans L'Equipe. � Nice, �a a l'air de tra�ner. J'attends de savoir ce qu'il en est, mais il faut que �a se r�gle cette semaine.� Une troisi�me piste n'est toutefois totalement pas � exclure.</news>
			<news id="1836071" date="2010/05/19" hour="08:33" title="Cormier �a les pleins pouvoirs�">Bien que rel�gu� en Ligue 2, Le Mans a d�cid� de repartir avec l'entra�neur qui a fini la saison, soit Arnaud Cormier. L'ancien adjoint d'Yves Bertucci et Paulo Duarte voit m�me ses pr�rogatives �largies par le pr�sident sarthois Henri Legarda : �Il a les pleins pouvoirs. Pour le recrutement, le m�dical et le sch�ma tactique�. Le staff devrait rester inchang�. La tendance est que Laurent Peyrelade (adjoint), Olivier P�demas (entra�neur des gardiens) et Paolo Rongoni (pr�parateur physique) poursuivent au club � condition d'accepter des conditions salariales � la baisse. Le train de vie de la L1 n'est pas celui de la L2. �On a huit millions � trouver et tout le monde devra baisser son salaire�, a annonc� Legarda dans L'Equipe. Autour du pr�sident, Daniel Jeandupeux va rester son conseiller alors que la mission de Michel Moulin est floue. �Il continuera � nous donner un coup de main quand il en aura envie �.</news>
			<news id="1836031" date="2010/05/19" hour="08:10" title="Le PSG ne conserve pas Grondin">Troisi�me gardien du Paris-SG, Willy Grondin ne sera plus au club la saison prochaine. Le portier r�unionnais de 35 ans, qui a jou� trente minutes cette ann�e, a �t� inform� par Antoine Kombouar� qu'il n'�tait pas conserv�.</news>
			<news id="1835621" date="2010/05/18" hour="21:37" title="Rama Yade soutient Leproux">La secr�taire d'Etat charg�e des Sports, Rama Yade, soutient �pleinement les mesures courageuses� du pr�sident du Paris-SG, Robin Leproux, dans son plan pour enrayer la violence au Parc des Princes. Dans un communiqu� de presse, elle explique que �depuis trop longtemps, les rancoeurs et les hostilit�s s'�taient accumul�es, atteignant un point de non-retour�.

�Lorsque j'avais re�u le 5 mars dernier, suite aux violences qui ont eu lieu en marge du match PSG-OM, S�bastien Bazin et Robin Leproux, je leur avais dit que la survie �conomique du club �tait en jeu et je leur avais demand� de prendre sans tarder des mesures drastiques, avoue-t-elle. C'est ce qu'ils ont fait et je salue leur lucidit� et leur d�termination. Le plan annonc� est sans pr�c�dent et t�moigne de la volont� des dirigeants du PSG de lutter contre la violence, d'enrayer le cycle de la haine et d'encourager la mixit� et le retour d'un public familial (...) Il est temps de rendre le Parc des Princes au football et le PSG � ses vrais supporters�.</news>
			<news id="1835601" date="2010/05/18" hour="21:09" title="Chahechouhe passe pro">Le jeune milieu de terrain nanc�ien Aatif Chahechouhe, 23 ans, a sign� mardi son premier contrat pro avec la formation lorraine.</news>
			<news id="1835301" date="2010/05/18" hour="18:59" title="Les supporters contre l'Euro 2016">Une cinquantaine de supporters, surtout issus du PSG, ont interrompu, ce mardi apr�s-midi, une conf�rence de presse consacr�e � l'Euro 2016. Jean-Pierre Escalettes, le pr�sident de la FFF, son directeur g�n�ral Jacques Lambert et Fr�d�ric Thiriez, le pr�sident de la Ligue, �taient � la tribune lorsque les supporters sont entr�s en force dans la salle en lan�ant : �Nos imp�ts pas pour l'Euro !� Ils ont distribu� un tract d�non�ant notamment �que l'Etat ait d'ores et d�j� promis une enveloppe de 150 millions d'euros, alors que les conditions de vie de la population ne cessent de se d�grader.� Apr�s quelques minutes d'�changes anim�s, Escalettes, Lambert et Thiriez ont quitt� la salle, avant l'intervention de la police. Le d�part des supporters s'est a priori pass� dans le calme.- Etienne MOATTI</news>
			<news id="1835201" date="2010/05/18" hour="18:10" title="Ben Arfa sur le d�part?">La saison en demi-teinte d'Hatem Ben Arfa � l'OM, cumul�e � la non-s�lection en �quipe de France pour le Mondial en Afrique du Sud, semble faire cogiter le milieu de terrain. Selon L'Equipe, Ben Arfa aurait d�j� d�cid� de quitter Marseille. � 23 ans, il pourrait m�me dire adieu � la Ligue 1 pour prendre la destination de l'Angleterre. L'ancien Lyonnais ne souhaite donc plus se contenter d'un r�le de joker de luxe. Que ce soit avec Deschamps, Gerets ou encore Perrin (� Lyon) les ann�es pr�c�dentes, Ben Arfa n'a jamais �t� un titulaire indiscutable. Et m�me s'il se pla�t � Marseille, que ses rapports avec les dirigeants et les supporters sont excellents, Ben Arfa veut du temps de jeu. 

Depuis l'annonce des 24 de Raymond Domenech, les doutes sont d'ailleurs de plus en plus grands. Ben Arfa d�sire franchir un palier. Recrut� pour 12 millions d'euros en 2008, le Marseillais ne quitterait cependant pas le club en cas d'offre inf�rieure � la somme d'achat. Le PSG se serait d�j� renseign� aupr�s du club phoc�en.</news>
			<news id="1834921" date="2010/05/18" hour="16:07" title="Le TFC attend pour Gignac">L'entra�neur du Toulouse FC Alain Casanova a indiqu� mardi que le club cherchait � recruter �un attaquant en fonction du d�part ou pas de Gignac.� Le buteur international, officiellement sous contrat jusqu'en 2013 et auteur d'une saison tr�s moyenne (huit buts), pourrait quitter le club toulousain �en cas d'offre financi�rement satisfaisante�, selon le pr�sident Olivier Sadran. Casanova a, en outre, indiqu� que le TFC recherchait �un milieu droit et un arri�re droit�.</news>
			<news id="1834651" date="2010/05/18" hour="14:14" title="Leproux : �Pacifier le Parc�">Le pr�sident du Paris-SG Robin Leproux a d�voil� ce mardi la philosophie du plan antiviolence qu'il entend mettre sur pied d�s la saison prochaine pour, entre autres, �pacifier� le Parc. L'enceinte parisienne �tait bien gard�e ce mardi par des CRS et policiers pour la conf�rence de presse. Une philosophie que l'on peut r�sumer ainsi : �s�parer pour m�langer�. Il s'en est expliqu�. �On a atteint un point de non-retour entre deux tribunes avec deux morts en trois ans ! La situation n'a fait qu'empirer. C'est le devoir du PSG d'agir, m�me si le foot fran�ais file un mauvais coton. On ne doit plus d�tester l'autre mais l'accepter. Dans une autre soci�t� o� j'�tais (RTL, ndlr), c'�tait &quot;Vivre Ensemble&quot; et bien le Parc, cela doit �tre �a �, a-t-il martel�.

Apr�s avoir consult�, �cout� et pris diff�rents avis, Robin Leproux a donc tranch� et expos� son plan qui est �vital� selon lui pour �sauver� le club. �C'est plus facile de ne rien faire, a-t-il ajout�. Il faut des tribunes tol�rantes avec des origines diverses, je tiens � une mixit� compl�te. Le souhait du club n'est pas de faire du r�pressif. Il faut accepter d'�tre assis � c�t� de quelqu'un qui ne partage pas votre avis. C'est un nouveau virage, ce plan est novateur et c'est � ce prix qu'il faut sauver le club. Si on ne prend pas ce genre de d�cisions, on met le club en danger !.�

Le pr�sident du PSG n'a pas voulu chiffrer le co�t de ce plan car il est notamment �transitoire et �volutif�. �C'est un investissement, a-t-il pr�cis� quand m�me, �a peut �tre moins douloureux financi�rement si �a se passe vite et bien �. Le club de la capitale esp�re que ce plan trouvera son rythme de croisi�re �en moins d'une saison� mais les responsables reconnaissent que le Parc risque d'�tre �plus fade� un certain temps. � On n'aura pas cette ferveur mais c'est le prix � payer au d�but� veut croire Leproux. En conclusion de son intervention, Robin Leproux s'est interrog�. �Comment peut-on demander � des investisseurs de mettre de l'argent, si on a une guerre qui donne tous les trois ans un mort ? � - Christophe MICHEL, au Parc des Princes</news>
		</daynews>
	</action>
</richmediaresponse>