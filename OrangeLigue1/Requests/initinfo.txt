<?xml version="1.0" encoding="ISO-8859-1"?><richmediarequest module="sportL1"><appname>LIGUE1</appname><appversion>1.0</appversion><action name="initinfo"/></richmediarequest>

http://ea.orange.fr/sport/ligue1.jsp

<?xml version="1.0" encoding="ISO-8859-1"?>
<richmediaresponse module="sportL1">
	<action name="initinfo">
		<respstatus>0</respstatus>
		<date>
			<iso>2010-05-25T11:32:45+1:00</iso>
			<day>38</day>
			<long>25/05/2010</long>
		</date>
		<version>
			<status>0</status>
			<url></url>
			<message></message>
		</version>
		<options level="2">
			<message time="3">Vous pouvez regarder les matchs, mais vos options actuelles n'incluent pas l'illimit� sur les live.

Pensez � souscrire une option !</message>
			<option optcode="OptTv08Max">
				<shortDesc>option TV max</shortDesc>
				<longDesc><teaser></teaser>
<title>en illimite 24h/24, 7j/7 :</title>
<description>Toute la Ligue 1 sur L1 Player et Orange World y compris les matchs en direct, + de 60 chaines et 3500 videos a volonte ! </description></longDesc>
				<price>9,00</price>
			</option>
		</options>
		<season begin="2009" end="2010"/>
		<ticker>Tous les week-ends retrouvez les matchs de la Ligue 1 en live.</ticker>
		<teams logobase="http://ea.orange.fr/sport/logos/">
			<team id="42" short_name="VA" name="Valenciennes FC" city="Valenciennes" rank="10">
				<logos>
					<logo name="valenciennes_s0910" size="24" ext="png"/>
					<logo name="valenciennes_s0910" size="32" ext="png"/>
					<logo name="valenciennes_s0910" size="44" ext="png"/>
					<logo name="valenciennes_s0910" size="64" ext="png"/>
					<logo name="valenciennes_s0910" size="56" ext="png"/>
				</logos>
			</team>
			<team id="32" short_name="USB" name="US Boulogne CO" city="Boulogne" rank="19">
				<logos>
					<logo name="boulognes_s0910" size="24" ext="png"/>
					<logo name="boulognes_s0910" size="32" ext="png"/>
					<logo name="boulognes_s0910" size="44" ext="png"/>
					<logo name="boulognes_s0910" size="64" ext="png"/>
					<logo name="boulognes_s0910" size="56" ext="png"/>
				</logos>
			</team>
			<team id="31" short_name="OM" name="Olympique de Marseille" city="Marseille" rank="1">
				<logos>
					<logo name="marseille_s0910" size="24" ext="png"/>
					<logo name="marseille_s0910" size="32" ext="png"/>
					<logo name="marseille_s0910" size="44" ext="png"/>
					<logo name="marseille_s0910" size="64" ext="png"/>
					<logo name="marseille_s0910" size="56" ext="png"/>
				</logos>
			</team>
			<team id="38" short_name="SOC" name="FC Sochaux Montb�liard" city="Sochaux" rank="16">
				<logos>
					<logo name="sochaux_s0910" size="24" ext="png"/>
					<logo name="sochaux_s0910" size="32" ext="png"/>
					<logo name="sochaux_s0910" size="44" ext="png"/>
					<logo name="sochaux_s0910" size="64" ext="png"/>
					<logo name="sochaux_s0910" size="56" ext="png"/>
				</logos>
			</team>
			<team id="27" short_name="LOS" name="Lille OSC" city="Lille" rank="4">
				<logos>
					<logo name="lille_s0910" size="24" ext="png"/>
					<logo name="lille_s0910" size="32" ext="png"/>
					<logo name="lille_s0910" size="44" ext="png"/>
					<logo name="lille_s0910" size="64" ext="png"/>
					<logo name="lille_s0910" size="56" ext="png"/>
				</logos>
			</team>
			<team id="41" short_name="TFC" name="Toulouse FC" city="Toulouse" rank="14">
				<logos>
					<logo name="tfc_s0910" size="24" ext="png"/>
					<logo name="tfc_s0910" size="32" ext="png"/>
					<logo name="tfc_s0910" size="44" ext="png"/>
					<logo name="tfc_s0910" size="64" ext="png"/>
					<logo name="tfc_s0910" size="56" ext="png"/>
				</logos>
			</team>
			<team id="39" short_name="STE" name="AS Saint-Etienne" city="Saint-Etienne" rank="17">
				<logos>
					<logo name="saintetienne_s0910" size="24" ext="png"/>
					<logo name="saintetienne_s0910" size="32" ext="png"/>
					<logo name="saintetienne_s0910" size="44" ext="png"/>
					<logo name="saintetienne_s0910" size="64" ext="png"/>
					<logo name="saintetienne_s0910" size="56" ext="png"/>
				</logos>
			</team>
			<team id="30" short_name="MUC" name="Le Mans U.C. 72" city="Le Mans" rank="18">
				<logos>
					<logo name="lemans_s0910" size="24" ext="png"/>
					<logo name="lemans_s0910" size="32" ext="png"/>
					<logo name="lemans_s0910" size="44" ext="png"/>
					<logo name="lemans_s0910" size="64" ext="png"/>
					<logo name="lemans_s0910" size="56" ext="png"/>
				</logos>
			</team>
			<team id="26" short_name="RCL" name="RC Lens" city="Lens" rank="11">
				<logos>
					<logo name="lens_s0910" size="24" ext="png"/>
					<logo name="lens_s0910" size="32" ext="png"/>
					<logo name="lens_s0910" size="44" ext="png"/>
					<logo name="lens_s0910" size="64" ext="png"/>
					<logo name="lens_s0910" size="56" ext="png"/>
				</logos>
			</team>
			<team id="43" short_name="FCL" name="FC Lorient-Bretagne Sud" city="Lorient" rank="7">
				<logos>
					<logo name="lorient_s0910" size="24" ext="png"/>
					<logo name="lorient_s0910" size="32" ext="png"/>
					<logo name="lorient_s0910" size="44" ext="png"/>
					<logo name="lorient_s0910" size="64" ext="png"/>
					<logo name="lorient_s0910" size="56" ext="png"/>
				</logos>
			</team>
			<team id="37" short_name="REN" name="Stade Rennais FC" city="Rennes" rank="9">
				<logos>
					<logo name="rennes_s0910" size="24" ext="png"/>
					<logo name="rennes_s0910" size="32" ext="png"/>
					<logo name="rennes_s0910" size="44" ext="png"/>
					<logo name="rennes_s0910" size="64" ext="png"/>
					<logo name="rennes_s0910" size="56" ext="png"/>
				</logos>
			</team>
			<team id="23" short_name="AJA" name="AJ Auxerre" city="Auxerre" rank="3">
				<logos>
					<logo name="auxerre_s0910" size="32" ext="png"/>
					<logo name="auxerre_s0910" size="24" ext="png"/>
					<logo name="auxerre_s0910" size="64" ext="png"/>
					<logo name="auxerre_s0910" size="44" ext="png"/>
					<logo name="auxerre_s0910" size="56" ext="png"/>
				</logos>
			</team>
			<team id="33" short_name="ASM" name="AS Monaco" city="Monaco" rank="8">
				<logos>
					<logo name="monaco_s0910" size="24" ext="png"/>
					<logo name="monaco_s0910" size="32" ext="png"/>
					<logo name="monaco_s0910" size="44" ext="png"/>
					<logo name="monaco_s0910" size="64" ext="png"/>
					<logo name="monaco_s0910" size="56" ext="png"/>
				</logos>
			</team>
			<team id="28" short_name="ASN" name="A.S. Nancy Lorraine" city="Nancy" rank="12">
				<logos>
					<logo name="nancy_s0910" size="24" ext="png"/>
					<logo name="nancy_s0910" size="32" ext="png"/>
					<logo name="nancy_s0910" size="44" ext="png"/>
					<logo name="nancy_s0910" size="64" ext="png"/>
					<logo name="nancy_s0910" size="56" ext="png"/>
				</logos>
			</team>
			<team id="29" short_name="OL" name="Olympique Lyonnais" city="Lyon" rank="2">
				<logos>
					<logo name="lyon_s0910" size="24" ext="png"/>
					<logo name="lyon_s0910" size="32" ext="png"/>
					<logo name="lyon_s0910" size="44" ext="png"/>
					<logo name="lyon_s0910" size="64" ext="png"/>
					<logo name="lyon_s0910" size="56" ext="png"/>
				</logos>
			</team>
			<team id="35" short_name="OGC" name="OGC Nice C�te d'Azur" city="Nice" rank="15">
				<logos>
					<logo name="nice_s0910" size="24" ext="png"/>
					<logo name="nice_s0910" size="32" ext="png"/>
					<logo name="nice_s0910" size="44" ext="png"/>
					<logo name="nice_s0910" size="64" ext="png"/>
					<logo name="nice_s0910" size="56" ext="png"/>
				</logos>
			</team>
			<team id="40" short_name="GRE" name="Grenoble Foot 38" city="Grenoble" rank="20">
				<logos>
					<logo name="grenoble_s0910" size="24" ext="png"/>
					<logo name="grenoble_s0910" size="32" ext="png"/>
					<logo name="grenoble_s0910" size="44" ext="png"/>
					<logo name="grenoble_s0910" size="64" ext="png"/>
					<logo name="grenoble_s0910" size="56" ext="png"/>
				</logos>
			</team>
			<team id="36" short_name="PSG" name="Paris Saint Germain" city="Paris" rank="13">
				<logos>
					<logo name="paris_s0910" size="24" ext="png"/>
					<logo name="paris_s0910" size="32" ext="png"/>
					<logo name="paris_s0910" size="44" ext="png"/>
					<logo name="paris_s0910" size="64" ext="png"/>
					<logo name="paris_s0910" size="56" ext="png"/>
				</logos>
			</team>
			<team id="25" short_name="MTP" name="Montpellier HSC" city="Montpellier" rank="5">
				<logos>
					<logo name="montpellier_s0910" size="24" ext="png"/>
					<logo name="montpellier_s0910" size="32" ext="png"/>
					<logo name="montpellier_s0910" size="44" ext="png"/>
					<logo name="montpellier_s0910" size="64" ext="png"/>
					<logo name="montpellier_s0910" size="56" ext="png"/>
				</logos>
			</team>
			<team id="24" short_name="BOR" name="FC Girondins de Bordeaux" city="Bordeaux" rank="6">
				<logos>
					<logo name="bordeaux_s0910" size="24" ext="png"/>
					<logo name="bordeaux_s0910" size="32" ext="png"/>
					<logo name="bordeaux_s0910" size="44" ext="png"/>
					<logo name="bordeaux_s0910" size="64" ext="png"/>
					<logo name="bordeaux_s0910" size="56" ext="png"/>
				</logos>
			</team>
		</teams>
	</action>
</richmediaresponse>

http://qlf-ea.orange.fr/sport/ligue1.jsp

<?xml version="1.0" encoding="ISO-8859-1"?>
<richmediaresponse module="sportL1">
	<action name="initinfo">
		<respstatus>0</respstatus>
		<date>
			<iso>2010-05-25T11:33:17+1:00</iso>
			<day>21</day>
			<long>20/01/2010</long>
		</date>
		<version>
			<status>0</status>
			<url></url>
			<message></message>
		</version>
		<options level="3">
			<message time="5">Pour regarder les matchs live et les vid�os Ligue 1 en illimit�, pensez � souscrire une option.

Bons matchs !
</message>
			<option optcode="OptFoot">
				<shortDesc>Option Foot</shortDesc>
				<longDesc><teaser></teaser>
<title> en illimite 24h/24 et 7j/7 : </title>
<description>Toute la Ligue1 sur L1 Player et Orange World y compris les matchs en direct, les resultats et les alertes but par SMS.</description></longDesc>
				<price>6,00</price>
			</option>
			<option optcode="OptTv08Max">
				<shortDesc>option TV max</shortDesc>
				<longDesc><teaser></teaser>
<title> en illimite 24h/24 et 7j/7 : </title>
<description>Ttes les chaînes en illiiiiiimité !</description></longDesc>
				<price>9,00</price>
			</option>
		</options>
		<season begin="2009" end="2010"/>
		<ticker>Rendez-vous le samedi 8 ao�t pour la reprise du championnat de Ligue1 2009-2010.</ticker>
		<teams logobase="http://qlf-ea.orange.fr/sport/logos/">
			<team id="35" short_name="OGC" name="OGC Nice C�te d'Azur" city="Nice" rank="16">
				<logos>
					<logo name="nice_s0910" size="24" ext="png"/>
					<logo name="nice_s0910" size="32" ext="png"/>
					<logo name="nice_s0910" size="44" ext="png"/>
					<logo name="nice_s0910" size="64" ext="png"/>
					<logo name="nice_s0910" size="56" ext="png"/>
				</logos>
			</team>
			<team id="40" short_name="GRE" name="Grenoble Foot 38" city="Grenoble" rank="20">
				<logos>
					<logo name="grenoble_s0910" size="24" ext="png"/>
					<logo name="grenoble_s0910" size="32" ext="png"/>
					<logo name="grenoble_s0910" size="44" ext="png"/>
					<logo name="grenoble_s0910" size="64" ext="png"/>
					<logo name="grenoble_s0910" size="56" ext="png"/>
				</logos>
			</team>
			<team id="25" short_name="MTP" name="Montpellier HSC" city="Montpellier" rank="2">
				<logos>
					<logo name="montpellier_s0910" size="24" ext="png"/>
					<logo name="montpellier_s0910" size="32" ext="png"/>
					<logo name="montpellier_s0910" size="44" ext="png"/>
					<logo name="montpellier_s0910" size="64" ext="png"/>
					<logo name="montpellier_s0910" size="56" ext="png"/>
				</logos>
			</team>
			<team id="39" short_name="STE" name="AS Saint-Etienne" city="Saint-Etienne" rank="17">
				<logos>
					<logo name="saintetienne_s0910" size="24" ext="png"/>
					<logo name="saintetienne_s0910" size="32" ext="png"/>
					<logo name="saintetienne_s0910" size="44" ext="png"/>
					<logo name="saintetienne_s0910" size="64" ext="png"/>
					<logo name="saintetienne_s0910" size="56" ext="png"/>
				</logos>
			</team>
			<team id="27" short_name="LOS" name="Lille OSC" city="Lille" rank="3">
				<logos>
					<logo name="lille_s0910" size="24" ext="png"/>
					<logo name="lille_s0910" size="32" ext="png"/>
					<logo name="lille_s0910" size="44" ext="png"/>
					<logo name="lille_s0910" size="64" ext="png"/>
					<logo name="lille_s0910" size="56" ext="png"/>
				</logos>
			</team>
			<team id="42" short_name="VA" name="Valenciennes FC" city="Valenciennes" rank="13">
				<logos>
					<logo name="valenciennes_s0910" size="24" ext="png"/>
					<logo name="valenciennes_s0910" size="32" ext="png"/>
					<logo name="valenciennes_s0910" size="44" ext="png"/>
					<logo name="valenciennes_s0910" size="64" ext="png"/>
					<logo name="valenciennes_s0910" size="56" ext="png"/>
				</logos>
			</team>
			<team id="29" short_name="OL" name="Olympique Lyonnais" city="Lyon" rank="5">
				<logos>
					<logo name="lyon_s0910" size="24" ext="png"/>
					<logo name="lyon_s0910" size="32" ext="png"/>
					<logo name="lyon_s0910" size="44" ext="png"/>
					<logo name="lyon_s0910" size="64" ext="png"/>
					<logo name="lyon_s0910" size="56" ext="png"/>
				</logos>
			</team>
			<team id="26" short_name="RCL" name="RC Lens" city="Lens" rank="15">
				<logos>
					<logo name="lens_s0910" size="24" ext="png"/>
					<logo name="lens_s0910" size="32" ext="png"/>
					<logo name="lens_s0910" size="44" ext="png"/>
					<logo name="lens_s0910" size="64" ext="png"/>
					<logo name="lens_s0910" size="56" ext="png"/>
				</logos>
			</team>
			<team id="36" short_name="PSG" name="Paris Saint Germain" city="Paris" rank="10">
				<logos>
					<logo name="paris_s0910" size="24" ext="png"/>
					<logo name="paris_s0910" size="32" ext="png"/>
					<logo name="paris_s0910" size="44" ext="png"/>
					<logo name="paris_s0910" size="64" ext="png"/>
					<logo name="paris_s0910" size="56" ext="png"/>
				</logos>
			</team>
			<team id="37" short_name="REN" name="Stade Rennais FC" city="Rennes" rank="9">
				<logos>
					<logo name="rennes_s0910" size="24" ext="png"/>
					<logo name="rennes_s0910" size="32" ext="png"/>
					<logo name="rennes_s0910" size="44" ext="png"/>
					<logo name="rennes_s0910" size="64" ext="png"/>
					<logo name="rennes_s0910" size="56" ext="png"/>
				</logos>
			</team>
			<team id="43" short_name="FCL" name="FC Lorient-Bretagne Sud" city="Lorient" rank="8">
				<logos>
					<logo name="lorient_s0910" size="24" ext="png"/>
					<logo name="lorient_s0910" size="32" ext="png"/>
					<logo name="lorient_s0910" size="44" ext="png"/>
					<logo name="lorient_s0910" size="64" ext="png"/>
					<logo name="lorient_s0910" size="56" ext="png"/>
				</logos>
			</team>
			<team id="31" short_name="OM" name="Olympique de Marseille" city="Marseille" rank="4">
				<logos>
					<logo name="marseille_s0910" size="24" ext="png"/>
					<logo name="marseille_s0910" size="32" ext="png"/>
					<logo name="marseille_s0910" size="44" ext="png"/>
					<logo name="marseille_s0910" size="64" ext="png"/>
					<logo name="marseille_s0910" size="56" ext="png"/>
				</logos>
			</team>
			<team id="38" short_name="SOC" name="FC Sochaux" city="Sochaux" rank="12">
				<logos>
					<logo name="sochaux_s0910" size="24" ext="png"/>
					<logo name="sochaux_s0910" size="32" ext="png"/>
					<logo name="sochaux_s0910" size="44" ext="png"/>
					<logo name="sochaux_s0910" size="64" ext="png"/>
					<logo name="sochaux_s0910" size="56" ext="png"/>
				</logos>
			</team>
			<team id="41" short_name="TFC" name="Toulouse FC" city="Toulouse" rank="11">
				<logos>
					<logo name="tfc_s0910" size="24" ext="png"/>
					<logo name="tfc_s0910" size="32" ext="png"/>
					<logo name="tfc_s0910" size="44" ext="png"/>
					<logo name="tfc_s0910" size="64" ext="png"/>
					<logo name="tfc_s0910" size="56" ext="png"/>
				</logos>
			</team>
			<team id="30" short_name="MUC" name="Le Mans U.C. 72" city="Le Mans" rank="18">
				<logos>
					<logo name="lemans_s0910" size="24" ext="png"/>
					<logo name="lemans_s0910" size="32" ext="png"/>
					<logo name="lemans_s0910" size="44" ext="png"/>
					<logo name="lemans_s0910" size="64" ext="png"/>
					<logo name="lemans_s0910" size="56" ext="png"/>
				</logos>
			</team>
			<team id="28" short_name="ASN" name="A.S. Nancy Lorraine" city="Nancy" rank="14">
				<logos>
					<logo name="nancy_s0910" size="24" ext="png"/>
					<logo name="nancy_s0910" size="32" ext="png"/>
					<logo name="nancy_s0910" size="44" ext="png"/>
					<logo name="nancy_s0910" size="64" ext="png"/>
					<logo name="nancy_s0910" size="56" ext="png"/>
				</logos>
			</team>
			<team id="33" short_name="ASM" name="AS Monaco" city="Monaco" rank="6">
				<logos>
					<logo name="monaco_s0910" size="24" ext="png"/>
					<logo name="monaco_s0910" size="32" ext="png"/>
					<logo name="monaco_s0910" size="44" ext="png"/>
					<logo name="monaco_s0910" size="64" ext="png"/>
					<logo name="monaco_s0910" size="56" ext="png"/>
				</logos>
			</team>
			<team id="32" short_name="USB" name="US Boulogne C�te d'Opale" city="Boulognes" rank="19">
				<logos>
					<logo name="boulognes_s0910" size="24" ext="png"/>
					<logo name="boulognes_s0910" size="32" ext="png"/>
					<logo name="boulognes_s0910" size="44" ext="png"/>
					<logo name="boulognes_s0910" size="64" ext="png"/>
					<logo name="boulognes_s0910" size="56" ext="png"/>
				</logos>
			</team>
			<team id="24" short_name="BOR" name="FC Girondins de Bordeaux" city="Bordeaux" rank="1">
				<logos>
					<logo name="bordeaux_s0910" size="24" ext="png"/>
					<logo name="bordeaux_s0910" size="32" ext="png"/>
					<logo name="bordeaux_s0910" size="44" ext="png"/>
					<logo name="bordeaux_s0910" size="64" ext="png"/>
					<logo name="bordeaux_s0910" size="56" ext="png"/>
				</logos>
			</team>
			<team id="23" short_name="AJA" name="AJ Auxerre" city="Auxerre" rank="7">
				<logos>
					<logo name="auxerre_s0910" size="32" ext="png"/>
					<logo name="auxerre_s0910" size="24" ext="png"/>
					<logo name="auxerre_s0910" size="64" ext="png"/>
					<logo name="auxerre_s0910" size="44" ext="png"/>
					<logo name="auxerre_s0910" size="56" ext="png"/>
				</logos>
			</team>
		</teams>
		<matches day="21">
			<match id="82395" status="2" live="1" date="2010/01/20" hour="19:00">
				<teams>
					<localTeam id="41" score="0"/>
					<visitorTeam id="28" score="0"/>
				</teams>
				<videos>
					<video type="L">
						<url>http://tp2.orange.fr/httppvc_clnssorangeavsppse.avsp.cvf.fr/reference.jgi?pse=cvf&amp;entree=cvf&amp;sujet=livetv_preprod1&amp;fcav=f_cvf</url>
					</video>
				</videos>
			</match>
			<match id="82397" status="2" live="1" date="2010/01/20" hour="19:00">
				<teams>
					<localTeam id="26" score="0"/>
					<visitorTeam id="25" score="0"/>
				</teams>
				<videos>
					<video type="L">
						<url>http://tp2.orange.fr/httppvc_clnssorangeavsppse.avsp.cvf.fr/reference.jgi?pse=cvf&amp;entree=cvf&amp;sujet=livetv_preprod1&amp;fcav=f_cvf</url>
					</video>
				</videos>
			</match>
			<match id="82398" status="2" live="1" date="2010/01/20" hour="19:00">
				<teams>
					<localTeam id="40" score="0"/>
					<visitorTeam id="24" score="0"/>
				</teams>
				<videos>
					<video type="L">
						<url>http://tp2.orange.fr/httppvc_clnssorangeavsppse.avsp.cvf.fr/reference.jgi?pse=cvf&amp;entree=cvf&amp;sujet=livetv_preprod1&amp;fcav=f_cvf</url>
					</video>
				</videos>
			</match>
			<match id="82399" status="2" live="1" date="2010/01/20" hour="19:00">
				<teams>
					<localTeam id="43" score="0"/>
					<visitorTeam id="29" score="0"/>
				</teams>
				<videos>
					<video type="L">
						<url>http://tp2.orange.fr/httppvc_clnssorangeavsppse.avsp.cvf.fr/reference.jgi?pse=cvf&amp;entree=cvf&amp;sujet=livetv_preprod1&amp;fcav=f_cvf</url>
					</video>
				</videos>
			</match>
			<match id="82400" status="2" live="1" date="2010/01/20" hour="19:00">
				<teams>
					<localTeam id="36" score="0"/>
					<visitorTeam id="33" score="0"/>
				</teams>
				<videos>
					<video type="L">
						<url>http://tp2.orange.fr/httppvc_clnssorangeavsppse.avsp.cvf.fr/reference.jgi?pse=cvf&amp;entree=cvf&amp;sujet=livetv_preprod1&amp;fcav=f_cvf</url>
					</video>
				</videos>
			</match>
			<match id="82401" status="2" live="1" date="2010/01/20" hour="19:00">
				<teams>
					<localTeam id="31" score="0"/>
					<visitorTeam id="30" score="0"/>
				</teams>
				<videos>
					<video type="L">
						<url>http://tp2.orange.fr/httppvc_clnssorangeavsppse.avsp.cvf.fr/reference.jgi?pse=cvf&amp;entree=cvf&amp;sujet=livetv_preprod1&amp;fcav=f_cvf</url>
					</video>
				</videos>
			</match>
			<match id="82402" status="2" live="1" date="2010/01/20" hour="19:00">
				<teams>
					<localTeam id="38" score="0"/>
					<visitorTeam id="27" score="0"/>
				</teams>
				<videos>
					<video type="L">
						<url>http://tp2.orange.fr/httppvc_clnssorangeavsppse.avsp.cvf.fr/reference.jgi?pse=cvf&amp;entree=cvf&amp;sujet=livetv_preprod1&amp;fcav=f_cvf</url>
					</video>
				</videos>
			</match>
			<match id="82403" status="2" live="0" date="2010/01/20" hour="21:00">
				<teams>
					<localTeam id="32" score="0"/>
					<visitorTeam id="42" score="0"/>
				</teams>
				<videos>
					<video type="L">
						<url>http://tp2.orange.fr/httppvc_clnssorangeavsppse.avsp.cvf.fr/reference.jgi?pse=cvf&amp;entree=cvf&amp;sujet=livetv_preprod1&amp;fcav=f_cvf</url>
					</video>
				</videos>
			</match>
			<match id="82404" status="2" live="1" date="2010/01/20" hour="19:00">
				<teams>
					<localTeam id="35" score="0"/>
					<visitorTeam id="23" score="0"/>
				</teams>
				<videos>
					<video type="L">
						<url>http://tp2.orange.fr/httppvc_clnssorangeavsppse.avsp.cvf.fr/reference.jgi?pse=cvf&amp;entree=cvf&amp;sujet=livetv_preprod1&amp;fcav=f_cvf</url>
					</video>
				</videos>
			</match>
		</matches>
	</action>
</richmediaresponse>

 <?xml version="1.0" encoding="ISO-8859-1" ?> 
- <richmediaresponse module="sportL1">
- <action name="initinfo">
  <respstatus>0</respstatus> 
- <date>
  <iso>2010-09-10T14:01:28+1:00</iso> 
  <day>5</day> 
  <long>10/09/2010</long> 
  </date>
- <params>
  <param name="init" value="false" /> 
  </params>
  <update freqR="60" freqL="303" freqLive="60" /> 
- <version>
  <status>0</status> 
  <url /> 
  <message /> 
  </version>
  <options /> 
  <season begin="2010" end="2011" /> 
  <ticker>Tous les week-ends retrouvez les matchs de la Ligue 1 en live.</ticker> 
- <teams logobase="http://ea.orange.fr/sport/logos/" logobasewifi="http://ampfr-rp-orange.aw.atosorigin.com/ligue1/logos/">
- <team id="23" short_name="AJA" name="AJ Auxerre" city="Auxerre" rank="19">
- <logos>
  <logo name="auxerre_s0910" size="32" ext="png" /> 
  <logo name="auxerre_s0910" size="24" ext="png" /> 
  <logo name="auxerre_s0910" size="64" ext="png" /> 
  <logo name="auxerre_s0910" size="44" ext="png" /> 
  <logo name="auxerre_s0910" size="56" ext="png" /> 
  </logos>
  </team>
- <team id="33" short_name="ASM" name="AS Monaco" city="Monaco" rank="7">
- <logos>
  <logo name="monaco_s0910" size="24" ext="png" /> 
  <logo name="monaco_s0910" size="32" ext="png" /> 
  <logo name="monaco_s0910" size="44" ext="png" /> 
  <logo name="monaco_s0910" size="64" ext="png" /> 
  <logo name="monaco_s0910" size="56" ext="png" /> 
  </logos>
  </team>
- <team id="27" short_name="LOS" name="Lille OSC" city="Lille" rank="11">
- <logos>
  <logo name="lille_s0910" size="24" ext="png" /> 
  <logo name="lille_s0910" size="32" ext="png" /> 
  <logo name="lille_s0910" size="44" ext="png" /> 
  <logo name="lille_s0910" size="64" ext="png" /> 
  <logo name="lille_s0910" size="56" ext="png" /> 
  </logos>
  </team>
- <team id="44" short_name="SMC" name="Stade Malherbe Caen" city="Caen" rank="6">
- <logos>
  <logo name="CAEN" size="24" ext="png" /> 
  <logo name="CAEN" size="32" ext="png" /> 
  <logo name="CAEN" size="44" ext="png" /> 
  <logo name="CAEN" size="56" ext="png" /> 
  <logo name="CAEN" size="64" ext="png" /> 
  </logos>
  </team>
- <team id="43" short_name="FCL" name="FC Lorient-Bretagne Sud" city="Lorient" rank="12">
- <logos>
  <logo name="LORIENT" size="24" ext="png" /> 
  <logo name="LORIENT" size="32" ext="png" /> 
  <logo name="LORIENT" size="44" ext="png" /> 
  <logo name="LORIENT" size="56" ext="png" /> 
  <logo name="LORIENT" size="64" ext="png" /> 
  </logos>
  </team>
- <team id="35" short_name="OGC" name="OGC Nice C�te d'Azur" city="Nice" rank="8">
- <logos>
  <logo name="nice_s0910" size="24" ext="png" /> 
  <logo name="nice_s0910" size="32" ext="png" /> 
  <logo name="nice_s0910" size="44" ext="png" /> 
  <logo name="nice_s0910" size="64" ext="png" /> 
  <logo name="nice_s0910" size="56" ext="png" /> 
  </logos>
  </team>
- <team id="29" short_name="OL" name="Olympique Lyonnais" city="Lyon" rank="17">
- <logos>
  <logo name="lyon_s0910" size="24" ext="png" /> 
  <logo name="lyon_s0910" size="32" ext="png" /> 
  <logo name="lyon_s0910" size="44" ext="png" /> 
  <logo name="lyon_s0910" size="64" ext="png" /> 
  <logo name="lyon_s0910" size="56" ext="png" /> 
  </logos>
  </team>
- <team id="28" short_name="ASN" name="A.S. Nancy Lorraine" city="Nancy" rank="18">
- <logos>
  <logo name="nancy_s0910" size="24" ext="png" /> 
  <logo name="nancy_s0910" size="32" ext="png" /> 
  <logo name="nancy_s0910" size="44" ext="png" /> 
  <logo name="nancy_s0910" size="64" ext="png" /> 
  <logo name="nancy_s0910" size="56" ext="png" /> 
  </logos>
  </team>
- <team id="45" short_name="SB29" name="Stade Brestois 29" city="Brest" rank="15">
- <logos>
  <logo name="SB29" size="24" ext="png" /> 
  <logo name="SB29" size="32" ext="png" /> 
  <logo name="SB29" size="44" ext="png" /> 
  <logo name="SB29" size="56" ext="png" /> 
  <logo name="SB29" size="64" ext="png" /> 
  </logos>
  </team>
- <team id="26" short_name="RCL" name="RC Lens" city="Lens" rank="16">
- <logos>
  <logo name="lens_s0910" size="24" ext="png" /> 
  <logo name="lens_s0910" size="32" ext="png" /> 
  <logo name="lens_s0910" size="44" ext="png" /> 
  <logo name="lens_s0910" size="64" ext="png" /> 
  <logo name="lens_s0910" size="56" ext="png" /> 
  </logos>
  </team>
- <team id="37" short_name="REN" name="Stade Rennais FC" city="Rennes" rank="2">
- <logos>
  <logo name="rennes_s0910" size="24" ext="png" /> 
  <logo name="rennes_s0910" size="32" ext="png" /> 
  <logo name="rennes_s0910" size="44" ext="png" /> 
  <logo name="rennes_s0910" size="64" ext="png" /> 
  <logo name="rennes_s0910" size="56" ext="png" /> 
  </logos>
  </team>
- <team id="36" short_name="PSG" name="Paris Saint Germain" city="Paris" rank="13">
- <logos>
  <logo name="paris_s0910" size="24" ext="png" /> 
  <logo name="paris_s0910" size="32" ext="png" /> 
  <logo name="paris_s0910" size="44" ext="png" /> 
  <logo name="paris_s0910" size="64" ext="png" /> 
  <logo name="paris_s0910" size="56" ext="png" /> 
  </logos>
  </team>
- <team id="38" short_name="SOC" name="FC Sochaux Montb�liard" city="Sochaux" rank="4">
- <logos>
  <logo name="SOCHAUX" size="24" ext="png" /> 
  <logo name="SOCHAUX" size="32" ext="png" /> 
  <logo name="SOCHAUX" size="44" ext="png" /> 
  <logo name="SOCHAUX" size="56" ext="png" /> 
  <logo name="SOCHAUX" size="64" ext="png" /> 
  </logos>
  </team>
- <team id="25" short_name="MTP" name="Montpellier HSC" city="Montpellier" rank="3">
- <logos>
  <logo name="montpellier_s0910" size="24" ext="png" /> 
  <logo name="montpellier_s0910" size="32" ext="png" /> 
  <logo name="montpellier_s0910" size="44" ext="png" /> 
  <logo name="montpellier_s0910" size="64" ext="png" /> 
  <logo name="montpellier_s0910" size="56" ext="png" /> 
  </logos>
  </team>
- <team id="42" short_name="VA" name="Valenciennes FC" city="Valenciennes" rank="9">
- <logos>
  <logo name="valenciennes_s0910" size="24" ext="png" /> 
  <logo name="valenciennes_s0910" size="32" ext="png" /> 
  <logo name="valenciennes_s0910" size="44" ext="png" /> 
  <logo name="valenciennes_s0910" size="64" ext="png" /> 
  <logo name="valenciennes_s0910" size="56" ext="png" /> 
  </logos>
  </team>
- <team id="31" short_name="OM" name="Olympique de Marseille" city="Marseille" rank="10">
- <logos>
  <logo name="marseille_s0910" size="24" ext="png" /> 
  <logo name="marseille_s0910" size="32" ext="png" /> 
  <logo name="marseille_s0910" size="44" ext="png" /> 
  <logo name="marseille_s0910" size="64" ext="png" /> 
  <logo name="marseille_s0910" size="56" ext="png" /> 
  </logos>
  </team>
- <team id="24" short_name="BOR" name="FC Girondins de Bordeaux" city="Bordeaux" rank="14">
- <logos>
  <logo name="bordeaux_s0910" size="24" ext="png" /> 
  <logo name="bordeaux_s0910" size="32" ext="png" /> 
  <logo name="bordeaux_s0910" size="44" ext="png" /> 
  <logo name="bordeaux_s0910" size="64" ext="png" /> 
  <logo name="bordeaux_s0910" size="56" ext="png" /> 
  </logos>
  </team>
- <team id="39" short_name="STE" name="AS Saint-Etienne" city="Saint-Etienne" rank="5">
- <logos>
  <logo name="saintetienne_s0910" size="24" ext="png" /> 
  <logo name="saintetienne_s0910" size="32" ext="png" /> 
  <logo name="saintetienne_s0910" size="44" ext="png" /> 
  <logo name="saintetienne_s0910" size="64" ext="png" /> 
  <logo name="saintetienne_s0910" size="56" ext="png" /> 
  </logos>
  </team>
- <team id="46" short_name="ACA" name="AC Arles-Avignon" city="Arles-Avignon" rank="20">
- <logos>
  <logo name="ACA" size="24" ext="png" /> 
  <logo name="ACA" size="32" ext="png" /> 
  <logo name="ACA" size="44" ext="png" /> 
  <logo name="ACA" size="56" ext="png" /> 
  <logo name="ACA" size="64" ext="png" /> 
  </logos>
  </team>
- <team id="41" short_name="TFC" name="Toulouse FC" city="Toulouse" rank="1">
- <logos>
  <logo name="tfc_s0910" size="24" ext="png" /> 
  <logo name="tfc_s0910" size="32" ext="png" /> 
  <logo name="tfc_s0910" size="44" ext="png" /> 
  <logo name="tfc_s0910" size="64" ext="png" /> 
  <logo name="tfc_s0910" size="56" ext="png" /> 
  </logos>
  </team>
  </teams>
  </action>
  </richmediaresponse>