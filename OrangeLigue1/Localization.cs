// -------------------------------  Copyright ---------------------------------
//	This software has been developped by France Telecom, FT/RD/RD/MAPS/DVC/MSF
//
//	Copyright (c) France Telecom 2009-2010
//
// COPYRIGHT    : This file is the property of FRANCE TELECOM.
//                It cannot be copied, used, or modified without obtaining
//                an authorization from the authors or a mandated
//                member of FRANCE TELECOM.
//                If such an authorization is provided, any modified version or
//                copy of the software has to contain this header.
//
// WARRANTIES   : This software is made available by the authors in the  hope
//                that it will be useful, but without any warranty.
//                France Telecom is not liable for any consequence related to the
//                use of the provided software.
//
// AUTHORS      : France Telecom  / RD / MAPS / DVC / MSF
// ----------------------------------------------------------------------------
using System.Reflection;
using System.Resources;

namespace OrangeLigue1
{
    /// <summary>
    /// Provides access to the application's localized resources.
    /// </summary>
    public class Localization
    {
        /// <summary>
        /// Gets the localized string for the specified key.
        /// </summary>
        /// <param name="key">The key of the localized string.</param>
        /// <returns>The localized string.</returns>
        public static string GetString(string key)
        {
            return Localization.SharedInstance.stringLibraryManager.GetString(key);
        }

        private static Localization sharedInstance;

        private static Localization SharedInstance
        {
            get
            {
                if (sharedInstance == null)
                {
                    sharedInstance = new Localization();
                }
                return sharedInstance;
            }
        }

        private ResourceManager stringLibraryManager = null;

        private Localization()
        {
            stringLibraryManager = new ResourceManager("OrangeLigue1.Strings", Assembly.GetExecutingAssembly());
        }
    }
}

