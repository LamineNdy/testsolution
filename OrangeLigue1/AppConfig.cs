// -------------------------------  Copyright ---------------------------------
//	This software has been developped by France Telecom, FT/RD/RD/MAPS/DVC/MSF
//
//	Copyright (c) France Telecom 2009-2010
//
// COPYRIGHT    : This file is the property of FRANCE TELECOM.
//                It cannot be copied, used, or modified without obtaining
//                an authorization from the authors or a mandated
//                member of FRANCE TELECOM.
//                If such an authorization is provided, any modified version or
//                copy of the software has to contain this header.
//
// WARRANTIES   : This software is made available by the authors in the  hope
//                that it will be useful, but without any warranty.
//                France Telecom is not liable for any consequence related to the
//                use of the provided software.
//
// AUTHORS      : France Telecom  / RD / MAPS / DVC / MSF
// ----------------------------------------------------------------------------
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.IO;
using System.Security.Cryptography;
using System.Text;
using System.Windows;
using System.Xml;
using System.Xml.Serialization;
using Orange.SDK.Enablers;
using OrangeLigue1.Model;

namespace OrangeLigue1
{
    /// <summary>
    /// Describes the configuration of the application.
    /// </summary>
    public class AppConfig
    {
        private static AppConfig _current = null;

        /// <summary>
        /// The special <see cref="AppConfig"/> instance used when the application's configuration
        /// cannot be determined.
        /// </summary>
        public static readonly AppConfig Empty = new AppConfig();

        /// <summary>
        /// The application's configuration.
        /// </summary>
        public static AppConfig Current
        {
            get
            {
                if (_current == null)
                {
                    _current = LoadConfiguration();
                }
                return _current;
            }
        }

        /// <summary>
        /// Loads and returns the application configuration.
        /// </summary>
        private static AppConfig LoadConfiguration()
        {
            string configName = App.CurrentApp.CurrentConfigurationName;
            if (configName == null)
            {
                return Empty;
            }

            // load the configuration from App.CurrentConfigurationName
            string configFilePath = Path.Combine("Configurations", string.Format("{0}.xml", configName));
            try
            {
                using (XmlReader configFileReader = XmlReader.Create(configFilePath))
                {
                    XmlSerializer serializer = new XmlSerializer(typeof(AppConfig));
                    return (AppConfig)serializer.Deserialize(configFileReader);
                }
            }
            catch (Exception error)
            {
                Logger.LogException(error, "Failed to read configuration file '{0}'!", configFilePath);
                MessageBox.Show(error.InnerException.ToString(), "Configuration error", MessageBoxButton.OK);
                if (configName == App.CurrentApp.DefaultConfigurationName)
                {
                    throw;
                }
            }

            // load the configuration from App.DefaultConfigurationName
            try
            {
                configFilePath = Path.Combine("Configurations", string.Format("{0}.xml", App.CurrentApp.DefaultConfigurationName));
                using (XmlReader configFileReader = XmlReader.Create(configFilePath))
                {
                    XmlSerializer serializer = new XmlSerializer(typeof(AppConfig));
                    return (AppConfig)serializer.Deserialize(configFileReader);
                }
            }
            catch (Exception error)
            {
                Logger.LogException(error, "Failed to read default configuration file '{0}'!", configFilePath);
                MessageBox.Show(error.InnerException.ToString(), "Configuration error", MessageBoxButton.OK);
                throw;
            }
        }

        public AppConfig()
        {

        }

        /// <summary>
        /// The configuration of the authentication manager.
        /// </summary>
        public AuthenticationManagerConfig AuthenticationManager
        {
            get;
            set;
        }

        /// <summary>
        /// The configuration of the ligue 1 AMP client.
        /// </summary>
        public AmpClientConfig AmpConfig
        {
            get;
            set;
        }

        /// <summary>
        /// The max number of alerts
        /// </summary>
        public int MaxAlertsNumber
        {
            get
            {
                if (string.IsNullOrEmpty(MaxAlertsNumberString))
                {
                    return 5;
                }
                try
                {
                    return Convert.ToInt32(MaxAlertsNumberString);
                }
                catch (Exception error)
                {
                    Logger.LogException(error, "MaxAlertsNumberString ('{0}')!", MaxAlertsNumberString);
                    return 5;
                }
            }
        }

        /// <summary>
        /// The int representation of the max alerts number
        /// </summary>
        [EditorBrowsable(EditorBrowsableState.Never)]
        public string MaxAlertsNumberString
        {
            get;
            set;
        }

        /// <summary>
        /// Specifies the configuration of the authentication manager.
        /// </summary>
        public class AuthenticationManagerConfig
        {
            public AuthenticationManagerConfig()
            {
            }

            /// <summary>
            /// The application identifier for the authentication manager.
            /// </summary>
            public string ApplicationId
            {
                get;
                set;
            }

            /// <summary>
            /// The application secret for the authentication manager.
            /// </summary>
            public string ApplicationSecret
            {
                get;
                set;
            }
        }
    }
}

