// -------------------------------  Copyright ---------------------------------
//	This software has been developped by France Telecom, FT/RD/RD/MAPS/DVC/MSF
//
//	Copyright (c) France Telecom 2009-2010
//
// COPYRIGHT    : This file is the property of FRANCE TELECOM.
//                It cannot be copied, used, or modified without obtaining
//                an authorization from the authors or a mandated
//                member of FRANCE TELECOM.
//                If such an authorization is provided, any modified version or
//                copy of the software has to contain this header.
//
// WARRANTIES   : This software is made available by the authors in the  hope
//                that it will be useful, but without any warranty.
//                France Telecom is not liable for any consequence related to the
//                use of the provided software.
//
// AUTHORS      : France Telecom  / RD / MAPS / DVC / MSF
// ----------------------------------------------------------------------------
using System;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using System.Reflection;
using System.Collections.Generic;
using System.Collections;

using System.Xml.Linq;

namespace Orange.SDK
{
    public class XmlSerializer
    {
        public XElement XmlElement { get; set; }

        private Type ObjectSerialized;

        private IEnumerable<SerializationInfo> AttributeTree;

        public XmlSerializer(Type MyObjectType)
        {
            MySerializableAttribute att = TypeHelper.GetAttribute<MySerializableAttribute>(MyObjectType);
            if (att != null)
        	{
        	    AttributeTree = GetMySerializationInfos(MyObjectType);	 
        	}
            ObjectSerialized = MyObjectType;
        }

        /// <summary>
        /// DeSerialize an object from its metadata
        /// (MySerializable & MySerialized)
        /// </summary>
        /// <remarks>if xml is null it returns an empty instanciated object</remarks>
        /// <param name="xml">the xml elemnt corresponding to the object</param>
        /// <returns>the instanciated object</returns>
        public Object ReadObject(XElement xml)
        {
            XmlElement = xml;
            Object ret = TypeHelper.Instantiate(ObjectSerialized);
            if (xml != null)
            {
                foreach (SerializationInfo serInfo in AttributeTree)
                {
                    string serializableName = serInfo.GetSerializableName();
                    string eltName = !String.IsNullOrEmpty(serializableName) && String.IsNullOrEmpty(serInfo.MySerializedAtt.Name) ? serializableName : serInfo.MySerializedAtt.Name;
                    PropertyInfo prop = serInfo.Property;
                    Type PropType = serInfo.Property.GetGetMethod().ReturnType;

                    if (PropType.IsValueType || PropType == typeof(string))
                    {
                        switch (serInfo.MySerializedAtt.ContentLevel)
                        {
                            case MemberValuePosition.Attribute:
                                SetPropertyValue(ret, serInfo.Property, GetAttributeValue(eltName, xml));
                                break;
                            case MemberValuePosition.Element:
                                SetPropertyValue(ret, serInfo.Property, GetElementValue(eltName, xml));
                                break;
                            case MemberValuePosition.All:
                                string value = GetAttributeValue(eltName, xml);
                                if (string.IsNullOrEmpty(value))
                                {
                                    value = GetElementValue(eltName, xml);
                                }
                                SetPropertyValue(ret, serInfo.Property, value);

                                break;
                            case MemberValuePosition.Content:
                                SetPropertyValue(ret, serInfo.Property, xml != null ? xml.Value : String.Empty);
                                break;
                            case MemberValuePosition.NestedAttribute:
                                SetPropertyValue(ret, serInfo.Property,GetAttributeValue(serInfo.MySerializedAtt.NestedAttributeName,xml.Element(eltName)));
                                break;
                            default: //cas par défaut
                                SetPropertyValue(ret, serInfo.Property, String.Empty);
                                break;
                        }
                    }
                    else if (Implements(PropType, typeof(IList)))
                    {
                        XElement listelem = xml;
                        if (serInfo.MySerializedAtt.ContentLevel == MemberValuePosition.NestedElementsList)
                        {
                            string nestname = eltName;
                            XElement nest = xml.Element(nestname);
                            if (nest != null)
                            {
                                listelem = nest;
                                eltName = serInfo.MySerializedAtt.NestedElementItemsName;	    	 
                	        }
                        }
                        IEnumerable<XElement> elems = listelem.Elements(eltName);
                        SetListPropertyValues(ret, serInfo.Property, elems);
                    }
                    else
                    {
                        XmlSerializer ser = new XmlSerializer(PropType);
                        MethodInfo setmethod = prop.GetSetMethod();
                        setmethod.Invoke(ret, new Object[] { ser.ReadObject(xml.Element(eltName)) });
                    }

                }
            }
            return ret;

        }

        private bool Implements(Type objType, Type iface)
        {
            Type[] types = objType.GetInterfaces();

            foreach (Type t in types)
            {
                if (t==iface)
                {
                    return true;
                }
            }
            return false;
        }

        private void SetListPropertyValues(Object obj, PropertyInfo prop, IEnumerable<XElement> elems)
        {
            Type ListType = prop.GetGetMethod().ReturnType;
            Type[] generics = ListType.GetGenericArguments();
            Type t;
            Object myObj = ListType.GetConstructor(new Type[] { }).Invoke(null);
            prop.GetSetMethod().Invoke(obj, new Object[] { myObj });
            if (generics.Length > 0)
            {
                t = generics[0];


                if (t.IsValueType || t == typeof(string))
                {
                    foreach (XElement item in elems)
                    {
                        if (t == typeof(string))
                        {
                            ListType.GetMethod("Add").Invoke(myObj, GetStringDefault(item.Value));
                        }
                        else if (t == typeof(int))
                        {
                            ListType.GetMethod("Add").Invoke(myObj, GetIntDefault(item.Value));
                        }
                        else if (t == typeof(double))
                        {
                            ListType.GetMethod("Add").Invoke(myObj, GetDoubleDefault(item.Value));
                        }
                    }
                }
                else
                {
                    XmlSerializer ser = new XmlSerializer(t);
                    foreach (XElement item in elems)
                    {
                        ListType.GetMethod("Add").Invoke(myObj, new Object[] { ser.ReadObject(item) });
                    }
                }
            }
                                   
        }

        /// <summary>
        /// Convert a string to the specified return type of an object propertie
        /// and pass the converted value to the propertie setter
        /// </summary>
        /// <param name="obj">the object which propertie has to be set</param>
        /// <param name="prop">The property to set</param>
        /// <param name="value">the value to cast & set</param>
        private void SetPropertyValue(Object obj, PropertyInfo prop, string value)
        {
            if(prop.GetGetMethod().ReturnType == typeof(string))
            {
                prop.GetSetMethod().Invoke(obj, GetStringDefault(value));
            }
            else if(prop.GetGetMethod().ReturnType == typeof(int))
            {
                prop.GetSetMethod().Invoke(obj, GetIntDefault(value));
            }
            else if(prop.GetGetMethod().ReturnType == typeof(double))
            {
                prop.GetSetMethod().Invoke(obj, GetDoubleDefault(value));
            }
            else
            {
            }
        }

        private Object[] GetStringDefault(string value)
        {
            object[] values;
            if (string.IsNullOrEmpty(value))
                values = new Object[] { String.Empty };
            else
                values = new Object[] { value };
            return values;
        }

        private Object[] GetIntDefault(string value)
        {
            object[] values;
            if (string.IsNullOrEmpty(value))
                values = new Object[] { int.MinValue };
            else
            {
                try
                {
                    values = new Object[] { Convert.ToInt32(value) };
                }
                catch (Exception)
                {
                    values = new Object[] { int.MinValue };
                    System.Diagnostics.Debug.WriteLine("Error while converting string(" + value + ") to int");
                }

            }
            return values;
        }

        private Object[] GetDoubleDefault(string value)
        {
            object[] values;
            if (string.IsNullOrEmpty(value))
                values = new Object[] { Double.MinValue };
            else
            {
                try
                {
                    try
                    {
                        values = new Object[] { Convert.ToDouble(value) };
                    }
                    catch (Exception)
                    {
                        System.Diagnostics.Debug.WriteLine("Error while converting string (" + value + ") to double with current culture parameters, falling back to invariant");
                        values = new Object[] { Convert.ToDouble(value, System.Globalization.CultureInfo.InvariantCulture) };
                    }

                }
                catch (Exception)
                {
                    values = new Object[] { Double.MinValue };
                    System.Diagnostics.Debug.WriteLine("Error while converting string ("+value+") to double");
                }

            }
            return values;

        }

        /// <summary>
        /// Get an xml elemnts's attribute value. If attribute doesn't exists it sets the value to
        /// String.Empty
        /// </summary>
        /// <param name="attName">Name of the Attribute</param>
        /// <param name="xml">The XML Element</param>
        /// <returns>The Attribute Value</returns>
        private string GetAttributeValue(string attName, XElement xml)
        {
            string ret = string.Empty;
            if (xml != null)
            {
                XAttribute att = xml.Attribute(attName);

                if (att != null)
                {
                    ret = att.Value;
                }
            }
            return ret;
        }

        /// <summary>
        /// Get an xml elemnts's inner value. If the Element doesn't exists it sets the value to
        /// String.Empty
        /// </summary>
        /// <param name="attName">Name of the Child Element</param>
        /// <param name="xml">The parent XML Element</param>
        /// <returns>The inner Element Value</returns>
        private string GetElementValue(string eltName, XElement xml)
        {
            string ret = string.Empty;
            if (xml != null)
            {
                XElement att = xml.Element(eltName);
                
                if (att != null)
                {
                    ret = att.Value;
                }
            }
            return ret;
        }

        

        private IEnumerable<SerializationInfo> GetMySerializationInfos(Type objType)
        {
            foreach (PropertyInfo prop in objType.GetProperties())
            {
                MySerializedAttribute att = TypeHelper.GetAttribute<MySerializedAttribute>(prop);
                if (att != null)
                {
                    yield return new SerializationInfo(prop, att);
                }
            }
        }
    }

    internal class SerializationInfo
	{
        public PropertyInfo Property { get; set; }		
        public MySerializedAttribute MySerializedAtt { get; set; }

        public SerializationInfo()
        {
        }

        public SerializationInfo(PropertyInfo prop, MySerializedAttribute att)
        {
            Property = prop;
            MySerializedAtt = att;
        }

        public Type GetPropertyType()
        {
            return Property.GetGetMethod().ReturnType;
        }

        public string GetSerializableName()
        {
            MySerializableAttribute att = TypeHelper.GetAttribute<MySerializableAttribute>(GetPropertyType());
            return att != null ? att.Name : String.Empty;
        }

	}

    [global::System.AttributeUsage(AttributeTargets.Class, Inherited = false, AllowMultiple = false)]
    public sealed class MySerializableAttribute : Attribute
    {

        public string Name { get; set; }
        // This is a positional argument
        public MySerializableAttribute(string name)
        {
            Name = name;
        }
    }
    /// <summary>
    /// Describe the position of the content of the current object 
    /// to deserialize respectively to its parent
    /// </summary>
    /// <example>
    /// <item exatt1="toto">
    ///     <Subitem>tagada</Subitem>
    /// </item>
    /// 
    /// The class "item" has 2 members : exatt1 and subitem
    /// 
    /// The exatt1 member will have an AttibuteContentPosition set to Attribute (respectively to its parent : item)
    /// whereas the Subitem member will have an AttributeContentPosition set To Content;
    /// </example>
    public enum MemberValuePosition
    {
        Attribute,
        Element,
        NestedAttribute,
        NestedElementsList,
        Content,
        All
    }

    [global::System.AttributeUsage(AttributeTargets.Property, Inherited = false, AllowMultiple = true)]
    public sealed class MySerializedAttribute : Attribute
    {
        public MemberValuePosition ContentLevel { get; set; }
        public string Name;
        public string NestedAttributeName { get; set; }
        public string NestedElementItemsName { get; set; }

        // This is a positional argument
        public MySerializedAttribute()
        {
        }

    }


}

