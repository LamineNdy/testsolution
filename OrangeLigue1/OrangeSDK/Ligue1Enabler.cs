// -------------------------------  Copyright ---------------------------------
//	This software has been developped by France Telecom, FT/RD/RD/MAPS/DVC/MSF
//
//	Copyright (c) France Telecom 2009-2010
//
// COPYRIGHT    : This file is the property of FRANCE TELECOM.
//                It cannot be copied, used, or modified without obtaining
//                an authorization from the authors or a mandated
//                member of FRANCE TELECOM.
//                If such an authorization is provided, any modified version or
//                copy of the software has to contain this header.
//
// WARRANTIES   : This software is made available by the authors in the  hope
//                that it will be useful, but without any warranty.
//                France Telecom is not liable for any consequence related to the
//                use of the provided software.
//
// AUTHORS      : France Telecom  / RD / MAPS / DVC / MSF
// ----------------------------------------------------------------------------
using System;
using System.Threading;
using System.Xml.Linq;
using Orange.SDK.Enablers.Ligue1;
using System.IO;
using System.Text;
using System.Collections.Generic;
using System.Windows.Threading;
using OrangeLigue1.Storage;
using OrangeLigue1.Tools;
using OrangeLigue1;
using OrangeLigue1.Model;
using Microsoft.Phone.Info;
using Microsoft.Phone.Net.NetworkInformation;
using AuthenticationManager;
using System.IO.IsolatedStorage;

namespace Orange.SDK.Enablers
{
    /// <summary>
    /// The Ligue1Enabler class gives a simple interface to manipulate
    /// the Ligue1 webservice.
    /// It has been designed asynchrounously, you simply have to call a method
    /// that returns instantly and an event is triggered when the corresponding request
    /// ends.
    /// <remarks>To be implemented : WebServiceRequest Pool.
    /// </remarks>
    /// </summary>
    public class Ligue1Enabler
    {
        const string AID_HTTPHEADER_NAME = "X_AMP_AID";
        const string MID_HTTPHEADER_NAME = "X_AMP_MID";
        const string COOKIE_HTTPHEADER_NAME = "Cookie";
        private string m_CurrentSetCookie = "";
        private IsolatedStorageSettings m_oUserSettings;

        public static string SampleNativeHostUAString = "NativeHost";

        private string parameters { get; set; }
        private Dispatcher dispatcher;
        private const string encoding = "UTF-8";

        private AmpClientConfig config;

        public Ligue1Enabler(AmpClientConfig config)
        {
            //Get Current Dispatcher, to Send Events and Avoid IllegalCrossThreadAccess Exception
            dispatcher = System.Windows.Deployment.Current.Dispatcher;
            this.config = config;
        }

        public void Request(string action)
        {
            this.Request(action, "GET", null);
        }

        public void Request(string action, string method, string parameters)
        {
            BackgroundWebRequest webRequest = null;

            if (String.IsNullOrEmpty(action) || config == null || string.IsNullOrEmpty(config.NominalServerUri))
            {
                throw new InvalidOperationException();
            }

            Uri nominalUri = new Uri(config.NominalServerUri + action);
            Uri cellularOnlyUri = null;
            if (!string.IsNullOrEmpty(config.CellularOnlyServerUri))
            {
                cellularOnlyUri = new Uri(config.CellularOnlyServerUri + action);
            }
            if (cellularOnlyUri != null)
            {
                webRequest = BackgroundWebRequest.CreateAuthenticated(cellularOnlyUri, nominalUri);
            }
            else
            {
                webRequest = BackgroundWebRequest.Create(nominalUri);
            }

            webRequest.Method = method;
            if (parameters != null)
            {
                webRequest.ContentType = "application/xml";
                webRequest.RequestData = System.Text.Encoding.GetEncoding(encoding).GetBytes(parameters);
            }
            webRequest.Headers["User-Agent"] = App.CurrentApp.CurrentUserAgent;
            //webRequest.Headers["X_AMP_MID"] = DeviceExtendedProperties.GetValue("DeviceUniqueId").ToString();
            webRequest.Headers["X_AMP_UA"] = App.CurrentApp.CurrentUserAgent;
            if (ConnectivityWatcher.CurrentType == ConnectivityType.WiFi)
            {
                if(DeviceNetworkInformation.IsCellularDataEnabled)
                    webRequest.Headers["X_AMP_BEARER"] = "0";
                else
                    webRequest.Headers["X_AMP_BEARER"] = "1";
            }
            else
                webRequest.Headers["X_AMP_BEARER"] = "0";

            webRequest.Headers[MID_HTTPHEADER_NAME] = getUUID();
            if (!string.IsNullOrEmpty(getCookie()))
            {
                if (action != AppConfig.Current.AmpConfig.DynamicInit + "?forceauthent")
                    webRequest.Headers[COOKIE_HTTPHEADER_NAME] = m_CurrentSetCookie;
            }
            webRequest.Completed += OnBackgroundWebRequestCompleted;

            webRequest.Start(null);
        }

        private string getCookie()
        {
            m_oUserSettings = IsolatedStorageSettings.ApplicationSettings;
            try
            {                
                m_CurrentSetCookie = m_oUserSettings[OLAuthenticationManager.USSO_LAST_SETCOOKIE_VALUE].ToString();
            }
            catch (KeyNotFoundException)
            {
                m_CurrentSetCookie = "";
            }
            return m_CurrentSetCookie;
        }

        private string getUUID()
        {
            byte[] uniqueIdBytes = DeviceExtendedProperties.GetValue("DeviceUniqueId") as byte[];
            string uUID = BitConverter.ToString(uniqueIdBytes);
            uUID = uUID.Replace("-", "");
            return uUID;
        }

        /// <summary>
        /// Called when a background web request initiated by the network cache has completed.
        /// </summary>
        private void OnBackgroundWebRequestCompleted(object sender, BackgroundRequestCompletedEventArgs e)
        {
            string response = null;

            BackgroundWebRequest request = sender as BackgroundWebRequest;
            if (request == null)
            {
                return;
            }
            try
            {
                if (e.Error != null ||
                    e.Cancelled)
                {
                    if (ConnectionErrorRequestEnd != null)
                    {
                        ConnectionErrorRequestEnd(this, e);
                    }
                    return;
                }
                response = BuildString(e.Data, encoding);
                dispatcher.BeginInvoke(new Action(() =>
                {
                    XDocument xDoc = null;

                    try
                    {
                        xDoc = XDocument.Parse(response);
                    }
                    catch (Exception error)
                    {
                        xDoc = null;
                    }

                    if (xDoc != null)
                    {
                        RequestDispatcher(xDoc, request);
                    }
                    else if (ConnectionErrorRequestEnd != null)
                    {
                        ConnectionErrorRequestEnd(this, e);
                    }

                }), null);
            }
            catch (Exception)
            {
                if (ConnectionErrorRequestEnd != null)
                {
                    ConnectionErrorRequestEnd(this, e);
                }
            }
        }

        /// <summary>
        /// Builds a string instance from the specified data.
        /// Returns <c>null</c> if the data cannot be read.
        /// </summary>
        public static string BuildString(byte[] data, string encoding)
        {
            if (data == null || data.Length == 0)
            {
                return null;
            }
            try
            {
                return Encoding.GetEncoding(encoding).GetString(data, 0, data.Length);
            }
            catch (Exception error)
            {
                Logger.DebugTrace("Failed to build string: {0}", error);
                return null;
            }
        }

        #region "Requests"
        /// <summary>
        /// Initiate the initinfo request to gather initialization informations
        /// <remarks>Raise the Initialized event on completion</remarks>
        /// </summary>
        public void DynamicInitialize()
        {
            if (ConnectivityWatcher.CurrentType == ConnectivityType.WiFi)
                this.Request(AppConfig.Current.AmpConfig.DynamicInit);
            else if (ConnectivityWatcher.CurrentType == ConnectivityType.Cellular)
                this.Request(AppConfig.Current.AmpConfig.DynamicInit + "?forceauthent"); //force new cookie for 3G 
            else
                this.Request(AppConfig.Current.AmpConfig.DynamicInit);
            
        }

        /// <summary>
        /// Initiate the initinfo request to gather initialization informations
        /// <remarks>Raise the Initialized event on completion</remarks>
        /// </summary>
        public void Initialize()
        {
            this.Request(AppConfig.Current.AmpConfig.InitInfoString);
        }

        public void RequestLightSettings()
        {
            this.Request(AppConfig.Current.AmpConfig.LightSettingsString);
        }

        /// <summary>
        /// Initiate the userinfo request to gather informations
        /// <remarks>Raise the Initialized event on completion</remarks>
        /// </summary>
        public void RequestUserInfo()
        {
            this.Request(AppConfig.Current.AmpConfig.UserInfoString);
        }

        /// <summary>
        /// The wording request provides the application with the wording to use to display information at the end-user
        /// </summary>
        public void RequestWording()
        {
            this.Request(AppConfig.Current.AmpConfig.WordingString);
        }

        /// <summary>
        /// Get the options catalog
        /// <remarks>Raise the Initialized event on completion</remarks>
        /// </summary>
        public void RequestOptionCatalog()
        {
            this.Request(AppConfig.Current.AmpConfig.OptionCatalogString);
        }

        /// <summary>
        /// Get the options catalog
        /// <remarks>Raise the Initialized event on completion</remarks>
        /// </summary>
        /// <param name="id">the day id</param>
        public void RequestSubscribeOption(string id, string type)
        {
            this.Request(AppConfig.Current.AmpConfig.OptionSubscribeString+"="+id+"&type="+type);
        }

        /// <summary>
        /// Initiate the "Calendar" request on one or more days
        /// </summary>
        /// <remarks>The request is sent everytime because the dayslist / matches are highly mutable objects.
        /// They need to be Deleted/reconstructed everytime before implementation of smarter behaviour.</remarks>
        /// <exception cref="ArgumentNullException">no days are specified</exception>
        /// <param name="days">the day id</param>
        public void RequestCalendar(params int[] days)
        {
            string paramday = string.Empty;

            if (days.Length > 0)
            {
                foreach (int day in days)
                {
                    paramday += day.ToString() + ";";
                }
                paramday = paramday.TrimEnd(';');

                this.Request(AppConfig.Current.AmpConfig.CalendarString + paramday + "?nocache=" + DateTime.Now.Ticks.ToString());
            }
            else
            {
                this.Request(AppConfig.Current.AmpConfig.CalendarString + "?nocache=" + DateTime.Now.Ticks.ToString());
            }

        }

        /// <summary>
        /// Initiate the daynews request to obtain a quick list of "n" news titles
        /// </summary>
        /// <param name="news_number">number of news to obtain, specifying 0 uses the webservice default value which is 10</param>
        public void RequestDayNews(int news_number)
        {
            this.Request(AppConfig.Current.AmpConfig.DayNewsString + "?nocache=" + DateTime.Now.Ticks.ToString());
        }

        /// <summary>
        /// Initiate the daynews request to obtain a quick list of "n" news titles
        /// specified before "offset"
        /// </summary>
        /// <param name="news_number">number of news to obtain, specifying 0 uses the webservice default value which is 10</param>
        /// <param name="id_offset">The id of a news that limits the upper bound of the news list</param>
        public void RequestDayNews(int news_number, int id_offset)
        {

            string Format = "<?xml version=\"1.0\" encoding=\"ISO-8859-1\"?><richmediarequest module=\"sportL1\"><appname>LIGUE1</appname><appversion>" + AppConfig.Current.AmpConfig.AppVersion + "</appversion><action name=\"daynews\">{0}</action></richmediarequest>";
            string format_offset = "<param name=\"offset\" value=\"{0}\" /> ";
            string format_number = "<param name=\"number\" value=\"{0}\" /> ";
            string res_format = String.Empty;

            if (news_number > 0)
                res_format = String.Format(format_number, news_number.ToString());
            if (id_offset > 0)
                res_format += String.Format(format_offset, id_offset.ToString());


            this.parameters = String.Format(Format, res_format);
            this.Request(AppConfig.Current.AmpConfig.DayNewsString + "?nocache=" + DateTime.Now.Ticks.ToString());
        }

        /// <summary>
        /// Request a detailed view (= with text) of the news ids specified in argument
        /// </summary>
        /// <remarks>If all the news ids already exists in the Newz collection
        /// the request is not launched and the event is triggered immediately.
        /// If only certains Ids already exists the request only asks for the missing ones</remarks>
        /// <param name="ids">ids of the wanted detailed news</param>
        public void RequestDayNews(params int[] ids)
        {
            if (!(ids.Length > 0))
                throw new ArgumentNullException();

            string Format = "<?xml version=\"1.0\" encoding=\"ISO-8859-1\"?><richmediarequest module=\"sportL1\"><appname>LIGUE1</appname><appversion>" + AppConfig.Current.AmpConfig.AppVersion + "</appversion><action name=\"daynews\"><param name=\"id\" value=\"{0}\"/></action></richmediarequest>";
            string paramid = string.Empty;

            foreach (int id in ids)
            {
                //vérification de la non existence de la news complète dans le cache
                paramid += id.ToString() + ";";
            }
            paramid = paramid.TrimEnd(';');

            this.parameters = String.Format(Format, paramid);
            this.Request(AppConfig.Current.AmpConfig.DayNewsString + "?nocache=" + DateTime.Now.Ticks.ToString());
        }
        /// <summary>
        /// Send the allmatch request
        /// </summary>
        /// <remarks>
        /// Use case 1: The application is not on match page and just need to have the minimal and mandatory information about all the current day matches.
        ///The request allmatch doesn’t specify any parameter so that the response will contain the new information about all the matches since the last update of the user.
        ///The returned information are : score evolution, status of match evolution and new special events  (redcard, goal, or video).
        ///Use Case 2 : The application is on a match page and need to have the mandatory information about all the current day matches but also the comments on the match of the page.
        ///The request specify a match id and an event id so that the response will contain the new information about all the matches since the last update of the user but also the new comments of the match given in parameter.
        ///The returned information are : score evolution, status of match evolution, new special events  (redcard, goal, or video) and the last events of the match given in parameter after the event given in parameter.</remarks>
        /// <param name="match_id">the match id which we want to obtain informations about</param>
        /// <param name="event_id">the last event id we got about this match.</param>
        /// <param name="get_videos">Set the get video flag to obtain event's video information</param>
        ///<exception cref="ArgumentException">according to the use case event_id and match_id must be zeroed together or non-zeroed together.</exception>
        public void RequestAllMatch(int match_id, int event_id, bool get_videos = true)
        {
            this.Request(AppConfig.Current.AmpConfig.AllMatchString+"?nocache=" + DateTime.Now.Ticks.ToString());
        }

        /// <summary>
        /// Initiate a match request
        /// </summary>
        /// <param name="matchId">mandatory parameter : id of the match which we want to get infos about</param>
        /// <param name="getstats">video flag : true > gets all the videos concerning the mathc, false > no videos</param>
        /// <param name="eventNumber">facultative, if specified : puts a limit of events returned with the match info</param>
        /// <param name="eventOffset">facultative : event id specifying the upper bound of the events to get.</param>
        public void RequestMatch(int matchId, bool getstats = true, int eventNumber = 0, int eventOffset = 0)
        {
            this.Request(AppConfig.Current.AmpConfig.MatchString + matchId + "?nocache=" + DateTime.Now.Ticks.ToString());
        }

        /// <summary>
        /// Initiate a teams request.
        /// </summary>
        public void RequestTeams()
        {
            this.Request(AppConfig.Current.AmpConfig.TeamsString + "?nocache=" + DateTime.Now.Ticks.ToString());
        }

        /// <summary>
        /// Initiate a vod request.
        /// </summary>
        public void RequestVod()
        {
            this.Request(AppConfig.Current.AmpConfig.VodString + "?nocache=" + DateTime.Now.Ticks.ToString());
        }

        public enum NotifSettingsSubscription
        {
            NotifSettingsSubscriptionDeactivated,
            NotifSettingsSubscriptionLightMode,
            NotifSettingsSubscriptionExpertMode
        }

        public enum NotifSettingsAction
        {
            NotifSettingsActionGet,
            NotifSettingsActionSet
        }

        /// <summary>
        /// 
        /// </summary>
        public void RequestNotifSettings(NotifSettingsAction action, NotifSettingsSubscription subscription, List<string> teamsId)
        {
            string parameters = "<?xml version=\"1.0\" encoding=\"UTF-8\"?><richmediaresponse module=\"sportL1\">";
            string request = AppConfig.Current.AmpConfig.NotifSettings;

            if (action == NotifSettingsAction.NotifSettingsActionSet)
            {
                request += "SET";
                parameters += "<action name=\"push\"><channel type=\"MPNS\"";

                switch (subscription)
                {
                    case NotifSettingsSubscription.NotifSettingsSubscriptionDeactivated:
                        parameters += " action_id=\"0\"";
                        break;
                    case NotifSettingsSubscription.NotifSettingsSubscriptionLightMode:
                        parameters += " action_id=\"1\"";
                        break;
                    case NotifSettingsSubscription.NotifSettingsSubscriptionExpertMode:
                        parameters += " action_id=\"2\"";
                        break;
                }

                parameters += " teams_id=\"";
                for (int i = 0; i < teamsId.Count; i++)
                {
                    if (i > 0)
                    {
                        parameters += ";";
                    }
                    parameters += teamsId[i];
                }
                parameters += "\"";

                //PushNotificationsHandler pushHandler = PushNotificationsHandler.SharedInstance;
                
                //Microsoft.Phone.Notification.HttpNotificationChannel pushChannel = pushHandler.PushChannel;
                if (!string.IsNullOrEmpty(App.CurrentChannel.ChannelUri.ToString()))
                {
                    parameters += " token=\"" + App.CurrentChannel.ChannelUri.ToString() + "\"";
                }

                parameters += "/></action>";
            }
            else
            {
                request += "GET";
            }

            parameters += "</richmediaresponse>";

            this.Request(request, "POST", parameters);
        }

        /// <summary>
        /// Gets the xml result of a request and call the right function to interpret it.
        /// </summary>
        /// <param name="requestResult">XML result</param>???
        #endregion

        private void RequestDispatcher(XDocument requestResult, BackgroundWebRequest request)
        {
            var xElement = requestResult.Element("richmediaresponse");
            if (xElement == null)
            {
                if (ConnectionErrorRequestEnd != null)
                {
                    ConnectionErrorRequestEnd(this, null);
                }
                return;
            }

            foreach (XElement elementAction in requestResult.Descendants("action"))
            {
                //XElement elementAction = xElement.Element("action");

                if (elementAction == null)
                {
                    if (ConnectionErrorRequestEnd != null)
                    {
                        ConnectionErrorRequestEnd(this, null);
                    }
                    return;
                }

                var xAttribute = elementAction.Attribute("name");
                if (xAttribute == null)
                {
                    bool dispatched = false;

                    if (!dispatched) dispatched = this.DispatchUnnamedAction<InitinfoAction>(AppConfig.Current.AmpConfig.InitInfoString, request, elementAction, InitinfoRequestEnd);
                    if (!dispatched) dispatched = this.DispatchUnnamedAction<LightSettingsAction>(AppConfig.Current.AmpConfig.LightSettingsString, request, elementAction, LightSettingsRequestEnd);
                    if (!dispatched) dispatched = this.DispatchUnnamedAction<UserinfoAction>(AppConfig.Current.AmpConfig.UserInfoString, request, elementAction, UserinfoRequestEnd);
                    if (!dispatched) dispatched = this.DispatchUnnamedAction<WordingAction>(AppConfig.Current.AmpConfig.WordingString, request, elementAction, WordingRequestEnd);
                    if (!dispatched) dispatched = this.DispatchUnnamedAction<OptionCatalogAction>(AppConfig.Current.AmpConfig.OptionCatalogString, request, elementAction, OptionCatalogRequestEnd);
                    if (!dispatched) dispatched = this.DispatchUnnamedAction<OptionSubscribeAction>(AppConfig.Current.AmpConfig.OptionSubscribeString, request, elementAction, OptionSubscribeRequestEnd);
                    if (!dispatched) dispatched = this.DispatchUnnamedAction<CalendarAction>(AppConfig.Current.AmpConfig.CalendarString, request, elementAction, CalendarRequestEnd);
                    if (!dispatched) dispatched = this.DispatchUnnamedAction<DaynewsAction>(AppConfig.Current.AmpConfig.DayNewsString, request, elementAction, NewsRequestEnd);
                    if (!dispatched) dispatched = this.DispatchUnnamedAction<AllMatchAction>(AppConfig.Current.AmpConfig.AllMatchString, request, elementAction, AllMatchRequestEnd);
                    if (!dispatched) dispatched = this.DispatchUnnamedAction<MatchAction>(AppConfig.Current.AmpConfig.MatchString, request, elementAction, MatchRequestEnd);
                    if (!dispatched) dispatched = this.DispatchUnnamedAction<TeamsAction>(AppConfig.Current.AmpConfig.TeamsString, request, elementAction, TeamsRequestEnd);
                    if (!dispatched) dispatched = this.DispatchUnnamedAction<VoDAction>(AppConfig.Current.AmpConfig.VodString, request, elementAction, VodRequestEnd);
                    if (!dispatched) dispatched = this.DispatchUnnamedAction<PushSettingsAction>(AppConfig.Current.AmpConfig.NotifSettings, request, elementAction, PushSettingsRequestEnd);

                    if (!dispatched && ConnectionErrorRequestEnd != null) ConnectionErrorRequestEnd(this, null);

                    return;
                }

                var action = xAttribute.Value;
                System.Diagnostics.Debug.WriteLine("CURRENT ACTION: "+action);
                if (action.Contains("calendar"))
                {
                    // detecting action of type calendar$day
                    string day = System.Text.RegularExpressions.Regex.Replace(action, "[^0-9]+", string.Empty);
                    if (day != "" && day != null)
                        action = "calendarday";
                }

                XmlSerializer ser;
                switch (action)
                {
                    case "initinfo":
                        ser = new XmlSerializer(typeof(InitinfoAction));
                        if (InitinfoRequestEnd != null)
                        {
                            InitinfoRequestEnd(this, (InitinfoAction)ser.ReadObject(elementAction));
                        }
                        break;
                    case "versioning":
                        ser = new XmlSerializer(typeof(DynamicInitAction));
                        if (DynamicInitRequestEnd != null)
                        {
                            DynamicInitRequestEnd(this, (DynamicInitAction)ser.ReadObject(elementAction));
                        }
                        break;
                    case "settingslight":
                        ser = new XmlSerializer(typeof(LightSettingsAction));
                        if (LightSettingsRequestEnd != null)
                        {
                            LightSettingsRequestEnd(this, (LightSettingsAction)ser.ReadObject(elementAction));
                        }
                        break;
                    case "userinfo":
                        ser = new XmlSerializer(typeof(UserinfoAction));
                        if (UserinfoRequestEnd != null)
                        {
                            UserinfoRequestEnd(this, (UserinfoAction)ser.ReadObject(elementAction));
                        }
                        break;
                    case "wording":
                        ser = new XmlSerializer(typeof(WordingAction));
                        if (WordingRequestEnd != null)
                        {
                            WordingRequestEnd(this, (WordingAction)ser.ReadObject(elementAction));
                        }
                        break;
                    case "catalog":
                        ser = new XmlSerializer(typeof(OptionCatalogAction));
                        if (OptionCatalogRequestEnd != null)
                        {
                            OptionCatalogRequestEnd(this, (OptionCatalogAction)ser.ReadObject(elementAction));
                        }
                        break;
                    case "subscribe":
                        ser = new XmlSerializer(typeof(OptionSubscribeAction));
                        if (OptionSubscribeRequestEnd != null)
                        {
                            OptionSubscribeRequestEnd(this, (OptionSubscribeAction)ser.ReadObject(elementAction));
                        }
                        break;
                    case "daynews":
                        ser = new XmlSerializer(typeof(DaynewsAction));
                        if (NewsRequestEnd != null)
                        {
                            NewsRequestEnd(this, (DaynewsAction)ser.ReadObject(elementAction));
                        }
                        break;
                    case "vod":
                        ser = new XmlSerializer(typeof(VoDAction));
                        if (VodRequestEnd != null)
                        {
                            VodRequestEnd(this, (VoDAction)ser.ReadObject(elementAction));
                        }
                        break;
                    case "match":
                        ser = new XmlSerializer(typeof(MatchAction));
                        if (MatchRequestEnd != null)
                        {
                            MatchRequestEnd(this, (MatchAction)ser.ReadObject(elementAction));
                        }
                        break;
                    case "matches":
                        ser = new XmlSerializer(typeof(AllMatchAction));
                        if (AllMatchRequestEnd != null)
                        {
                            AllMatchRequestEnd(this, (AllMatchAction)ser.ReadObject(elementAction));
                        }
                        break;
                    case "calendarday":
                        ser = new XmlSerializer(typeof(CalendarAction));
                        if (CalendarRequestEnd != null)
                        {
                            CalendarRequestEnd(this, (CalendarAction)ser.ReadObject(elementAction));
                        }
                        break;
                    case "calendar":
                        ser = new XmlSerializer(typeof(WholeCalendarAction));
                        if (WholeCalendarRequestEnd != null)
                        {
                            WholeCalendarRequestEnd(this, (WholeCalendarAction)ser.ReadObject(elementAction));
                        }
                        break;
                    case "teams":
                        ser = new XmlSerializer(typeof(TeamsAction));
                        if (TeamsRequestEnd != null)
                        {
                            TeamsRequestEnd(this, (TeamsAction)ser.ReadObject(elementAction));
                        }
                        break;
                    case "push":
                        ser = new XmlSerializer(typeof(PushSettingsAction));
                        if (PushSettingsRequestEnd != null)
                        {
                            PushSettingsRequestEnd(this, (PushSettingsAction)ser.ReadObject(elementAction));
                        }
                        break;
                    default:
                        if (ConnectionErrorRequestEnd != null)
                        {
                            ConnectionErrorRequestEnd(this, null);
                        }
                        break;
                } //end of switch

            }


        }

        private bool DispatchUnnamedAction<T>(string requestPath, BackgroundWebRequest request, XElement actionElement, EventHandler<T> handler) where T : EventArgs
        {
            string uri = request.RequestUri.ToString();

            if (uri.Contains(requestPath)) 
            {
                if (handler != null)
                {
                    XmlSerializer ser = new XmlSerializer(typeof(T));
                    handler(this, (T)ser.ReadObject(actionElement));
                }
                return true;
            }

            return false;
        }

        #region "Event/Args"
        public event EventHandler<InitinfoAction> InitinfoRequestEnd;
        public event EventHandler<DynamicInitAction> DynamicInitRequestEnd;
        public event EventHandler<LightSettingsAction> LightSettingsRequestEnd;
        public event EventHandler<UserinfoAction> UserinfoRequestEnd;
        public event EventHandler<WordingAction> WordingRequestEnd;
        public event EventHandler<OptionCatalogAction> OptionCatalogRequestEnd;
        public event EventHandler<OptionSubscribeAction> OptionSubscribeRequestEnd;
        public event EventHandler<TeamsAction> TeamsRequestEnd;
        public event EventHandler<CalendarAction> CalendarRequestEnd;
        public event EventHandler<WholeCalendarAction> WholeCalendarRequestEnd;
        public event EventHandler<DaynewsAction> NewsRequestEnd;
        public event EventHandler<MatchAction> MatchRequestEnd;
        public event EventHandler<AllMatchAction> AllMatchRequestEnd;
        public event EventHandler<VoDAction> VodRequestEnd;
        public event EventHandler<BackgroundRequestCompletedEventArgs> ConnectionErrorRequestEnd;
        public event EventHandler<PushSettingsAction> PushSettingsRequestEnd;
        #endregion
    }

    public enum Ligue1Actions
    {
        daynews,
        initinfo,
        init,
        optioncatalog,
        optionsubscribe,
        lightsettings,
        wording,
        initwidget,
        calendar,
        vod,
        teams,
        matches,
        match
    }

    #region "New object w/ serialization metadata"
    namespace Ligue1
    {
        [MySerializable("action")]
        public class DynamicInitAction : EventArgs
        {
            [MySerialized(Name = "respStatus", ContentLevel = MemberValuePosition.Element)]
            public int RespStatus { get; set; }
            [MySerialized]
            public VersionActionInfo Version { get; set; }
        }

        [MySerializable("action")]
        public class LightSettingsAction : EventArgs
        {
            [MySerialized(Name = "respStatus", ContentLevel = MemberValuePosition.Element)]
            public int RespStatus { get; set; }
            [MySerialized]
            public InfoParams InitInfo { get; set; }
            [MySerialized]
            public SeasonActionInfo Season { get; set; }
            [MySerialized(Name = "ticker")]
            public string Ticker { get; set; }
            [MySerialized]
            public TeamsList Teams { get; set; }
            [MySerialized(Name = "managePass")]
            public string ManagePass { get; set; }
        }

        [MySerializable("action")]
        public class InitinfoAction : EventArgs
        {
            [MySerialized(Name = "respStatus", ContentLevel = MemberValuePosition.Element)]
            public int RespStatus { get; set; }
            [MySerialized]
            public InfoParams InitInfo { get; set; }
            [MySerialized]
            public DayInfo Matches { get; set; }
            [MySerialized(Name = "daynews", ContentLevel = MemberValuePosition.Element)]
            public DayNewsInfo DayNews { get; set; }
        }

        [MySerializable("action")]
        public class UserinfoAction : EventArgs
        {
            [MySerialized(Name = "respStatus", ContentLevel = MemberValuePosition.Element)]
            public int RespStatus { get; set; }
            [MySerialized(Name = "message", ContentLevel = MemberValuePosition.Element)]
            public string Message { get; set; }
            [MySerialized(Name = "l1compliant", ContentLevel = MemberValuePosition.Element)]
            public int L1Compliant { get; set; }
            [MySerialized(Name = "l1compliantMessage", ContentLevel = MemberValuePosition.Element)]
            public string L1CompliantMessage { get; set; }
            [MySerialized(Name = "dayPassCompliant", ContentLevel = MemberValuePosition.Element)]
            public int DayPassCompliant { get; set; }
            [MySerialized(Name = "dayPassCompliantMessage", ContentLevel = MemberValuePosition.Element)]
            public string DayPassCompliantMessage { get; set; }
            [MySerialized(Name = "upgradePlan", ContentLevel = MemberValuePosition.Element)]
            public int UpgradePlan { get; set; }
            [MySerialized(Name = "fairUse", ContentLevel = MemberValuePosition.Element)]
            public int FairUse { get; set; }
        }

        [MySerializable("action")]
        public class WordingAction : EventArgs
        {
            [MySerialized(Name = "respStatus", ContentLevel = MemberValuePosition.Element)]
            public int RespStatus { get; set; }
            [MySerialized]
            public InfoParams WordingInfo { get; set; }
            [MySerialized(Name = "wording", ContentLevel = MemberValuePosition.NestedElementsList, NestedElementItemsName = "element")]
            public List<WordingInfo> Elements { get; set; }
        }

        [MySerializable("action")]
        public class OptionCatalogAction : EventArgs
        {
            [MySerialized(Name = "respStatus", ContentLevel = MemberValuePosition.Element)]
            public int RespStatus { get; set; }
            [MySerialized(Name = "catalog", ContentLevel = MemberValuePosition.NestedElementsList, NestedElementItemsName = "option")]
            public List<OptionInfo> Options { get; set; }
            [MySerialized(Name = "catalog", ContentLevel = MemberValuePosition.NestedElementsList, NestedElementItemsName = "pass")]
            public List<PassInfo> Pass { get; set; }
        }

        [MySerializable("action")]
        public class OptionSubscribeAction : EventArgs
        {
            [MySerialized(Name = "respStatus", ContentLevel = MemberValuePosition.Element)]
            public int RespStatus { get; set; }
            [MySerialized(Name = "message", ContentLevel = MemberValuePosition.Element)]
            public string Message { get; set; }
            [MySerialized(Name = "confirmSubcribe", ContentLevel = MemberValuePosition.Element)]
            public string ConfirmMessage { get; set; }
        }

        [MySerializable("action")]
        public class CalendarAction : EventArgs
        {
            [MySerialized(Name = "respStatus", ContentLevel = MemberValuePosition.Element)]
            public int RespStatus { get; set; }
            [MySerialized(Name = "message", ContentLevel = MemberValuePosition.Element)]
            public string Message { get; set; }
            [MySerialized(Name = "calendar", ContentLevel = MemberValuePosition.NestedElementsList, NestedElementItemsName = "day")]
            public List<CalendarDayInfo> Matches { get; set; }
            /*[MySerialized(Name = "matches")]
            public List<DayInfo> Matches { get; set; } //CalendarDayInfo*/
        }

        [MySerializable("action")]
        public class WholeCalendarAction : EventArgs
        {
            [MySerialized(Name = "respStatus", ContentLevel = MemberValuePosition.Element)]
            public int RespStatus { get; set; }
            [MySerialized(Name = "calendar", ContentLevel = MemberValuePosition.NestedElementsList, NestedElementItemsName = "day")]
            public List<CalendarDayInfo> Matches { get; set; }
        }

        [MySerializable("action")]
        public class DaynewsAction : EventArgs
        {
            [MySerialized(Name = "respStatus", ContentLevel = MemberValuePosition.Element)]
            public int RespStatus { get; set; }
            [MySerialized(Name = "daynews", ContentLevel = MemberValuePosition.Element)]
            public DayNewsInfo DayNews { get; set; }
        }

        [MySerializable("action")]
        public class TeamsAction : EventArgs
        {
            [MySerialized(Name = "respStatus", ContentLevel = MemberValuePosition.Element)]
            public int RespStatus { get; set; }
            [MySerialized(Name = "teams", ContentLevel = MemberValuePosition.Element)]
            public TeamsList Teams { get; set; }
        }

        [MySerializable("action")]
        public class MatchAction : EventArgs
        {
            [MySerialized(Name = "respStatus", ContentLevel = MemberValuePosition.Element)]
            public int RespStatus { get; set; }
            [MySerialized]
            public MatchInfoParams MatchInfo { get; set; }
            [MySerialized(Name = "match", ContentLevel = MemberValuePosition.Element)]
            public List<MatchInfo> Matches { get; set; }
        }

        [MySerializable("action")]
        public class AllMatchAction : EventArgs
        {
            [MySerialized(Name = "respStatus", ContentLevel = MemberValuePosition.Element)]
            public int RespStatus { get; set; }
            [MySerialized]
            public MatchInfoParams MatchInfo { get; set; }
            [MySerialized(Name = "matches", ContentLevel = MemberValuePosition.Element)]
            public DayInfo Matches { get; set; }
        }

        [MySerializable("action")]
        public class VoDAction : EventArgs
        {
            [MySerialized(Name = "respStatus", ContentLevel = MemberValuePosition.Element)]
            public int RespStatus { get; set; }
            [MySerialized(Name = "videos", ContentLevel = MemberValuePosition.Element)]
            public List<VideoInfoList> Videos { get; set; }
        }

        [MySerializable("action")]
        public class PushSettingsAction : EventArgs
        {
            [MySerialized(Name = "respStatus", ContentLevel = MemberValuePosition.Element)]
            public int RespStatus { get; set; }
            [MySerialized(Name = "message", ContentLevel = MemberValuePosition.Element)]
            public string Message { get; set; }
            [MySerialized(Name = "channel", ContentLevel = MemberValuePosition.Element)]
            public PushSettingsInfos Infos { get; set; }
        }

        [MySerializable("param")]
        public class ParamInfo
        {
            [MySerialized(Name = "value", ContentLevel = MemberValuePosition.Attribute)]
            public int Value { get; set; }
            [MySerialized(Name = "name", ContentLevel = MemberValuePosition.Attribute)]
            public string Name { get; set; }
        }

        [MySerializable("params")]
        public class InfoParams
        {
            [MySerialized(Name = "param", ContentLevel = MemberValuePosition.Element)]
            public List<ParamInfo> Param { get; set; }
        }

        [MySerializable("params")]
        public class MatchInfoParams
        {
            [MySerialized(Name = "param", ContentLevel = MemberValuePosition.Element)]
            public List<ParamInfo> Param { get; set; }
        }

        [MySerializable("calendar")]
        public class InfoCalendar
        {
            [MySerialized(Name = "day", ContentLevel = MemberValuePosition.Element)]
            public CalendarDayInfo Param { get; set; }
        }

        [MySerializable("date")]
        public class DateActionInfo
        {
            [MySerialized(Name = "iso", ContentLevel = MemberValuePosition.Element)]
            public string Iso { get; set; }
            [MySerialized(Name = "day", ContentLevel = MemberValuePosition.Element)]
            public string Day { get; set; }
            [MySerialized(Name = "long", ContentLevel = MemberValuePosition.Element)]
            public string Long { get; set; }
        }

        [MySerializable("update")]
        public class UpdateActionInfo
        {
            [MySerialized(Name = "freqR", ContentLevel = MemberValuePosition.Attribute)]
            public int FreqR { get; set; }
            [MySerialized(Name = "freqL", ContentLevel = MemberValuePosition.Attribute)]
            public int FreqL { get; set; }
            [MySerialized(Name = "freqLive", ContentLevel = MemberValuePosition.Attribute)]
            public int FreqLive { get; set; }
        }

        [MySerializable("version")]
        public class VersionActionInfo
        {
            [MySerialized(Name = "actionType", ContentLevel = MemberValuePosition.Element)]
            public int Status { get; set; }
            [MySerialized(Name = "url", ContentLevel = MemberValuePosition.Element)]
            public string Url { get; set; }
            [MySerialized(Name = "message", ContentLevel = MemberValuePosition.Element)]
            public string Message { get; set; }
        }


        /// <summary>
        /// Enumerates the possible version actions returned by the Orange Ligue 1 enabler.
        /// </summary>
        public enum VersionStatus
        {
            None = 0,
            MandatoryUpdate = 1,
            OptionalUpdate = 2,
            EndOfLife = 3,
            Message = 4,
        };

        /// <summary>
        /// Enumerates the possible status of L1 option
        /// </summary>
        public enum L1StatusOption
        {
            CantSubscribe = 0,
            CanSubscribe = 1,
            OptionSubscribed = 2,
            SubscribeInProgress = 3,
            OptionOffered = 4,
        };

        /// <summary>
        /// Enumerates the possible status of L1 day pass
        /// </summary>
        public enum L1StatusDayPass
        {
            CantSubscribe = 0,
            CanSubscribe = 1,
            OptionActivated = 2,
            SubscribeInProgress = 3,
            OptionOffered = 4,
            SecondarySim = 5,
            PassSubscribed = 6,
        };

        /// <summary>
        /// Enumerates the status of 4G upgrade plan
        /// </summary>
        public enum UpgradePlanStatus
        {
            DefaultState = 0,
            UpgradedState = 1,
        };

        [MySerializable("options")]
        public class OptionsActionInfo
        {
            [MySerialized(Name = "level", ContentLevel = MemberValuePosition.Attribute)]
            public int Level { get; set; }
            [MySerialized]
            public OptionsMessageInfo Message { get; set; }
            [MySerialized(Name = "option")]
            public List<OptionActionInfoItem> Options { get; set; }
            [MySerialized(Name = "pass")]
            public OptionActionInfoItem Pass { get; set; }
        }

        [MySerializable("message")]
        public class OptionsMessageInfo
        {
            [MySerialized(Name = "time", ContentLevel = MemberValuePosition.Attribute)]
            public string Time { get; set; }
            [MySerialized(ContentLevel = MemberValuePosition.Content)]
            public string Message { get; set; }
        }

        [MySerializable("option")]
        public class OptionActionInfoItem
        {
            [MySerialized(Name = "optcode", ContentLevel = MemberValuePosition.Attribute)]
            public string OptCode { get; set; }
            [MySerialized(Name = "shortDesc", ContentLevel = MemberValuePosition.Element)]
            public string ShortDesc { get; set; }
            [MySerialized(Name = "longDesc", ContentLevel = MemberValuePosition.Element)]
            public string LongDesc { get; set; }
            [MySerialized(Name = "price", ContentLevel = MemberValuePosition.Element)]
            public string Price { get; set; }
        }

        [MySerializable("season")]
        public class SeasonActionInfo
        {
            [MySerialized(Name = "begin", ContentLevel = MemberValuePosition.Attribute)]
            public string Begin { get; set; }
            [MySerialized(Name = "end", ContentLevel = MemberValuePosition.Attribute)]
            public string End { get; set; }
            [MySerialized(Name = "logo", ContentLevel = MemberValuePosition.Attribute)]
            public string Logo { get; set; }
        }

        [MySerializable("video")]
        public class VideoInfo
        {
            [MySerialized(Name = "id", ContentLevel = MemberValuePosition.Attribute)]
            public int id { get; set; }
            [MySerialized(Name = "type", ContentLevel = MemberValuePosition.Attribute)]
            public string Type { get; set; }
            [MySerialized(Name = "title", ContentLevel = MemberValuePosition.Attribute)]
            public string Title { get; set; }
            [MySerialized(Name = "image", ContentLevel = MemberValuePosition.Element)]
            public string Image { get; set; }
            [MySerialized(Name = "url", ContentLevel = MemberValuePosition.Element)]
            public string Link { get; set; }
            [MySerialized(Name = "url_wifi", ContentLevel = MemberValuePosition.Element)]
            public string LinkWifi { get; set; }
        }

        [MySerializable("videos")]
        public class VideoInfoList
        {
            [MySerialized(Name = "day", ContentLevel = MemberValuePosition.Attribute)]
            public int Day { get; set; }
            [MySerialized(Name = "video", ContentLevel = MemberValuePosition.Element)]
            public List<VideoInfo> Videos { get; set; }

            public VideoInfoList()
            {
                Day = 0;
            }
        }

        [MySerializable("stats")]
        public class MatchStatsInfo
        {
            [MySerialized(Name = "possession", ContentLevel = MemberValuePosition.Attribute)]
            public double PossessionPercentage { get; set; }
            [MySerialized(Name = "corner", ContentLevel = MemberValuePosition.Attribute)]
            public int Corners { get; set; }
            [MySerialized(Name = "foul", ContentLevel = MemberValuePosition.Attribute)]
            public int Fouls { get; set; }
            [MySerialized(Name = "shotin", ContentLevel = MemberValuePosition.Attribute)]
            public int ShotsIn { get; set; }
            [MySerialized(Name = "shotout", ContentLevel = MemberValuePosition.Attribute)]
            public int ShotsOut { get; set; }
            [MySerialized(Name = "offside", ContentLevel = MemberValuePosition.Attribute)]
            public int OffSides { get; set; }
            [MySerialized(Name = "freekick", ContentLevel = MemberValuePosition.Attribute)]
            public int FreeKicks { get; set; }
            [MySerialized(Name = "yelcard", ContentLevel = MemberValuePosition.Attribute)]
            public int YellowCards { get; set; }
            [MySerialized(Name = "redcard", ContentLevel = MemberValuePosition.Attribute)]
            public int RedCards { get; set; }
        }

        [MySerializable("event")]
        public class MatchEventInfo
        {
            [MySerialized(Name = "id", ContentLevel = MemberValuePosition.Attribute)]
            public int Id { get; set; }
            [MySerialized(Name = "type", ContentLevel = MemberValuePosition.Attribute)]
            public int Type { get; set; }
            [MySerialized(Name = "minute", ContentLevel = MemberValuePosition.Attribute)]
            public int Minute { get; set; }
            [MySerialized]
            public InfoEventInfo Info { get; set; }
            [MySerialized]
            public VideoInfo Video { get; set; }
        }

        [MySerializable("element")]
        public class WordingInfo
        {
            [MySerialized(Name = "key", ContentLevel = MemberValuePosition.Attribute)]
            public string Key { get; set; }
            [MySerialized(Name = "value", ContentLevel = MemberValuePosition.Element)]
            public string Value { get; set; }
        }

        [MySerializable("info")]
        public class InfoEventInfo
        {
            [MySerialized(Name = "teamId", ContentLevel = MemberValuePosition.Attribute)]
            public int TeamId { get; set; }
            [MySerialized(Name = "player", ContentLevel = MemberValuePosition.Attribute)]
            public string Player { get; set; }
            [MySerialized(Name = "type", ContentLevel = MemberValuePosition.Attribute)]
            public string Type { get; set; }
            [MySerialized(ContentLevel = MemberValuePosition.Content)]
            public string Text { get; set; }
        }

        public enum EventType
        {
            Comment = 0,
            Goal = 1,
            RedCard = 2,
            Status = 3,
            Video = 4
        }

        /// <summary>
        /// Class representing the Match element of the Ligue1 Webservice
        /// </summary>
        [MySerializable("match")]
        public class MatchInfo
        {
            [MySerialized(Name = "id", ContentLevel = MemberValuePosition.Attribute)]
            public int Id { get; set; }
            [MySerialized(Name = "status", ContentLevel = MemberValuePosition.Attribute)]
            public int Status { get; set; }
            /*[MySerialized(Name = "live", ContentLevel = MemberValuePosition.Attribute)]
            public int Live { get; set; }*/
            [MySerialized(Name = "date", ContentLevel = MemberValuePosition.Attribute)]
            public string Date { get; set; }
            [MySerialized(Name = "hour", ContentLevel = MemberValuePosition.Attribute)]
            public string Hour { get; set; }
            [MySerialized(Name = "url", ContentLevel = MemberValuePosition.Attribute)]
            public string Url { get; set; }
            [MySerialized]
            public TeamsMatchInfo Teams { get; set; }
            [MySerialized(Name = "events", ContentLevel = MemberValuePosition.NestedElementsList, NestedElementItemsName = "event")]
            public List<MatchEventInfo> Events { get; set; }
            [MySerialized]
            public VideoInfoList Videos { get; set; }
        }

        [MySerializable("teams")]
        public class TeamsMatchInfo
        {
            [MySerialized]
            public TeamMatchInfo LocalTeam { get; set; }
            [MySerialized(Name = "visitorTeam", ContentLevel = MemberValuePosition.Element)]
            public TeamMatchInfo VisitorTeam { get; set; }
        }

        [MySerializable("localTeam")]
        public class TeamMatchInfo
        {
            [MySerialized(Name = "id", ContentLevel = MemberValuePosition.Attribute)]
            public int Id { get; set; }
            [MySerialized(Name = "score", ContentLevel = MemberValuePosition.Attribute)]
            public int Score { get; set; }
            [MySerialized]
            public MatchStatsInfo Stats { get; set; }

        }

        [MySerializable("stats")]
        public class TeamStatsInfo
        {
            [MySerialized(Name = "points", ContentLevel = MemberValuePosition.Attribute)]
            public int Points { get; set; }
            [MySerialized(Name = "mp", ContentLevel = MemberValuePosition.Attribute)]
            public int PlayedMatchs { get; set; }
            [MySerialized(Name = "mw", ContentLevel = MemberValuePosition.Attribute)]
            public int WonMatchs { get; set; }
            [MySerialized(Name = "md", ContentLevel = MemberValuePosition.Attribute)]
            public int DrawMatchs { get; set; }
            [MySerialized(Name = "ml", ContentLevel = MemberValuePosition.Attribute)]
            public int LostMatchs { get; set; }
            [MySerialized(Name = "gs", ContentLevel = MemberValuePosition.Attribute)]
            public int ScoredGoals { get; set; }
            [MySerialized(Name = "gc", ContentLevel = MemberValuePosition.Attribute)]
            public int ConcededGoals { get; set; }
            [MySerialized(Name = "gd", ContentLevel = MemberValuePosition.Attribute)]
            public int GoalsDelta { get; set; }
        }

        [MySerializable("teams")]
        public class TeamsList
        {
            [MySerialized(Name = "team", ContentLevel = MemberValuePosition.Element)]
            public List<TeamInfo> Teams { get; set; }
            [MySerialized(Name = "logo_base_url", ContentLevel = MemberValuePosition.Attribute)]
            public string LogoBaseUrl { get; set; }
            [MySerialized(Name = "logobasewifi", ContentLevel = MemberValuePosition.Attribute)]
            public string LogoBaseWifiUrl { get; set; }
        }

        [MySerializable("team")]
        public class TeamInfo
        {
            [MySerialized(Name = "id", ContentLevel = MemberValuePosition.Attribute)]
            public int Id { get; set; }
            [MySerialized(Name = "short_name", ContentLevel = MemberValuePosition.Attribute)]
            public string ShortName { get; set; }
            [MySerialized(Name = "name", ContentLevel = MemberValuePosition.Attribute)]
            public string Name { get; set; }
            [MySerialized(Name = "city", ContentLevel = MemberValuePosition.Attribute)]
            public string City { get; set; }
            [MySerialized(Name = "rank", ContentLevel = MemberValuePosition.Attribute)]
            public int Rank { get; set; }
            [MySerialized(Name = "logos", ContentLevel = MemberValuePosition.NestedElementsList, NestedElementItemsName = "pict")]
            public List<LogoInfo> Logos { get; set; }
            [MySerialized(Name = "stats", ContentLevel = MemberValuePosition.Attribute)]
            public TeamStatsInfo Stats { get; set; }
        }

        [MySerializable("logo")]
        public class LogoInfo
        {
            [MySerialized(Name = "name", ContentLevel = MemberValuePosition.Attribute)]
            public string Name { get; set; }
            [MySerialized(Name = "width", ContentLevel = MemberValuePosition.Attribute)]
            public int Width { get; set; }
            [MySerialized(Name = "height", ContentLevel = MemberValuePosition.Attribute)]
            public int Height { get; set; }
            [MySerialized(Name = "ext", ContentLevel = MemberValuePosition.Attribute)]
            public string Ext { get; set; }
        }

        [MySerializable("news")]
        public class NewsInfo
        {
            [MySerialized(Name = "id", ContentLevel = MemberValuePosition.Attribute)]
            public int Id { get; set; }
            [MySerialized(Name = "date", ContentLevel = MemberValuePosition.Attribute)]
            public string Date { get; set; }
            [MySerialized(Name = "time", ContentLevel = MemberValuePosition.Attribute)]
            public string Hour { get; set; }
            [MySerialized(Name = "title", ContentLevel = MemberValuePosition.Attribute)]
            public String Title { get; set; }
            [MySerialized(Name = "text", ContentLevel = MemberValuePosition.Content)]
            public String Text { get; set; }
        }

        [MySerializable("daynews")]
        public class DayNewsInfo
        {
            //saves daynews_id
            [MySerialized(Name = "params", ContentLevel = MemberValuePosition.Element)]
            public ParamInfo Params { get; set; }
            [MySerialized(Name = "mag", ContentLevel = MemberValuePosition.Attribute)]
            public string MagUrl { get; set; }
            [MySerialized(Name = "mag_wifi", ContentLevel = MemberValuePosition.Attribute)]
            public string MagWifiUrl { get; set; }
            [MySerialized(Name = "news")]
            public List<NewsInfo> NewsList { get; set; }
        }

        /// <summary>
        /// List of catalog options
        /// </summary>
        [MySerializable("option")]
        public class OptionInfo
        {
            // The Option Id
            [MySerialized(Name = "id", ContentLevel = MemberValuePosition.Element)]
            public string Id { get; set; }
            [MySerialized(Name = "name", ContentLevel = MemberValuePosition.Element)]
            public string Name { get; set; }
            [MySerialized(Name = "desc", ContentLevel = MemberValuePosition.Element)]
            public string Desc { get; set; }
            [MySerialized(Name = "price", ContentLevel = MemberValuePosition.Element)]
            public string Price { get; set; }
            [MySerialized(Name = "legal", ContentLevel = MemberValuePosition.Element)]
            public string Legal { get; set; }
        }

        /// <summary>
        /// List of catalog options
        /// </summary>
        [MySerializable("pass")]
        public class PassInfo
        {
            // The Option Id
            [MySerialized(Name = "id", ContentLevel = MemberValuePosition.Element)]
            public string Id { get; set; }
            [MySerialized(Name = "name", ContentLevel = MemberValuePosition.Element)]
            public string Name { get; set; }
            [MySerialized(Name = "desc", ContentLevel = MemberValuePosition.Element)]
            public string Desc { get; set; }
            [MySerialized(Name = "price", ContentLevel = MemberValuePosition.Element)]
            public string Price { get; set; }
            [MySerialized(Name = "legal", ContentLevel = MemberValuePosition.Element)]
            public string Legal { get; set; }
        }

        /// <summary>
        /// List of match in a Calendar day
        /// </summary>
        [MySerializable("day")]
        public class CalendarDayInfo
        {
            // The day number
            [MySerialized(Name = "id", ContentLevel = MemberValuePosition.Attribute)]
            public int Day { get; set; }
            [MySerialized(Name = "match", ContentLevel = MemberValuePosition.Element)]
            public List<MatchInfo> Matches { get; set; }
        }

        /// <summary>
        /// List of match in a Day
        /// </summary>
        [MySerializable("matches")]
        public class DayInfo
        {
            /// <summary>
            /// The day number
            /// </summary>
            [MySerialized(Name = "day", ContentLevel = MemberValuePosition.Attribute)]
            public int Day { get; set; }
            [MySerialized(Name = "match", ContentLevel = MemberValuePosition.Element)]
            public List<MatchInfo> Matches { get; set; }
        }

        [MySerializable("channel")]
        public class PushSettingsInfos
        {
            [MySerialized(Name = "type", ContentLevel = MemberValuePosition.Attribute)]
            public string type { get; set; }
            [MySerialized(Name = "action_id", ContentLevel = MemberValuePosition.Attribute)]
            public int action { get; set; }
            [MySerialized(Name = "mid", ContentLevel = MemberValuePosition.Attribute)]
            public string mid { get; set; }
            [MySerialized(Name = "teams_id", ContentLevel = MemberValuePosition.Attribute)]
            public string teams { get; set; }
            [MySerialized(Name = "token", ContentLevel = MemberValuePosition.Attribute)]
            public string token { get; set; }
        }
    }
    #endregion
}

