// -------------------------------  Copyright ---------------------------------
//	This software has been developped by France Telecom, FT/RD/RD/MAPS/DVC/MSF
//
//	Copyright (c) France Telecom 2009-2010
//
// COPYRIGHT    : This file is the property of FRANCE TELECOM.
//                It cannot be copied, used, or modified without obtaining
//                an authorization from the authors or a mandated
//                member of FRANCE TELECOM.
//                If such an authorization is provided, any modified version or
//                copy of the software has to contain this header.
//
// WARRANTIES   : This software is made available by the authors in the  hope
//                that it will be useful, but without any warranty.
//                France Telecom is not liable for any consequence related to the
//                use of the provided software.
//
// AUTHORS      : France Telecom  / RD / MAPS / DVC / MSF
// ----------------------------------------------------------------------------
using System;
using System.Net;
using System.Collections.Generic;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using System.Xml;
using System.Linq;

namespace Orange.SDK.Communication
{
    /// <summary>
    /// WebServiceParameters holds all the informations of a webservice client > server transaction
    /// </summary>
    public class WebServiceParameters
    {
        private Dictionary<string, string> m_http_parameters = new Dictionary<string,string>();
        public Uri BaseUri { get; set; }
        public Uri FullUri
        {
            get
            {
                string paramstring = ParametersString();
                string format;
                //if no parameters => different Uri  formatting
                if (paramstring == String.Empty)
                {
                    format = "{0}";
                }
                else
                {
                    format = "{0}?{1}";
                }
                return new Uri(String.Format(format,BaseUri.ToString(),paramstring));
            }
        }
        public string Content {get; set;}

        public WebServiceParameters(Uri baseuri)
        {
            BaseUri = baseuri;
        }

        public WebServiceParameters(string baseuri)
        {
            BaseUri = new Uri(baseuri);
        }

        public void AddUriParameter(string paramname, string value)
        {
            m_http_parameters.Add(paramname,value);
        }

        public bool RemoveUriParameter(string paramname)
        {
            return m_http_parameters.Remove(paramname);
        }

        private string ParametersString()
        {
            string ret = "";
            foreach (string key in m_http_parameters.Keys)
            {
                ret += String.Format("{0}={1}&",Uri.EscapeDataString(key),Uri.EscapeDataString(m_http_parameters[key]));
            }
            ret = ret.TrimEnd('&');
            return ret;
        }

        public WebServiceParameters Copy()
        {
            WebServiceParameters ret = new WebServiceParameters(this.BaseUri);
            foreach (string key in m_http_parameters.Keys)
            {
                ret.AddUriParameter(key, m_http_parameters[key]);
            }
            ret.Content = Content;
            return ret;
        }
        

    }
}

