// -------------------------------  Copyright ---------------------------------
//	This software has been developped by France Telecom, FT/RD/RD/MAPS/DVC/MSF
//
//	Copyright (c) France Telecom 2009-2010
//
// COPYRIGHT    : This file is the property of FRANCE TELECOM.
//                It cannot be copied, used, or modified without obtaining
//                an authorization from the authors or a mandated
//                member of FRANCE TELECOM.
//                If such an authorization is provided, any modified version or
//                copy of the software has to contain this header.
//
// WARRANTIES   : This software is made available by the authors in the  hope
//                that it will be useful, but without any warranty.
//                France Telecom is not liable for any consequence related to the
//                use of the provided software.
//
// AUTHORS      : France Telecom  / RD / MAPS / DVC / MSF
// ----------------------------------------------------------------------------
using System;
using System.Net;
using System.Collections.Generic;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using System.Xml;
using System.IO;

namespace Orange.SDK.Communication
{
    /// <summary>
    /// WebserviceRequest class
    /// Initiate a webservice call via the Webclient Object
    /// Implements IWebServiceRequest
    /// </summary>
    public class WebServiceRequest : IWebServiceRequest
    {

        protected WebClient m_web_client = new WebClient();
        protected WebServiceParameters m_param;
        
        /// <summary>
        /// Sets up event handlers
        /// </summary>
        public WebServiceRequest()
        {
            m_web_client.UploadStringCompleted += new UploadStringCompletedEventHandler(m_web_client_UploadStringCompleted);
            m_web_client.DownloadStringCompleted += new DownloadStringCompletedEventHandler(m_web_client_DownloadStringCompleted);
            m_web_client.Encoding = System.Text.Encoding.GetEncoding("ISO-8859-1");
        }

        #region "Private Methods"
        void m_web_client_DownloadStringCompleted(object sender, DownloadStringCompletedEventArgs e)
        {
            if (e.Cancelled)
            {
                CompletedHandler((WebException)e.Error, string.Empty, e.Cancelled);
            }
            else
            {
                if (e.Error == null)
                {
                    CompletedHandler((WebException)e.Error, e.Result, e.Cancelled);
                }
                else
                    CompletedHandler((WebException)e.Error, null, e.Cancelled);
                
            }
            
        }

        void m_web_client_UploadStringCompleted(object sender, UploadStringCompletedEventArgs e)
        {
            if (e.Cancelled)
            {
                CompletedHandler((WebException)e.Error, string.Empty, e.Cancelled);
            }
            else
            {
                if (e.Error == null)
                {
                    CompletedHandler((WebException)e.Error, e.Result, e.Cancelled);
                }
                else
                    CompletedHandler((WebException)e.Error, null, e.Cancelled);
            }            
        }

        /// <summary>
        /// Generic function to handle the *StringCompleted Event
        /// raise the Success event if no error
        /// else raise the Error event
        /// </summary>
        /// <param name="error">the error argument of a *StringCompletedEventArgs</param>
        /// <param name="result">the result argument of a *StringCompletedEventArgs</param>
        /// <param name="cancelled">The cancellation status of the request</param>
        void CompletedHandler(WebException error, string result, bool cancelled)
        {
            if (!cancelled)
            {
                if (error != null)
                {
                    HttpStatusCode err = ((HttpWebResponse)error.Response).StatusCode;
                    Error(this, new RequestErrorEventArgs(err) { StatusCode = err, Params = m_param});
                }
                else
                {

                    try
                    {
                        byte[] isoBytes = System.Text.Encoding.GetEncoding("iso-8859-1").GetBytes(result);

                        byte[] utf8Bytes = System.Text.Encoding.Convert(System.Text.Encoding.GetEncoding("iso-8859-1"), System.Text.Encoding.UTF8, isoBytes);

                        string myString = Convert.ToBase64String(utf8Bytes);

                        using (StringReader sr = new StringReader(result))
                        {
                            Success(sr);
                        }
                        
                    }
                    catch (Exception)
                    {
                        Error(this, new RequestErrorEventArgs() { Params = m_param});
                    }
                }
                m_param = null;
            }
            else if(m_param != null)
            {
                _Request(m_param);
            }

        }
        #endregion

        #region "IWebServiceRequest Implementation"
        /// <summary>
        /// Sends a request with the given WebServiceParameter or enqueue if there an already going request.
        /// </summary>
        /// <remarks>Use a POST method if the parameter as a content.
        /// If no content in parameter it uses a GET method.</remarks>
        /// <param name="param"></param>
        public void Request(WebServiceParameters param)
        {
            if (m_web_client.IsBusy)
            {
                m_web_client.CancelAsync();
                m_param = param;
            }
            else
                _Request(param);
        }

        /// <summary>
        /// Private function that actually sends the request
        /// </summary>
        /// <param name="param">The parameters to send in the requests.</param>
        private void _Request(WebServiceParameters param)
        {
            if (param.Content != null && param.Content != String.Empty)
                m_web_client.UploadStringAsync(param.FullUri, "POST", param.Content);
            else
                m_web_client.DownloadStringAsync(param.FullUri);
        }


        public event RequestErrorDelegate Error;
        public event RequestSuccesDelegate Success;
        #endregion
    }
}

