// -------------------------------  Copyright ---------------------------------
//	This software has been developped by France Telecom, FT/RD/RD/MAPS/DVC/MSF
//
//	Copyright (c) France Telecom 2009-2010
//
// COPYRIGHT    : This file is the property of FRANCE TELECOM.
//                It cannot be copied, used, or modified without obtaining
//                an authorization from the authors or a mandated
//                member of FRANCE TELECOM.
//                If such an authorization is provided, any modified version or
//                copy of the software has to contain this header.
//
// WARRANTIES   : This software is made available by the authors in the  hope
//                that it will be useful, but without any warranty.
//                France Telecom is not liable for any consequence related to the
//                use of the provided software.
//
// AUTHORS      : France Telecom  / RD / MAPS / DVC / MSF
// ----------------------------------------------------------------------------
using System;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using System.IO;

namespace Orange.SDK.Communication
{
    public interface IWebServiceRequest
    {
        void Request(WebServiceParameters param);
        event RequestErrorDelegate Error;
        event RequestSuccesDelegate Success;
    }

    public delegate void RequestSuccesDelegate(StringReader sr);
    public delegate void RequestErrorDelegate(Object sender, RequestErrorEventArgs e);

    public class RequestErrorEventArgs : EventArgs
    {
        public HttpStatusCode StatusCode { get; set; }
        public string StatusDescription { get; set; }
        public WebServiceParameters Params { get; set; }

        public RequestErrorEventArgs()
            : base()
        {
        }
        public RequestErrorEventArgs(HttpStatusCode code)
            : this()
        {
            StatusCode = code;
            StatusDescription = code.ToString();
        }

    }
    
}

