// -------------------------------  Copyright ---------------------------------
//	This software has been developped by France Telecom, FT/RD/RD/MAPS/DVC/MSF
//
//	Copyright (c) France Telecom 2009-2010
//
// COPYRIGHT    : This file is the property of FRANCE TELECOM.
//                It cannot be copied, used, or modified without obtaining
//                an authorization from the authors or a mandated
//                member of FRANCE TELECOM.
//                If such an authorization is provided, any modified version or
//                copy of the software has to contain this header.
//
// WARRANTIES   : This software is made available by the authors in the  hope
//                that it will be useful, but without any warranty.
//                France Telecom is not liable for any consequence related to the
//                use of the provided software.
//
// AUTHORS      : France Telecom  / RD / MAPS / DVC / MSF
// ----------------------------------------------------------------------------
using System;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;

using System.Reflection;

namespace Orange.SDK
{
    public class TypeHelper
    {
        /// <summary>
        /// Instantiate an object of the type given in argument
        /// </summary>
        /// <remarks>The class must provide a default constructor with no argument</remarks>
        /// <param name="objType">Type of the object to instantiate</param>
        /// <returns>An instance of the 'objType' class</returns>
        public static Object Instantiate(Type objType)
        {
            ConstructorInfo ctor = objType.GetConstructor(new Type[] { });
            if (ctor == null)
            {
                throw new TypeHelperException("Unable to find a default constructor for this type");
            }
            return ctor.Invoke(null);
        }

        public class TypeHelperException : Exception
        {
            public TypeHelperException(string message) : base(message)
            {
            }
        }

        /// <summary>
        /// Extract the T's Type attribute from a MemberInfo (any object, property, method ...)
        /// </summary>
        /// <typeparam name="T">The Type of the attribute to extract</typeparam>
        /// <param name="objType">The Member which to extract the attribute</param>
        /// <returns>the attribute instance or null if member does not have this attribute</returns>
        public static T GetAttribute<T>(MemberInfo objType) where T : Attribute
        {
            T ret = null;
            object[] attArray = objType.GetCustomAttributes(typeof(T), false);
            if (attArray != null && attArray.Length > 0)
            {
                ret = (T)attArray[0];
            }
            return ret;
        }

        /// <summary>
        /// Extract all the instance of the T attributes from a memberinfo (any object, property, method ...)
        /// </summary>
        /// <typeparam name="T">The Type of the attribute to extract</typeparam>
        /// <param name="objType">The Member which to extract the attribute</param>
        /// <returns>the attributes instance array or null if member does not have this attribute</returns>
        public static T[] GetAttributes<T>(MemberInfo objType) where T : Attribute
        {
            return (T[])objType.GetCustomAttributes(typeof(T), false);
        }


        /// <summary>
        /// Return true if targetType implements the interfaceType interface
        /// </summary>
        /// <param name="targetType">The type to introspect</param>
        /// <param name="interfaceType">The Interface type to look for</param>
        /// <returns>true if type presents the interface, false otherwise</returns>
        public static bool Implement(Type targetType,Type interfaceType)
        {
            Type[] types = targetType.GetInterfaces();
            foreach (Type t in types)
            {
                if (t == interfaceType)
                {
                    return true;
                }
            }
            return false;
        }

        public static bool InheritsFrom(Type target, Type parentClass)
        {
            bool ret = false;
            Type parentType = target;
            //while (parentType != typeof(System.Object))
            //{

            //}
            return ret;
        }
    }
}

