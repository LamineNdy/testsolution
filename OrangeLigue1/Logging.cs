// -------------------------------  Copyright ---------------------------------
//	This software has been developped by France Telecom, FT/RD/RD/MAPS/DVC/MSF
//
//	Copyright (c) France Telecom 2009-2010
//
// COPYRIGHT    : This file is the property of FRANCE TELECOM.
//                It cannot be copied, used, or modified without obtaining
//                an authorization from the authors or a mandated
//                member of FRANCE TELECOM.
//                If such an authorization is provided, any modified version or
//                copy of the software has to contain this header.
//
// WARRANTIES   : This software is made available by the authors in the  hope
//                that it will be useful, but without any warranty.
//                France Telecom is not liable for any consequence related to the
//                use of the provided software.
//
// AUTHORS      : France Telecom  / RD / MAPS / DVC / MSF
// ----------------------------------------------------------------------------
using System;
using System.Diagnostics;
using System.Globalization;
using System.IO;
using System.IO.IsolatedStorage;
using System.Net;
using System.Reflection;
using System.Text;

namespace OrangeLigue1
{
    /// <summary>
    /// The entry point for logging operations.
    /// </summary>
    public class Logger
    {
        private static ILogger _actualLogger = null;
        private static readonly object _lock = new object();

        /// <summary>
        /// The actual <see cref="ILogger"/> instance used to log message.
        /// </summary>
        public static ILogger ActualLogger
        {
            get
            {
                lock (_lock)
                {
                    return _actualLogger;
                }
            }
            set
            {
                lock (_lock)
                {
                    _actualLogger = value;
                }
            }
        }

        /// <summary>
        /// Logs the specified message.
        /// </summary>
        public static void Log(string format, params object[] args)
        {
            DateTime timestamp = DateTime.Now;

            ILogger logger;
            lock (_lock)
            {
                logger = _actualLogger;
            }
            if (logger == null)
            {
                return;
            }

            try
            {
                logger.Log(timestamp, format, args);
            }
            catch (Exception error)
            {
                if (Debugger.IsAttached)
                {
                    Debug.WriteLine("*** Error while logging: {0}", error);
                }
            }
        }

        /// <summary>
        /// Tries to decode the specified byte array as a string using common encodings,
        /// and logs this string is decoding is successful.
        /// </summary>
        public static void LogEncodedString(byte[] encodedData)
        {
            DateTime timestamp = DateTime.Now;

            ILogger logger;
            lock (_lock)
            {
                logger = _actualLogger;
            }
            if (logger == null)
            {
                return;
            }

            try
            {
                if (encodedData == null || encodedData.Length == 0)
                {
                    return;
                }
                string str = null;
                try
                {
                    str = Encoding.UTF8.GetString(encodedData, 0, encodedData.Length);
                }
                catch (Exception) { }
                if (str == null)
                {
                    try
                    {
                        str = Encoding.GetEncoding("ISO-8859-1").GetString(encodedData, 0, encodedData.Length);
                    }
                    catch (Exception) { }
                }
                if (str == null)
                {
                    return;
                }
                string[] lines = str.Split('\n');
                foreach (string line in lines)
                {
                    logger.Log(timestamp, line.Replace("\r", ""));
                }
            }
            catch (Exception) { }
        }

        /// <summary>
        /// Logs the specified exception with specified optional message.
        /// </summary>
        public static void LogException(Exception error, string message, params object[] args)
        {
            DateTime timestamp = DateTime.Now;
            if (error == null)
            {
                return;
            }

            StringBuilder exceptionMessage = new StringBuilder();
            try
            {
                exceptionMessage.Append(error.GetType().Name);
                if (!string.IsNullOrEmpty(error.Message))
                {
                    exceptionMessage.AppendFormat(": {0}", error.Message);
                }
                if (error is WebException)
                {
                    WebException webError = (WebException)error;
                    if (webError.Response != null && webError.Response is HttpWebResponse)
                    {
                        exceptionMessage.AppendFormat(" (HTTP status: {0})", ((HttpWebResponse)webError.Response).StatusCode);
                    }
                }
            }
            catch (Exception) { }

            ILogger logger;
            lock (_lock)
            {
                logger = _actualLogger;
            }
            if (logger == null)
            {
                return;
            }

            try
            {
                if (!string.IsNullOrEmpty(message))
                {
                    logger.Log(timestamp, message, args);
                }
                logger.Log(timestamp, exceptionMessage.ToString());
            }
            catch (Exception logError)
            {
                if (Debugger.IsAttached)
                {
                    Debug.WriteLine("*** Error while logging: {0}", logError);
                }
            }
        }

        /// <summary>
        /// Writes the specified message to the debugger.
        /// </summary>
        public static void DebugTrace(string format, params object[] args)
        {
            if (App.IsProductionBuild || !Debugger.IsAttached)
            {
                return;
            }
            Debug.WriteLine(string.Format("{0} {1}", FormatTimestamp(DateTime.UtcNow), format), args);
        }

        public static string FormatTimestamp(DateTime timestamp)
        {
            try
            {
                return timestamp.ToUniversalTime().ToString("yyMMdd-HHmmss.fff", CultureInfo.InvariantCulture);
            }
            catch (Exception)
            {
                return "???";
            }
        }
    }

    /// <summary>
    /// The interface implemented by actual loggers.
    /// </summary>
    public interface ILogger
    {
        void Log(DateTime timestamp, string format, params object[] args);
    }

    /// <summary>
    /// Implements a logger that writes log message to the isolated storage.
    /// </summary>
    public class PersistentLogger : ILogger
    {
        private readonly string _logFilePath;
        private readonly object _storeLock = new object();

        /// <summary>
        /// Initializes a new <see cref="PersistentLogger"/> instance.
        /// </summary>
        public PersistentLogger(string logFilePath)
        {
            if (string.IsNullOrEmpty(logFilePath))
            {
                throw new ArgumentException();
            }
            _logFilePath = logFilePath;
        }

        /// <summary>
        /// Implements <see cref="ILogger.Log"/>.
        /// </summary>
        public void Log(DateTime timestamp, string format, params object[] args)
        {
            StringBuilder message = new StringBuilder();
            try
            {
                message.Append(Logger.FormatTimestamp(timestamp));
            }
            catch (Exception)
            {
                message.Append("???");
            }
            message.Append(" ");
            try
            {
                message.AppendFormat(format, args);
            }
            catch (Exception)
            {
                try
                {
                    message.Append(format);
                }
                catch (Exception)
                {
                    message.Append("???");
                }
            }
            if (Debugger.IsAttached)
            {
                Debug.WriteLine(message.ToString());
            }
            try
            {
                lock (_storeLock)
                {
                    using (IsolatedStorageFile store = IsolatedStorageFile.GetUserStoreForApplication())
                    {
                        FileMode fileMode;
                        bool fileWasCreated = false;
                        if (store.FileExists(_logFilePath))
                        {
                            fileMode = FileMode.Append;
                        }
                        else
                        {
                            fileMode = FileMode.CreateNew;
                            fileWasCreated = true;
                        }
                        IsolatedStorageFileStream fileStream = store.OpenFile(_logFilePath, fileMode, FileAccess.Write);
                        using (StreamWriter writer = new StreamWriter(fileStream, Encoding.UTF8))
                        {
                            if (fileWasCreated)
                            {
                                try
                                {
                                    writer.WriteLine(string.Format("Log file for {0}", Assembly.GetExecutingAssembly().FullName));
                                }
                                catch (Exception) { }
                            }
                            writer.WriteLine(message.ToString());
                            writer.Flush();
                        }
                    }
                }
            }
            catch (Exception error)
            {
                if (Debugger.IsAttached)
                {
                    Debug.WriteLine("Error while appending to log file '{0}': {1}", _logFilePath, error);
                }
            }
        }

        /// <summary>
        /// Returns the contents of the log file, or <c>null</c> if the log file is empty.
        /// </summary>
        public string GetAll()
        {
            try
            {
                string contents;
                lock (_storeLock)
                {
                    using (IsolatedStorageFile store = IsolatedStorageFile.GetUserStoreForApplication())
                    {
                        if (store.FileExists(_logFilePath) == false)
                        {
                            return null;
                        }
                        IsolatedStorageFileStream fileStream = store.OpenFile(_logFilePath, FileMode.Open, FileAccess.Read);
                        using (StreamReader reader = new StreamReader(fileStream, Encoding.UTF8))
                        {
                            contents = reader.ReadToEnd();
                        }
                    }
                }
                return contents;
            }
            catch (Exception error)
            {
                if (Debugger.IsAttached)
                {
                    Debug.WriteLine("Error while reading log file '{0}': {1}", _logFilePath, error);
                }
                return null;
            }
        }

        /// <summary>
        /// Clears all the log messages from the isolated storage.
        /// </summary>
        public void ClearAll()
        {
            try
            {
                lock (_storeLock)
                {
                    using (IsolatedStorageFile store = IsolatedStorageFile.GetUserStoreForApplication())
                    {
                        if (store.FileExists(_logFilePath))
                        {
                            store.DeleteFile(_logFilePath);
                        }
                    }
                }
            }
            catch (Exception error)
            {
                if (Debugger.IsAttached)
                {
                    Debug.WriteLine("Error while deleting log file '{0}': {1}", _logFilePath, error);
                }
            }
        }
    }

    /// <summary>
    /// Implements a logger that writes log message to the debugger's output window.
    /// </summary>
    public class DebuggerLogger : ILogger
    {
        public DebuggerLogger()
        {
        }

        public void Log(DateTime timestamp, string format, params object[] args)
        {
            StringBuilder message = new StringBuilder();
            try
            {
                message.Append(Logger.FormatTimestamp(timestamp));
            }
            catch (Exception)
            {
                message.Append("???");
            }
            message.Append(" ");
            try
            {
                message.AppendFormat(format, args);
            }
            catch (Exception)
            {
                try
                {
                    message.Append(format);
                }
                catch (Exception)
                {
                    message.Append("???");
                }
            }
            Debug.WriteLine(message.ToString());
        }
    }
}

