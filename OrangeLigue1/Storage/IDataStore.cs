// -------------------------------  Copyright ---------------------------------
//	This software has been developped by France Telecom, FT/RD/RD/MAPS/DVC/MSF
//
//	Copyright (c) France Telecom 2009-2010
//
// COPYRIGHT    : This file is the property of FRANCE TELECOM.
//                It cannot be copied, used, or modified without obtaining
//                an authorization from the authors or a mandated
//                member of FRANCE TELECOM.
//                If such an authorization is provided, any modified version or
//                copy of the software has to contain this header.
//
// WARRANTIES   : This software is made available by the authors in the  hope
//                that it will be useful, but without any warranty.
//                France Telecom is not liable for any consequence related to the
//                use of the provided software.
//
// AUTHORS      : France Telecom  / RD / MAPS / DVC / MSF
// ----------------------------------------------------------------------------
using System;

namespace OrangeLigue1.Storage
{
    /// <summary>
    /// Defines the interface of data store objects.
    /// </summary>
    public interface IDataStore
    {
        /// <summary>
        /// Saves the object with the specified key to the data store.
        /// If an object with the same key already exists in the data store, it will be overwritten.
        /// </summary>
        /// <param name="key">The key identifying the object to save.</param>
        /// <param name="value">The object to save.</param>
        void Save(string key, object value);

        /// <summary>
        /// Indicates whether the data store contains the object with the specified key.
        /// </summary>
        /// <param name="key">The key identifying the object</param>
        /// <returns><c>true</c> if the data store contains the object with the specified key,
        /// <c>false</c> otherwise.</returns>
        bool Contains(string key);

        /// <summary>
        /// Loads the object with the specified key from the data store.
        /// </summary>
        /// <param name="key">The key identifying the object to load.</param>
        /// <param name="typeOfValue">The expected type of the object to load.</param>
        /// <returns>The loaded object, or <c>null</c> if there is no object for <paramref name="key"/> in the data store.</returns>
        object Load(string key, Type typeOfValue);

        /// <summary>
        /// Deletes the object with the specified key in the data store, if it exists.
        /// </summary>
        /// <param name="key">The key identifying the object to delete.</param>
        void Delete(string key);

        /// <summary>
        /// Deletes all the objects in the data store.
        /// </summary>
        void DeleteAll();

        /// <summary>
        /// Closes the data store, ensuring that all pending operations are correctly completed.
        /// </summary>
        void Close();
    }
}

