// -------------------------------  Copyright ---------------------------------
//	This software has been developped by France Telecom, FT/RD/RD/MAPS/DVC/MSF
//
//	Copyright (c) France Telecom 2009-2010
//
// COPYRIGHT    : This file is the property of FRANCE TELECOM.
//                It cannot be copied, used, or modified without obtaining
//                an authorization from the authors or a mandated
//                member of FRANCE TELECOM.
//                If such an authorization is provided, any modified version or
//                copy of the software has to contain this header.
//
// WARRANTIES   : This software is made available by the authors in the  hope
//                that it will be useful, but without any warranty.
//                France Telecom is not liable for any consequence related to the
//                use of the provided software.
//
// AUTHORS      : France Telecom  / RD / MAPS / DVC / MSF
// ----------------------------------------------------------------------------
using System;
using System.Collections.Generic;
using System.IO;
using System.IO.IsolatedStorage;
using System.Threading;
using System.Windows;
using System.Xml.Serialization;

namespace OrangeLigue1.Storage
{
    /// <summary>
    /// Implements a persistent data store keeping serialized objects in the isolated storage.
    /// </summary>
    public class PersistentDataStore : IDataStore
    {
        private readonly string _rootDirPath;
        private object _cacheLock = new object();
        private Dictionary<string, byte[]> _cache = new Dictionary<string, byte[]>();
        private bool _mustDeleteAllOnNextFlush = false;
        private object _storeLock = new object();
        private IsolatedStorageFile _store = null;
        private XmlSerializer _lastXmlSerializer = null;
        private Type _lastXmlSerializerType = null;

        private static string BuildRootDirPath(string dataStoreName)
        {
            if (string.IsNullOrEmpty(dataStoreName))
            {
                dataStoreName = "Default";
            }
            return string.Format("DataStore_{0}", dataStoreName);
        }

        /// <summary>
        /// Initializes a new <see cref="PersistentDataStore"/> instance with the specified name.
        /// </summary>
        public PersistentDataStore(string dataStoreName)
        {
            _rootDirPath = BuildRootDirPath(dataStoreName);
            IsClosed = false;
        }

        /// <summary>
        /// Indicates whether the store is closed or being closed.
        /// </summary>
        public bool IsClosed
        {
            get;
            private set;
        }

        /// <summary>
        /// The isolated storage store used by the data store.
        /// </summary>
        private IsolatedStorageFile Store
        {
            get
            {
                lock (_storeLock)
                {
                    if (_store == null)
                    {
                        _store = IsolatedStorageFile.GetUserStoreForApplication();
                    }
                    return _store;
                }
            }
        }

        /// <summary>
        /// Implements <see cref="IDataStore.Save"/>.
        /// </summary>
        public void Save(string key, object value)
        {
            if (IsClosed || string.IsNullOrEmpty(key) || value == null)
            {
                throw new InvalidOperationException();
            }

            byte[] _serializedData;
            if (value is byte[])
            {
                // special case for byte array values
                _serializedData = (byte[])((byte[])value).Clone();
            }
            else
            {
                using (MemoryStream memoryStream = new MemoryStream())
                {
                    GetXmlSerializer(value.GetType()).Serialize(memoryStream, value);
                    _serializedData = memoryStream.ToArray();
                }
            }

            lock (_cacheLock)
            {
                _cache[key] = _serializedData;
            }

            RequestFlush();
        }

        /// <summary>
        /// Implements <see cref="IDataStore.Contains"/>.
        /// </summary>
        public bool Contains(string key)
        {
            if (IsClosed)
            {
                throw new InvalidOperationException();
            }

            if (string.IsNullOrEmpty(key))
            {
                return false;
            }

            lock (_cacheLock)
            {
                if (_mustDeleteAllOnNextFlush)
                {
                    return false;
                }
                if (_cache.ContainsKey(key))
                {
                    return true;
                }
            }

            return SerializedDataExists(key);
        }

        /// <summary>
        /// Implements <see cref="IDataStore.Load"/>.
        /// </summary>
        public object Load(string key, Type typeOfValue)
        {
            if (IsClosed)
            {
                throw new InvalidOperationException();
            }

            if (string.IsNullOrEmpty(key))
            {
                return null;
            }

            byte[] serializedData = null;
            lock (_cacheLock)
            {
                if (_mustDeleteAllOnNextFlush)
                {
                    return null;
                }
                if (_cache.ContainsKey(key))
                {
                    serializedData = _cache[key];
                    if (serializedData == null)
                    {
                        // object deleted in cache
                        return null;
                    }
                }
            }

            if (serializedData == null)
            {
                try
                {
                    serializedData = ReadSerializedData(key);
                }
                catch (Exception error)
                {
                    Logger.LogException(error, "Error while reading serialized data!");
                    return null;
                }
                if (serializedData == null)
                {
                    return null;
                }
            }

            if (typeOfValue.IsAssignableFrom(typeof(byte[])))
            {
                // special case for byte array values
                return serializedData;
            }

            try
            {
                using (MemoryStream memoryStream = new MemoryStream(serializedData))
                {
                    return GetXmlSerializer(typeOfValue).Deserialize(memoryStream);
                }
            }
            catch (Exception error)
            {
                Logger.LogException(error, "Error while deserializing object (XML follows)!");
                Logger.LogEncodedString(serializedData);
                return null;
            }
        }

        /// <summary>
        /// Implements <see cref="IDataStore.Delete"/>.
        /// </summary>
        public void Delete(string key)
        {
            if (IsClosed)
            {
                throw new InvalidOperationException();
            }

            if (string.IsNullOrEmpty(key))
            {
                return;
            }

            lock (_cacheLock)
            {
                _cache[key] = null;
            }

            RequestFlush();
        }

        /// <summary>
        /// Implements <see cref="IDataStore.DeleteAll"/>.
        /// </summary>
        public void DeleteAll()
        {
            lock (_cacheLock)
            {
                _cache.Clear();
                _mustDeleteAllOnNextFlush = true;
            }
            RequestFlush();
        }

        /// <summary>
        /// Implements <see cref="IDataStore.Close"/>.
        /// </summary>
        public void Close()
        {
            if (IsClosed)
            {
                return;
            }
            Logger.DebugTrace("Closing data store.");
            IsClosed = true;
            Logger.DebugTrace("Flushing data store cache...");
            DoFlush(null);
            Logger.DebugTrace("Data store cache flushed.");

            lock (_storeLock)
            {
                if (_store != null)
                {
                    try { _store.Dispose(); }
                    catch (Exception) { }
                    _store = null;
                }
            }
        }

        /// <summary>
        /// Requests the cached data to be flushed to isolated storage.
        /// </summary>
        private void RequestFlush()
        {
            Deployment.Current.Dispatcher.BeginInvoke(() =>
            {
                ThreadPool.QueueUserWorkItem(DoFlush);
            });
        }

        /// <summary>
        /// Flushes the cached data to the isolated storage.
        /// </summary>
        private void DoFlush(object state)
        {
            try
            {
                lock (_cacheLock)
                {
                    if (_mustDeleteAllOnNextFlush)
                    {
                        _mustDeleteAllOnNextFlush = false;
                        try
                        {
                            DeleteAllSerializedData();
                        }
                        catch (Exception error)
                        {
                            Logger.LogException(error, "Error while deleting all serialized data!");
                        }
                    }
                }
                while (true)
                {
                    string key;
                    byte[] serializedData;
                    lock (_cacheLock)
                    {
                        IEnumerator<string> keyEnumerator = _cache.Keys.GetEnumerator();
                        if (keyEnumerator.MoveNext() == false)
                        {
                            // the cache is empty
                            return;
                        }
                        key = keyEnumerator.Current;
                        serializedData = _cache[key];
                        _cache.Remove(key);
                    }
                    if (serializedData != null)
                    {
                        try
                        {
                            WriteSerializedData(key, serializedData);
                        }
                        catch (Exception error)
                        {
                            Logger.LogException(error, "Error while writing serialized data!");
                        }
                    }
                    else
                    {
                        try
                        {
                            DeleteSerializedData(key);
                        }
                        catch (Exception error)
                        {
                            Logger.LogException(error, "Error while deleting serialized data!");
                        }
                    }
                }
            }
            catch (Exception error)
            {
                Logger.LogException(error, "Error while flushing!");
            }
        }

        /// <summary>
        /// Indicates whether the store contains serialized data for the specified key.
        /// </summary>
        private bool SerializedDataExists(string key)
        {
            lock (_storeLock)
            {
                return Store.FileExists(FilePathForKey(key));
            }
        }

        /// <summary>
        /// Reads the serialized data for the specified key from the store.
        /// Returns <c>null</c> if there is no serialized data for the key.
        /// </summary>
        private byte[] ReadSerializedData(string key)
        {
            string filePath = FilePathForKey(key);
            byte[] serializedData;
            lock (_storeLock)
            {
                try
                {
                    // it's faster to catch an exception when the file does not exist
                    // than to call IsolatedStorageFile.FileExists() before opening the file
                    using (IsolatedStorageFileStream fileStream = Store.OpenFile(filePath, FileMode.Open, FileAccess.Read))
                    {
                        serializedData = new byte[fileStream.Length];
                        int readOffset = 0;
                        while (readOffset < serializedData.Length)
                        {
                            int readCount = fileStream.Read(serializedData, readOffset, serializedData.Length - readOffset);
                            if (readCount == 0)
                            {
                                throw new NotSupportedException();
                            }
                            readOffset += readCount;
                        }
                    }
                }
                catch (IsolatedStorageException)
                {
                    return null;
                }
            }
            return serializedData;
        }

        /// <summary>
        /// Writes the serialized data for the specified key to the store.
        /// </summary>
        private void WriteSerializedData(string key, byte[] serializedData)
        {
            Logger.DebugTrace("Flushing data for '{0}' to data store ({1} bytes).", key, serializedData.Length);
            string filePath = FilePathForKey(key);
            lock (_storeLock)
            {
                try
                {
                    if (!Store.DirectoryExists(_rootDirPath))
                    {
                        Store.CreateDirectory(_rootDirPath);
                    }
                    IsolatedStorageFileStream fileStream;
                    if (Store.FileExists(filePath))
                    {
                        fileStream = Store.OpenFile(filePath, FileMode.Create, FileAccess.Write);
                    }
                    else
                    {
                        fileStream = Store.OpenFile(filePath, FileMode.CreateNew, FileAccess.Write);
                    }
                    using (fileStream)
                    {
                        fileStream.Seek(0, SeekOrigin.Begin);
                        while (fileStream.Position < serializedData.Length)
                        {
                            fileStream.Write(serializedData, (int)fileStream.Position, (int)(serializedData.Length - fileStream.Position));
                        }
                    }
                }
                catch (Exception e)
                {
                    if (Store.FileExists(filePath))
                    {
                        try
                        {
                            Store.DeleteFile(filePath);
                        }
                        catch (Exception e2)
                        {
                            Logger.LogException(e2, "Error while deleting serialized data!");
                        }
                    }
                    throw;
                }
            }
        }

        /// <summary>
        /// Deletes the serialized data for the specified key from the store.
        /// </summary>
        private void DeleteSerializedData(string key)
        {
            string filePath = FilePathForKey(key);
            lock (_storeLock)
            {
                if (Store.FileExists(filePath))
                {
                    Store.DeleteFile(filePath);
                }
            }
        }

        /// <summary>
        /// Deletes all the serialized data from the store.
        /// </summary>
        private void DeleteAllSerializedData()
        {
            string searchPattern = Path.Combine(_rootDirPath, "*.*");
            lock (_storeLock)
            {
                if (!Store.DirectoryExists(_rootDirPath))
                {
                    return;
                }
                string[] fileNames = Store.GetFileNames(searchPattern);
                foreach (string fileName in fileNames)
                {
                    string filePath = Path.Combine(_rootDirPath, fileName);
                    try
                    {
                        Store.DeleteFile(filePath);
                    }
                    catch (Exception error)
                    {
                        Logger.LogException(error, "Error while deleting file '{0}'!", filePath);
                    }
                }
            }
        }

        /// <summary>
        /// Returns the path of the file for the specified key.
        /// </summary>
        private string FilePathForKey(string key)
        {
            return System.IO.Path.Combine(_rootDirPath, key);
        }

        private XmlSerializer GetXmlSerializer(Type type)
        {
            if (_lastXmlSerializer == null || _lastXmlSerializerType != type)
            {
                _lastXmlSerializer = new XmlSerializer(type);
                _lastXmlSerializerType = type;
            }
            return _lastXmlSerializer;
        }

        /// <summary>
        /// Overrides Object.ToString().
        /// </summary>
        public override string ToString()
        {
            return "PersistentDataStore";
        }
    }
}

