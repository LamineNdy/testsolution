// -------------------------------  Copyright ---------------------------------
//	This software has been developped by France Telecom, FT/RD/RD/MAPS/DVC/MSF
//
//	Copyright (c) France Telecom 2009-2010
//
// COPYRIGHT    : This file is the property of FRANCE TELECOM.
//                It cannot be copied, used, or modified without obtaining
//                an authorization from the authors or a mandated
//                member of FRANCE TELECOM.
//                If such an authorization is provided, any modified version or
//                copy of the software has to contain this header.
//
// WARRANTIES   : This software is made available by the authors in the  hope
//                that it will be useful, but without any warranty.
//                France Telecom is not liable for any consequence related to the
//                use of the provided software.
//
// AUTHORS      : France Telecom  / RD / MAPS / DVC / MSF
// ----------------------------------------------------------------------------
using System;
using System.Collections.Generic;
using System.IO;
using System.Xml.Serialization;

namespace OrangeLigue1.Storage
{
    /// <summary>
    /// Implements a volatile data store keeping serialized objects in memory for testing purposes.
    /// </summary>
    public class VolatileDataStore : IDataStore
    {
        private Dictionary<string, byte[]> _serializedObjects = new Dictionary<string, byte[]>();

        /// <summary>
        /// Initializes a new instance of the <see cref="VolatileDataStore"/> class.
        /// </summary>
        public VolatileDataStore()
        {
        }

        /// <summary>
        /// Implements <see cref="IDataStore.Save"/>.
        /// </summary>
        public void Save(string key, object value)
        {
            using (MemoryStream memoryStream = new MemoryStream())
            {
                XmlSerializer serializer = new XmlSerializer(value.GetType());
                serializer.Serialize(memoryStream, value);
                _serializedObjects[key] = memoryStream.ToArray();
            }
        }

        /// <summary>
        /// Implements <see cref="IDataStore.Contains"/>.
        /// </summary>
        public bool Contains(string key)
        {
            return _serializedObjects.ContainsKey(key);
        }

        /// <summary>
        /// Implements <see cref="IDataStore.Load"/>.
        /// </summary>
        public object Load(string key, Type typeOfValue)
        {
            if (_serializedObjects.ContainsKey(key) == false)
            {
                return null;
            }
            using (MemoryStream memoryStream = new MemoryStream(_serializedObjects[key]))
            {
                XmlSerializer serializer = new XmlSerializer(typeOfValue);
                return serializer.Deserialize(memoryStream);
            }
        }

        /// <summary>
        /// Implements <see cref="IDataStore.Delete"/>.
        /// </summary>
        public void Delete(string key)
        {
            _serializedObjects.Remove(key);
        }

        /// <summary>
        /// Implements <see cref="IDataStore.DeleteAll"/>.
        /// </summary>
        public void DeleteAll()
        {
            _serializedObjects.Clear();
        }

        /// <summary>
        /// Implements <see cref="IDataStore.Close"/>.
        /// </summary>
        public void Close()
        {
            // do nothing
        }
    }
}

