// -------------------------------  Copyright ---------------------------------
//	This software has been developped by France Telecom, FT/RD/RD/MAPS/DVC/MSF
//
//	Copyright (c) France Telecom 2009-2010
//
// COPYRIGHT    : This file is the property of FRANCE TELECOM.
//                It cannot be copied, used, or modified without obtaining
//                an authorization from the authors or a mandated
//                member of FRANCE TELECOM.
//                If such an authorization is provided, any modified version or
//                copy of the software has to contain this header.
//
// WARRANTIES   : This software is made available by the authors in the  hope
//                that it will be useful, but without any warranty.
//                France Telecom is not liable for any consequence related to the
//                use of the provided software.
//
// AUTHORS      : France Telecom  / RD / MAPS / DVC / MSF
// ----------------------------------------------------------------------------
using System;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using Microsoft.Expression.Interactivity.Core;
using System.Windows.Interactivity;
using System.Collections;
using System.Globalization;

namespace OrangeLigue1.Behaviors
{
    public class ToggleStateAction : TargetedTriggerAction<FrameworkElement>
    {

        bool isInitialState = true;

        [CustomPropertyValueEditor(System.Windows.Interactivity.CustomPropertyValueEditor.StateName)]
        public string TargetInitialState
        {
            get { return (string)GetValue(TargetInitialStateProperty); }
            set { SetValue(TargetInitialStateProperty, value); }
        }

        // Using a DependencyProperty as the backing store for TargetState1.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty TargetInitialStateProperty =
            DependencyProperty.Register("TargetInitialState", typeof(string), typeof(ToggleStateAction), null);

        [CustomPropertyValueEditor(System.Windows.Interactivity.CustomPropertyValueEditor.StateName)]
        public string TargetToggledState
        {
            get { return (string)GetValue(TargetToggledStateProperty); }
            set { SetValue(TargetToggledStateProperty, value); }
        }

        // Using a DependencyProperty as the backing store for TargetState1.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty TargetToggledStateProperty =
            DependencyProperty.Register("TargetToggledState", typeof(string), typeof(ToggleStateAction), null);



        /// <summary>
        /// Whether or not to use a VisualTransition to transition between states.
        /// </summary>
        public bool UseTransitions
        {
            get { return (bool)GetValue(UseTransitionsProperty); }
            set { SetValue(UseTransitionsProperty, value); }
        }

        // Using a DependencyProperty as the backing store for UseTransitions.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty UseTransitionsProperty =
            DependencyProperty.Register("UseTransitions", typeof(bool), typeof(ToggleStateAction),null);

        protected override void OnAttached()
        {
            base.OnAttached();
            Control stateTarget = this.StateTarget as Control;
            if (stateTarget != null)
            {
                VisualStateManager.GoToState(stateTarget, TargetInitialState, UseTransitions);
                isInitialState = true;
            }
        }

        protected override void Invoke(object parameter)
        {
            Control stateTarget = this.StateTarget as Control;
            if (stateTarget != null)
            {
                stateTarget.ApplyTemplate();
                if (isInitialState == true)
                {
                    VisualStateManager.GoToState(stateTarget, TargetToggledState, UseTransitions);
                    isInitialState = false;
                }
                else
                {
                    VisualStateManager.GoToState(stateTarget, TargetInitialState, UseTransitions);
                    isInitialState = true;
                }
            }
        }

        protected override void OnTargetChanged(FrameworkElement oldTarget, FrameworkElement newTarget)
        {
            base.OnTargetChanged(oldTarget, newTarget);
            FrameworkElement target = null;
            if (!string.IsNullOrEmpty(base.TargetName))
            {
                target = base.Target;
            }
            else
            {
                target = base.AssociatedObject as FrameworkElement;
                if (target == null)
                {
                    this.StateTarget = null;
                    return;
                }
                for (FrameworkElement element2 = target.Parent as FrameworkElement; element2 != null; element2 = element2.Parent as FrameworkElement)
                {
                    if (element2 is UserControl)
                    {
                        break;
                    }
                    if (element2.Parent == null)
                    {
                        FrameworkElement parent = VisualTreeHelper.GetParent(element2) as FrameworkElement;
                        if ((parent == null) || (!(parent is Control) && !(parent is ContentPresenter)))
                        {
                            break;
                        }
                    }
                    target = element2;
                }
                if (VisualStateManager.GetVisualStateGroups(target).Count != 0)
                {
                    FrameworkElement element4 = VisualTreeHelper.GetParent(target) as FrameworkElement;
                    if ((element4 != null) && (element4 is Control))
                    {
                        target = element4;
                    }
                }
                else
                {
                    this.StateTarget = null;
                    throw new InvalidOperationException(string.Format(CultureInfo.CurrentCulture, "Target has no state group", new object[] { target.Name }));
                }
            }
            this.StateTarget = target;
            
        }

        public FrameworkElement StateTarget { get; set; }

    }
}

