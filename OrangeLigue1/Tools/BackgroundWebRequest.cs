// -------------------------------  Copyright ---------------------------------
//	This software has been developped by France Telecom, FT/RD/RD/MAPS/DVC/MSF
//
//	Copyright (c) France Telecom 2009-2010
//
// COPYRIGHT    : This file is the property of FRANCE TELECOM.
//                It cannot be copied, used, or modified without obtaining
//                an authorization from the authors or a mandated
//                member of FRANCE TELECOM.
//                If such an authorization is provided, any modified version or
//                copy of the software has to contain this header.
//
// WARRANTIES   : This software is made available by the authors in the  hope
//                that it will be useful, but without any warranty.
//                France Telecom is not liable for any consequence related to the
//                use of the provided software.
//
// AUTHORS      : France Telecom  / RD / MAPS / DVC / MSF
// ----------------------------------------------------------------------------

// comment to disable references to Authentication Manager
#define WITH_AUTHENTICATION_MANAGER

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.IO;
using System.Net;
using System.Threading;
using System.Windows;

#if WITH_AUTHENTICATION_MANAGER
using AuthenticationManager;
using Microsoft.Phone.Net.NetworkInformation;
#endif

namespace OrangeLigue1.Tools
{
    /// <summary>
    /// Performs a web request in a background thread.
    /// </summary>
    public class BackgroundWebRequest
    {
        private static readonly TimeSpan WaitTimeout = TimeSpan.FromSeconds(30);

        /// <summary>
        /// Creates and returns a new <see cref="BackgroundWebRequest"/> for the specified URI.
        /// The request won't be authenticated.
        /// </summary>
        public static BackgroundWebRequest Create(Uri uri)
        {
            return new BackgroundWebRequest(uri, false, null);
        }

        /// <summary>
        /// Creates and returns a new authenticated <see cref="BackgroundWebRequest"/> for the specified URI.
        /// If <paramref name="fallbackUri"/> is not null, it specifies the URI to connect to if the request
        /// fails on the initial URI.
        /// </summary>
        public static BackgroundWebRequest CreateAuthenticated(Uri uri, Uri fallbackUri)
        {
#if WITH_AUTHENTICATION_MANAGER
            return new BackgroundWebRequest(uri, true, fallbackUri);
#else
            throw new InvalidOperationException();
#endif
        }

        /// <summary>
        /// The possible states of a request.
        /// </summary>
        private enum State
        {
            Initialized,
            Started,
            Processing,
            Finished
        }

        private State _state = State.Initialized;
        private WebRequest _webRequest;
        private BackgroundWebRequestPriority _priority = BackgroundWebRequestPriority.Normal;
        private bool _isCancelled = false;
        private byte[] _requestData = null;
        private object _userState = null;
        private List<BackgroundRequestCompletedEventHandler> _completedHandlers = new List<BackgroundRequestCompletedEventHandler>();

        /// <summary>
        /// Initializes a new <see cref="BackgroundWebRequest"/> instance.
        /// </summary>
        private BackgroundWebRequest(Uri uri, bool isAuthenticated, Uri fallbackUri)
        {
            FallbackUri = fallbackUri;
            _webRequest = null;
            if (isAuthenticated)
            {
#if WITH_AUTHENTICATION_MANAGER
                _webRequest = OLAuthenticationManager.Create(uri);
                ((OLHttpWebRequest)_webRequest).AllowReadStreamBuffering = false;  // reduces impact on UI thread
                ((OLHttpWebRequest)_webRequest).SetNetworkPreference(NetworkSelectionCharacteristics.Cellular);
#endif
            }
            else
            {
                _webRequest = HttpWebRequest.Create(uri);
                ((HttpWebRequest)_webRequest).AllowReadStreamBuffering = false;  // reduces impact on UI thread
                ((HttpWebRequest)_webRequest).SetNetworkPreference(NetworkSelectionCharacteristics.Cellular);
            }
            if (_webRequest == null)
            {
                throw new ArgumentException();
            }
        }

        private HttpWebRequest ExtractActualRequestInstance(WebRequest instance)
        {
            var olhttpRequest = instance as OLHttpWebRequest;
            if (olhttpRequest != null)
            {
                return (HttpWebRequest) olhttpRequest;
            }
            else
            {
                return (HttpWebRequest) instance;
            }
        }

        public bool AllowAutoRedirect
        {
            get { return ExtractActualRequestInstance(_webRequest).AllowAutoRedirect; }
            set { ExtractActualRequestInstance(_webRequest).AllowAutoRedirect = value; }
        }

        /// <summary>
        /// Indicates whether the request is authenticated.
        /// </summary>
        public bool IsAuthenticated
        {
            get
            {
#if WITH_AUTHENTICATION_MANAGER
                return (_webRequest != null && _webRequest is OLHttpWebRequest);
#else
                return false;
#endif
            }
        }

        /// <summary>
        /// The nominal URI of the request.
        /// </summary>
        public Uri RequestUri
        {
            get
            {
                return _webRequest.RequestUri;
            }
        }

        /// <summary>
        /// The fallback URI (<c>null</c> if none).
        /// </summary>
        public Uri FallbackUri
        {
            get;
            private set;
        }

        /// <summary>
        /// The priority of the request.
        /// </summary>
        public BackgroundWebRequestPriority Priority
        {
            get
            {
                return _priority;
            }
            set
            {
                lock (this)
                {
                    CheckNotStarted();
                    _priority = value;
                }
            }
        }

        /// <summary>
        /// The method for the request.
        /// </summary>
        public string Method
        {
            get
            {
                return _webRequest.Method;
            }
            set
            {
                lock (this)
                {
                    CheckNotStarted();
                    _webRequest.Method = value;
                }
            }
        }

        /// <summary>
        /// The network credentials used for authenticating the request.
        /// </summary>
        public ICredentials Credentials
        {
            get
            {
                return _webRequest.Credentials;
            }
            set
            {
                lock (this)
                {
                    CheckNotStarted();
                    _webRequest.Credentials = value;
                }
            }
        }

        /// <summary>
        /// The collection of the HTTP headers for the request.
        /// </summary>
        public WebHeaderCollection Headers
        {
            get
            {
                return _webRequest.Headers;
            }
        }

        /// <summary>
        /// The CookieContainer object for the request.
        /// </summary>
        public CookieContainer CookieContainer
        {
            get
            {
#if WITH_AUTHENTICATION_MANAGER
                if (IsAuthenticated)
                {
                    return ((OLHttpWebRequest)_webRequest).CookieContainer;
                }
#endif
                return ((HttpWebRequest)_webRequest).CookieContainer;
            }
            set
            {
#if WITH_AUTHENTICATION_MANAGER
                if (IsAuthenticated)
                {
                    ((OLHttpWebRequest)_webRequest).CookieContainer = value;
                    return;
                }
#endif
                ((HttpWebRequest)_webRequest).CookieContainer = value;
            }
        }

        /// <summary>
        /// The data of the request's body.
        /// </summary>
        public byte[] RequestData
        {
            get
            {
                return _requestData;
            }
            set
            {
                lock (this)
                {
                    CheckNotStarted();
                    _requestData = value;
                }
            }
        }

        /// <summary>
        /// The content type for the request.
        /// </summary>
        public string ContentType
        {
            get
            {
                return _webRequest.ContentType;
            }
            set
            {
                lock (this)
                {
                    CheckNotStarted();
                    _webRequest.ContentType = value;
                }
            }
        }

        /// <summary>
        /// Throws an InvalidOperationException if the request has already been started.
        /// </summary>
        private void CheckNotStarted()
        {
            lock (this)
            {
                if (_state != State.Initialized)
                {
                    throw new InvalidOperationException();
                }
            }
        }

        /// <summary>
        /// Throws an appropriate exception if the request has been cancelled.
        /// </summary>
        private void CheckNotCancelled()
        {
            lock (this)
            {
                if (_isCancelled)
                {
                    throw new WebException("", WebExceptionStatus.RequestCanceled);
                }
            }
        }

        /// <summary>
        /// Starts the request in the default background request queue.
        /// The background operation raises the <see cref="Completed"/> event when completed;
        /// the event is raised in the UI thread.
        /// </summary>
        public void Start(object userState)
        {
            Start(null, userState);
        }

        /// <summary>
        /// Starts the request in the specified background request queue.
        /// If <paramref name="queue"/> is <c>null</c>, the default background request queue is used.
        /// The background operation raises the <see cref="Completed"/> event when completed;
        /// the event is raised in the UI thread.
        /// </summary>
        public void Start(BackgroundWebRequestQueue queue, object userState)
        {
            if (queue == null)
            {
                queue = BackgroundWebRequestQueue.Default;
            }
            lock (this)
            {
                CheckNotStarted();
                _state = State.Started;
                _userState = userState;
            }
            queue.Enqueue(this);
        }

        /// <summary>
        /// Requests the background operation to be cancelled, if possible.
        /// This method does nothing if the request has not been started yet.
        /// </summary>
        public void Cancel()
        {
            lock (this)
            {
                switch (_state)
                {
                    case State.Initialized:
                    case State.Finished:
                        return;
                    case State.Started:
                        NotifyFailure(new WebException("", WebExceptionStatus.RequestCanceled));
                        return;
                    default:
                        Logger.DebugTrace("Cancelling {0}", this);
                        _isCancelled = true;
                        _webRequest.Abort();
                        Monitor.PulseAll(this);
                        break;
                }
            }
        }

        /// <summary>
        /// Processes the web request.
        /// Do not call this method directly.
        /// </summary>
        internal void Process()
        {
            lock (this)
            {
                if (_state != State.Started)
                {
                    NotifyFailure(new InvalidOperationException());
                    return;
                }
                _state = State.Processing;
            }

            try
            {
                ProcessCurrentRequest();
                return;
            }
            catch (Exception error)
            {
                if (!IsAuthenticated || FallbackUri == null)
                {
                    NotifyFailure(error);
                    return;
                }
                Logger.DebugTrace("Error while processing {0}: {1} {2}", this, error.GetType().Name, error.Message);
                Logger.DebugTrace("Trying once again with fallback URI ({0})", FallbackUri);
            }

#if WITH_AUTHENTICATION_MANAGER
            try
            {
                OLHttpWebRequest initialWebRequest = (OLHttpWebRequest)_webRequest;
                OLHttpWebRequest fallbackWebRequest = (OLHttpWebRequest)OLAuthenticationManager.Create(FallbackUri);
                fallbackWebRequest.Accept = initialWebRequest.Accept;
                fallbackWebRequest.AllowReadStreamBuffering = false;  // reduces impact on UI thread
                fallbackWebRequest.ContentType = initialWebRequest.ContentType;
                fallbackWebRequest.CookieContainer = initialWebRequest.CookieContainer;
                fallbackWebRequest.Credentials = initialWebRequest.Credentials;
                fallbackWebRequest.Headers = initialWebRequest.Headers;
                fallbackWebRequest.Method = initialWebRequest.Method;
                _webRequest = fallbackWebRequest;
                ProcessCurrentRequest();
            }
            catch (Exception error)
            {
                NotifyFailure(error);
            }
#endif
        }

        private void ProcessCurrentRequest()
        {
            HttpWebResponse webResponse = null;
            try
            {
                webResponse = SendRequest();
                byte[] responseData = ReceiveResponse(webResponse);
                NotifySuccess(webResponse, responseData);
            }
            finally
            {
                if (webResponse != null)
                {
                    try
                    {
                        webResponse.Close();
                    }
                    catch (Exception) { }
                }
            }
        }

        /// <summary>
        /// Sends the request and returns the resulting HttpWebResponse.
        /// </summary>
        private HttpWebResponse SendRequest()
        {
            CheckNotCancelled();
            SendRequestData();
            bool responseReady = false;
            IAsyncResult asyncResult = _webRequest.BeginGetResponse(
                (ar) =>
                {
                    responseReady = true;
                    lock (this)
                    {
                        Monitor.PulseAll(this);
                    }
                },
                null);
            CancellableWait(ref responseReady, WaitTimeout);
            return (HttpWebResponse)_webRequest.EndGetResponse(asyncResult);
        }

        /// <summary>
        /// Sends the request data, if any.
        /// </summary>
        private void SendRequestData()
        {
            if (_requestData == null)
            {
                return;
            }

            CheckNotCancelled();
            bool requestReady = false;
            IAsyncResult asyncResult = _webRequest.BeginGetRequestStream(
                (ar) =>
                {
                    requestReady = true;
                    lock (this)
                    {
                        Monitor.PulseAll(this);
                    }
                },
                null);
            CancellableWait(ref requestReady, WaitTimeout);

            using (Stream requestStream = _webRequest.EndGetRequestStream(asyncResult))
            {
                bool writeComplete = false;
                asyncResult = requestStream.BeginWrite(
                    _requestData, 0, _requestData.Length,
                    (ar) =>
                    {
                        writeComplete = true;
                        lock (this)
                        {
                            Monitor.PulseAll(this);
                        }
                    },
                    null);
                CancellableWait(ref writeComplete, WaitTimeout);
                requestStream.EndWrite(asyncResult);
                asyncResult = null;
            }
        }

        /// <summary>
        /// Receives the response of the request and returns it as a byte array.
        /// </summary>
        private byte[] ReceiveResponse(HttpWebResponse webResponse)
        {
            CheckNotCancelled();
            using (Stream responseInputStream = webResponse.GetResponseStream())
            using (MemoryStream responseDataStream = new MemoryStream())
            {
                if (webResponse.ContentLength > 0 && webResponse.ContentLength <= int.MaxValue)
                {
                    responseDataStream.Capacity = (int)webResponse.ContentLength;
                }
                byte[] responseDataBuffer = new byte[1024];
                while (true)
                {
                    CheckNotCancelled();
                    bool readComplete = false;
                    IAsyncResult asyncResult = responseInputStream.BeginRead(
                        responseDataBuffer, 0, responseDataBuffer.Length,
                        (ar) =>
                        {
                            readComplete = true;
                            lock (this)
                            {
                                Monitor.PulseAll(this);
                            }
                        },
                        null);
                    CancellableWait(ref readComplete, WaitTimeout);
                    int readCount = responseInputStream.EndRead(asyncResult);
                    asyncResult = null;
                    if (readCount <= 0)
                    {
                        break;
                    }
                    responseDataStream.Write(responseDataBuffer, 0, readCount);
                }
                return responseDataStream.ToArray();
            }
        }

        /// <summary>
        /// Waits for the specified boolean to become true.
        /// Throws an appropriate exception if the wait times out or if the request has been cancelled.
        /// </summary>
        private void CancellableWait(ref bool predicate, TimeSpan timeout)
        {
            DateTime timeLimit = DateTime.UtcNow.Add(timeout);
            lock (this)
            {
                while (timeLimit >= DateTime.UtcNow)
                {
                    if (_isCancelled)
                    {
                        throw new WebException("", WebExceptionStatus.RequestCanceled);
                    }
                    if (predicate)
                    {
                        return;
                    }
                    TimeSpan waitTimeout = timeLimit.Subtract(DateTime.UtcNow);
                    if (waitTimeout.TotalMilliseconds > 0)
                    {
                        Monitor.Wait(this, waitTimeout);
                    }
                }
            }
            throw new TimeoutException();
        }

        /// <summary>
        /// Occurs when the request completes.
        /// Handlers of this event are invoked in the UI thread.
        /// </summary>
        public event BackgroundRequestCompletedEventHandler Completed
        {
            add
            {
                lock (this)
                {
                    if (value == null || _completedHandlers.Contains(value))
                    {
                        return;
                    }
                    _completedHandlers.Add(value);
                }
            }
            remove
            {
                lock (this)
                {
                    if (value == null)
                    {
                        return;
                    }
                    _completedHandlers.Remove(value);
                }
            }
        }

        /// <summary>
        /// Notifies the successful completion of the request.
        /// </summary>
        private void NotifySuccess(HttpWebResponse webResponse, byte[] data)
        {
            BackgroundRequestCompletedEventArgs eventArgs = new BackgroundRequestCompletedEventArgs(null, false, _webRequest.RequestUri, _userState)
            {
                StatusCode = webResponse.StatusCode,
                ContentType = webResponse.ContentType,
                Headers = webResponse.Headers,
                Data = data
            };
            RaiseCompleted(eventArgs);
        }

        /// <summary>
        /// Notifies a failure that occured while processing the request.
        /// </summary>
        private void NotifyFailure(Exception error)
        {
            bool cancelled = false;
            WebException webException = error as WebException;
            if (webException != null && webException.Status == WebExceptionStatus.RequestCanceled)
            {
                error = null;
                cancelled = true;
            }
            BackgroundRequestCompletedEventArgs eventArgs = new BackgroundRequestCompletedEventArgs(error, cancelled, _webRequest.RequestUri, _userState);
            RaiseCompleted(eventArgs);
        }

        /// <summary>
        /// Raises the <see cref="Completed"/> event.
        /// </summary>
        private void RaiseCompleted(BackgroundRequestCompletedEventArgs eventArgs)
        {
            lock (this)
            {
                _state = State.Finished;
            }
            Deployment.Current.Dispatcher.BeginInvoke(() =>
            {
                lock (this)
                {
                    foreach (BackgroundRequestCompletedEventHandler completedHandler in _completedHandlers)
                    {
                        try
                        {
                            completedHandler(this, eventArgs);
                        }
                        catch (Exception handlerError)
                        {
                            Logger.Log("Unhandled exception in BackgroundRequestCompletedEventHandler: {0}", handlerError);
                        }
                    }
                }
            });
        }

        /// <summary>
        /// Overrides object.ToString().
        /// </summary>
        public override string ToString()
        {
            return string.Format("BackgroundWebRequest({0})", _webRequest.RequestUri);
        }
    }

    /// <summary>
    /// Enumerates the avaible priorities for background web requests.
    /// </summary>
    public enum BackgroundWebRequestPriority
    {
        Low = 0,
        Normal = 1,
        High = 2,
    };

    /// <summary>
    /// References a method that will handle the <see cref="BackgroundWebRequest.Completed"/> event.
    /// </summary>
    public delegate void BackgroundRequestCompletedEventHandler(object sender, BackgroundRequestCompletedEventArgs e);

    /// <summary>
    /// Provides data for the <see cref="BackgroundWebRequest.Completed"/> event.
    /// </summary>
    public class BackgroundRequestCompletedEventArgs : AsyncCompletedEventArgs
    {
        private HttpStatusCode _statusCode;
        private string _contentType;
        private WebHeaderCollection _headers;
        private byte[] _data;

        /// <summary>
        /// Initializes a new <see cref="BackgroundRequestCompletedEventArgs"/> instance.
        /// </summary>
        public BackgroundRequestCompletedEventArgs(Exception error, bool cancelled, Uri requestUri, object userState)
            : base(error, cancelled, userState)
        {
            RequestUri = requestUri;
        }

        /// <summary>
        /// The original URI of the request.
        /// </summary>
        public Uri RequestUri
        {
            get;
            private set;
        }

        /// <summary>
        /// The HTTP status code of the response.
        /// </summary>
        public HttpStatusCode StatusCode
        {
            get
            {
                CheckSuccessful();
                return _statusCode;
            }
            set
            {
                _statusCode = value;
            }
        }

        /// <summary>
        /// The content data of the received data.
        /// </summary>
        public string ContentType
        {
            get
            {
                CheckSuccessful();
                return _contentType;
            }
            set
            {
                _contentType = value;
            }
        }

        /// <summary>
        /// A collection of the HTTP headers of the response.
        /// </summary>
        public WebHeaderCollection Headers
        {
            get
            {
                CheckSuccessful();
                return _headers;
            }
            set
            {
                _headers = value;
            }
        }

        /// <summary>
        /// The received data.
        /// </summary>
        public byte[] Data
        {
            get
            {
                CheckSuccessful();
                return (_data == null ? new byte[0] : _data);
            }
            set
            {
                _data = value;
            }
        }

        /// <summary>
        /// Throws an InvalidOperationException if the request was not successful.
        /// </summary>
        private void CheckSuccessful()
        {
            if (Error != null || Cancelled)
            {
                throw new InvalidOperationException();
            }
        }
    }

    /// <summary>
    /// Manages a queue of background web requests.
    /// </summary>
    public class BackgroundWebRequestQueue
    {
        private static BackgroundWebRequestQueue _defaultQueue = null;
        private static readonly object _defaultQueueLock = new object();
        private static readonly List<WeakReference> _registeredQueues = new List<WeakReference>();

        /// <summary>
        /// References the default queue of background web requests.
        /// </summary>
        public static BackgroundWebRequestQueue Default
        {
            get
            {
                lock (_defaultQueueLock)
                {
                    if (_defaultQueue == null || _defaultQueue.IsStopped)
                    {
                        _defaultQueue = new BackgroundWebRequestQueue("default");
                    }
                    return _defaultQueue;
                }
            }
        }

        private static void Register(BackgroundWebRequestQueue queue)
        {
            if (queue == null)
            {
                return;
            }
            PrunedRegisteredQueues();
            lock (_registeredQueues)
            {
                foreach (WeakReference queueRef in _registeredQueues)
                {
                    if (queueRef.Target == queue)
                    {
                        return;
                    }
                }
                _registeredQueues.Add(new WeakReference(queue));
            }
        }

        private static void PrunedRegisteredQueues()
        {
            List<WeakReference> deadQueueRefs = null;
            lock (_registeredQueues)
            {
                foreach (WeakReference queueRef in _registeredQueues)
                {
                    if (queueRef.Target == null)
                    {
                        if (deadQueueRefs == null)
                        {
                            deadQueueRefs = new List<WeakReference>();
                        }
                        deadQueueRefs.Add(queueRef);
                    }
                }
                if (deadQueueRefs == null)
                {
                    return;
                }
                foreach (WeakReference deadQueueRef in deadQueueRefs)
                {
                    _registeredQueues.Remove(deadQueueRef);
                }
            }
        }

        /// <summary>
        /// Stops all background web requests queues.
        /// Pending requests, if any, are allowed to complete for the specified delay.
        /// </summary>
        public static void StopAll(TimeSpan delay)
        {
            try
            {
                DateTime timeout = DateTime.UtcNow.Add(delay);

                Logger.DebugTrace("Stopping all background web request queues (delay: {0})", delay);

                // Copy the list of registered queues
                List<WeakReference> queues;
                lock (_registeredQueues)
                {
                    queues = new List<WeakReference>(_registeredQueues);
                }

                // Prepare the queues for stop
                foreach (WeakReference queueRef in queues)
                {
                    BackgroundWebRequestQueue queue = queueRef.Target as BackgroundWebRequestQueue;
                    if (queue != null)
                    {
                        queue.PrepareForStop();
                    }
                }

                // Give some chance for pending requests to complete
                while (timeout >= DateTime.UtcNow)
                {
                    Thread.Sleep(1);
                    bool hasPendingRequests = false;
                    foreach (WeakReference queueRef in queues)
                    {
                        BackgroundWebRequestQueue queue = queueRef.Target as BackgroundWebRequestQueue;
                        if (queue != null && queue.HasPendingRequests)
                        {
                            hasPendingRequests = true;
                            break;
                        }
                    }
                    if (!hasPendingRequests)
                    {
                        break;
                    }
                }

                // Stop all the queues
                foreach (WeakReference queueRef in queues)
                {
                    BackgroundWebRequestQueue queue = queueRef.Target as BackgroundWebRequestQueue;
                    if (queue != null)
                    {
                        queue.Stop();
                    }
                }
            }
            catch (Exception error)
            {
                Logger.LogException(error, "Error while stopping the background web request queue: {0}");
            }
        }

        private readonly List<BackgroundWebRequest> _requests = new List<BackgroundWebRequest>();
        private bool _isStopped = false;
        private BackgroundWebRequest _processedRequest = null;

        /// <summary>
        /// Initializes a new <see cref="RequestQueue"/> instance.
        /// </summary>
        public BackgroundWebRequestQueue(string name)
        {
            Name = name;
            Register(this);

            Thread thread = new Thread(Work);
            thread.IsBackground = true;
            thread.Start();
        }

        /// <summary>
        /// The name of the background web request queue.
        /// </summary>
        public string Name
        {
            get;
            private set;
        }

        /// <summary>
        /// Indicates whether the queue has been stopped.
        /// </summary>
        private bool IsStopped
        {
            get
            {
                lock (this)
                {
                    return _isStopped;
                }
            }
        }

        /// <summary>
        /// References the currently processed request (<c>null</c> if there is none).
        /// </summary>
        private BackgroundWebRequest ProcessedRequest
        {
            get
            {
                lock (this)
                {
                    return _processedRequest;
                }
            }
            set
            {
                lock (this)
                {
                    _processedRequest = value;
                }
            }
        }

        private bool HasPendingRequests
        {
            get
            {
                lock (this)
                {
                    return (ProcessedRequest != null) || (_requests.Count >= 1);
                }
            }
        }

        /// <summary>
        /// Adds the specified request to the queue to be processed in background.
        /// </summary>
        public void Enqueue(BackgroundWebRequest request)
        {
            if (request == null || IsStopped)
            {
                return;
            }
            lock (this)
            {
                if (!_requests.Contains(request))
                {
                    bool inserted = false;
                    for (int i = _requests.Count - 2; !inserted && i >= 0; i--)
                    {
                        if (_requests[i].Priority >= request.Priority)
                        {
                            _requests.Insert(i + 1, request);
                            inserted = true;
                        }
                    }
                    if (!inserted)
                    {
                        _requests.Add(request);
                    }
                    Logger.DebugTrace("{0}: Enqueued {1}, priority {2}.", this, request, request.Priority);
                    Monitor.PulseAll(this);
                }
            }
        }

        private void PrepareForStop()
        {
            lock (this)
            {
                if (IsStopped)
                {
                    return;
                }

                Logger.DebugTrace("{0}: Preparing for stop.", this);

                int requestIndex = 0;
                while (requestIndex < _requests.Count)
                {
                    BackgroundWebRequest request = _requests[requestIndex];
                    if (request.Priority != BackgroundWebRequestPriority.High)
                    {
                        Logger.DebugTrace("{0}: Dequeuing {1}.", this, request);
                        _requests.Remove(request);
                    }
                    else
                    {
                        requestIndex += 1;
                    }
                }

                if (ProcessedRequest != null && ProcessedRequest.Priority != BackgroundWebRequestPriority.High)
                {
                    Logger.DebugTrace("{0}: Cancelling {1}.", this, ProcessedRequest);
                    ProcessedRequest.Cancel();
                }

                Monitor.PulseAll(this);
            }
        }

        /// <summary>
        /// Stops the queue from processing requests.
        /// Requests that are in the queue are cancelled.
        /// </summary>
        public void Stop()
        {
            if (IsStopped)
            {
                return;
            }

            Logger.DebugTrace("Stopping {0}", this);
            lock (this)
            {
                // signal the stop event
                _isStopped = true;
                Monitor.PulseAll(this);

                // cancel the pending requests
                while (_requests.Count > 0)
                {
                    BackgroundWebRequest request = _requests[0];
                    _requests.RemoveAt(0);
                    request.Cancel();
                }
                BackgroundWebRequest processedRequest = ProcessedRequest;
                if (processedRequest != null)
                {
                    processedRequest.Cancel();
                    processedRequest = null;
                }
            }
        }

        /// <summary>
        /// The entry point for the background thread that runs the queue.
        /// </summary>
        private void Work()
        {
            Logger.DebugTrace("Worker thread started for {0}", this);
            while (!IsStopped)
            {
                try
                {
                    ProcessQueue();
                }
                catch (Exception) { }
                lock (this)
                {
                    if (!IsStopped && _requests.Count == 0)
                    {
                        Monitor.Wait(this);
                    }
                }
            }
            Logger.DebugTrace("Worker thread stopped for {0}", this);
        }

        /// <summary>
        /// Processes the requests in the queue.
        /// </summary>
        private void ProcessQueue()
        {
            while (true)
            {
                if (IsStopped)
                {
                    return;
                }
                BackgroundWebRequest request;
                lock (this)
                {
                    if (_requests.Count == 0)
                    {
                        return;
                    }
                    request = _requests[0];
                    _requests.RemoveAt(0);
                    Logger.DebugTrace("{0}: Processing {1} ({2} requests left in queue)...", this, request, _requests.Count);
                    ProcessedRequest = request;
                }
                if (request == null)
                {
                    continue;
                }
                try
                {
                    request.Process();
                    Logger.DebugTrace("{0}: Finished processing {1}.", this, request);
                }
                catch (ThreadAbortException)
                {
                    Logger.DebugTrace("{0}: Processing of {1} failed because of a ThreadAbortException.", this, request);
                }
                catch (Exception error)
                {
                    Logger.DebugTrace("{0}: Unhandled exception while processing {1}: {2}", this, request, error);
                }
                finally
                {
                    ProcessedRequest = null;
                }
                Thread.Sleep(50);  // See issue #7191.
            }
        }

        public override string ToString()
        {
            return string.Format("BackgroundWebRequest({0})", (Name != null ? Name : "???"));
        }
    }
}

