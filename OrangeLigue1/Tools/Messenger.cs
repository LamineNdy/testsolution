// -------------------------------  Copyright ---------------------------------
//	This software has been developped by France Telecom, FT/RD/RD/MAPS/DVC/MSF
//
//	Copyright (c) France Telecom 2009-2010
//
// COPYRIGHT    : This file is the property of FRANCE TELECOM.
//                It cannot be copied, used, or modified without obtaining
//                an authorization from the authors or a mandated
//                member of FRANCE TELECOM.
//                If such an authorization is provided, any modified version or
//                copy of the software has to contain this header.
//
// WARRANTIES   : This software is made available by the authors in the  hope
//                that it will be useful, but without any warranty.
//                France Telecom is not liable for any consequence related to the
//                use of the provided software.
//
// AUTHORS      : France Telecom  / RD / MAPS / DVC / MSF
// ----------------------------------------------------------------------------
using System;
using System.Collections.Generic;
using System.Reflection;
using System.Windows;

namespace OrangeLigue1.Tools
{
    /// <summary>
    /// Provides for sending messages to listeners that register as weak references.
    /// </summary>
    public class Messenger
    {
        private static Messenger _current = null;

        /// <summary>
        /// The singleton instance.
        /// </summary>
        public static Messenger Current
        {
            get
            {
                if (_current == null)
                {
                    _current = new Messenger();
                }
                return _current;
            }
        }

        private Dictionary<string, List<ListenerReference>> _registeredListener = new Dictionary<string, List<ListenerReference>>();

        /// <summary>
        /// Initializes a new <see cref="Messenger"/> instance.
        /// </summary>
        private Messenger()
        {
        }

        /// <summary>
        /// Registers a listener for the specified message.
        /// Note that the method referenced by <paramref name="listener"/> must be public.
        /// </summary>
        public void Register(string message, Listener listener)
        {
            if (string.IsNullOrEmpty(message) || listener == null)
            {
                return;
            }
            if (_registeredListener.ContainsKey(message))
            {
                foreach (ListenerReference listenerReference in _registeredListener[message])
                {
                    if (listenerReference.IsEqualToListener(listener))
                    {
                        return;
                    }
                }
            }
            else
            {
                _registeredListener[message] = new List<ListenerReference>();
            }
            new WeakReference(listener);
            _registeredListener[message].Add(new ListenerReference(listener));
        }

        /// <summary>
        /// Unregisters the specified listener from all the messages it listens to.
        /// </summary>
        public void Unregister(Listener listener)
        {
            if (listener == null)
            {
                return;
            }
            List<string> messages = new List<string>(_registeredListener.Keys);
            foreach (string message in messages)
            {
                List<ListenerReference> recipientsCopy = new List<ListenerReference>(_registeredListener[message]);
                foreach (ListenerReference listenerReference in recipientsCopy)
                {
                    if (!listenerReference.TargetReference.IsAlive || listenerReference.IsEqualToListener(listener))
                    {
                        _registeredListener[message].Remove(listenerReference);
                        break;
                    }
                }
                if (_registeredListener[message].Count == 0)
                {
                    _registeredListener.Remove(message);
                }
            }
        }

        /// <summary>
        /// Sends the specified message with a null parameter.
        /// </summary>
        /// <exception cref="UnauthorizedAccessException">The current thread is not the main thread.</exception>
        public void Send(object sender, string message)
        {
            Send(sender, message, null);
        }

        /// <summary>
        /// Sends the specified message with the specified parameter.
        /// </summary>
        /// <exception cref="UnauthorizedAccessException">The current thread is not the main thread.</exception>
        public void Send(object sender, string message, object param)
        {
            if (!Deployment.Current.Dispatcher.CheckAccess())
            {
                throw new UnauthorizedAccessException("Invalid cross-thread access");
            }
            if (string.IsNullOrEmpty(message) || !_registeredListener.ContainsKey(message))
            {
                return;
            }
            foreach (ListenerReference listenerReference in _registeredListener[message])
            {
                listenerReference.Invoke(sender, message, param);
            }
        }

        /// <summary>
        /// References a listener for messages sent with <see cref="Messenger"/>.
        /// </summary>
        public delegate void Listener(object sender, string message, object param);

        private class ListenerReference
        {
            public ListenerReference(Listener listener)
            {
                TargetReference = new WeakReference(listener.Target);
                Method = listener.Method;
            }

            public WeakReference TargetReference { get; private set; }

            public MethodInfo Method { get; private set; }

            public bool IsEqualToListener(Listener listener)
            {
                return (TargetReference.IsAlive && TargetReference.Target == listener.Target && Method == listener.Method);
            }

            public void Invoke(object sender, string message, object param)
            {
                if (!TargetReference.IsAlive)
                {
                    return;
                }
                try
                {
                    Method.Invoke(TargetReference.Target, new object[] { sender, message, param });
                }
                catch (MethodAccessException)
                {
                    Logger.Log("Method access exception for message listener method {0}.{1}; please ensure that the method is public",
                        TargetReference.Target.GetType().FullName, Method.Name);
                }
                catch (Exception listenerError)
                {
                    Logger.LogException(listenerError, "Unhandled exception in listener for message '{0}'!", message);
                }
            }
        }
    }
}

