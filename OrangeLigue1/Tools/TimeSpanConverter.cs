// -------------------------------  Copyright ---------------------------------
//	This software has been developped by France Telecom, FT/RD/RD/MAPS/DVC/MSF
//
//	Copyright (c) France Telecom 2009-2010
//
// COPYRIGHT    : This file is the property of FRANCE TELECOM.
//                It cannot be copied, used, or modified without obtaining
//                an authorization from the authors or a mandated
//                member of FRANCE TELECOM.
//                If such an authorization is provided, any modified version or
//                copy of the software has to contain this header.
//
// WARRANTIES   : This software is made available by the authors in the  hope
//                that it will be useful, but without any warranty.
//                France Telecom is not liable for any consequence related to the
//                use of the provided software.
//
// AUTHORS      : France Telecom  / RD / MAPS / DVC / MSF
// ----------------------------------------------------------------------------
using System;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using System.Windows.Data;
using System.Globalization;

namespace OrangeLigue1.Tools
{
    /// <summary>
    /// Implements a converter that returns human-readable durations.
    /// </summary>
    public class TimeSpanConverter : IValueConverter
    {
        /// <summary>
        /// Initializes a new <see cref="TimeSpanConverter"/> instance.
        /// </summary>
        public TimeSpanConverter()
        {
        }

        /// <summary>
        /// Implements <see cref="IValueConverter.Convert"/>.
        /// </summary>
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            try
            {
                TimeSpan? duration = value as TimeSpan?;
                if (!duration.HasValue)
                {
                    return "";
                }
                if (duration.Value.TotalMilliseconds <= 1000 || duration.Value.TotalSeconds <= 59)
                {
                    return String.Format(Localization.GetString("TimeSpanConverter_Nseconds"), Math.Max(duration.Value.Seconds, 1));
                }
                if (duration.Value.TotalMinutes <= 59)
                {
                    if (duration.Value.Seconds == 0)
                    {
                        return String.Format(Localization.GetString("TimeSpanConverter_Nminutes"), duration.Value.Minutes);
                    }
                    else
                    {
                        return String.Format(Localization.GetString("TimeSpanConverter_NminutesNseconds"),
                            duration.Value.Minutes, duration.Value.Seconds);
                    }
                }
                return duration.Value.ToString();
            }
            catch (Exception)
            {
                return "";
            }
        }

        /// <summary>
        /// Implements <see cref="IValueConverter.ConvertBack"/>.
        /// </summary>
        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}

