// -------------------------------  Copyright ---------------------------------
//	This software has been developped by France Telecom, FT/RD/RD/MAPS/DVC/MSF
//
//	Copyright (c) France Telecom 2009-2010
//
// COPYRIGHT    : This file is the property of FRANCE TELECOM.
//                It cannot be copied, used, or modified without obtaining
//                an authorization from the authors or a mandated
//                member of FRANCE TELECOM.
//                If such an authorization is provided, any modified version or
//                copy of the software has to contain this header.
//
// WARRANTIES   : This software is made available by the authors in the  hope
//                that it will be useful, but without any warranty.
//                France Telecom is not liable for any consequence related to the
//                use of the provided software.
//
// AUTHORS      : France Telecom  / RD / MAPS / DVC / MSF
// ----------------------------------------------------------------------------
using System;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using System.IO;

namespace OrangeLigue1.Tools
{
    public class SDPParser
    {
        private const string rstpControlFlag = "a=control:";

        public static string RetrieveSdpStreamFromString(string content)
        {
            using (StringReader sr = new StringReader(content))
            {
                String line;
                while ((line = sr.ReadLine()) != null)
                {
                    //find the first line containing rstp control stream, extract it, and return it
                    if (line.Contains(rstpControlFlag))
                    {
                        line = line.Replace(rstpControlFlag, "");
                        return line;
                    }
                }
            }
            return null;
        }
    }
}

