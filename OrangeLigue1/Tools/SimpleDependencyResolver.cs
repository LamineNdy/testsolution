// -------------------------------  Copyright ---------------------------------
//	This software has been developped by France Telecom, FT/RD/RD/MAPS/DVC/MSF
//
//	Copyright (c) France Telecom 2009-2010
//
// COPYRIGHT    : This file is the property of FRANCE TELECOM.
//                It cannot be copied, used, or modified without obtaining
//                an authorization from the authors or a mandated
//                member of FRANCE TELECOM.
//                If such an authorization is provided, any modified version or
//                copy of the software has to contain this header.
//
// WARRANTIES   : This software is made available by the authors in the  hope
//                that it will be useful, but without any warranty.
//                France Telecom is not liable for any consequence related to the
//                use of the provided software.
//
// AUTHORS      : France Telecom  / RD / MAPS / DVC / MSF
// ----------------------------------------------------------------------------
using System;
using System.Collections.Generic;
using System.Reflection;

namespace OrangeLigue1.Tools
{
    /// <summary>
    /// Implements a simple dependency resolver.
    /// </summary>
    public class SimpleDependencyResolver : IDependencyResolver
    {
        private readonly Dictionary<Type, object> registeredObjects = new Dictionary<Type, object>();

        /// <summary>
        /// Registers a type mapping with the resolver so that it returns a new instance of the
        /// <typeparamref name="TTo"/> type when <see cref="Resolve"/> is called to request an instance
        /// of the <typeparamref name="TFrom"/> type.
        /// </summary>
        /// <typeparam name="TFrom">The type that will be requested.</typeparam>
        /// <typeparam name="TTo">The type that will actually be returned.</typeparam>
        public void RegisterType<TFrom, TTo>() where TTo : TFrom
        {
            registeredObjects.Add(typeof(TFrom), typeof(TTo));
        }

        /// <summary>
        /// Registers an instance mapping with the resolver so that it returns <paramref name="obj"/>
        /// when <see cref="Resolve"/> is called to request an instance of the <typeparamref name="T"/> type.
        /// </summary>
        /// <typeparam name="T">The type that will be requested</typeparam>
        /// <param name="obj">The instance that will actually be returned.</param>
        public void RegisterInstance<T>(object obj)
        {
            if (obj is T == false)
            {
                throw new InvalidOperationException();
            }
            registeredObjects.Add(typeof(T), obj);
        }

        /// <summary>
        /// Resolves an instance for the requested type.
        /// </summary>
        /// <typeparam name="T">The requested type.</typeparam>
        /// <returns>The instance registered for the type <typeparamref name="T"/>.</returns>
        public T Resolve<T>()
        {
            if (registeredObjects.ContainsKey(typeof(T)) == false)
            {
                Type type = typeof(T);
                ConstructorInfo ctorInfo = type.GetConstructor(new Type[] { });
                return (T)ctorInfo.Invoke(null);
            }
            object obj = registeredObjects[typeof(T)];
            if (obj is Type)
            {
                Type type = (Type)obj;
                if (type.IsInterface || type.IsAbstract)
                {
                    throw new InvalidOperationException();
                }
                ConstructorInfo ctorInfo = type.GetConstructor(new Type[] { });
                return (T)ctorInfo.Invoke(null);
            }
            return (T)obj;
        }
    }
}

