// -------------------------------  Copyright ---------------------------------
//	This software has been developped by France Telecom, FT/RD/RD/MAPS/DVC/MSF
//
//	Copyright (c) France Telecom 2009-2010
//
// COPYRIGHT    : This file is the property of FRANCE TELECOM.
//                It cannot be copied, used, or modified without obtaining
//                an authorization from the authors or a mandated
//                member of FRANCE TELECOM.
//                If such an authorization is provided, any modified version or
//                copy of the software has to contain this header.
//
// WARRANTIES   : This software is made available by the authors in the  hope
//                that it will be useful, but without any warranty.
//                France Telecom is not liable for any consequence related to the
//                use of the provided software.
//
// AUTHORS      : France Telecom  / RD / MAPS / DVC / MSF
// ----------------------------------------------------------------------------
using System;
using System.Globalization;
using System.Windows.Data;

namespace OrangeLigue1.Tools
{
    public class NewsRelativeTimestampConverter : IValueConverter
    {
        /// <summary>
        /// Initializes a new <see cref="NewsRelativeTimestampConverter"/> instance.
        /// </summary>
        public NewsRelativeTimestampConverter()
        {
        }

        public static string GetNormalRepresentation(DateTime timestamp)
        {
            try
            {
                if (timestamp.Year == 1)
                {
                    return "";
                }
                timestamp = timestamp.ToLocalTime();

                DateTime now = DateTime.Now;
                if (timestamp > now)
                {
                    return timestamp.ToString("f", new CultureInfo("fr-FR"));
                }
                TimeSpan delta = now.Subtract(timestamp);
                if (delta.TotalMinutes < 2.0)
                {
                    return Localization.GetString("RelativeTimestampConverter_OneMinuteAgo");
                }
                for (int i = 5; i <= 45; i += 5)
                {
                    if (delta.TotalMinutes < (double)(i + 1))
                    {
                        return string.Format(Localization.GetString("RelativeTimestampConverter_NMinutesAgo"), i);
                    }
                }
                if (delta.TotalHours < 1.75)
                {
                    return Localization.GetString("RelativeTimestampConverter_AboutAnHourAgo");
                }
                for (int i = 2; i <= 12; i++)
                {
                    if (delta.TotalHours < ((double)i) + 0.75)
                    {
                        return string.Format(Localization.GetString("RelativeTimestampConverter_AboutNHoursAgo"), i);
                    }
                }
                if (timestamp.Date == now.Date)
                {
                    return string.Format(Localization.GetString("RelativeTimestampConverter_TodayAt"), timestamp.ToString("t", new CultureInfo("fr-FR")));
                }
                if (timestamp.Date == now.Date.Subtract(TimeSpan.FromDays(1)))
                {
                    return Localization.GetString("RelativeTimestampConverter_Yesterday");
                }
                if (timestamp.Date >= now.Date.Subtract(TimeSpan.FromDays(6)))
                {
                    return timestamp.ToString("dddd");
                }
                if (timestamp.Date >= now.Date.Subtract(TimeSpan.FromDays(365)))
                {
                    return timestamp.ToString("f", new CultureInfo("fr-FR"));
                }
                return timestamp.ToString("f", new CultureInfo("fr-FR"));
            }
            catch (Exception)
            {
                return "";
            }
        }
		
		public static string GetShortRepresentation(DateTime timestamp)
        {
            try
            {
                if (timestamp == DateTime.MinValue)
                {
                    return "";
                }
                timestamp = timestamp.ToLocalTime();

                DateTime now = DateTime.Now;
                if (timestamp.Date == now.Date)
                {
                    return timestamp.ToString("t");
                }
                if (timestamp.Date == now.Date.Subtract(TimeSpan.FromDays(1)))
                {
                    return Localization.GetString("RelativeTimestampConverter_Yesterday");
                }
                if (timestamp.Date >= now.Date.Subtract(TimeSpan.FromDays(6)) && timestamp.Date < now.Date)
                {
                    return timestamp.ToString("dddd");
                }
                return timestamp.ToString("d");
            }
            catch (Exception)
            {
                return "";
            }
        }

        /// <summary>
        /// Implements <see cref="IValueConverter.Convert"/>.
        /// </summary>
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if (!targetType.IsAssignableFrom(typeof(string)))
            {
                return "";
            }

            DateTime? timestampRef = value as DateTime?;
            if (timestampRef == null || !timestampRef.HasValue)
            {
                return "";
            }
            if (parameter as string == "short")
            {
                return GetShortRepresentation(timestampRef.Value);
            }
            return GetNormalRepresentation(timestampRef.Value);
        }

        /// <summary>
        /// Implements <see cref="IValueConverter.ConvertBack"/>.
        /// </summary>
        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}

