// -------------------------------  Copyright ---------------------------------
//	This software has been developped by France Telecom, FT/RD/RD/MAPS/DVC/MSF
//
//	Copyright (c) France Telecom 2009-2010
//
// COPYRIGHT    : This file is the property of FRANCE TELECOM.
//                It cannot be copied, used, or modified without obtaining
//                an authorization from the authors or a mandated
//                member of FRANCE TELECOM.
//                If such an authorization is provided, any modified version or
//                copy of the software has to contain this header.
//
// WARRANTIES   : This software is made available by the authors in the  hope
//                that it will be useful, but without any warranty.
//                France Telecom is not liable for any consequence related to the
//                use of the provided software.
//
// AUTHORS      : France Telecom  / RD / MAPS / DVC / MSF
// ----------------------------------------------------------------------------
using System;
using System.Collections.Generic;

namespace OrangeLigue1.Tools
{
    /// <summary>
    /// Provides a volatile repository where objects are stored with an associated string key.
    /// </summary>
    public class ObjectRepository
    {
        private static ObjectRepository _staticRepository = null;

        private static ObjectRepository StaticRepository
        {
            get
            {
                if (_staticRepository == null)
                {
                    _staticRepository = new ObjectRepository();
                }
                return _staticRepository;
            }
        }

        /// <summary>
        /// Retrieves the object with the specified key in the repository.
        /// </summary>
        /// <param name="key">The object's key.</param>
        /// <returns>The object identified by <paramref name="key"/> in the repository,
        /// or <c>null</c> if there is no such object.</returns>
        public static object Get(string key)
        {
            ObjectRepository repository = StaticRepository;
            if (repository == null)
            {
                return null;
            }
            return repository.GetObject(key);
        }

        /// <summary>
        /// Stores the object with the specified key in the repository.
        /// The object that was previously associated with the key in the repository, if any, is overriden.
        /// </summary>
        /// <param name="key">The object's key.</param>
        /// <param name="value">The object's value.</param>
        /// <exception cref="InvalidOperationException">If <paramref name="key"/> is null or empty.</exception>
        public static void Store(string key, object value)
        {
            ObjectRepository repository = StaticRepository;
            if (repository == null)
            {
                return;
            }
            repository.StoreObject(key, value);
        }

        /// <summary>
        /// Stores the object with the specified key in the repository as a weak reference.
        /// The object that was previously associated with the key in the repository, if any, is overriden.
        /// </summary>
        /// <param name="key">The object's key.</param>
        /// <param name="value">The object's value.</param>
        /// <exception cref="InvalidOperationException">If <paramref name="key"/> is null or empty.</exception>
        public static void StoreWeakly(string key, object value)
        {
            ObjectRepository repository = StaticRepository;
            if (repository == null)
            {
                return;
            }
            repository.StoreObjectWeakly(key, value);
        }

        /// <summary>
        /// Removes the object with the specified key from the repository.
        /// This method does nothing if there is no such object in the repository.
        /// </summary>
        /// <param name="key">The object's key.</param>
        public static void Remove(string key)
        {
            ObjectRepository repository = StaticRepository;
            if (repository == null)
            {
                return;
            }
            repository.RemoveObject(key);
        }

        private Dictionary<string, object> _dictionary = new Dictionary<string, object>();
        private object _dictionaryLock = new object();

        private ObjectRepository()
        {
        }

        private object GetObject(string key)
        {
            if (string.IsNullOrEmpty(key))
            {
                return null;
            }
            object value;
            lock (_dictionaryLock)
            {
                if (!_dictionary.ContainsKey(key))
                {
                    return null;
                }
                value = _dictionary[key];
            }
            if (value == null)
            {
                return null;
            }
            if (value is WeakReference)
            {
                value = ((WeakReference)value).Target;
            }
            return value;
        }

        private void StoreObject(string key, object value)
        {
            if (string.IsNullOrEmpty(key))
            {
                throw new InvalidOperationException();
            }
            lock (_dictionaryLock)
            {
                _dictionary[key] = value;
            }
        }

        private void StoreObjectWeakly(string key, object value)
        {
            if (string.IsNullOrEmpty(key) || (value != null && value is WeakReference))
            {
                throw new InvalidOperationException();
            }
            WeakReference weakReference = new WeakReference(value);
            lock (_dictionaryLock)
            {
                _dictionary[key] = weakReference;
            }
        }

        private void RemoveObject(string key)
        {
            if (string.IsNullOrEmpty(key))
            {
                return;
            }
            lock (_dictionaryLock)
            {
                _dictionary.Remove(key);
            }
        }
    }
}

