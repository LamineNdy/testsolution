﻿// -------------------------------  Copyright ---------------------------------
//	This software has been developped by France Telecom, FT/RD/RD/MAPS/DVC/MSF
//
//	Copyright (c) France Telecom 2009-2010
//
// COPYRIGHT    : This file is the property of FRANCE TELECOM.
//                It cannot be copied, used, or modified without obtaining
//                an authorization from the authors or a mandated
//                member of FRANCE TELECOM.
//                If such an authorization is provided, any modified version or
//                copy of the software has to contain this header.
//
// WARRANTIES   : This software is made available by the authors in the  hope
//                that it will be useful, but without any warranty.
//                France Telecom is not liable for any consequence related to the
//                use of the provided software.
//
// AUTHORS      : France Telecom  / RD / MAPS / DVC / MSF
// ----------------------------------------------------------------------------

using System;
using System.Runtime.CompilerServices;
using System.Threading;
using System.Windows;
using Microsoft.Phone.Net.NetworkInformation;

namespace OrangeLigue1.Tools
{
    /// <summary>
    /// Watches the connectivity of the device.
    /// </summary>
    public static class ConnectivityWatcher
    {
        /// <summary>
        /// The message sent through <see cref="Messenger"/> when the value of <see cref="CurrentType"/> changes.
        /// </summary>
        public const string CurrentTypeChangedMessage = "ConnectivityWatcher.CurrentTypeChangedMessage";

        private static bool _alreadySetup = false;
        private static ConnectivityType _currentType = ConnectivityType.Unknown;

        /// <summary>
        /// The current connectivity type.
        /// </summary>
        public static ConnectivityType CurrentType
        {
            [MethodImpl(MethodImplOptions.Synchronized)] 
            get
            {
                Setup();
                return _currentType;
            }
            [MethodImpl(MethodImplOptions.Synchronized)]
            private set
            {
                if (_currentType == value) { return; }
                _currentType = value;
                if (Deployment.Current.Dispatcher.CheckAccess())
                {
                    Messenger.Current.Send(typeof(ConnectivityWatcher), CurrentTypeChangedMessage);
                }
                else
                {
                    Deployment.Current.Dispatcher.BeginInvoke(() =>
                        {
                            Messenger.Current.Send(typeof(ConnectivityWatcher), CurrentTypeChangedMessage);
                        });
                }
            }
        }

        [MethodImpl(MethodImplOptions.Synchronized)]
        private static void Setup()
        {
            if (_alreadySetup)
            {
                return;
            }
            ThreadPool.QueueUserWorkItem((state) => { RetrieveConnectivityType(); }, null);
            System.Net.NetworkInformation.NetworkChange.NetworkAddressChanged += OnNetworkAddressChanged;
            _alreadySetup = true;
        }

        private static void OnNetworkAddressChanged(object sender, System.EventArgs e)
        {
            ThreadPool.QueueUserWorkItem((state) => { RetrieveConnectivityType(); }, null);
        }

        private static void RetrieveConnectivityType()
        {
            try
            {
                CurrentType = GetConnectivityTypeFromNetworkInterfaceType(NetworkInterface.NetworkInterfaceType);
            }
            catch (Exception)
            {
                CurrentType = ConnectivityType.Unknown;
            }
        }

        private static ConnectivityType GetConnectivityTypeFromNetworkInterfaceType(NetworkInterfaceType networkInterfaceType)
        {
            switch (networkInterfaceType)
            {
                case NetworkInterfaceType.None:
                    return ConnectivityType.None;
                case NetworkInterfaceType.Unknown:
                    return ConnectivityType.Unknown;
                case NetworkInterfaceType.MobileBroadbandGsm:
                case NetworkInterfaceType.MobileBroadbandCdma:
                    return ConnectivityType.Cellular;
                case NetworkInterfaceType.Wireless80211:
                default:
                    return ConnectivityType.WiFi;
            }
        }
    }

    /// <summary>
    /// Enumerates connectivity types.
    /// </summary>
    public enum ConnectivityType
    {
        Unknown,
        None,
        Cellular,
        WiFi,
    }
}
