﻿using Orange.SDK;
using OrangeLigue1.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Threading;

namespace OrangeLigue1.Tools
{
    public class DispatcherManager : ViewModel
    {

        #region singleton
        private static DispatcherManager _singleton = null;
        public static DispatcherManager Current
        {
            get
            {
                if (_singleton == null)
                {
                    _singleton = new DispatcherManager();
                }
                return _singleton;
            }
        }
        #endregion

        private DispatcherManager()
        {
            myLigue1 = App.CurrentApp.MyLigue1;
        }

        private static int FreqLive = 30;
        private static TimeSpan refreshFreqLiveTimerInterval = TimeSpan.FromSeconds(FreqLive);

        public int CurrentMatchId
        {
            get;
            set;
        }

        public DispatcherTimer RefreshFreqLiveTimer
        {
            get;
            set;
        }

        private Ligue1 myLigue1;
        public Ligue1 MyLigue1
        {
            get { return myLigue1; }
            set { myLigue1 = value; OnPropertyChanged("MyLigue1"); }
        }

        public void StartRefreshFreqLiveTimer()
        {
            if (RefreshFreqLiveTimer != null)
            {
                RefreshFreqLiveTimer.Stop();
                RefreshFreqLiveTimer = null;
            }
            RefreshFreqLiveTimer = new DispatcherTimer();
            RefreshFreqLiveTimer.Interval = refreshFreqLiveTimerInterval;
            RefreshFreqLiveTimer.Tick += new EventHandler(OnRefreshFreqLiveTimerTick);
            RefreshFreqLiveTimer.Start();
        }

        public void StopRefreshFreqLiveTimer()
        {
            if (RefreshFreqLiveTimer != null)
            {
                RefreshFreqLiveTimer.Stop();
                RefreshFreqLiveTimer = null;
            }
        }

        public bool RefreshFreqLiveTimerIsStarted()
        {
            if (RefreshFreqLiveTimer != null)
            {
                return RefreshFreqLiveTimer.IsEnabled;
            }
            else
            {
                return false;
            }
        }

        /// <summary>
        /// Called when the refresh freqLive timer ticks.
        /// </summary>
        private void OnRefreshFreqLiveTimerTick(object sender, EventArgs e)
        {
            DispatcherTimer timer = sender as DispatcherTimer;
            if (timer == null)
            {
                return;
            }
            if (timer != RefreshFreqLiveTimer)
            {
                return;
            }

            if (CurrentMatchId != 0)
            {
                myLigue1.RequestMatch(CurrentMatchId);
            }
            
        }
    }
}
