// -------------------------------  Copyright ---------------------------------
//	This software has been developped by France Telecom, FT/RD/RD/MAPS/DVC/MSF
//
//	Copyright (c) France Telecom 2009-2010
//
// COPYRIGHT    : This file is the property of FRANCE TELECOM.
//                It cannot be copied, used, or modified without obtaining
//                an authorization from the authors or a mandated
//                member of FRANCE TELECOM.
//                If such an authorization is provided, any modified version or
//                copy of the software has to contain this header.
//
// WARRANTIES   : This software is made available by the authors in the  hope
//                that it will be useful, but without any warranty.
//                France Telecom is not liable for any consequence related to the
//                use of the provided software.
//
// AUTHORS      : France Telecom  / RD / MAPS / DVC / MSF
// ----------------------------------------------------------------------------
using System;

namespace OrangeLigue1.Tools
{
    /// <summary>
    /// The container used to resolve dependencies.
    /// </summary>
    public class DependencyContainer
    {
        private static IDependencyResolver actualResolver = null;

        /// <summary>
        /// Resolves an instance for the requested type.
        /// </summary>
        /// <typeparam name="T">The requested type.</typeparam>
        /// <returns>The instance registered for the type <typeparamref name="T"/>.</returns>
        public static T Resolve<T>()
        {
            if (actualResolver == null)
            {
                throw new InvalidOperationException();
            }
            return actualResolver.Resolve<T>();
        }

        /// <summary>
        /// Initializes the container with the specified dependency resolver.
        /// </summary>
        /// <param name="resolver">The resolver that will be used by the container.</param>
        public static void Initialize(IDependencyResolver resolver)
        {
            if (actualResolver != null)
            {
                throw new InvalidOperationException();
            }
            actualResolver = resolver;
        }

        /// <summary>
        /// Uninitializes the container.
        /// Subsequent calls to <see cref="Resolve<T>"/> will fail until <see cref="Initialize()"/> is called again.
        /// </summary>
        public static void Uninitialize()
        {
            actualResolver = null;
        }
    }
}

