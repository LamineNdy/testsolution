// -------------------------------  Copyright ---------------------------------
//	This software has been developped by France Telecom, FT/RD/RD/MAPS/DVC/MSF
//
//	Copyright (c) France Telecom 2009-2010
//
// COPYRIGHT    : This file is the property of FRANCE TELECOM.
//                It cannot be copied, used, or modified without obtaining
//                an authorization from the authors or a mandated
//                member of FRANCE TELECOM.
//                If such an authorization is provided, any modified version or
//                copy of the software has to contain this header.
//
// WARRANTIES   : This software is made available by the authors in the  hope
//                that it will be useful, but without any warranty.
//                France Telecom is not liable for any consequence related to the
//                use of the provided software.
//
// AUTHORS      : France Telecom  / RD / MAPS / DVC / MSF
// ----------------------------------------------------------------------------
using System;
using System.Globalization;
using System.Windows.Data;

namespace OrangeLigue1.Tools
{
    public class CalendarTimestampConverter : IValueConverter
    {
        /// <summary>
        /// Initializes a new <see cref="NewsRelativeTimestampConverter"/> instance.
        /// </summary>
        public CalendarTimestampConverter()
        {

        }

        /// <summary>
        /// Implements <see cref="IValueConverter.Convert"/>.
        /// </summary>
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            try
            {
                if (!targetType.IsAssignableFrom(typeof(string)))
                {
                    return "";
                }

                DateTime? timestampRef = value as DateTime?;
                if (timestampRef == null || !timestampRef.HasValue)
                {
                    return "";
                }
                DateTime timestamp = timestampRef.Value;
                if (timestamp.Year == 1)
                {
                    return "";
                }
                timestamp = timestamp.ToLocalTime();

                DateTime now = DateTime.Now;
                if (timestamp < now && timestamp.AddHours(2) > now)
                {
                    return string.Format(Localization.GetString("RelativeTimestampConverter_Currently"));
                }
                if (timestamp.Date.Day == now.Date.Day &&
                    timestamp.Date.Month == now.Date.Month &&
                    timestamp.Date.Year == now.Date.Year)
                {
                    return string.Format(Localization.GetString("RelativeTimestampConverter_Today"), timestamp.ToString("D", new CultureInfo("fr-FR")));
                }
                return timestamp.ToString("D", new CultureInfo("fr-FR"));
            }
            catch (Exception)
            {
                return "";
            }
        }

        /// <summary>
        /// Implements <see cref="IValueConverter.ConvertBack"/>.
        /// </summary>
        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}

