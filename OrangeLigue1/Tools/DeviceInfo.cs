// -------------------------------  Copyright ---------------------------------
//	This software has been developped by France Telecom, FT/RD/RD/MAPS/DVC/MSF
//
//	Copyright (c) France Telecom 2009-2010
//
// COPYRIGHT    : This file is the property of FRANCE TELECOM.
//                It cannot be copied, used, or modified without obtaining
//                an authorization from the authors or a mandated
//                member of FRANCE TELECOM.
//                If such an authorization is provided, any modified version or
//                copy of the software has to contain this header.
//
// WARRANTIES   : This software is made available by the authors in the  hope
//                that it will be useful, but without any warranty.
//                France Telecom is not liable for any consequence related to the
//                use of the provided software.
//
// AUTHORS      : France Telecom  / RD / MAPS / DVC / MSF
// ----------------------------------------------------------------------------
using System;
using Microsoft.Phone.Info;

namespace OrangeLigue1.Tools
{
    /// <summary>
    /// Helper class that retrieves information on the device.
    /// </summary>
    public class DeviceInfo
    {
        /// <summary>
        /// The value returned by <see cref="UniqueId"/> when the unique identifier of the
        /// device is unknown.
        /// </summary>
        public const string UnknownUniqueId = "unknown";

        /// <summary>
        /// The value returned by <see cref="Manufacturer"/> when the manufacturer of the
        /// device is unknown.
        /// </summary>
        public const string UnknownManufacturer = "unknown";

        /// <summary>
        /// The value returned by <see cref="Name"/> when the name of the
        /// device is unknown.
        /// </summary>
        public const string UnknownName = "unknown";

        private DeviceInfo()
        {
        }

        /// <summary>
        /// The unique identifier of the device (<see cref="UnknownUniqueId"/> if unknown).
        /// </summary>
        public static string UniqueId
        {
            get
            {
                try
                {
                    byte[] uniqueIdBytes = DeviceExtendedProperties.GetValue("DeviceUniqueId") as byte[];
                    if (uniqueIdBytes == null || uniqueIdBytes.Length == 0)
                    {
                        return UnknownUniqueId;
                    }
                    return BitConverter.ToString(uniqueIdBytes).Replace("-", "");
                }
                catch (Exception)
                {
                    return UnknownUniqueId;
                }
            }
        }

        /// <summary>
        /// The name of the manufacturer of the device (<see cref="UnknownManufacturer"/> if unknown).
        /// </summary>
        public static string Manufacturer
        {
            get
            {
                try
                {
                    string manufacturer = DeviceExtendedProperties.GetValue("DeviceManufacturer") as string;
                    if (string.IsNullOrEmpty(manufacturer))
                    {
                        return UnknownManufacturer;
                    }
                    return manufacturer;
                }
                catch (Exception)
                {
                    return UnknownManufacturer;
                }
            }
        }

        /// <summary>
        /// The name of the the device (<see cref="UnknownName"/> if unknown).
        /// </summary>
        public static string Name
        {
            get
            {
                try
                {
                    string name = DeviceExtendedProperties.GetValue("DeviceName") as string;
                    if (string.IsNullOrEmpty(name))
                    {
                        return UnknownName;
                    }
                    return name;
                }
                catch (Exception)
                {
                    return UnknownName;
                }
            }
        }
    }
}

