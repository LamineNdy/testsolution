﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Windows.Shapes;

namespace AuthenticationManager
{
    /// <summary>
    ///  IOLAuthenticationManagerDelegate delegate methods that must be implemented to receive callbacks.
    /// </summary>
    public interface IOLAuthenticationManagerDelegate
    {
        /// <summary>
        /// This method provides to the delegate that the internal state changed.
        /// </summary>
        /// <param name="oldState">The old authentication manager state.</param>
        /// <param name="newState">The new authentication manager state.</param>
        void stateChanged(OLAuthenticationManager.State oldState, OLAuthenticationManager.State newState);
        /// <summary>
        /// This method provide to the delegate that a internal configuration value has changed and the application had to save it.
        /// This method if only called if autosave is false.
        /// </summary>
        /// <param name="configKey">The config key name. Depends on the mode selected. The name is the same that the one provided in the initialize method.</param>
        /// <param name="configValue">new configuration value to be stored.</param>
        void configChanged(string configKey, string configValue);
    };
    
    /// <summary>
    ///  Main Manager used by user to choose his authentication mode and intract with it.
    /// </summary>
    public class OLAuthenticationManager
    {
        /// <summary>
        /// TOKEN MODE: Last token received config key
        /// </summary>
        public  const string TOKEN_LAST_TOKEN="LAST_TOKEN";   
        /// <summary>
        /// TOKEN MODE: Application identifier config key
        /// </summary>
        public const string TOKEN_APPLICATION_ID="APPLICATION_ID";        
        /// <summary>
        /// TOKEN MODE: Application secret config key
        /// </summary>
        public const string TOKEN_APPLICATION_SECRET = "APPLICATION_SECRET";
        /// <summary>
        /// uSSO MODE: Last wassup cookie value
        /// </summary>
        public const string USSO_LAST_COOKIE_VALUE = "LAST_COOKIE";
        public const string USSO_LAST_SETCOOKIE_VALUE = "LAST_SETCOOKIE";

        /// <summary>
        /// Initializes a new System.Net.WebRequest instance for the specified URI scheme.
        /// </summary>
        /// <param name="requestUri">A System.Uri containing the URI of the requested resource.</param>
        /// <returns>A System.Net.WebRequest descendant for the specific URI scheme.</returns>
        /// <exception cref="System.NotSupportedException">The request scheme specified in requestUri has not been registered.</exception>
        /// <exception cref="System.ArgumentNullException">requestUri is null.</exception>
        /// <exception cref="System.Security.SecurityException">
        ///     The caller does not have permission to connect to the requested URI or a
        ///     URI that the request is redirected to.
        /// </exception>
        /// <exception cref="System.UriFormatException">The URI specified in requestUri is not a valid URI.</exception>
        public static System.Net.WebRequest Create(System.Uri requestUri)
        {
            if (requestUri.Scheme == "http")
                return defaultAuthenticationManager().CreateRequest(requestUri);
            else return WebRequest.Create(requestUri);
        }
        /// <summary>
        /// Initializes a new System.Net.WebRequest instance for the specified URI scheme.
        /// </summary>
        /// <param name="requestUriString">The URI that identifies the Internet resource.</param>
        /// <returns>A System.Net.WebRequest descendant for the specific URI scheme.</returns>
        /// <exception cref="System.NotSupportedException">The request scheme specified in requestUriString has not been registered.</exception>
        /// <exception cref="System.ArgumentNullException">RequestUriString is null.</exception>
        /// <exception cref="System.Security.SecurityException">
        ///     The caller does not have permission to connect to the requested URI or a
        ///     URI that the request is redirected to.
        /// </exception>
        /// <exception cref="System.UriFormatException">The URI specified in requestUriString is not a valid URI.</exception>
        public static System.Net.WebRequest Create(string requestUriString)
        {
            Uri requestUri = new Uri(requestUriString);
            if (requestUri.Scheme == "http")
                return defaultAuthenticationManager().CreateRequest(requestUri);
            else return WebRequest.Create(requestUri);
        }
        /// <summary>
        /// Initializes a new System.Net.WebRequest instance for the specified URI scheme.
        /// </summary>
        /// <param name="requestUri">A System.Uri containing the URI of the requested resource.</param>
        /// <returns>A System.Net.WebRequest descendant for the specific URI scheme.</returns>
        /// <exception cref="System.NotSupportedException">The request scheme specified in requestUri has not been registered.</exception>
        /// <exception cref="System.ArgumentNullException">requestUri is null.</exception>
        /// <exception cref="System.Security.SecurityException">
        ///     The caller does not have permission to connect to the requested URI or a
        ///     URI that the request is redirected to.
        /// </exception>
        /// <exception cref="System.UriFormatException">The URI specified in requestUri is not a valid URI.</exception>
        protected System.Net.WebRequest CreateRequest(System.Uri requestUri)
        {
            return m_oManagerImpl == null ? new OLHttpWebRequest(requestUri)/*System.Net.WebRequest.Create(requestUri)*/ : m_oManagerImpl.Create(requestUri);
        }        
        /// <summary>
        /// The authentication mode.
        /// </summary>
        public enum Mode {
            /// <summary>
            /// No authentication.
            /// </summary>
            None = 0,
            /// <summary>
            /// Token authentication.
            /// </summary>
            Token,
            /// <summary>
            /// uTunneling authentication.
            /// </summary>
            uTunneling,
            /// <summary>
            /// uSSO G4 authentication.
            /// </summary>
            uSSOG4
        };
        /// <summary>
        /// Authentication State.
        /// </summary>
        public enum State {
            /// <summary>
            /// State unknown
            /// </summary>
            Unknown = 0,       
            /// <summary>
            /// State not authenticated
            /// </summary>
            NotAuthenticated,
            /// <summary>
            /// State authenticated
            /// </summary>
            Authenticated
        };    
        /// <summary>
        /// Method used to create and/or retrieve Authentication manager singleton
        /// </summary>
        /// <returns> The default manager instance</returns>
        public static OLAuthenticationManager defaultAuthenticationManager()
        {
            lock (singletonMutex)
            {
                if (oSingleton == null)
                    oSingleton = new OLAuthenticationManager();

                return oSingleton;
            }
        }
        /// <summary>
        /// Release Authentication manager singleton
        /// </summary>
        /// <returns>true on success, false on failure</returns>
        public static bool releaseAuthenticationManager()
        {
            bool result = true;
            lock (oSingleton)
            {
                result = oSingleton.shutdown();
                if (result)
                {
                    OLLog.Debug("Delete OLAuthenticationManager singleton");                    
                    oSingleton = null;
                }
            }
            return result;
        }
        /// <summary>
        ///     Initializes the authentication manager.
        /// </summary>
        /// <param name="delegateObject">Delegate object for callbacks.</param>
        /// <param name="_eAuthMode">The authentication mode.</param>
        /// <param name="_oAuthParams">The parameters needed for the chosen mode.</param>
        /// <param name="_bAutoSave">If true the internal state is automatically saved, if false the signal configChanged will be emitted.</param>
        /// <returns>true on success, false on failure</returns>
        public bool initialize(IOLAuthenticationManagerDelegate delegateObject, Mode _eAuthMode, Dictionary<string, string> _oAuthParams, bool _bAutoSave = true)
        {
            lock (managerMutex)
            {
                if (m_oManagerImpl != null) return false;
            }

            bool result=false;
            switch (_eAuthMode)
            {

                case Mode.Token:
                    lock (managerMutex)
                    {
                        m_oManagerImpl = new OLAuthenticationManagerTokenImpl();
                        result = m_oManagerImpl.initializeImpl(delegateObject,_oAuthParams, _bAutoSave);
                       /* if (!result)
                            m_oManagerImpl = null;*/
                    }
                    break;
                case Mode.uSSOG4:
                    lock (managerMutex)
                    {
                        m_oManagerImpl = new OLAuthenticationManageruSSOImpl();
                        result = m_oManagerImpl.initializeImpl(delegateObject, _oAuthParams, _bAutoSave);
                        /* if (!result)
                             m_oManagerImpl = null;*/
                    }
                    break;
                default:
                    lock (managerMutex)
                    {
                        m_oManagerImpl = null;
                    }
                    result=false;
                    break;
            }

          /* if(result)
            enable();*/

           return result;
        }

        /// <summary>
        /// Enables the authentication.
        /// </summary>
        /// <returns>true on success, false on failure</returns>
        public bool enable()
        {
            lock (managerMutex)
            {
                return m_oManagerImpl==null?false:m_oManagerImpl.enableImpl();
            }
        }
        /// <summary>
        /// Resets the authentication manager.The authentication manager goes in a state just after an initialize and erase all the saved configuration.
        /// </summary>
        /// <returns>true on success, false on failure</returns>
        public bool reset()
        {
            lock (managerMutex)
            {
                return  m_oManagerImpl == null ? false : m_oManagerImpl.resetImpl();
            }
        }
        /// <summary>
        /// Disables the authentication.
        /// </summary>
        /// <returns>true on success, false on failure</returns>
        public bool disable()
        {
            lock (managerMutex)
            {
                return m_oManagerImpl == null ? false : m_oManagerImpl.disableImpl();
            }       
        }
        /// <summary>
        /// Releases resources and may save the internal state. Releases resources and may save the internal state.
        /// </summary>
        /// <returns>true on success, false on failure</returns>
        public bool shutdown()
        {
            lock (managerMutex)
            {
                bool result= m_oManagerImpl == null ? true : m_oManagerImpl.shutdownImpl();
                if (result)
                    m_oManagerImpl = null;

                return result;
            }
        }
        /// <summary>
        /// Return the statistics. Statictics depend of authentication mode selected.
        /// </summary>
        /// <returns>a Dictionary with key/value statistics</returns>
        public Dictionary<string, string> statistics()
        {
            lock (managerMutex)
            {
                return m_oManagerImpl == null ? null : m_oManagerImpl.statisticsImpl();

            }
        }
        /// <summary>
        /// Clears statistics.
        /// </summary>
        /// <returns>true on success, false on failure</returns>
        public bool clearStatistics()
        {
            lock (managerMutex)
            {
                return m_oManagerImpl == null ? false : m_oManagerImpl.clearStatisticsImpl();

            }
        }
        /// <summary>
        /// Contructor protected.
        /// </summary>
         OLAuthenticationManager() { m_oManagerImpl = null; }
    
        /// <summary>
        /// Internal Authentication singleton.
        /// </summary>
        private static OLAuthenticationManager oSingleton;

        /// <summary>
        /// Internal mutex.
        /// </summary>
        private static string singletonMutex = "";

        /// <summary>
        /// Internal State
        /// </summary>
        private static bool isEnabled;

        /// <summary>
        /// Internal implentation singleton.
        /// </summary>
        private OLAuthenticationManagerBaseImpl m_oManagerImpl;
    
        /// <summary>
        /// Internal protection mutex.
        /// </summary>
        private string managerMutex = "";
    }
}
