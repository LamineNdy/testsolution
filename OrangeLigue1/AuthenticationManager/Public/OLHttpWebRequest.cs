﻿using System;
using System.Net;
using System.Collections;
using Microsoft.Phone.Net.NetworkInformation;

namespace AuthenticationManager
{
    /// <summary>
    /// Exception used when authentication error occured.
    /// </summary>
    public class OLAuthenticationException  : WebException
    {
        /// <summary>
        /// Contructor
        /// </summary>
        public OLAuthenticationException ():base()
        {
            m_sErrorCode = "";
            m_sErrorMsg = "";
        }
        /// <summary>
        /// Constructor with parameters
        /// </summary>
        /// <param name="code">The Exception code</param>
        /// <param name="message">The Exception Error Message</param>
        public OLAuthenticationException(string code, string message)
            : base()
        {
            m_sErrorCode = code;
            m_sErrorMsg = message;
        }
        /// <summary>
        /// Returns the exception error code.
        /// </summary>
        public string ErrorCode
        {
           
            get
            {
                return m_sErrorCode;
            }
        }
        /// <summary>
        /// Returns the exception error message.
        /// </summary>
        public string ErrorMessage
        {

            get
            {
                return m_sErrorMsg;
            }
        }
        private string m_sErrorCode;
        private string m_sErrorMsg;
    };

    /// <summary>
    ///     Provides an Orange HTTP-specific implementation of the System.Net.WebRequest class.
    /// </summary>
    public class OLHttpWebRequest : System.Net.WebRequest
    {
        /// <summary>
        /// Private HTTP request object used by object.
        /// </summary>
        protected System.Net.HttpWebRequest m_oHttpWebRequest;

        /// <summary>
        ///     Cancels a request to an Internet resource.
        /// </summary>
        /// <exception cref="System.NotImplementedException">This method is not implemented.</exception>     
        public override void Abort()
        {
            m_oHttpWebRequest.Abort();
        }
        /// <summary>        
        ///     Begins an asynchronous request for a System.IO.Stream object to use to write
        ///     data.
        /// </summary>
        /// <param name="callback">The System.AsyncCallback delegate.</param>
        /// <param name="state">The state object for this request.</param>
        /// <returns>
        ///      An System.IAsyncResult that references the asynchronous request.
        /// </returns>
        /// <exception cref="System.Net.ProtocolViolationException">
        ///     The System.Net.HttpWebRequest.Method property is GET.
        ///  </exception> 
        /// <exception cref="System.InvalidOperationException">
        ///     The stream is being used by a previous call to System.Net.HttpWebRequest.BeginGetRequestStream(System.AsyncCallback,System.Object)-or-
        ///     The thread pool is running out of threads.
        ///  </exception> 
        /// <exception cref="System.NotImplementedException">
        ///     This method is not implemented.
        ///  </exception> 
        /// <exception cref="System.NotSupportedException">
        ///     The request cache validator indicated that the response for this request
        ///     can be served from the cache; however, requests that write data must not
        ///     use the cache. This exception can occur if you are using a custom cache validator
        ///     that is incorrectly implemented.
        ///  </exception> 
        /// <exception cref="System.Net.WebException">
        ///     System.Net.HttpWebRequest.Abort() was previously called.
        ///  </exception> 
        /// <exception cref="System.ObjectDisposedException">
        ///     In a .NET Framework application, a request stream with zero content length
        ///     was not obtained and closed correctly.
        ///  </exception> 
        public override System.IAsyncResult BeginGetRequestStream(System.AsyncCallback callback, object state)
        {            
            return m_oHttpWebRequest.BeginGetRequestStream(callback, state);
        }
        /// <summary>
        ///     Begins an asynchronous request to an Internet resource.
        /// </summary>
        /// <param name="callback">The System.AsyncCallback delegate</param>
        /// <param name="state">The state object for this request.</param>
        /// <returns>
        ///       An System.IAsyncResult that references the asynchronous request for a response.
        /// </returns>
        /// <exception cref="System.InvalidOperationException">       
        ///   System.InvalidOperationException:
        ///     The stream is already in use by a previous call to System.Net.HttpWebRequest.BeginGetResponse(System.AsyncCallback,System.Object)-or-
        ///     The thread pool is running out of threads.
        ///  </exception> 
        /// <exception cref="System.NotImplementedException">
        ///     This method is not implemented.
        /// </exception>
        /// <exception cref="System.NotSupportedException">
        ///  The callback parameter is null.
        /// </exception>
        /// <exception cref="System.Net.ProtocolViolationException">
        ///  System.Net.HttpWebRequest.Method is GET.
        /// </exception>
        /// <exception cref="System.Net.WebException">
        /// System.Net.HttpWebRequest.Abort() was previously called.
        /// </exception>     
        public override System.IAsyncResult BeginGetResponse(System.AsyncCallback callback, object state)
        {           
            return m_oHttpWebRequest.BeginGetResponse(callback, state);
        }
        /// <summary>
        ///      Ends an asynchronous request for a System.IO.Stream object to use to write
        ///    data.
        /// </summary>
        /// <param name="asyncResult">The pending request for a stream.</param> 
        /// <returns>A System.IO.Stream to use to write request data.</returns>
        /// <exception cref="System.ArgumentNullException">
        ///  asyncResult is null.
        /// </exception>        
        /// <exception cref="System.IO.IOException">
        ///  The request did not complete, and no stream is available.
        /// </exception>        
        /// <exception cref="System.ArgumentException">
        ///  asyncResult was not returned by the current instance from a call to System.Net.HttpWebRequest.BeginGetRequestStream(System.AsyncCallback,System.Object).
        /// </exception>        
        /// <exception cref="System.InvalidOperationException">
        ///  This method was called previously using asyncResult.
        /// </exception>        
        /// <exception cref="System.NotImplementedException">
        ///  This method is not implemented.
        /// </exception>
        /// <exception cref="System.Net.WebException">
        ///  System.Net.HttpWebRequest.Abort() was previously called.-or- An error occurred
        ///     while processing the request.
        /// </exception>
        public override System.IO.Stream EndGetRequestStream(System.IAsyncResult asyncResult)
        {
            return m_oHttpWebRequest.EndGetRequestStream(asyncResult);
        }
        /// <summary>Ends an asynchronous request to an Internet resource.</summary>
        /// <param name="asyncResult">The pending request for a response.</param> 
        /// <returns>A System.Net.WebResponse that contains the response from the Internet resource.</returns>
        /// <exception cref="System.ArgumentNullException"> asyncResult is null.</exception>
        /// <exception cref="System.InvalidOperationException"> This method was called previously using asyncResult.</exception>
        /// <exception cref="System.NotImplementedException"> This method is not implemented.</exception>
        /// <exception cref="AuthenticationManager.OLAuthenticationException"> Authentication Exception.</exception>
        /// <exception cref="System.Net.WebException">
        ///     System.Net.HttpWebRequest.Abort() was previously called.-or- An error occurred
        ///     while processing the request.
        /// </exception>
        /// <exception cref="System.ArgumentException">
        /// asyncResult was not returned by the current instance from a call to System.Net.HttpWebRequest.BeginGetResponse(System.AsyncCallback,System.Object).
        /// </exception>
        public override System.Net.WebResponse EndGetResponse(System.IAsyncResult asyncResult) 
        {           
            return m_oHttpWebRequest.EndGetResponse(asyncResult);          
        }
        /// <summary>
        /// Contructor
        /// NB: DO NOT use this constructor directly. use OLAuthenticationManager.Create method.
        /// </summary>
        /// <param name="StringUri"> The string URI that identifies the Internet resource.</param>
        public OLHttpWebRequest(string StringUri): base()
        {           
            m_oHttpWebRequest = (HttpWebRequest)Create(StringUri);  
        }
        /// <summary>
        /// Contructor
        /// NB: DO NOT use this constructor directly. use OLAuthenticationManager.Create method.
        /// </summary>
        /// <param name="uri">The URI that identifies the Internet resource.</param>
        public OLHttpWebRequest(System.Uri uri): base()
        {            
                m_oHttpWebRequest = (HttpWebRequest)Create(uri);                    
        }
        /// <summary>Gets or sets the value of the Accept HTTP header. </summary>
        /// <returns>The value of the Accept HTTP header. The default value is null.</returns>       
        public string Accept
        {
            set
            {
                m_oHttpWebRequest.Accept = value;
            }
            get
            {
                return m_oHttpWebRequest.Accept;
            }
        }

        public void SetNetworkPreference(NetworkSelectionCharacteristics preference)
        {
            m_oHttpWebRequest.SetNetworkPreference(preference);
        }
        /// <summary>
        ///     Gets or sets a value that indicates whether to buffer the data read from
        ///     the Internet resource.
        ///</summary>
        /// <returns>
        ///     true to enable buffering of the data received from the Internet resource;
        ///     false to disable buffering. The default is true.
        /// </returns>       
        /// <exception cref="System.NotImplementedException"> This property is not implemented.</exception>
        public  bool AllowReadStreamBuffering
        {
            set
            {
                m_oHttpWebRequest.AllowReadStreamBuffering = value;
            }
            get
            {
                return m_oHttpWebRequest.AllowReadStreamBuffering;
            }
        }
        /// <summary>Gets or sets the value of the Content-type HTTP header.</summary>
        /// <returns>The value of the Content-type HTTP header. The default value is null.</returns>
        public override string ContentType
        {
            set
            {
                m_oHttpWebRequest.ContentType = value;
            }
            get
            {
                return m_oHttpWebRequest.ContentType;
            }
        }
        /// <summary>
        ///     Specifies the collection of System.Net.CookieCollection objects associated
        ///     with the HTTP request.
        /// </summary>
        /// <returns>
        ///     A System.Net.CookieContainer that contains a collection of System.Net.CookieCollection
        ///     objects associated with the HTTP request.
        /// </returns>
        /// <exception cref="System.NotImplementedException">This property is not implemented.</exception>       
        public System.Net.CookieContainer CookieContainer
        {
            set
            {
                m_oHttpWebRequest.CookieContainer = value;
            }
            get
            {
                return m_oHttpWebRequest.CookieContainer;
            }
        }
        /// <summary>
        ///     Gets or sets authentication information for the request..
        /// </summary>
        /// <returns>An System.Net.ICredentials that contains the authentication credentials associated with the request. The default is null.</returns>
        public override System.Net.ICredentials Credentials
        {
            set
            {
                m_oHttpWebRequest.Credentials = value;
            }
            get
            {
                return m_oHttpWebRequest.Credentials;
            }
        }
        /// <summary>
        ///     Gets a value that indicates whether a response has been received from an Internet resource.
        /// </summary>
        /// <returns> true if a response has been received; otherwise, false.</returns>
        /// <exception cref="System.NotImplementedException">This property is not implemented.</exception> 
        public bool HaveResponse
        {
            get
            {
                return m_oHttpWebRequest.HaveResponse;
            }
        }
        /// <summary>Specifies a collection of the name/value pairs that make up the HTTP headers.</summary>
        /// <returns>
        /// A System.Net.WebHeaderCollection that contains the name/value pairs that make up the headers for the HTTP request.
        /// </returns>
        /// <exception cref="System.InvalidOperationException">
        ///     The request has been started by calling the System.Net.HttpWebRequest.BeginGetRequestStream(System.AsyncCallback,System.Object)
        ///     or System.Net.HttpWebRequest.BeginGetResponse(System.AsyncCallback,System.Object)
        ///     method.
        /// </exception>        
        public override System.Net.WebHeaderCollection Headers
        {
            set
            {
                m_oHttpWebRequest.Headers = value;
            }
            get
            {
                return m_oHttpWebRequest.Headers;
            }
        }
        /// <summary>
        ///     Gets or sets the method for the request.
        /// </summary>
        /// <returns>
        ///     The request method to use to contact the Internet resource. The default value is GET.
        /// </returns>
        /// <exception cref="System.ArgumentException">
        ///     No method is supplied.-or- The method string contains invalid characters.
        /// </exception>
        /// <exception cref="System.NotImplementedException">
        ///     This property is not implemented.
        /// </exception>
        /// <exception cref="System.NotSupportedException">
        ///     The System.Net.HttpWebRequest.Method property is not GET or POST.
        /// </exception>        
        public override string Method
        {
            set
            {
                m_oHttpWebRequest.Method = value;
            }
            get
            {
                return m_oHttpWebRequest.Method;
            }
        }
        /// <summary>
        ///     Gets the original Uniform Resource Identifier (URI) of the request.
        /// </summary>
        /// <returns>
        ///      A System.Uri that contains the URI of the Internet resource passed to the System.Net.WebRequest.Create(System.Uri) method.
        /// </returns>
        /// <exception cref="System.NotImplementedException">
        ///     This property is not implemented.
        /// </exception>       
        public override System.Uri RequestUri
        {
            get
            {
                return m_oHttpWebRequest.RequestUri;
            }
        }

        /// <summary>
        /// Returns the underlying HttpWebRequest object used by the <see cref="OLHttpWebRequest"/> instance.
        /// </summary>
        /// <param name="webRequest">The <see cref="OLHttpWebRequest"/> instance.</param>
        /// <returns>The underlying HttpWebRequest object.</returns>
        public static explicit operator System.Net.HttpWebRequest(OLHttpWebRequest webRequest)
        {
            return webRequest == null ? null : webRequest.m_oHttpWebRequest;
        }
    }
}
