﻿using System;
using System.Net;
using System.IO;


namespace AuthenticationManager
{
    public class OLuSSOHttpWebRequest : OLHttpWebRequest
    {
        protected OLAuthenticationManageruSSOImpl m_oParent;

        public OLuSSOHttpWebRequest(string StringUri, OLAuthenticationManageruSSOImpl parent)
            : base(StringUri)
        {
            //System.Diagnostics.Debug.WriteLine("Constructor OLuSSOHttpWebRequest String");        
            m_oParent = parent;
            
        }
        public OLuSSOHttpWebRequest(System.Uri uri, OLAuthenticationManageruSSOImpl parent)
            : base(uri)
        {

           // System.Diagnostics.Debug.WriteLine("Constructor OLuSSOHttpWebRequest Uri");
            m_oParent = parent;
           
        }
        public override System.Net.WebResponse EndGetResponse(System.IAsyncResult asyncResult)
        {
            OLLog.Debug("OLuSSOHttpWebRequest.EndGetResponse - Check headers");
            //Check headers
            HttpWebResponse response = null;
            try
            {
                response = (HttpWebResponse)m_oHttpWebRequest.EndGetResponse(asyncResult);
                if (m_oParent != null)
                {
                    m_oParent.checkNewSetCookie(response);
                    m_oParent.checkNewCookie(response);
                }
            }
            catch (WebException e)
            {
                //System.Diagnostics.Debug.WriteLine("WebException {0}", e);

                if (m_oParent != null && (!m_oParent.checkResponse((HttpWebResponse)e.Response)))
                {
                    StreamReader reader = new StreamReader(e.Response.GetResponseStream());
                    string contents = reader.ReadToEnd();
                    e.Response.Close();
                    OLLog.Debug("Throw OLAuthenticationException ");
                    throw new OLAuthenticationException("AUTHENTICATION_ERROR", contents);
                }
            }

            

            return response;
        }
        
    }
}
