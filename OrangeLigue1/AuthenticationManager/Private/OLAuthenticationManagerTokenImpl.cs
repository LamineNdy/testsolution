﻿using System;
using System.Net;
using System.Collections.Generic;
using System.IO.IsolatedStorage;
using System.Security.Cryptography;
using Microsoft.Phone.Info;

namespace AuthenticationManager
{
    public class OLAuthenticationManagerTokenImpl : OLAuthenticationManagerBaseImpl
    {
        const string DIGEST_HTTPHEADER_NAME="X_AMP_FP";
        const string AID_HTTPHEADER_NAME="X_AMP_AID";
        const string MID_HTTPHEADER_NAME="X_AMP_MID";
        const string TOKEN_HTTPHEADER_NAME="Set-Cookie";
        
        public bool addHeadersInfos(WebHeaderCollection reqHeaders)
        {
            OLLog.Debug(string.Format("Add header: {0}, value: {1}", AID_HTTPHEADER_NAME, m_sApplicationID));
            //System.Diagnostics.Debug.WriteLine("Add header: {0}, value: {1}", AID_HTTPHEADER_NAME, m_sApplicationID);
            reqHeaders[AID_HTTPHEADER_NAME] = m_sApplicationID;
            lock (m_sCurrentDigest)
            {
                if (m_sCurrentDigest != "")
                {
                    OLLog.Debug(string.Format("Add header: {0}, value: {1}", DIGEST_HTTPHEADER_NAME, m_sCurrentDigest));
                    reqHeaders[DIGEST_HTTPHEADER_NAME] = m_sCurrentDigest;
                }
            }
            OLLog.Debug(string.Format("Add header: {0}, value: {1}", MID_HTTPHEADER_NAME, m_sMobileID));
            reqHeaders[MID_HTTPHEADER_NAME] = m_sMobileID;

            return true;
        }

        public bool checkResponseHeaders(WebHeaderCollection respHeaders)
        {         
            string token = null;
            bool result = true;
            try
            {                
                //respHeaders                
                string tokenReceived = respHeaders[TOKEN_HTTPHEADER_NAME];
                token = tokenReceived ;//!= null ? tokenReceived : "";
               
            }            
            catch (Exception e)
            {               
               /* result=false;
                token = "";*/
                token = null;
            }

            if (token == null) //no token in header
                return true;

            if (result && token!=null && token.ToUpper() == "KO")
            {
                result = false;
              //  token = "";
            }

            if(result)
                tokenChanged(token);
            
            if(result)
                changeState(OLAuthenticationManager.State.Authenticated);
            else changeState(OLAuthenticationManager.State.NotAuthenticated);

            return result;          
        }

        protected string getUUID()
        {
            byte[] uniqueIdBytes = DeviceExtendedProperties.GetValue("DeviceUniqueId") as byte[];
            string uUID = BitConverter.ToString(uniqueIdBytes);
            uUID = uUID.Replace("-", "");
            /*
            IsolatedStorageSettings personnalSettings = IsolatedStorageSettings.ApplicationSettings;
            string uUID = "";
            try
            {
                uUID = personnalSettings["WAS_TOKEN_UUID"].ToString();
            }
            catch (Exception e)
            { }

            if (string.IsNullOrEmpty(uUID))
            {
                //generate UUID
                OLLog.Debug("Generate New UUID..");
                Random random=new Random();
                int firstRandomPart=random.Next();
                int secondRandomPart=random.Next();
                DateTime current = new DateTime();

                uUID = string.Format("{0}{1}{2}", current.ToLongTimeString(), firstRandomPart, secondRandomPart);

                personnalSettings.Add("WAS_TOKEN_UUID", uUID);
                personnalSettings.Save();
            }

             **/

            OLLog.Debug(string.Format(" UUID : {0}", uUID));
            
            return uUID; 
        }
        
        public OLAuthenticationManagerTokenImpl()
        {
            isEnabled = false;
            m_sCurrentSecret = null;
            m_sApplicationID = null;
            m_sCurrentToken = "";
            m_eCurrentState = OLAuthenticationManager.State.Unknown;
            m_oUserSettings = null;
            m_sCurrentDigest = "";
            m_sMobileID = getUUID();
        }

        public override System.Net.WebRequest Create(System.Uri requestUri)
        {
            OLLog.Debug(string.Format("OLAuthenticationManagerTokenImpl.Create {0}", isEnabled));
            if (isEnabled)
                return new OLTokenHttpWebRequest(requestUri,this);
            return new OLHttpWebRequest(requestUri);
            //return System.Net.WebRequest.Create(requestUri);
        }

        public override bool initializeImpl(IOLAuthenticationManagerDelegate delegateObject, Dictionary<string, string> _oAuthParams, bool _bAutoSave = true)
        {
            bool result = true;
            m_oDelegate = delegateObject;
            m_bAutoSave = _bAutoSave;

            if (isEnabled)
            {
                System.Diagnostics.Debug.WriteLine("Aready initialized");
                result = false;
            }
            
            if (result)
            {
                try
                {
                    m_sCurrentSecret = _oAuthParams[OLAuthenticationManager.TOKEN_APPLICATION_SECRET];
                    m_sApplicationID = _oAuthParams[OLAuthenticationManager.TOKEN_APPLICATION_ID];
                }
                catch (KeyNotFoundException)
                {
                    result = false;
                    OLLog.Error("Valid Keys not found in config parameters");
                    //System.Diagnostics.Debug.WriteLine("Key not found in config parameters");
                }

                string token = "";
                if (_bAutoSave)
                {                    
                    m_oUserSettings = IsolatedStorageSettings.ApplicationSettings;
                    try
                    {
                        Object tokenObject= m_oUserSettings[OLAuthenticationManager.TOKEN_LAST_TOKEN];
                        if(tokenObject!=null)
                            token = tokenObject.ToString();
                    }
                    catch (KeyNotFoundException)
                    { }
                }
                else
                {
                    try
                    {
                        token=_oAuthParams[OLAuthenticationManager.TOKEN_LAST_TOKEN];
                    }
                    catch (KeyNotFoundException)
                    { }
                }

                if (token == null)
                    token = "";

                OLLog.Debug(string.Format("new Token {0}", token));
                tokenChanged(token);
                /*
                if (m_sCurrentSecret == null || m_sApplicationID == null)
                    result = false;*/
            }

            if (result)
            {
                enableImpl();

                if (m_sCurrentToken.Length > 0)
                {
                    changeState(OLAuthenticationManager.State.Authenticated);
                }
                else changeState(OLAuthenticationManager.State.NotAuthenticated);
            }

            return result;
        }

        public override bool enableImpl()
        {
            isEnabled = true;
            return true;
        }

        public override bool resetImpl()
        {
            tokenChanged("");
            changeState(OLAuthenticationManager.State.NotAuthenticated);
            return true;
        }

        public override bool disableImpl()
        {
            isEnabled = false;
            return true;
        }

        public override bool shutdownImpl()
        {
            isEnabled = false;
            return true;
        }

        public override Dictionary<string, string> statisticsImpl()
        {
            return null;
        }

        public override bool clearStatisticsImpl()
        {
            return true;
        }

        protected void tokenChanged(string newToken)
        {
            lock (m_sCurrentToken)
            {
                m_sCurrentToken = newToken;
                //compute new digest
                if (m_sCurrentToken != "")
                {
                    SHA1 sha = new SHA1Managed();
                    string stringToHash = m_sApplicationID;
                    stringToHash += m_sMobileID;
                    stringToHash += m_sCurrentToken;
                    stringToHash += m_sCurrentSecret;

                    System.Text.Encoding encode = System.Text.Encoding.UTF8;
                    byte[] charSHA = encode.GetBytes(stringToHash);
                    byte[] bDigest = sha.ComputeHash(charSHA);

                    string hex = BitConverter.ToString(bDigest);

                    lock (m_sCurrentDigest)
                    {
                        m_sCurrentDigest = hex.Replace("-", "");
                    }
                }
                else
                {
                    lock (m_sCurrentDigest)
                    {
                        m_sCurrentDigest = "";
                    }
                }
            }
            if (m_bAutoSave)
            {
                if (m_oUserSettings != null)
                {
                    m_oUserSettings.Remove(OLAuthenticationManager.TOKEN_LAST_TOKEN);
                    m_oUserSettings.Add(OLAuthenticationManager.TOKEN_LAST_TOKEN, newToken);
                    m_oUserSettings.Save();
                    OLLog.Debug(string.Format("Token Saved {0}, Value:{1}", m_oUserSettings, m_oUserSettings[OLAuthenticationManager.TOKEN_LAST_TOKEN]));                    
                }
            }
            else
            {
                configChanged(OLAuthenticationManager.TOKEN_LAST_TOKEN, newToken);
            }                      
        }

        protected string m_sCurrentToken;
        protected string m_sCurrentSecret;
        protected string m_sApplicationID;
        protected string m_sCurrentDigest;
        protected string m_sMobileID;
        protected bool m_bAutoSave;
        private IsolatedStorageSettings m_oUserSettings;
    }
}
