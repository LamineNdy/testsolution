﻿using System;
using System.Net;
using System.Collections.Generic;
using AuthenticationManager;

namespace AuthenticationManager
{
    public class OLAuthenticationManagerNoneImpl : OLAuthenticationManagerBaseImpl
    {
        public OLAuthenticationManagerNoneImpl()
        {

        }

        public override System.Net.WebRequest Create(System.Uri requestUri)
        {
            return new OLHttpWebRequest(requestUri);
        }

        public override bool initializeImpl(IOLAuthenticationManagerDelegate delegateObject, Dictionary<string, string> _oAuthParams, bool _bAutoSave = true)
        {
            changeState(OLAuthenticationManager.State.Authenticated);
            return true;
        }

        public override bool enableImpl()
        {            
            return true;
        }

        public override bool resetImpl()
        {
            changeState(OLAuthenticationManager.State.Authenticated);
            return true;
        }

        public override bool disableImpl()
        {           
            return true;
        }

        public override bool shutdownImpl()
        {           
            return true;
        }

        public override Dictionary<string, string> statisticsImpl()
        {
            return null;
        }

        public override bool clearStatisticsImpl()
        {
            return true;
        }
    }
}
