﻿using System;
using System.Net;
using System.Xml;

namespace AuthenticationManager
{   
    public class OLTokenHttpWebRequest : OLHttpWebRequest
    {        
        protected OLAuthenticationManagerTokenImpl m_oParent;
        protected bool HeadersAdded = false;

        protected void addHeaders()
        {
            if (HeadersAdded) return;

            OLLog.Debug("OLTokenHttpWebRequest - Add headers");
            //Add headers
            WebHeaderCollection requestHeaders = m_oHttpWebRequest.Headers;
            OLLog.Debug(string.Format("Add headers, count {0}", requestHeaders.Count));
            if (m_oParent != null)
                m_oParent.addHeadersInfos(requestHeaders);

            OLLog.Debug(string.Format("Add headers End, count {0}", requestHeaders.Count));

            HeadersAdded = true;
        }

        public override System.IAsyncResult BeginGetRequestStream(System.AsyncCallback callback, object state)
        {
            addHeaders();
            return m_oHttpWebRequest.BeginGetRequestStream(callback, state);
        }

        public override System.IAsyncResult BeginGetResponse(System.AsyncCallback callback, object state)     
        {
            addHeaders();
            return m_oHttpWebRequest.BeginGetResponse(callback, state);
        }       

        public override System.Net.WebResponse EndGetResponse(System.IAsyncResult asyncResult)
        {
           
            //Check headers
            OLLog.Debug("EndGetResponse -Check headers");
            
            HttpWebResponse response = (HttpWebResponse)m_oHttpWebRequest.EndGetResponse(asyncResult);
            if (m_oParent != null && (!m_oParent.checkResponseHeaders(response.Headers)))
            {
                //parse XML content and Throw Exception
                OLAuthenticationException exception = null;
                try
                {
                    string errorCode = "";
                    string errorMsg = "";
                    string eltName="";
                    XmlReader reader = XmlReader.Create(response.GetResponseStream());
                    while (reader.Read())
                    {
                        XmlNodeType nType = reader.NodeType;
                        if (nType == XmlNodeType.Element)
                        {
                            eltName = reader.Name.ToString();
                            /*if (reader.Name.ToString() == "respStatus")
                            {
                                localString = errorCode;
                                //errorCode=reader.Value.ToString();
                            }
                            else if(reader.Name.ToString() == "errorMessage")
                            {
                                localString = errorMsg;
                               // errorMsg=reader.Value.ToString();
                            }*/
                        }
                        else if (nType == XmlNodeType.Text)
                        {
                            if (eltName == "respStatus")
                            {
                                errorCode = reader.Value.ToString();
                            }
                            else if (eltName == "errorMessage")
                            {
                                errorMsg = reader.Value.ToString();
                            }
                        }
                        else eltName = "";
                    }
                    /*reader.ReadToFollowing("respStatus");
                    string errorCode = reader.ReadContentAsString();
                    reader.ReadToFollowing("errorMessage");
                    string errorMsg = reader.ReadContentAsString();*/
                    exception = new OLAuthenticationException(errorCode, errorMsg);
                }
                catch (Exception e)
                {
                    exception = new OLAuthenticationException("WAS_INTERNAL_ERROR", "Unable to parse datas from AMP");
                }
                response.Close();
                OLLog.Debug("Throw OLAuthenticationException ");
                throw exception;
            }
               
            return response;
        }

        public OLTokenHttpWebRequest(string StringUri, OLAuthenticationManagerTokenImpl parent)
            : base(StringUri)
        {
            //System.Diagnostics.Debug.WriteLine("Constructor OLTokenHttpWebRequest String");            
            m_oParent = parent;
            
        }
        public OLTokenHttpWebRequest(System.Uri uri,OLAuthenticationManagerTokenImpl parent)
            : base(uri)
        {                        
                
            //System.Diagnostics.Debug.WriteLine("Constructor OLTokenHttpWebRequest Uri");
            m_oParent = parent;
           
        }        
    }
}
