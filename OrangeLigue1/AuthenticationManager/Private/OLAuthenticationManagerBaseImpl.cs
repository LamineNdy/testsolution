﻿using System;
using System.Net;
using System.Collections.Generic;
using AuthenticationManager;

namespace AuthenticationManager
{

    public class OLLog
    {
        public static void Debug(string _debugMessage)
        {
            if (debugInfos)
            {
                System.Diagnostics.Debug.WriteLine(_debugMessage);
                //Console.WriteLine(_debugMessage);
            }
        }
        public static void Error(string _errorMessage)
        {
            System.Diagnostics.Debug.WriteLine("Was Error >> " + _errorMessage);
            //Console.Error.WriteLine("Was Error >> "+_errorMessage);
        }
        public static  bool LogDebugInfos
        {
            get
            {
                return debugInfos;
            }
            set
            {
                debugInfos = value;
            }
        }
        static bool debugInfos = false;
    }


    public class OLAuthenticationManagerBaseImpl
    {
        public virtual System.Net.WebRequest Create(System.Uri requestUri)
        {
            //return null;
            throw new NotSupportedException();
        }

        public virtual bool initializeImpl(IOLAuthenticationManagerDelegate delegateObject, Dictionary<string, string> _oAuthParams, bool _bAutoSave = true) { return false; }

        public virtual bool enableImpl() { return false; }

        public virtual bool resetImpl() { return false; }

        public virtual bool disableImpl() { return false; }

        public virtual bool shutdownImpl() { return false; }

        public virtual Dictionary<string, string> statisticsImpl() { return null; }

        public virtual bool clearStatisticsImpl() { return false; }

        protected void changeState(OLAuthenticationManager.State newState)
        {
            OLLog.Debug(string.Format("changeState new:{0}", newState));
            OLAuthenticationManager.State oldState;
            lock (stateMutex)
            {
                oldState = m_eCurrentState;
                m_eCurrentState = newState;
            }

            if(newState!=oldState)
            {
                try
                {
                    m_oDelegate.stateChanged(oldState, newState);
                }
                catch (Exception e)
                {
                    OLLog.Error(string.Format("ChangeState Exception {0}", e.ToString()));
                    //System.Diagnostics.Debug.WriteLine("changeState Exception {0}", e.ToString());
                }
            }
        }

        protected void configChanged(string configKey, string configValue)
        {
            m_oDelegate.configChanged(configKey, configValue);
        }

        protected OLAuthenticationManager m_oParent;

        protected  OLAuthenticationManager.State m_eCurrentState;
        protected string stateMutex = "";

        protected bool isEnabled;

        protected IOLAuthenticationManagerDelegate m_oDelegate;
    }
}
