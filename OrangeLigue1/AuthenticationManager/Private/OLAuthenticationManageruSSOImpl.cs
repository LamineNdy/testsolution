﻿using System;
using System.Net;
using System.Collections.Generic;
using System.IO.IsolatedStorage;
using Microsoft.Phone.Info;

namespace AuthenticationManager
{
    public class OLAuthenticationManageruSSOImpl : OLAuthenticationManagerBaseImpl
    {
        //const string AID_HTTPHEADER_NAME = "X_AMP_AID";
        //const string MID_HTTPHEADER_NAME = "X_AMP_MID";
        //const string COOKIE_HTTPHEADER_NAME = "Cookie";

        public OLAuthenticationManageruSSOImpl()
        {
            m_oCookieContainer = null;
            isEnabled = false;
            m_sCurrentCookie = "";
            m_CurrentSetCookie = "";
        }

        public override System.Net.WebRequest Create(System.Uri requestUri)
        {
            if (isEnabled)
            {
                OLHttpWebRequest request=new OLuSSOHttpWebRequest(requestUri, this);
                request.CookieContainer=m_oCookieContainer;
                //if (!string.IsNullOrEmpty(m_CurrentSetCookie))
                   // request.Headers[COOKIE_HTTPHEADER_NAME] = m_CurrentSetCookie; //"proxyIn_com_pphom=mspvr363; wassup=3235374237334243323234363830323734444236324633353432374131344236563d563220202020393230383030643930303030303066303031343038303030303038303030303030303030303038305a7249564b5166556f634e615853366b62376d4f6451714e78615076324a4c39722f5557334c6833665a4d376e717930677a4c5842306555535133564f6f4b46456f7a757551326162677a307a392b6763395374396f526a37794333453932304542702f59596347584135636c5a4b79314359763970635a7a6a62335958746f4c73776274666b682b31773752542b4e6e4e3355337834745849796a50446d4c596e4645334d695747756d47594b57516d5450704b397a4878394c486653754d654e6b33706965616a7677784f4c7970544d686972616857535444336c32704c31632f63316d5444432b3074623438793161314662564e7a4a466d616565364542455835337658785355775364747976307643694e537966646e6c50735163765335354e662f3937436f4e587932614d567a4a5a466c557447756165454477527c4d434f3d4f46527c563d56347c585f5741535355505f415554485f4d4554483d337c585f5741535355505f53594e435f434f4f4b49453d3939373338327c585f5741535355505f56414c49445f444154453d32303133303331323131303330317c616f6c3d31307c7761643d32303133303331323130343735347c7763743d32443870786d4749304f68696c7930516c4b58746a77304b755938724256386e33;expires=Sat, 14-Sep-2013 11:03:01 GMT;domain=.orange;";
                //request.Headers[AID_HTTPHEADER_NAME] = m_sApplicationID;
                //request.Headers[MID_HTTPHEADER_NAME] = getUUID();
                return request;
            }
            return new OLHttpWebRequest(requestUri);          
        }

        public override bool initializeImpl(IOLAuthenticationManagerDelegate delegateObject, Dictionary<string, string> _oAuthParams, bool _bAutoSave = true)
        {
            bool result = true;
            m_oDelegate = delegateObject;
            m_bAutoSave = _bAutoSave;
            if (isEnabled)
            {
                OLLog.Error("Aready initialized");                
                result = false;
            }

            if (result)
            {   
                m_oCookieContainer = new CookieContainer();
                OLLog.Debug(string.Format("Size Cookie :{0}", m_oCookieContainer.MaxCookieSize));
                //m_oCookieContainer.MaxCookieSize = 8192;
                try
                {
                    m_sApplicationID = _oAuthParams[OLAuthenticationManager.TOKEN_APPLICATION_ID];
                }
                catch (KeyNotFoundException)
                {
                    result = false;
                    OLLog.Error("Valid Keys not found in config parameters");
                    //System.Diagnostics.Debug.WriteLine("Key not found in config parameters");
                }

                if (_bAutoSave)
                {
                    m_oUserSettings = IsolatedStorageSettings.ApplicationSettings;
                    try
                    {
                        m_sCurrentCookie = m_oUserSettings[OLAuthenticationManager.USSO_LAST_COOKIE_VALUE].ToString();
                        m_CurrentSetCookie = m_oUserSettings[OLAuthenticationManager.USSO_LAST_SETCOOKIE_VALUE].ToString();
                    }
                    catch (KeyNotFoundException)
                    { }
                }
                else
                {
                    if (_oAuthParams != null)
                    {
                        try
                        {
                            m_sCurrentCookie = _oAuthParams[OLAuthenticationManager.USSO_LAST_COOKIE_VALUE];
                            m_CurrentSetCookie = m_oUserSettings[OLAuthenticationManager.USSO_LAST_SETCOOKIE_VALUE].ToString();
                        }
                        catch (KeyNotFoundException)
                        { }
                    }
                    else result = false;
                }

                if (m_sCurrentCookie != null && m_sCurrentCookie != "" /*&& m_CurrentSetCookie != "" && m_CurrentSetCookie != null*/)
                {
                    // Add Cookie Value to container
                    addCookieToContainer(m_sCurrentCookie);
                    /*
                    Cookie wassupCookie = getCookieFromString(m_sCurrentCookie);
                    if (wassupCookie != null)
                    {
                        Uri hostname= new Uri("http://" + wassupCookie.Domain);                      
                        OLLog.Debug(string.Format("Path : {0}", wassupCookie.Path));                      
                        //m_oCookieContainer.Add(hostname, wassupCookie);
                        m_oCookieContainer.SetCookies(hostname, wassupCookie.ToString());                      
                    }
                    else OLLog.Error("Unable to parse uSSO Cookie");
                    */
                    changeState(OLAuthenticationManager.State.Authenticated);
                }
                else
                    changeState(OLAuthenticationManager.State.NotAuthenticated);
            }

            if(result)
                isEnabled = true;
                //enableImpl();
                
            //changeState(OLAuthenticationManager.State.Authenticated);
            return result;
        }

        public override bool enableImpl()
        {
            //Add Cookie to container
            addCookieToContainer(m_sCurrentCookie);
            isEnabled = true;
            return true;
        }

        public override bool resetImpl()
        {
            removeCookieFromContainer();
            CookieChanged("");            
            changeState(OLAuthenticationManager.State.NotAuthenticated);
            return true;
        }

        public override bool disableImpl()
        {
            isEnabled = false;
            removeCookieFromContainer();
            return true;
        }

        public override bool shutdownImpl()
        {
            isEnabled = false;
            return true;
        }

        public override Dictionary<string, string> statisticsImpl()
        {
            return null;
        }

        public override bool clearStatisticsImpl()
        {
            return true;
        }

        protected string getUUID()
        {
            byte[] uniqueIdBytes = DeviceExtendedProperties.GetValue("DeviceUniqueId") as byte[];
            string uUID = BitConverter.ToString(uniqueIdBytes);
            uUID = uUID.Replace("-", "");
            return uUID;
        }

        protected void addCookieToContainer(string _sCookieString)
        {
            Cookie wassupCookie = getCookieFromString(_sCookieString);
            if (wassupCookie != null)
            {
                Uri hostname;
                if(wassupCookie.Domain.StartsWith("."))
                    hostname = new Uri("http://" + wassupCookie.Domain.Substring(1));
                else
                    hostname = new Uri("http://" + wassupCookie.Domain);

                OLLog.Debug(string.Format("Path : {0}", wassupCookie.Path));
                /* wassupCookie.Path = "/"; // UGLY FIX
                 OLLog.Debug(string.Format("cookie hostname {0}",hostname));
                 OLLog.Error("UGLY FIX to cookie Path");*/
                //m_oCookieContainer.Add(hostname, wassupCookie);
               m_oCookieContainer.SetCookies(hostname, wassupCookie.ToString());
                //m_oCookieContainer.SetCookies(hostname, m_CurrentSetCookie.ToString());
            }
            else OLLog.Error("Unable to parse uSSO Cookie");
        }

        protected void removeCookieFromContainer()
        {
            
            lock (m_sCurrentCookie)
            {
                Cookie currentCookie = getCookieFromString(m_sCurrentCookie);
                //OLLog.Debug(string.Format("removeCookieFromContainer initial cookie : {0}", m_oCookieContainer.GetCookieHeader(new Uri("http://c-hod-osmose.rd.francetelecom.fr/osmose/AuthenticationServer/trunk/uSSOSimulator.php"))));
                if (currentCookie != null)
                {
                    currentCookie.Expires = DateTime.Now.AddDays(-1d);
                    currentCookie.Value = "";
                    m_oCookieContainer.SetCookies(new Uri("http://"+currentCookie.Domain), currentCookie.ToString());
                    //OLLog.Debug(string.Format("removeCookieFromContainer final cookie : {0}", m_oCookieContainer.GetCookieHeader(new Uri("http://c-hod-osmose.rd.francetelecom.fr/osmose/AuthenticationServer/trunk/uSSOSimulator.php"))));
                }
            }
        }

        protected string getCookieString(Cookie _oCookie)
        {            
            string sResult = "";
            sResult += _oCookie.Name+"[@+]";    //0
            sResult += _oCookie.Value + "[@+]"; //1
            sResult += _oCookie.Domain + "[@+]";//2
            sResult += _oCookie.Path + "[@+]";  //3
            sResult += _oCookie.Port + "[@+]";  //4
            sResult += _oCookie.Secure.ToString() + "[@+]";     //5
            sResult += _oCookie.Expires.ToString() + "[@+]";    //6

            return sResult;
        }

        protected Cookie getCookieFromString(string _sCookie)
        {
            if (String.IsNullOrEmpty(_sCookie))
                return null;

            string[] stringSeparators = new string[] { "[@+]" };
            string[] cookieParameters = _sCookie.Split(stringSeparators, StringSplitOptions.None);

            Cookie oCookie = new Cookie(cookieParameters[0], cookieParameters[1],cookieParameters[3]/*,cookieParameters[2]*/);
            oCookie.Domain = cookieParameters[2];
            //  port
            if (!String.IsNullOrEmpty(cookieParameters[4]))
                oCookie.Port = cookieParameters[4];
            // isSecured
            if (!String.IsNullOrEmpty(cookieParameters[5]))
                oCookie.Secure = Convert.ToBoolean(cookieParameters[5]);
            // Expires
            if (!String.IsNullOrEmpty(cookieParameters[6]))
                oCookie.Expires = Convert.ToDateTime(cookieParameters[6]);

            return oCookie;
        }

        protected void CookieChanged(string newCookieString)
        {
            lock (m_sCurrentCookie)
            {
                OLLog.Debug(string.Format("Wassup Cookie changed: {0}",newCookieString));
                if (newCookieString != m_sCurrentCookie)
                {
                    m_sCurrentCookie = newCookieString;

                    if (m_bAutoSave)
                    {
                        if (m_oUserSettings != null)
                        {
                            m_oUserSettings.Remove(OLAuthenticationManager.USSO_LAST_COOKIE_VALUE);
                            m_oUserSettings.Add(OLAuthenticationManager.USSO_LAST_COOKIE_VALUE, m_sCurrentCookie);
                            m_oUserSettings.Save();                          
                        }
                    }
                    else
                    {
                        configChanged(OLAuthenticationManager.USSO_LAST_COOKIE_VALUE, m_sCurrentCookie);
                    }       
                }
            }
        }

        public bool checkResponse(HttpWebResponse response)
        {
            bool result = true;
            if (response == null)
                return true;

            if (response.StatusCode == HttpStatusCode.Forbidden)
                result = false;

            checkNewSetCookie(response);
            checkNewCookie(response);
            /*
            try
            {
                foreach (Cookie cookie in response.Cookies)
                {
                    OLLog.Debug(string.Format("Cookie found: {0}", cookie));
                    if (cookie.Name.ToLower() == "wassup")
                    {
                        CookieChanged(getCookieString(cookie));
                        break;
                    }
                }

            }
            catch(Exception e)
            {}*/

            return result;
        }

        public void checkNewSetCookie(HttpWebResponse response)
        {
            try
            {
                string CookieReceived = response.Headers["Set-Cookie"];
                if (!string.IsNullOrEmpty(CookieReceived))
                    SetCookieChanged(CookieReceived);
                if (CookieReceived != "")
                    changeState(OLAuthenticationManager.State.Authenticated);
                else changeState(OLAuthenticationManager.State.NotAuthenticated);
            }
            catch (Exception e)
            { }
        }

        private void SetCookieChanged(string CookieReceived)
        {
            lock (m_CurrentSetCookie)
            {
                OLLog.Debug(string.Format("Wassup Cookie changed: {0}", CookieReceived));
                if (CookieReceived != m_CurrentSetCookie)
                {
                    string[] temp = CookieReceived.Split(new string[] { ";" },StringSplitOptions.RemoveEmptyEntries);
                    string[] temp2 = temp[3].Split(new string[] { "wassup" }, StringSplitOptions.RemoveEmptyEntries);
                    m_CurrentSetCookie = temp[0] + "; " + "wassup" + temp2[1] + ";" + temp[4] + ";" + temp[5] + ";" + temp[6];

                    if (m_bAutoSave)
                    {
                        if (m_oUserSettings != null)
                        {
                            m_oUserSettings.Remove(OLAuthenticationManager.USSO_LAST_SETCOOKIE_VALUE);
                            m_oUserSettings.Add(OLAuthenticationManager.USSO_LAST_SETCOOKIE_VALUE, m_CurrentSetCookie);
                            m_oUserSettings.Save();
                        }
                    }
                    else
                    {
                        configChanged(OLAuthenticationManager.USSO_LAST_SETCOOKIE_VALUE, m_CurrentSetCookie);
                    }
                }
            }
        }

        public void checkNewCookie(HttpWebResponse response)
        {
            try
            {
                Cookie cookie = response.Cookies["wassup"];
              /*  m_oCookieContainer.Add(new Uri("http://" + cookie.Domain), cookie);
                string strCookie = getCookieString(cookie);
                Cookie testCookie = getCookieFromString(strCookie);
                OLLog.Debug(string.Format("initial Cookie: {0},\n PATh: {2} \n new Cookie: {1}", cookie, testCookie,cookie.Path));*/
                
                if (cookie != null)
                {
                    CookieChanged(getCookieString(cookie));
                    if (cookie.Value != "")
                        changeState(OLAuthenticationManager.State.Authenticated);
                    else changeState(OLAuthenticationManager.State.NotAuthenticated);
                }
            }
            catch (Exception e)
            { }
        }

        protected string m_sApplicationID;
        protected bool m_bAutoSave;
        protected string m_sCurrentCookie;
        protected CookieContainer m_oCookieContainer;
        protected string m_CurrentSetCookie;
        private IsolatedStorageSettings m_oUserSettings;
    }
}
