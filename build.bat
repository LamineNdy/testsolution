@echo off
setlocal
set BASEDIR=%~dp0
set BATCHNAME=%~nx0

echo.
echo %BATCHNAME%: Preparing builds...
set SOLUTION=%BASEDIR%OrangeLigue1.sln
if "%BUILD_NUMBER%"=="" (
	set APPVERSION=2.1.0.9999
) else (
	set APPVERSION=2.1.0.%BUILD_NUMBER%
)
set NUNIT="c:\Program Files (x86)\NUnit 2.5.3\bin\net-2.0\nunit-console.exe"
set DEVENV="%VS100COMNTOOLS%..\IDE\devenv.exe"
set OUTPUTDIR=%BASEDIR%BuildOutput
set BUILDLOG=%OUTPUTDIR%\build.log
set ZIPSOURCEDIR=%OUTPUTDIR%\OrangeLigue1_%APPVERSION%
set ZIPFILE=BuildOutput\OrangeLigue1_%APPVERSION%.zip
set EXIT_PARAMS=
if "%BUILD_NUMBER%"=="" set EXIT_PARAMS=/B
cd /d "%BASEDIR%"

if exist "%OUTPUTDIR%" rmdir /s /q "%OUTPUTDIR%"
mkdir "%OUTPUTDIR%"
if not exist "%OUTPUTDIR%" goto failure
mkdir "%ZIPSOURCEDIR%"
if not exist "%ZIPSOURCEDIR%" goto failure

if "%BUILD_NUMBER%"=="" (
	echo DEVELOPER BUILD
) else (
	"%BASEDIR%Tools\ssed.exe" -i.ORIGINAL "s/2.0.0.9999/%APPVERSION%/g" "%BASEDIR%OrangeLigue1\Properties\AssemblyInfo.cs"
	"%BASEDIR%Tools\ssed.exe" -i.ORIGINAL "s/2.0.0.9999/%APPVERSION%/g" "%BASEDIR%OrangeLigue1\Properties\WMAppManifest.xml"
	"%BASEDIR%Tools\ssed.exe" -i.ORIGINAL "s/\{Version\}/%APPVERSION%/g" "%BASEDIR%ReleaseNotes.txt"
)

echo.
echo %BATCHNAME%: Building project OrangeLigue1 (Release)...
call %DEVENV% "%SOLUTION%" /Clean "Release" /Project OrangeLigue1 /Out "%BUILDLOG%"
call %DEVENV% "%SOLUTION%" /Build "Release" /Project OrangeLigue1 /Out "%BUILDLOG%"
if not exist "%BASEDIR%OrangeLigue1\Bin\Release\OrangeLigue1.xap" goto failure
copy "%BASEDIR%OrangeLigue1\Bin\Release\OrangeLigue1.xap" "%ZIPSOURCEDIR%\OrangeLigue1_%APPVERSION%.xap"

echo.
echo %BATCHNAME%: Building project OrangeLigue1 (Debug)...
call %DEVENV% "%SOLUTION%" /Clean "Debug" /Project OrangeLigue1 /Out "%BUILDLOG%"
call %DEVENV% "%SOLUTION%" /Build "Debug" /Project OrangeLigue1 /Out "%BUILDLOG%"
if not exist "%BASEDIR%OrangeLigue1\Bin\Debug\OrangeLigue1.xap" goto failure
copy "%BASEDIR%OrangeLigue1\Bin\Debug\OrangeLigue1.xap" "%ZIPSOURCEDIR%\OrangeLigue1_%APPVERSION%_DEBUG.xap"

rem Reformat build output for Hudson parsing
"%BASEDIR%Tools\ssed.exe" -i.ORIGINAL -e "/warning : CA1405 :/d" -e "s/^\(\([a-zA-Z]:[^:]\+\)\|MSBUILD\) *: warning : \(CA[0-9]\+\) : \(.*\)$/\1: warning \3: \4/g" "%BUILDLOG%"
del /q "%BUILDLOG%.ORIGINAL"

echo.
echo %BATCHNAME%: Zipping...
if exist "%ZIPFILE%" del /q "%ZIPFILE%"
copy "%BASEDIR%ReleaseNotes.txt" "%ZIPSOURCEDIR%"
"%BASEDIR%Tools\7za.exe" a -tzip -mx=9 -r "%ZIPFILE%" "%ZIPSOURCEDIR%"
if not exist "%ZIPFILE%" goto failure
rmdir /s /q "%ZIPSOURCEDIR%"

echo.
echo %BATCHNAME%: Successful completion
exit %EXIT_PARAMS% 0

:failure
echo *** %BATCHNAME%: FAILURE!
exit %EXIT_PARAMS% 1
